# JASPAR 2024 motif discovery, anotation and curation pipeline #

Last updated: 23-08-2023

## Summary ##
  
[**JASPAR**](http://jaspar.genereg.net) is an open-access database of curated, non-redundant transcription factor (TF)-binding profiles stored as position frequency matrices (PFMs) for TFs across multiple species in different taxonomic groups.

The curated motifs were either directly taken from external sources (referred as **external motifs**) or generated using this pipeline (these generated motifs referred as **internal motif**).

### A. INTERNAL motif discovery and processing ###

In a nutshell, starting from a set of genomic coordinates in `.narrowPeak format` (e.g., derived from ChIP-seq or ChIP-exo experiments), the pipeline launches four motif discovery algorithms, scans the discovered motifs to predict TF binding sites (TFBSs) and selects those motifs whose TFBSs are enriched in the center of the peaks. Further, the selected motifs are compared against reference motif databases and their suggested uniprot IDs are automatically retrieved. The discovered motifs are also trimmed and all processing after the discovery is done on trimmed motifs as well, so it is possible to compare the results between trimmed and original motifs.

In addition to the motif analysis, this pipeline produces a table that will be used by the JASPAR curators and also a PDF document with the motif details (motif logo in forward and reverse orientation + centrality p-value). Furthermore, the discovered motifs are clustered together with the latest JASPAR collections for smoother curation process.

There are three main task/steps in this pipeline for internal motif processing:
  
- **Step 1:** motif discovery. The program *RSAT peak-motifs* is called, this program runs four motif discovery algorithms (over-represented k-mers, over-represented spaced-k-mers, positionally biased k-mers, k-mers enriched in central windows) to discover significant motifs on each set of ChIP-seq peaks. This can be launched by running the *Snakefile*, you can parallelize the process using the argument *--cores*.

- **Step 2:** selecting centrally enriched motifs. Note that given the ChIP-seq dataset
quality and the number of peaks may vary among the datasets, many datasets may not
produce any significant motif. A priori, the number of discovered motifs is unknown,
so we use *checkpoints* within the *Snakefile*.

- **Step 3:** annotating the selected motifs and building the curation table scaffold. The curation table is pre-filled with paths to the files, putative TF, families and classes names. Also suggestions for the UniProt IDs and JASPAR matrix IDs. This table is then automatically exported as a Google Sheets and could be used for manual curation.

### B. EXTERNAL motif processing ###

When motifs are already produced and available (e.g., motifs produced with SELEX or when peaks are not available), the pipeline will take such motifs in `.jaspar` format. These motifs are then also trimmed and converted to different formats. All motifs will be selected for curation and post-processing performed (same as *Step 3* for internal motifs).

In the case of external motifs, no PDF file with motifs logos is produced since no centrality analysis is performed. But motifs are clustered the same way and curation tables exported the same way as for internal motifs.

![Internal and External motifs](docs/readme_figures/discovery_curation.png){width = 300 height = 200}

**Figure 1. Curation explained.** Internal motifs are generated and external motifs collected, both processed and prepared for manual curation.

## Pipeline requirements ##

### Software required ###

- Regulatory sequences analysis tools [**RSAT**](http://rsat-tagc.univ-mrs.fr/rsat/)
- [**R**](https://www.r-project.org/) >= 4
- Perl TFBS modules: http://tfbs.genereg.net/
- *snakemake*: https://snakemake.readthedocs.io/en/stable/index.html
- pdfunite

### Required R packages ###

There is a list of R packages that are required for the pipeline to run. We provide an R script for easy installation of such packages.

```bash
## Run the script to install required R packages:
Rscript bin/installation/install_R_packages_curation.R
```

## Set-up the analysis pipeline ##

In order to successfully run this pipeline you must set it up. This includes setting-up Google Cloud key, creating a `mapping_file`, downloading the previous JASPAR release metadata and updating the `config.yaml` file.

### A. Google Cloud key ###

This key is required for the export of the curation tables scaffold into the Google sheets for further manual curation. The steps you need to complete to set up a key.

1. Go to the *https://console.cloud.google.com* and make sure you are logged in your Google account. Follow the instructions in *https://gargle.r-lib.org/articles/get-api-credentials.html#service-account-token*.
2. The path to the produced json file with the key should be specified in the `config.yaml` file (see *Configuration file* section below).
3. In addition, make sure that the Google Sheets API is active: *https://console.cloud.google.com/apis/api/sheets.googleapis.com/overview*.

![Google API](docs/readme_figures/google_api.png){width = 300 height = 200}

**Figure 2. Activating Google Sheets API.**

### B. Mapping file ###

A `mapping_file` is a tab-delimited file with the metadata of a ChIP-seq/-exo dataset, one line per dataset.

Required columns of the `mapping_file`:

  1. **Batch_name**     : The name of the collection where a dataset comes from (e.g., ReMap, GTRD). This name is used to group datasets and create a curation table for discovered motifs from this group of datasets
  2. **Peaks**          : File path to the ChIp-seq/exo peaks provided in .narrowPeak format. Please make sure to follow a [established .narrowPeak format](http://genome.ucsc.edu/FAQ/FAQformat.html#format12)
  3. **Folder**         : Path to the folder containing the peak files
  4. **Organism**       : Organism name
  5. **Taxon**          : Taxon name
  6. **Taxon_ID**       : Taxon ID, based on NCBI taxon database
  7. **Data**           : Data type (e.g., ChIP-seq, ChIP-exo, DAP-seq, PBM)
  8. **Source**         : Pubmed ID for the reference associated to a dataset
  9. **Genome_version** : Genome assembly name
  10. **Genome_fasta**   : File path to the genome in fasta format
  11. **Chrom_sizes**    : File path to the file indicating the chromosome sizes
  12. **TF**             : TF name
  13. **Dataset_ID**     : A unique identifier

Example of `mapping_file`:

```bash
Batch_name	Peaks	Folder	Organism	Taxon	Data	Source	Genome_version	Genome_fasta	Chrom_sizes	TF	Dataset_ID

Remap_human	data/hg38/BNC2/ReMap2022-hg38_BNC2_1.narrowPeak	data/hg38	Homo_sapiens	Vertebrates	ChIP-seq	34751401	hg38	hg38/genome.fa hg38/hg38.chrom.sizes	BNC2	DatasetID_1

Remap_human	data/hg38/THRA/ReMap2022-hg38_THRA_1.narrowPeak data/hg38	Homo_sapiens	Vertebrates	ChIP-seq	34751401	hg38	hg38/genome.fa hg38/hg38.chrom.sizes	THRA	DatasetID_2
```

#### How to generate a mapping file and biuld a required folder structure? ####

To be able to generate uniform output for further manual curation and downstream analysis, we have decided to require a specific input data folder structure together with a mapping file, which is used to guide the motif discovery and later in the downstream analysis. Here we describe the requirements and provide a script to generate such mapping file.

##### Expected folder structure #####

The Snakefile will launch the motif analysis and the automatic annotation for each
discovered motif of each dataset. This file expects as input a `.narrowPeak` (internal motif discovery) or `.jaspar` file that must be located in the 'Peaks' column in the `mapping_file` (see *Configuration file* section).

**Example of peak files for internal motif discovery:**

```bash
data/internal_peaks
│
├── BNC2
│ └── ReMap2022-hg38_BNC2_1.narrowPeak
│
└── ZNF880
    └── ReMap2022-hg38_ZNF880_1.narrowPeak
```

**Example of external motif files:**

```bash
data/external_motifs
│
├── BNC2_1.jaspar
│
└── ZNF880_1.jaspar
```

##### Expected input data format #####

**A. INTERNAL motifs:**

The *narrowPeak* filename must have the following format with 3 fields separated by an underscore '_':

1. Collection Name (any string, could have internal fields separated by '-')
2. TF name
3. Number (this is required to avoid duplicated names in cases when there are many datasets for the same TF)

See the previous example with two correclty named *narrowPeak* files

Here is an example of a *narrowPeak* file, more information [here](http://genome.ucsc.edu/FAQ/FAQformat.html#format12).

```bash
chr1    240533736   240534074   .       0       .       182     5.0945  -1  50
chr10   75734440    75734784    .       0       .       182     7.1925  -1  50
chr10   87818030    87818427    .       0       .       182     5.0344  -1  50
chr10   124953340   124953826   .       0       .       182     1.2928  -1  50
chr10   132128904   132129255   .       0       .       182     8.0294  -1  50
```

**B. EXTERNAL motifs:**

External motifs should be provided in *jaspar* format. Filename should have two fields separated by an underscore '_':

1. TF name
2. Number (this is required to avoid duplicated names in cases when there are many motifs for the same TF)

##### A script to generate mapping file #####

**NOTE!** Data can come in various shapes and sizes, which might require adjustment in mapping file generation script. But a script bellow can be used as an example. If the data is provided as explained above, this script will work.

```bash
bin/pre_processing/generate_mapping_file.R

## Running the script:
Rscript bin/pre_processing/generate_mapping_file.R -d data/mapping_files/JASPAR_datasets_info.tab -o data/mapping_files
```

To run the script, please provide information about the data in a tab-separated file. One row should provide information about one batch. Example file (`data/mapping_files/JASPAR_datasets_info.tab`):

```bash
Batch_name	Folder	Organism	Taxon	Taxon_ID  Data	Source	Genome_version	Genome_fasta	Chrom_sizes
Remap_mouse	/storage/mathelierarea/processed/jamondra/Projects/JASPAR/JASPAR_2024/jaspar_2024_curation_pipeline/data/mm10	Mus_musculus	Vertebrates 10090 ChIP-seq	34751401	mm10	/storage/mathelierarea/raw/UCSC/mm10/mm10.fa	/storage/mathelierarea/raw/UCSC/mm10/mm10.chrom.sizes
Remap_human	/storage/mathelierarea/processed/jamondra/Projects/JASPAR/JASPAR_2024/jaspar_2024_curation_pipeline/data/hg38	Homo_sapiens	Vertebrates 9606  ChIP-seq	34751401	hg38	/storage/mathelierarea/raw/UCSC/hg38/Sequence/WholeGenomeFasta/genome.fa	/storage/mathelierarea/raw/UCSC/hg38/Sequence/WholeGenomeFasta/hg38.chrom.sizes
```

### C. Downloading previous JASPAR release metadata ###

Previous JASPAR release metadata is used to map related matrix ID, TF, family and class names to ease manual curation process. In addition, latest JASPAR profiles will also be used to annotate the discovered motifs.

We provide a script `bin/retrieve_data/retrieve_latest_matrix_information.R` to download the metadata. This script will download metadata for the latest profiles and also will create separate metadata files for each taxon.

```bash
## Running the script:
Rscript bin/retrieve_data/retrieve_latest_matrix_information.R -o data/metadata
```

### D. Downloading previous JASPAR motif collections ###

For internal and external motif clustering with previous JASPAR motif collection, we provide instructions how to download the motifs and create taxon-specific CORE and UNVALIDATED collection (also merged CORE + UNVALIDATED collection.)

```bash
## Download individual matrices in CORE and UNVALIDATED collections:
wget -O JASPAR/2022/core_unvalidated_nonredundant/all_taxons/JASPAR2022_CORE_non-redundant_pfms_jaspar.zip https://jaspar.genereg.net/download/data/2022/CORE/JASPAR2022_CORE_non-redundant_pfms_jaspar.zip ;

wget -O JASPAR/2022/core_unvalidated_nonredundant/all_taxons/JASPAR2022_UNVALIDATED_non-redundant_pfms_jaspar.zip https://jaspar.genereg.net/download/data/2022/collections/JASPAR2022_UNVALIDATED_non-redundant_pfms_jaspar.zip ;

## Unarchive the data:
unzip JASPAR/2022/core_unvalidated_nonredundant/all_taxons/JASPAR2022_CORE_non-redundant_pfms_jaspar.zip -d JASPAR/2022/core_unvalidated_nonredundant/all_taxons/jaspar_all_motifs ;

unzip 2022/core_unvalidated_nonredundant/all_taxons/JASPAR2022_UNVALIDATED_non-redundant_pfms_jaspar.zip -d JASPAR/2022/core_unvalidated_nonredundant/all_taxons/jaspar_all_motifs ;
```

To create motif collections for individual taxons we provide a script:

```bash
## Listing all metadata files:
jaspar_metadata_files=$(find data/metadata -path '*_CORE_UNVALIDATED_*.tab' | tr '\n' ',' | sed 's/,$//')

## Creating motif collections:
Rscript bin/pre_processing/prepare_jaspar_motif_collections.R \
-j JASPAR/2022/core_unvalidated_nonredundant/all_taxons/jaspar_all_motifs \
-n $jaspar_metadata_files \
-o data/motif_databases
```

**TIPS&TRICKS** Run all data preparation scripts in one go with `bin/pre_processing/generate_metadata_and_mapping_files.sh`.

### E. Configuration file ###

Once you have aquired the data described above, you need to adjust the configuration file `config.yaml`. From JASPAR 2024 release onwards motif discovery and curation pipeline uses a single cnfiguration file Set the following variables in the config file:

**1. Input files:**

- `previous_jaspar_metadata` : a tab-delimited file containing the *JASPAR* motif metadata (how to download this, see section above)
- `mapping_file`             : a tab-delimited file with the metadata of a ChIP-seq/-exo dataset, one line per dataset (how to prepare this file, see section above).

**2. Output directory:**

The directory where all results from the analysis will be exported.

**3. Software variables:** (assuming that you are in the *jaspar_2024_curation_pipeline* folder)
  
- `bin`         : bin
- `python`      : python2.7
- `RSAT`        : /lsc/rsat
- `google_token`: gs4_config/jaspar2024-token.json
- `gs_url`      : https://docs.google.com/spreadsheets/d/1P6BgNtDvcheo-0gf1nzEY__Q27GWJG3StoKv-6xuAWE

You can see where *RSAT* scripts are located using the following command: ```echo $RSAT```

See section above for how to retrieve the google token.

**4. Motif databases:**

The discionary with motif databases is needed for the pipeline to know with what collection to compare the discovered internal or external motifs. We here compare the motifs with JASPAR CORE and UNVALIDATED collections separately, thus we need 2 dictionaries. In the section above we explained and provided instructions on how to retrieve these collections from JASPAR. Example of collections for plants and vertebrates:

```bash
CORE_motif_collections        :
{'Plants': 'data/motif_databases/plants_jaspar_CORE_motif_collection.tf',
'Vertebrates': 'data/motif_databases/vertebrates_jaspar_CORE_motif_collection.tf'}

UNVALIDATED_motif_collections : 
{'Plants': 'data/motif_databases/plants_jaspar_UNVALIDATED_motif_collection.tf',
'Vertebrates': 'data/motif_databases/vertebrates_jaspar_UNVALIDATED_motif_collection.tf'}
```

**5. Motif discovery parameters:**

These parameters are required for internal motif discovery. We suggest leaving default as indicated in the `config.yaml`.

- `peakmotifs_disco`
- `peakmotifs_disco_nb_motifs`
- `peakmotifs_disco_min_oligo`
- `peakmotifs_disco_max_oligo`
- `peakmotifs_class_interval`
- `central_pvalue`

**6. Other parameters:**

- `top_motifs`                : The number of motif selected based on the centrality p-value for each dataset. Default = 3.
- `n_hits_suggested_uniprots` : The number of *Uniprot IDs* suggested for a given TF name. Default = 1.
- `nb_comparison_matches`     : The most similar motifs (from a motif database). Default = 5.
- `TRIM_IC_THRESHOLD`         : IC threshold that is used when trimming the motifs.

## Launching the pipeline ##

To start the analysis just type:

```bash
## Running the analysis with 1 core:
snakemake --cores 1
```

## Pipeline output ##

### Output directory structure ####

The tree bellow illustrates the results folder structure (3 directories down). Results for internal and external motifs are located in separate directories. Further, each batch ID gets its own results, which main ones include the curation table (this will be exported into Google Sheets in separate tabs named after batch ID and including the date when the pipeline was started), clustering results and PDF file with centrality plots and logos (for internal motifs only).

```bash
results/
├── external
│   ├── Factorbook_ChIP-seq
│   │   ├── curation
│   │   ├── DatasetID_38
│   │   └── DatasetID_39
│   └── Factorbook_SELEX
│       ├── curation
│       └── DatasetID_11511
├── internal
│   └── Lai_ChIP-exo_2021
│       ├── curation
│       ├── DatasetID_14143
│       ├── DatasetID_14144
│       ├── DatasetID_14145
│       └── DatasetID_14890
└── run_time.txt
```

### Data flow into manual curation, downstream analysis and deployment steps ###

Motif discovery pipeline produces files of which some are most important for manual curation, then downstream analysis, while some are also deployed into the database. See bellow a more detailed explanation of the data flow.

In the workflow of data processing and motif curation (**Figure 1**) motifs are of two types: externally and internally generated motifs. This pipeline process both types and the motifs are then passed to manual curation step where they are manually inspected by a team of curators and orthogonal support in the literature is checked.

#### MANUAL CURATION ####

For manual curation, the most important output is the `curation table`, `clustering tree` and a `PDF document` with motif logos and centrality plots. The example paths to these files are:

```bash
## Curation tables (original and trimmed motifs):

results/internal/Lai_ChIP-exo_2021/curation/Selected_motifs_to_curate_log10_filtered.tab
results/internal/Lai_ChIP-exo_2021/curation/Selected_trimmed_motifs_to_curate_log10_filtered.tab
# Curation table for trimmed motifs is exported to Google Sheets table (each batch ID gets a new sheet)

## Clustering trees (original and trimmed motifs):

results/internal/Lai_ChIP-exo_2021/curation/motif_clustering/matrix-clustering_Lai_ChIP-exo_2021_SUMMARY.html
results/internal/Lai_ChIP-exo_2021/curation/motif_clustering_trimmed/matrix-clustering_Lai_ChIP-exo_2021_SUMMARY.html

## PDFs with centrality and logos (original and trimmed motifs):

results/internal/Lai_ChIP-exo_2021/curation/Selected_motifs_to_curate_log10_filtered.pdf
results/internal/Lai_ChIP-exo_2021/curation/Selected_trimmed_motifs_to_curate_log10_filtered.pdf
```

#### DOWNSTREAM analysis ####

Downstream analysis makes use of the `manually curated table`, which is taken from Google Sheets after mutual aggreement that the manual curation process is done. Furthermore, downstream analysis reads an initial `mapping file` (see *Configuration file* section). Other important files are defined in the curation table itself:

- PWMs
- FASTA files
- BED files
- Motifs logos

#### DEPLOYMENT ####

Deployment makes use of the `manually curated table`, which is taken from Google Sheets after mutual aggreement that the manual curation process is done. In addition, the downstream analysis must also be performed and finished to succesfully start the deployment process.

The paths to specific files in the curation table are important in the deployment:

- PWMs
- FASTA files
- BED files
- Centrality plots

## Troubleshooting ##

### rule RSAT_peakmotifs_per_exp ###

- *RSAT peak-motifs* none motif found: verify that the input *NarrowPeak* is not empty.

- *RSAT local-word-analysis* python version: this program is written in python and called within RSAT peak-motifs. In case that the default python environment is 3.5, local-word-analysis will not run, therefore the solution is modify directly the line in the $RSAT/perl-scripts/peak-motifs script adding "python2.7" before the program is called.

- *RSAT position-analysis* founds highly repetitive/uninformative motifs: The default interval for position-analysis is 50nt, which is too large for the JASPAR pipeline (where the peaks have a size of 101), this may produce non-relevant artifact motifs. By changing the interval to 25 the program may found the expected TF motif. Note that this change was already set in the config file.

### rule annotate_best_centrimo_experiment ###

- Verify that the variables in the *awk* command correspond to the correct fields in your experiment table.

### rule best_centrimo_experiment_logo ###

- Input files not found: this may occur when the Experiment_TF ID contains certain characters as: *(*, *)*, *[*, *]*, *.*, */*, " "(one or more spaces). Before launching the pipeline, verify that none of the folders in your data folder contains such characters, the simplest option is remove them or substitute them by '*-*'.

## Memory requirements ##

If you launch the *Snakefile* using multiple cores (See *Launching the *snakemake* workflow* section), each core will use ~1GB of memory in your computer.

## Related literature ##

- **The original JASPAR paper:** Sandelin A, et al. JASPAR: an open-access database for eukaryotic transcription factor binding profiles. Nucleic Acids Res. 2004; doi: [10.1093/nar/gkh012](https://doi.org/10.1093/nar/gkh012)
- **9th release:** Castro-Mondragon JA, Riudavets-Puig R, Rauluseviciute I et al. JASPAR 2022: the 9th release of the open-access database of transcription factor binding profiles. Nucleic Acids Res. 2022; doi: [10.1093/nar/gkab1113](https://doi.org/10.1093/nar/gkab1113)
- Thomas-Chollier M, et al. RSAT peak-motifs: motif analysis in full-size ChIP-seq datasets. Nucleic Acids Res. 2012; doi: [10.1093/nar/gkr1104)](https://doi.org/10.1093/nar/gkr1104)
- Thomas-Chollier M, et al. A complete workflow for the analysis of full-size ChIP-seq (and similar) data sets using peak-motifs. Nat Protoc. 2012; doi: [10.1038/nprot.2012.088](https://doi.org/10.1038/nprot.2012.088)

## Contact ##

- [Jaime A Castro-Mondragon](https://jaimicore.github.io/)  | j.a.c.mondragon@ncmm.uio.no
- [Rafael Riudavets Puig](r.r.puig@ncmm.uio.no)             | r.r.puig@ncmm.uio.no
- [Ieva Rauluseviciute](https://bitbucket.org/ievara/)      | ieva.rauluseviciute@ncmm.uio.no
- [Anthony Mathelier](https://mathelierlab.com/)            | anthony.mathelier@ncmm.uio.no

**End of README**