#################
## Description ##
#################

## Authors: Jaime A. Castro Mondragon, Rafael Riudavets Puig, Ieva Rauluseviciute
## Affiliation: NCMM, UiO, Oslo, Norway
## See README.md to get more information.

###############################
## How to run this pipeline? ##
###############################

## snakemake --cores 1

######################
## Import functions ##
######################

import itertools
import os
import pandas as pd
import subprocess
import time
from datetime import datetime

#################################
## Defining configuration file ##
#################################

configfile: "config.yaml"

##########################
## Initialize variables ##
##########################

## Record start time:
start = time.time()

## Current date:
todays_date = datetime.date(datetime.now())
print(datetime.date(datetime.now()))

## Reading the mapping file:
mapping = pd.read_csv(config["mapping_file"], header = 0, sep = "\t", index_col = 12)
# mapping = mapping.head()

## Variables from the config to record in the runtime.txt:
used_mapping_file                = config["mapping_file"]
used_previous_jaspar_annotations = config["previous_jaspar_annotations"]
used_out_dir                     = config["out_dir"]
used_scripts_dir                 = config["bin"]

##################################################
## Determine the internal and external datasets ##
##################################################

mapping_internal = mapping[mapping['Peaks'].str.contains("narrowPeak$")]
mapping_external = mapping[mapping['Peaks'].str.contains("jaspar$")]

## Length of internal and external data:
nb_datasets_internal = len(mapping_internal.index)
print("; Analysing ", nb_datasets_internal, " .narrowPeak files.")
nb_datasets_external = len(mapping_external.index)
print("; Analysing ", nb_datasets_external, " .jaspar motifs.")

##=================================##
## A. Internal motifs mapping info ##
##=================================##

internal_dataset_IDs  = mapping_internal.index.to_list()
internal_dataset_dict = mapping_internal.to_dict(orient = "index")
INTERNAL_BATCH_NAME   = mapping_internal["Batch_name"].to_list()

internal_batch_name_to_taxon_dict = {}
internal_batch_names = set(mapping_internal["Batch_name"])
for internal_batch_name in internal_batch_names:
    taxon = mapping_internal.loc[mapping_internal["Batch_name"] == internal_batch_name, "Taxon"][0]
    internal_batch_name_to_taxon_dict[internal_batch_name] = taxon

##=================================##
## B. External motifs mapping info ##
##=================================##

external_dataset_IDs  = mapping_external.index.to_list()
external_dataset_dict = mapping_external.to_dict(orient = "index")
EXTERNAL_BATCH_NAME   = mapping_external["Batch_name"].to_list()

external_batch_name_to_taxon_dict = {}
external_batch_names = set(mapping_external["Batch_name"])
for external_batch_name in external_batch_names:
    taxon = mapping_external.loc[mapping_external["Batch_name"] == external_batch_name, "Taxon"][0]
    external_batch_name_to_taxon_dict[external_batch_name] = taxon

###########################################################
## Peak summit extension (both sides the same extension) ##
## This is needed to discover motifs de novo and then    ##
## perform the centrimo analysis (internal motifs only)  ##
###########################################################

PEAK_LENGTH = "101 501".split()
PEAK_EXTENSIONS =  {
    '101' : '50',
    '501' : '250'
}

########################
## Output directories ##
########################

OUT_DIR               = config["out_dir"]

INTERNAL_RESULTS_DIR  = os.path.join(OUT_DIR, "internal", "{internal_batch_name}", "{internal_dataset_ID}")
INTERNAL_CURATION_DIR = os.path.join(OUT_DIR, "internal", "{internal_batch_name}", "curation")

EXTERNAL_RESULTS_DIR  = os.path.join(OUT_DIR, "external", "{external_batch_name}", "{external_dataset_ID}")
EXTERNAL_CURATION_DIR = os.path.join(OUT_DIR, "external", "{external_batch_name}", "curation")

###############
## Functions ##
###############

### Start-up, error and exit messages:
## When pipeline is executed succesfully:
onsuccess:
    print("\n##---------------------------##\n; Workflow finished, no errors!\n##---------------------------##\n")
    success_runtime = round((time.time() - start)/60,3)
    print("; Running time in minutes:\n %s" % success_runtime)
    f = open("{dir}/run_time.txt".format(dir = OUT_DIR), "w+")
    print("; Running time in minutes:\n %s" % success_runtime, file = f)
    print("\n; Used mapping file:\n %s" % used_mapping_file, file = f)
    print("\n; Used JASPAR annotations:\n %s" % used_previous_jaspar_annotations, file = f)
    print("\n; Used output directory:\n %s" % used_out_dir, file = f)
    print("\n; Used scripts directory:\n %s" % used_scripts_dir, file = f)

## When error occurs:
onerror:
    print("\n##-----------------##\n; An error occurred!\n##-----------------##\n")
    error_runtime = round((time.time() - start)/60,3)
    print("; Running time in minutes:\n %s" % error_runtime)
    print("\n; Used mapping file:\n %s" % used_mapping_file)
    print("\n; Used JASPAR annotations:\n %s" % used_previous_jaspar_annotations)
    print("\n; Used output directory:\n %s" % used_out_dir)
    print("\n; Used scripts directory:\n %s" % used_scripts_dir)

## When starting the run:
onstart:
    print("\n##-----------------------------------------##\n; Reading input and firing up the analysis...\n##-----------------------------------------##\n")
    print("\n; Used mapping file:\n %s" % used_mapping_file)
    print("\n; Used JASPAR annotations:\n %s" % used_previous_jaspar_annotations)
    print("\n; Used output directory:\n %s" % used_out_dir)
    print("\n; Used scripts directory:\n %s" % used_scripts_dir)

##################
## Output files ##
##################

##------------------------------##
## A. INTERNAL MOTIF PROCESSING ##
##------------------------------##

## Filtered tables:
INTERNAL_CURATION_TABLE_FILTERED      = os.path.join(INTERNAL_CURATION_DIR, "internal_motifs_curation_filtered.tab")

## Motif clustering:
## Clustering with CORE collection:
INTERNAL_MOTIF_CLUSTERING_CORE        = os.path.join(INTERNAL_CURATION_DIR, "motif_clustering_with_CORE", "matrix-clustering_{internal_batch_name}.txt")

## Clustering with UNVALIDATED collection:
INTERNAL_MOTIF_CLUSTERING_UNVALIDATED = os.path.join(INTERNAL_CURATION_DIR, "motif_clustering_with_UNVALIDATED", "matrix-clustering_{internal_batch_name}.txt")

## Exported table to google sheet document
INTERNAL_GOOGLE_SHEETS_LOG            = os.path.join(INTERNAL_CURATION_DIR, "Google_sheets_exported_log.txt")

##------------------------------##
## B. EXTERNAL MOTIF PROCESSING ##
##------------------------------##

## Filtered tables:
EXTERNAL_CURATION_TABLE               = os.path.join(EXTERNAL_CURATION_DIR, "external_motifs_curation.tab")

## Motif clustering:
## Clustering with CORE collection:
EXTERNAL_MOTIF_CLUSTERING_CORE        = os.path.join(EXTERNAL_CURATION_DIR, "motif_clustering_with_CORE", "matrix-clustering_{external_batch_name}.txt")

## Clustering with UNVALIDATED collection:
EXTERNAL_MOTIF_CLUSTERING_UNVALIDATED = os.path.join(EXTERNAL_CURATION_DIR, "motif_clustering_with_UNVALIDATED", "matrix-clustering_{external_batch_name}.txt")

## Exported table to google sheet document
EXTERNAL_GOOGLE_SHEETS_LOG            = os.path.join(EXTERNAL_CURATION_DIR, "Google_sheets_exported_log.txt")

###############
## Rules ALL ##
###############

if ((nb_datasets_internal > 0) and (nb_datasets_external == 0)):

    rule all:
        input:
            expand(INTERNAL_CURATION_TABLE_FILTERED,      internal_batch_name = INTERNAL_BATCH_NAME), \
            expand(INTERNAL_MOTIF_CLUSTERING_CORE,        internal_batch_name = INTERNAL_BATCH_NAME), \
            expand(INTERNAL_MOTIF_CLUSTERING_UNVALIDATED, internal_batch_name = INTERNAL_BATCH_NAME), \
            expand(INTERNAL_GOOGLE_SHEETS_LOG,            internal_batch_name = INTERNAL_BATCH_NAME)

elif ((nb_datasets_external > 0) and (nb_datasets_internal == 0)):

    rule all:
        input:
            expand(EXTERNAL_CURATION_TABLE,               external_batch_name = EXTERNAL_BATCH_NAME), \
            expand(EXTERNAL_MOTIF_CLUSTERING_CORE,        external_batch_name = EXTERNAL_BATCH_NAME), \
            expand(EXTERNAL_MOTIF_CLUSTERING_UNVALIDATED, external_batch_name = EXTERNAL_BATCH_NAME), \
            expand(EXTERNAL_GOOGLE_SHEETS_LOG,            external_batch_name = EXTERNAL_BATCH_NAME)

else :

    rule all:
        input:
            expand(INTERNAL_CURATION_TABLE_FILTERED,      internal_batch_name = INTERNAL_BATCH_NAME), \
            expand(INTERNAL_MOTIF_CLUSTERING_CORE,        internal_batch_name = INTERNAL_BATCH_NAME), \
            expand(INTERNAL_MOTIF_CLUSTERING_UNVALIDATED, internal_batch_name = INTERNAL_BATCH_NAME), \
            expand(INTERNAL_GOOGLE_SHEETS_LOG,            internal_batch_name = INTERNAL_BATCH_NAME), \
            expand(EXTERNAL_CURATION_TABLE,               external_batch_name = EXTERNAL_BATCH_NAME), \
            expand(EXTERNAL_MOTIF_CLUSTERING_CORE,        external_batch_name = EXTERNAL_BATCH_NAME), \
            expand(EXTERNAL_MOTIF_CLUSTERING_UNVALIDATED, external_batch_name = EXTERNAL_BATCH_NAME), \
            expand(EXTERNAL_GOOGLE_SHEETS_LOG,            external_batch_name = EXTERNAL_BATCH_NAME)

##############
## Includes ##
##############

include: "process_internal.smk"
include: "process_external.smk"

######################
## End of Snakefile ##
######################
