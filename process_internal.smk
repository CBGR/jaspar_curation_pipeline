##################
## Output files ##
##################

## 1) Processing of the input peak files:
PEAK_SUMMIT                           = os.path.join(INTERNAL_RESULTS_DIR, "peak_summits", "{internal_dataset_ID}_peak_summits.bed")

EXTENDED_PEAK                         = os.path.join(INTERNAL_RESULTS_DIR, "extended_peaks", "{internal_dataset_ID}.{length}bp.bed")
EXTENDED_PEAK_501                     = os.path.join(INTERNAL_RESULTS_DIR, "extended_peaks", "{internal_dataset_ID}.501bp.bed")

EXTENDED_PEAK_FA                      = os.path.join(INTERNAL_RESULTS_DIR, "fasta", "{internal_dataset_ID}.{length}bp.fa") ## This is defined more specifically bellow, because certain rules will require specific extensions.
EXTENDED_PEAK_101_FA                  = os.path.join(INTERNAL_RESULTS_DIR, "fasta", "{internal_dataset_ID}.101bp.fa") ## For motif discovery.
EXTENDED_PEAK_501_FA                  = os.path.join(INTERNAL_RESULTS_DIR, "fasta", "{internal_dataset_ID}.501bp.fa") ## For centrimo scanning.

## 2) Motif discovery:
DISCO_MOTIFS                          = os.path.join(INTERNAL_RESULTS_DIR, "peak-motifs", "results", "discovered_motifs", "{internal_dataset_ID}_motifs_discovered.tf")
DISCO_MOTIFS_ID                       = os.path.join(INTERNAL_RESULTS_DIR, "peak-motifs", "results", "discovered_motifs", "{internal_dataset_ID}_motifs_map.tab")

## Splitting the discovered motifs into individual files: (THIS IS CHECKPOINT)
INTERNAL_JASPAR_PFM                   = os.path.join(INTERNAL_RESULTS_DIR, "motifs", "jaspar", "pfm", "{internal_dataset_ID}_peak-motifs_m{n}.jaspar")

## Converting the motifs:
INTERNAL_TRANSFAC_PFM                 = os.path.join(INTERNAL_RESULTS_DIR, "motifs", "jaspar", "pfm", "{internal_dataset_ID}_peak-motifs_m{n}.tf")
INTERNAL_TAB_PFM                      = os.path.join(INTERNAL_RESULTS_DIR, "motifs", "jaspar", "pfm", "{internal_dataset_ID}_peak-motifs_m{n}.tab")

## PWM and LOGO:
INTERNAL_JASPAR_PWM                   = os.path.join(INTERNAL_RESULTS_DIR, "motifs", "jaspar", "pwm", "{internal_dataset_ID}_peak-motifs_m{n}.jaspar.pssm")
INTERNAL_JASPAR_LOGO                  = os.path.join(INTERNAL_RESULTS_DIR, "motifs", "jaspar", "logos", "{internal_dataset_ID}_peak-motifs_m{n}_logo.png")

## 3) Motif comparison:
INTERNAL_PFM_COMPA_MATCHES            = os.path.join(INTERNAL_RESULTS_DIR, "motifs", "comparison", "{internal_dataset_ID}_peak-motifs_m{n}_motif_comparison.tab")

## 4) Matrix regions and sites:
PFM_SITES                             = os.path.join(INTERNAL_RESULTS_DIR, "matrix_sites", "{internal_dataset_ID}_peak-motifs_m{n}.tf.sites")
PFM_SITES_BED                         = os.path.join(INTERNAL_RESULTS_DIR, "matrix_sites", "{internal_dataset_ID}_peak-motifs_m{n}.tf.sites.bed")
PFM_SITES_FA                          = os.path.join(INTERNAL_RESULTS_DIR, "matrix_sites", "{internal_dataset_ID}_peak-motifs_m{n}.tf.sites.fasta")

## 5) Central enrichment:
## For individual matrices:
TFBSs_AROUND_501                      = os.path.join(INTERNAL_RESULTS_DIR, "scan", "501bp", "{internal_dataset_ID}_m{n}.501bp.fa")
CENTRIMO_PVALUE                       = os.path.join(INTERNAL_RESULTS_DIR, "central_enrichment", "{internal_dataset_ID}_m{n}.501bp.fa.sites.centrimo")
CENTRIMO_PLOT                         = os.path.join(INTERNAL_RESULTS_DIR, "central_enrichment", "{internal_dataset_ID}_m{n}.501bp.fa.sites.centrimo.pdf")

## Collecting results for datasets:
BEST_CENTRIMO_MOTIFS                  = os.path.join(INTERNAL_RESULTS_DIR, "central_enrichment", "selected_motif", "{internal_dataset_ID}.501bp.fa.sites.centrimo.best")

## 6) Annotations:
INTERNAL_UNIPROT_ANNOTATIONS          = os.path.join(INTERNAL_RESULTS_DIR, "annotations", "{internal_dataset_ID}_uniprot_annotation.txt")
BEST_INTERNAL_MOTIF_ANNOT_TMP         = os.path.join(INTERNAL_RESULTS_DIR, "annotations", "{internal_dataset_ID}_curation_tab_entry_tmp.txt")
BEST_INTERNAL_MOTIF_ANNOT             = os.path.join(INTERNAL_RESULTS_DIR, "annotations", "{internal_dataset_ID}_curation_tab_entry.txt")
BEST_INTERNAL_MOTIF_ANNOT_TEX         = os.path.join(INTERNAL_RESULTS_DIR, "annotations", "{internal_dataset_ID}.internal_logo.tex")
BEST_INTERNAL_MOTIF_ANNOT_PDF         = os.path.join(INTERNAL_RESULTS_DIR, "annotations", "{internal_dataset_ID}.internal_logo.pdf")

## 7) Curation table:
INTERNAL_CURATION_TABLE               = os.path.join(INTERNAL_CURATION_DIR, "internal_motifs_curation.tab")

## 8) Filtered curation table and PDF report:
INTERNAL_CURATION_TABLE_FILTERED      = os.path.join(INTERNAL_CURATION_DIR, "internal_motifs_curation_filtered.tab")
INTERNAL_CURATION_TABLE_FILTERED_PDF  = os.path.join(INTERNAL_CURATION_DIR, "internal_motifs_curation_filtered.pdf")

## 9) Selected motifs in jaspar format with renamed header:
INTERNAL_JASPAR_MOTIF_CONCAT          = os.path.join(INTERNAL_CURATION_DIR, "selected_motifs.jaspar")

## 10) Motif clustering:
INTERNAL_MOTIF_CLUSTERING_CORE        = os.path.join(INTERNAL_CURATION_DIR, "motif_clustering_with_CORE", "matrix-clustering_{internal_batch_name}.txt")
INTERNAL_MOTIF_CLUSTERING_UNVALIDATED = os.path.join(INTERNAL_CURATION_DIR, "motif_clustering_with_UNVALIDATED", "matrix-clustering_{internal_batch_name}.txt")

## 11) Exported table to google sheet document
INTERNAL_GOOGLE_SHEETS_LOG            = os.path.join(INTERNAL_CURATION_DIR, "Google_sheets_exported_log.txt")

###########
## RULES ##
###########

##-----------------------------------##
## 1) Processing of input peak files ##
##-----------------------------------##

rule extract_peak_summits:
    """
    1.1. Extract peak summits and output in a format - chromosome, start, end.
    """
    input:
        lambda wildcards: internal_dataset_dict[wildcards.internal_dataset_ID]["Peaks"]
    output:
        PEAK_SUMMIT
    message:
        "; Extracting peak summits for: {wildcards.internal_dataset_ID} "
    shell:
        """
        awk '{{print $1"\\t"($2+$10)"\\t"($2+$10+1)}}' \
          {input} \
          > {output};
        """

rule extend_peak_files:
    """
    1.2. Extend the peak summits to both sides by 50 bp (total window 101 bp) for discovery and by 250 bp (total window 501 bp) for the centrality analysis.
    """
    input:
        PEAK_SUMMIT
    output:
        EXTENDED_PEAK
    message:
        "; Extending peak summits for motif discovery and centrality analysis for : {wildcards.internal_dataset_ID}. Region length: {wildcards.length} "
    params:
        chrom_sizes = lambda wildcards: internal_dataset_dict[wildcards.internal_dataset_ID]["Chrom_sizes"],
        slop        = lambda wildcards: PEAK_EXTENSIONS[wildcards.length]
    shell:
        """
        bedtools slop \
          -b {params.slop} \
          -i {input} \
          -g {params.chrom_sizes} \
          > {output};
        """

rule get_fasta_for_peaks:
    """
    1.3. Obtain the fasta sequences for the extended regions.
    """
    input:
        EXTENDED_PEAK
    output:
        EXTENDED_PEAK_FA
    message:
        "; Retrieving FASTA sequences from extended peaks for: {wildcards.internal_dataset_ID}. Region length: {wildcards.length} "
    params:
        genome_fasta = lambda wildcards: internal_dataset_dict[wildcards.internal_dataset_ID]["Genome_fasta"]
    shell:
        """
        bedtools getfasta \
          -fi {params.genome_fasta} \
          -bed {input} \
          -fo {output};
        """

##-------------------------##
## 2) RSAT motif discovery ##
##-------------------------##

rule discover_motifs_with_RSAT_peak_motifs:
    """
    2.1. Run RSAT peak-motifs (motif discovery with different algorithms) on every peak set.
        The analysis is done on 101 bp lenght regions.
        The set of N discovered motifs is stored in a single transfac file.
    """
    input:
        EXTENDED_PEAK_101_FA
    output:
        DISCO_MOTIFS
    message:
        "; Running RSAT peak-motifs for: {wildcards.internal_dataset_ID} "
    params:
        RSAT          = config["RSAT"],
        disco         = config["peakmotifs_disco"],
        nb_motifs     = config["peakmotifs_disco_nb_motifs"],
        min_oligo     = config["peakmotifs_disco_min_oligo"],
        max_oligo     = config["peakmotifs_disco_max_oligo"],
        ci            = config["peakmotifs_class_interval"],
        task          = "purge,seqlen,composition,disco,merge_motifs,split_motifs,timelog,archive,synthesis",
        prefix        = "{internal_dataset_ID}",
        peakmo_outdir = os.path.join(INTERNAL_RESULTS_DIR, "peak-motifs")
    shell:
        """
        {params.RSAT}/perl-scripts/peak-motifs -v 2 \
          -r_plot \
          -title {wildcards.internal_dataset_ID} \
          -i {input} \
          -markov auto \
          -disco {params.disco} \
          -nmotifs {params.nb_motifs} \
          -minol {params.min_oligo} \
          -maxol {params.max_oligo} \
          -no_merge_lengths \
          -ci {params.ci} \
          -noov \
          -2str \
          -origin center \
          -scan_markov 1 \
          -task {params.task} \
          -prefix {params.prefix} \
          -img_format png \
          -outdir {params.peakmo_outdir} ;

        rm -r {params.peakmo_outdir}/data ;
        rm -r {params.peakmo_outdir}/images_html ;
        rm -r {params.peakmo_outdir}/reports ;
        rm {params.peakmo_outdir}/*_synthesis.html ;
        rm -r {params.peakmo_outdir}/results/sites ;
        rm -r {params.peakmo_outdir}/results/composition ;
        rm -r {params.peakmo_outdir}/results/merged_words ;
        """

rule map_motif_numbers_to_IDs:
    """
    2.2. Map table to associate motif numbers (m1, m2, ...) with motif names from peak-motifs (e.g., oligos_6nt_mkv3_m1)
    """
    input:
        DISCO_MOTIFS
    output:
        DISCO_MOTIFS_ID
    message:
        "; Associating motif numbers with motif IDs : {wildcards.internal_dataset_ID}"
    shell:
        """
        grep '^AC' {input} \
        | cut -d' ' -f3 \
        | perl -pe '$_ = "peak-motifs_m$.\t$_"' \
        > {output}
        """

checkpoint RSAT_PSSM_to_JASPAR_format:
    """
    2.3. Convert the RSAT discovered matrices from transfac (tf) format to JASPAR format.
    The transfac file is split in several files (one per motif) adding as suffix the motif number (e.g., 1,2,3,...).
    """
    input:
        DISCO_MOTIFS
    output:
       directory(os.path.join(INTERNAL_RESULTS_DIR, "motifs", "jaspar", "pfm"))
    message:
        "; Splitting RSAT discovered motifs into individual matrices in JASPAR format for: {wildcards.internal_dataset_ID} "
    params:
        RSAT          = config["RSAT"],
        return_fields = "counts",
        prefix        = "peak-motifs",
        prefix_motif  = os.path.join(INTERNAL_RESULTS_DIR, "motifs", "jaspar", "pfm", "{internal_dataset_ID}"),
        out_dir       = os.path.join(INTERNAL_RESULTS_DIR, "motifs", "jaspar", "pfm")
    shell:
        """
        n_lines_output=$(wc -l {input} | awk '{{print $1}}') ;

        mkdir -p {params.out_dir} ;

        if [ $n_lines_output -gt 0 ]
        then
          {params.RSAT}/perl-scripts/convert-matrix -v 2 \
            -from tf -to jaspar \
            -i {input} \
            -return {params.return_fields} \
            -split \
            -prefix {params.prefix} \
            -o {params.prefix_motif}
        fi
        """

rule discovered_motif_to_transfac:
    """
    2.4. Convert the discovered motif in JASPAR format into transfac format.
    """
    input:
        INTERNAL_JASPAR_PFM
    output:
        INTERNAL_TRANSFAC_PFM
    message:
        "; Converting discovered motif in JASPAR format into transfac format for: {wildcards.internal_dataset_ID} - Matrix number: {wildcards.n} "
    params:
        RSAT          = config["RSAT"],
        return_fields = "counts",
        prefix        = "peak-motifs",
        out_dir       = os.path.join(INTERNAL_RESULTS_DIR, "motifs", "jaspar", "pfm")
    shell:
        """
        {params.RSAT}/perl-scripts/convert-matrix -v 2 \
        -from jaspar -to tf \
        -i {input} \
        -return {params.return_fields} \
        -prefix {params.prefix} \
        -o {output} ;
        """

rule discovered_motif_to_tab:
    """
    2.5. Convert discovered motif in transfac format into tab format.
    """
    input:
        INTERNAL_TRANSFAC_PFM
    output:
        INTERNAL_TAB_PFM
    message:
        "; Coverting discovered motif in transfac format into tab format for: {wildcards.internal_dataset_ID} - Matrix number: {wildcards.n} "
    params:
        RSAT          = config["RSAT"],
        return_fields = "counts",
        prefix        = "peak-motifs",
        out_dir       = os.path.join(INTERNAL_RESULTS_DIR, "motifs", "jaspar", "pfm")
    shell:
        """
        {params.RSAT}/perl-scripts/convert-matrix -v 2 \
        -from tf -to tab \
        -i {input} \
        -return {params.return_fields} \
        -prefix {params.prefix} \
        -o {output};
        """   

rule JASPAR_PSSM_to_PWM:
    """
    2.6. Generate PWMs from motifs in JASPAR format.
        PWMs are later used to scan the regions for centrality analysis.
    """
    input:
        jaspar_pfm = INTERNAL_JASPAR_PFM, \
        tab_pfm    = INTERNAL_TAB_PFM ## To connect with the previous rule
    output:
        INTERNAL_JASPAR_PWM
    message:
        "; Generating PWM from JASPAR matrices for: {wildcards.internal_dataset_ID} - Matrix number: {wildcards.n} "
    params:
        scripts_bin     = os.path.join(config["bin"], "motif_discovery"),
        out_dir         = os.path.join(INTERNAL_RESULTS_DIR, "motifs", "jaspar", "pwm")
    shell:
        """
        mkdir -p {params.out_dir} ;

        perl {params.scripts_bin}/PCM_to_PWM.pl \
          -f {input.jaspar_pfm} \
          > {output}
        """

rule generate_internal_matrix_logo:
    """
    2.7. Generate motif logos using RSAT convert-matrix.
    """
    input:
        jaspar_pfm = INTERNAL_JASPAR_PFM, \
        jaspar_pwm = INTERNAL_JASPAR_PWM ## To connect with the previous rule
    output:
        INTERNAL_JASPAR_LOGO
    message:
        "; Generating sequence LOGO from JASPAR matrices for: {wildcards.internal_dataset_ID} - Matrix number: {wildcards.n} "
    params:
        logo_dir  = os.path.join(INTERNAL_RESULTS_DIR, "motifs", "jaspar", "logos"),
        logo_name = "{internal_dataset_ID}_peak-motifs_m{n}",
        RSAT      = config["RSAT"]
    shell:
        """
        {params.RSAT}/perl-scripts/convert-matrix -v 2 \
          -i {input.jaspar_pfm} \
          -from jaspar -to jaspar \
          -return logo \
          -logo_dir {params.logo_dir} \
          -logo_no_title \
          -prefix {params.logo_name}
        """

##------------------------------------------------------##
## 3) Motif comparison with know motifs to infer the TF ##
##------------------------------------------------------##

rule discovered_motifs_vs_databases:
    """
    3.1. Compare each discovered motif vs a reference database (JASPAR, cis-bp, ...)
    """
    input:
        transfac_pfm     = INTERNAL_TRANSFAC_PFM, \
        reference_motifs = lambda wildcards: config["motif_databases"][internal_dataset_dict[wildcards.internal_dataset_ID]["Taxon"]], \
        sequence_logo    = INTERNAL_JASPAR_LOGO
    output:
        INTERNAL_PFM_COMPA_MATCHES
    message:
        "; Comparing discovered motif vs reference databases for: {wildcards.internal_dataset_ID} - Matrix number: {wildcards.n} "
    params:
        scripts_bin             = os.path.join(config["bin"], "motif_comparison"),
        nb_of_matrices_to_match = config['nb_comparison_matches'],
        TF_name                 = lambda wildcards: internal_dataset_dict[wildcards.internal_dataset_ID]["TF"]
    shell:
        """
        bash {params.scripts_bin}/Compare_motifs.sh \
          -q {input.transfac_pfm} \
          -r {input.reference_motifs} \
          -t {params.nb_of_matrices_to_match} \
          -n {params.TF_name} \
          -o {output}
        """

##-----------------------------##
## 4) Matrix regions and sites ##
##-----------------------------##

rule find_RSAT_matrix_sites:
    """
    4.1. Find the TFBSs used to built the matrices.
    """
    input:
        tab_pfm           = INTERNAL_TAB_PFM, \
        map_table         = DISCO_MOTIFS_ID, \
        comparison_output = INTERNAL_PFM_COMPA_MATCHES ## To connect to previous rule
    output:
        PFM_SITES
    message:
        "; Extracting sites for: {wildcards.internal_dataset_ID} - Matrix number: {wildcards.n} "
    params:
        results_dir = os.path.join(INTERNAL_RESULTS_DIR, "peak-motifs", "results"), \
        sites_dir   = os.path.join(INTERNAL_RESULTS_DIR, "matrix_sites")
    shell:
        """
        mkdir -p {params.sites_dir} ;

        MOTIF_FILE="{input.tab_pfm}"
        MOTIF_NB=` echo ${{MOTIF_FILE##*/}} | perl -lne '$_ =~ s/^*_(peak-motifs_m\\d+)/$1/gi; print $1;' `

        MAP_TABLE="{input.map_table}"

        MOTIF_ID=` grep -P "$MOTIF_NB\\s" $MAP_TABLE | cut -f2 | perl -lne '$_ =~ s/_m\\d+//gi; print $_;' `
        MOTIF_ID_NB=` grep -P "$MOTIF_NB\\s" $MAP_TABLE | cut -f2 `

        SITES_FILE=` ls {params.results_dir}/$MOTIF_ID/*.ft `
        cat $SITES_FILE | grep $MOTIF_ID_NB | grep -v '^;' | grep -v '^#' > {output} ;
        """

rule convert_RSAT_matrix_sites_to_BED:
    """
    4.2. Get the genomic coordinates (BED file) of the matrix sites.
    """
    input:
        PFM_SITES
    output:
        PFM_SITES_BED
    message:
        "; Obtaining genomic coordinates from sites for: {wildcards.internal_dataset_ID} - Matrix number: {wildcards.n} "
    params:
        scripts_bin = os.path.join(config["bin"], "matrix_sites_regions")
    shell:
        """
        awk -f {params.scripts_bin}/sites-to-bed.awk \
          {input} \
          | bedtools sort \
          > {output};
        """

rule get_RSAT_matrix_sites_fasta:
    """
    4.3. Get the fasta sequences from genomic coordinates (BED file) of the matrix sites.
    This rule is executed for each discovered motif.
    """
    input:
        PFM_SITES_BED
    output:
        PFM_SITES_FA
    message:
        "; Obtaining fasta sequences from TFBS sites for: {wildcards.internal_dataset_ID} - Matrix number: {wildcards.n}"
    params:
        genome_fasta = lambda wildcards: internal_dataset_dict[wildcards.internal_dataset_ID]["Genome_fasta"]
    shell:
        """
        bedtools getfasta \
          -name \
          -s \
          -fi {params.genome_fasta} \
          -bed {input} \
          -fo {output};
        """

##-----------------------##
## 5) Central enrichment ##
##-----------------------##

rule scan_peaks_with_JASPAR_PWM:
    """
    5.1. Scan the PWM in the extended peaks (501bp).
        This rule is needed for the centrality analysis.
    """
    input:
        jaspar_pwm   = INTERNAL_JASPAR_PWM, \
        peaks_501_bp = EXTENDED_PEAK_501_FA, \
        fasta_sites  = PFM_SITES_FA ## To conncect with the previous rule
    output:
        TFBSs_AROUND_501
    message:
        "; Scanning 501 bp peaks with matrix for: {wildcards.internal_dataset_ID} - Matrix number: {wildcards.n} "
    params:
        scripts_bin = os.path.join(config["bin"], "central_enrichment")
    shell:
        """
        {params.scripts_bin}/pwm_searchPFF \
          {input.jaspar_pwm} \
          {input.peaks_501_bp} \
          0.85 \
          -b \
          > {output};
        """

rule calculate_centrimo_pvalue:
    """
    5.2. Calculate p-value from the central enrichment.
    """
    input:
        predicted_TFBSs = TFBSs_AROUND_501, \
        peaks           = EXTENDED_PEAK_501
    output:
        CENTRIMO_PVALUE
    message:
        "; Calculating central enrichment around peak summits for: {wildcards.internal_dataset_ID} - Matrix number: {wildcards.n} "
    params:
        scripts_bin     = os.path.join(config["bin"], "central_enrichment"),
        centrimo_folder = os.path.join(INTERNAL_RESULTS_DIR, "central_enrichment")
    shell:
        """
        mkdir -p {params.centrimo_folder};

        nb_TFBS="$(wc -l {input.predicted_TFBSs} | cut -d ' ' -f 1)";
        nb_peaks="$(wc -l {input.peaks} | cut -d " " -f 1)";

        {params.scripts_bin}/centrimo_pval \
          {input.predicted_TFBSs} \
          ${{nb_TFBS}} \
          ${{nb_peaks}} \
          250 \
          | awk '{{print -1 * $1}}' \
          > {output};
        """

rule generate_centrimo_plots:
    """
    5.3. Generate centrimo plots (local enrichment).
    """
    input:
        predicted_TFBSs = TFBSs_AROUND_501, \
        pvalue          = CENTRIMO_PVALUE ## To connect with the previous rule
    output:
        CENTRIMO_PLOT
    message:
        "; Generating centrimo plot for: {wildcards.internal_dataset_ID} - Matrix number: {wildcards.n} "
    params:
        scripts_bin = os.path.join(config["bin"], "central_enrichment")
    shell:
        """
        R --vanilla --slave --silent \
          -f {params.scripts_bin}/centrimo_plot.R \
          --args \
          {input.predicted_TFBSs} \
          {output};
        """

##############################################################
## Function that defines new wildcard "n" (motif number),   ##
## so the pipeline uses the checkpoint to run uninterupted. ##
##############################################################

def aggregate_motif_ind(wildcards):

    ## Name of the checkpoint rule
    checkpoint_output = checkpoints.RSAT_PSSM_to_JASPAR_format.get(**wildcards).output[0]

    ## Defining file names
    file_names = expand(CENTRIMO_PLOT,
                        internal_batch_name = wildcards.internal_batch_name,
                        internal_dataset_ID = wildcards.internal_dataset_ID,
                        n = glob_wildcards(os.path.join(checkpoint_output, "{internal_dataset_ID}_peak-motifs_m{n}.jaspar")).n)

    return file_names

##-----------------------------------##
## 5) Central enrichment - continued ##
##-----------------------------------##

rule choose_best_centrimo_experiment:
    """
    5.4. Select motifs with the best (lowest) centrimo p-value.
    """
    input:
        aggregate_motif_ind
    output:
        BEST_CENTRIMO_MOTIFS
    message:
        "; Selecting the most centrally enriched motifs for: {wildcards.internal_dataset_ID} "
    params:
        scripts_bin  = os.path.join(config["bin"], "central_enrichment"),
        centrimo_dir = os.path.join(INTERNAL_RESULTS_DIR, "central_enrichment"),
        nb_of_motifs = config['top_motifs']
    shell:
        """
        bash {params.scripts_bin}/best_centrimo.sh \
          -i {params.centrimo_dir} \
          -m {params.nb_of_motifs} \
          > {output};

        rm {params.centrimo_dir}/*.fa.sites.centrimo ;
        rm {params.centrimo_dir}/*.fa.sites.centrimo.pdf
        """

##----------------##
## 6) Annotations ##
##----------------##
rule get_uniprot_annotations_for_internal_motif:
    """
    6.1. Retrieving UniProt annotations for selected top most central motifs.
        This output will be temporal, since it is later added to the main table.
    """
    input:
        BEST_CENTRIMO_MOTIFS ## To connect with the previous rule
    output:
        temp(INTERNAL_UNIPROT_ANNOTATIONS)
    message:
        "; Retrieving UniProt annotation for: {wildcards.internal_dataset_ID} "
    params:
        scripts_bin               = os.path.join(config["bin"], "retrieve_data"), \
        dset_TF_name              = lambda wildcards: internal_dataset_dict[wildcards.internal_dataset_ID]["TF"], \
        tax_ID                    = lambda wildcards: internal_dataset_dict[wildcards.internal_dataset_ID]["Taxon_ID"], \
        n_hits_suggested_uniprots = config["n_hits_suggested_uniprots"]
    shell:
        """
        Rscript {params.scripts_bin}/get_uniprot_annotations.R \
        -g {params.dset_TF_name} \
        -t {params.tax_ID} \
        -n {params.n_hits_suggested_uniprots} \
        -o {output};
        """

rule annotate_internal_motifs:
    """
    6.2. Create a curation table entry for selected motifs given retrieved information and generated files.
    """
    input:
        selected_motifs    = BEST_CENTRIMO_MOTIFS, \
        suggested_uniprots = INTERNAL_UNIPROT_ANNOTATIONS, \
        previous_jaspar    = config["previous_jaspar_annotations"]
    output:
        temp(BEST_INTERNAL_MOTIF_ANNOT_TMP)
    message:
        "; Annotating and creating curation table entry for: {wildcards.internal_dataset_ID} "
    params:
        scripts_bin   = os.path.join(config["bin"], "annotations"), \
        dataset_dir   = INTERNAL_RESULTS_DIR, \
        dset_TF_name  = lambda wildcards: internal_dataset_dict[wildcards.internal_dataset_ID]["TF"], \
        taxon         = lambda wildcards: internal_dataset_dict[wildcards.internal_dataset_ID]["Taxon"], \
        tax_ID        = lambda wildcards: internal_dataset_dict[wildcards.internal_dataset_ID]["Taxon_ID"], \
        organism_name = lambda wildcards: internal_dataset_dict[wildcards.internal_dataset_ID]["Organism"], \
        data          = lambda wildcards: internal_dataset_dict[wildcards.internal_dataset_ID]["Data"], \
        source        = lambda wildcards: internal_dataset_dict[wildcards.internal_dataset_ID]["Source"]
    shell:
        """
        suggested_uniprots=$(cut -f 5 {input.suggested_uniprots} | grep -v 'Suggested_Uniprot' | uniq | paste -s -d '-') ;

        bash {params.scripts_bin}/annotate_best_centrimo_experiment.sh \
          {input.selected_motifs} \
          {input.previous_jaspar} \
          $suggested_uniprots \
          {params.dataset_dir} \
          {wildcards.internal_dataset_ID} \
          {params.dset_TF_name} \
          {params.organism_name} \
          {params.tax_ID} \
          {params.taxon} \
          {params.data} \
          {params.source} \
          {output} ;
        """

rule add_internal_motif_comparison_to_annotations:
    """
    6.3. Add matrix comparison results to the annotations (an entry in the curation table).
    """
    input:
        BEST_INTERNAL_MOTIF_ANNOT_TMP
    output:
        BEST_INTERNAL_MOTIF_ANNOT
    message:
        "; Adding matrix comparison to the curation table entry for: {wildcards.internal_dataset_ID} "
    params:
        scripts_bin          = os.path.join(config["bin"], "annotations"), \
        motif_comparison_dir = os.path.join(INTERNAL_RESULTS_DIR, "motifs", "comparison")
    shell:
        """
        Rscript {params.scripts_bin}/add_comparison_results_to_annotation_table.R \
          -d {input} \
          -r {params.motif_comparison_dir} \
          -o {output} ;
        """

rule best_centrimo_experiment_logo:
    """
    6.4. Export a PDF file with the motif logo, experiment, TF name and centrality p-value for the n top best discovered motifs.
    """
    input:
        BEST_INTERNAL_MOTIF_ANNOT
    output:
        BEST_INTERNAL_MOTIF_ANNOT_TEX, \
        BEST_INTERNAL_MOTIF_ANNOT_PDF
    message:
        "; Generating TEX and PDF logos for selected internal motifs for: {wildcards.internal_dataset_ID} "
    params:
        scripts_bin = os.path.join(config["bin"], "annotations")
    shell:
        """
        bash {params.scripts_bin}/create_latex_logos.sh \
          -l {params.scripts_bin}/latex_header.txt \
          -i {input} \
          -o {output};
        """

##---------------------------------------------------##
## 7) Combining all annotations into curation table ##
##---------------------------------------------------##

rule collect_individual_internal_motifs_annotations:
    """
    7.1. Concatenate the individual curations entried of the top most central motifs.
    """
    input:
        curation_tab_entry = expand(BEST_INTERNAL_MOTIF_ANNOT,     zip, internal_dataset_ID = internal_dataset_IDs, internal_batch_name = INTERNAL_BATCH_NAME), \
        motifs_in_tex      = expand(BEST_INTERNAL_MOTIF_ANNOT_TEX, zip, internal_dataset_ID = internal_dataset_IDs, internal_batch_name = INTERNAL_BATCH_NAME), \
        motifs_in_pdf      = expand(BEST_INTERNAL_MOTIF_ANNOT_PDF, zip, internal_dataset_ID = internal_dataset_IDs, internal_batch_name = INTERNAL_BATCH_NAME)
    output:
        INTERNAL_CURATION_TABLE
    message:
        "; Concatenating the tables with the annotated experiments."
    params:
        out_dir = os.path.join(OUT_DIR, "internal")
    shell:
        """
        ls {params.out_dir}/{wildcards.internal_batch_name}/*/annotations/*_curation_tab_entry.txt \
        | xargs cat \
        > {output};
        """

##-----------------------------##
## 8. Filtering curation table ##
##-----------------------------##

rule select_original_motifs_to_curate:
    """
    8.1. Select those motifs satisfying the centrality p-value threshold. These motifs should be used for curation.
    """
    input:
        INTERNAL_CURATION_TABLE
    output:
        INTERNAL_CURATION_TABLE_FILTERED
    message:
        "; Selecting motifs that pass the set centrality threshold - Centrality p-value: {{config['central_pvalue']}} "
    params:
        central_pval = config["central_pvalue"]
    shell:
        """
        cat {input} \
        | awk -F"\\t" '$20 >= {params.central_pval} {{ print }}' \
        | uniq \
        | sed -e '1i\curation_id\\tPWM\\tcurrent_BASE_ID\\tcurrent_VERSION\\tTF_NAME\\tUniProt\\tTAX_ID\\tclass\\tfamily\\tTFBS_shape_ID\\tData\\tSource\\tValidation\\tComment\\tDecision\\tBED\\tFASTA\\tlogo\\tlogo_rc\\tCent_pval\\tCent_plot\\tcuration_comment\\tDataset_TF_NAME\\tSuggested_MATRIX_ID\\tSuggested_TF_NAME\\tSuggested_UniProt_prev_jaspar\\tSuggested_class\\tSuggested_family\\tSuggested_UniProt_mygene\\tpdf_path\\tmatrix_comparison' > {output};
        """

rule convert_original_motifs_curation_table_to_PDF:
    """
    8.2. All logos in tex, pdf format and centrimo plots are collected and combined into a single pdf.
        This document is useful to visualize all motifs and is nice to have during curation.
    """
    input:
        INTERNAL_CURATION_TABLE_FILTERED
    output:
        INTERNAL_CURATION_TABLE_FILTERED_PDF
    message:
        "; Collecting all PDFs with motif logos into a single file."
    shell:
        """
        n_lines=$(wc -l {input} | awk '{{ print $1 }}') ;

        if [ $n_lines == 0 ]
        then
            echo '; No significant motifs to curate.'
            touch {output} ;
        else
            PDF_FILES=` awk -F"\\t" 'FNR>=2 {{ print $30 }}' {input} | uniq | xargs `
            pdfunite $PDF_FILES {output};
        fi ;
        """

##---------------------------------##
## 9. Selected motifs for curation ##
##---------------------------------##

rule collect_original_motif_rename_jaspar_motif_header:
    """
    9.1. Write the correct name to the jaspar motifs.
    The motifs resulting from peak-motifs have a header that looks like this:
    >peak-motifs_m3 peak-motifs_m3
    Change this header for an informative one
    >CTCF CTCF

    # 0 base
    Column 1  = Path to jaspar file
    Column 22 = Dataset TF name
    """
    input:
        curation_tab = INTERNAL_CURATION_TABLE_FILTERED, \
        curation_pdf = INTERNAL_CURATION_TABLE_FILTERED_PDF ## To connect with previous rule
    output:
        INTERNAL_JASPAR_MOTIF_CONCAT
    message:
        "; Collecting internal motifs into a single file."
    shell:
         """
        n_lines=$(wc -l {input.curation_tab} | awk '{{ print $1 }}') ;

        if [ $n_lines == 0 ]
        then
            echo '; No significant motifs to curate.'
            echo 'No significant motifs to curate.' > {output} ;
        else
            grep -v Dataset_TF_NAME {input.curation_tab} | \
            perl -lne '@sl = split(/\\t/, $_); \
                $convert_cmd = " convert-matrix -i ".$sl[1]. " -from jaspar -to jaspar -attr id ".$sl[22]."_".$sl[0]." -attr name ".$sl[22]."_".$sl[0]." -return counts > ".$sl[1]."tmp" ; \
                $rename_file = "mv ".$sl[1]."tmp ".$sl[1] ; \
                system($convert_cmd); \
                system($rename_file); ' ;

            grep -v Dataset_TF_NAME {input.curation_tab} | cut -f2 | xargs cat  > {output}
        fi ;
        """

##----------------------##
## 10. Motif clustering ##
##----------------------##

rule cluster_selected_internal_motifs_with_CORE:
    """
    10.1. Cluster selected motifs with CORE JASPAR collection.
    """
    input:
        curation_tab     = INTERNAL_CURATION_TABLE_FILTERED, \
        reference_motifs = lambda wildcards: config["CORE_motif_collections"][internal_batch_name_to_taxon_dict[wildcards.internal_batch_name]], \
        motifs_jaspar    = INTERNAL_JASPAR_MOTIF_CONCAT ## To connect with the previous rule
    output:
        INTERNAL_MOTIF_CLUSTERING_CORE
    message:
        "; Clustering discovered motifs with JASPAR CORE."
    params:
        v               = "2", \
        bin             = os.path.join(config["bin"], "clustering"), \
        RSAT            = config["RSAT"], \
        prefix          = os.path.join(INTERNAL_CURATION_DIR, "motif_clustering_with_CORE", "matrix-clustering_{internal_batch_name}"), \
        collection_name = "{internal_batch_name}", \
        output_dir      = os.path.join(INTERNAL_CURATION_DIR, "motif_clustering_with_CORE")
    shell:
        """
        echo "; Building clustering tables for subsets of motifs."
        Rscript {params.bin}/make_clustering_table.R \
        -m {input.curation_tab} \
        -o {params.output_dir} \
        -t TRUE \
        -e jaspar \
        -s 150 \
        -c {params.collection_name} \
        -j {input.reference_motifs} ;

        echo "; Clustering motifs in batches."
        for file in {params.output_dir}/clustering_tables/discovered_motifs_*.tab ;
        do
            filename=$(basename -- "$file") ;
            dataset_name="${{filename%.tab}}" ;
            echo "; Running clustering on $dataset_name" ;
            subset_output_dir={params.output_dir}/"$dataset_name" ;
            mkdir -p $subset_output_dir ;

            portable_tree="$subset_output_dir"/"$dataset_name"_portable_logo_tree.html

            if test -f "$portable_tree"; then
                echo "$portable_tree already exists. Skipping clustering."
            else
                matrix-clustering  \
                -v {params.v} \
                -matrix_file_table $file \
                -hclust_method average \
                -calc sum \
                -title "Selected motifs vs JASPAR 2022 CORE" \
                -label_motif name -metric_build_tree Ncor \
                -lth w 5 -lth cor 0.6 -lth Ncor 0.4 \
                -label_in_tree name -quick -return json \
                -o $subset_output_dir/"$dataset_name" ;
            fi ;

            rm $subset_output_dir/"$dataset_name"_aligned_logos/*.tf ;
            mv $subset_output_dir/"$dataset_name"_aligned_logos/ {params.output_dir} ;

            cp -r $subset_output_dir/js {params.output_dir} ;
            mv "$portable_tree" {params.output_dir} ;

            rm -r "$subset_output_dir" ;

        done ;

        rm -r {params.output_dir}/clustering_tables ;

        echo "Motif clustering with CORE DONE." > {output} ;
        """

rule cluster_selected_internal_motifs_with_UNVALIDATED:
    """
    10.2. Cluster selected motifs with UNVALIDATED JASPAR collection.
    """
    input:
        curation_tab     = INTERNAL_CURATION_TABLE_FILTERED, \
        reference_motifs = lambda wildcards: config["UNVALIDATED_motif_collections"][internal_batch_name_to_taxon_dict[wildcards.internal_batch_name]], \
        motifs_jaspar    = INTERNAL_JASPAR_MOTIF_CONCAT ## To connect with the previous rule
    output:
        INTERNAL_MOTIF_CLUSTERING_UNVALIDATED
    message:
        "; Clustering discovered motifs with JASPAR UNVALIDATED."
    params:
        v               = "2", \
        bin             = os.path.join(config["bin"], "clustering"), \
        RSAT            = config["RSAT"], \
        prefix          = os.path.join(INTERNAL_CURATION_DIR, "motif_clustering_with_UNVALIDATED", "matrix-clustering_{internal_batch_name}"), \
        collection_name = "{internal_batch_name}", \
        output_dir      = os.path.join(INTERNAL_CURATION_DIR, "motif_clustering_with_UNVALIDATED")
    shell:
        """
        echo "; Building clustering tables for subsets of motifs."
        Rscript {params.bin}/make_clustering_table.R \
        -m {input.curation_tab} \
        -o {params.output_dir} \
        -t TRUE \
        -e jaspar \
        -s 500 \
        -c {params.collection_name} \
        -j {input.reference_motifs} ;

        echo "; Clustering motifs in batches."
        for file in {params.output_dir}/clustering_tables/discovered_motifs_*.tab ;
        do
            filename=$(basename -- "$file") ;
            dataset_name="${{filename%.tab}}" ;
            echo "; Running clustering on $dataset_name" ;
            subset_output_dir={params.output_dir}/"$dataset_name" ;
            mkdir -p $subset_output_dir ;

            portable_tree="$subset_output_dir"/"$dataset_name"_portable_logo_tree.html

            if test -f "$portable_tree"; then
                echo "$portable_tree already exists. Skipping clustering."
            else
                matrix-clustering  \
                -v {params.v} \
                -matrix_file_table $file \
                -hclust_method average \
                -calc sum \
                -title "Selected motifs vs JASPAR 2022 UNVALIDATED" \
                -label_motif name -metric_build_tree Ncor \
                -lth w 5 -lth cor 0.6 -lth Ncor 0.4 \
                -label_in_tree name -quick -return json \
                -o $subset_output_dir/"$dataset_name" ;
            fi ;

            rm $subset_output_dir/"$dataset_name"_aligned_logos/*.tf ;
            mv $subset_output_dir/"$dataset_name"_aligned_logos/ {params.output_dir} ;

            cp -r $subset_output_dir/js {params.output_dir} ;
            mv "$portable_tree" {params.output_dir} ;

            rm -r "$subset_output_dir" ;

        done ;

        rm -r {params.output_dir}/clustering_tables ;

        echo "Motif clustering with UNVALIDATED DONE." > {output} ;
        """

##-------------------------------##
## 11. Exporting as google sheet ##
##-------------------------------##

rule export_internal_motifs_curation_table_to_googlesheets:
    """
    11.1. Automatically upload the curation table in a google document
    ## Rule added: 17-01-2021 by Jaime Castro-Mondragon
    ## NOTE: -o A
    ## -o is the option to select the output, the option 'A' stands for 'Add'. This means, given a existing google sheet URL, the script will create a new tab
    ## Other options are -o N (to create a New sheet) and -o O (to overwrite an existing ta within an existing sheet).
    """
    input:
        INTERNAL_CURATION_TABLE_FILTERED
    output:
        INTERNAL_GOOGLE_SHEETS_LOG
    message:
        "; Exporting curation table to a google sheet "
    params:
        gs_url          = config['gs_url'], ## Google sheet url
        json_key        = config['google_token'], \
        selected_motifs = INTERNAL_CURATION_TABLE_FILTERED, \
        date            = todays_date
    shell:
        """
        n_lines=$(wc -l {params.selected_motifs} | awk '{{ print $1 }}') ;

        if [ $n_lines == 0 ]
        then
            echo '; No significant motifs to curate.'
            echo "No significant motifs to curate." > {output} ;
        else
            Rscript bin/export_tables/Upload_table_to_googlesheet.R \
            -t {input} \
            -o A \
            -u {params.gs_url} \
            -n {wildcards.internal_batch_name}_{params.date} \
            -j {params.json_key} ;

            echo "Exported tables to google sheets." > {output}
        fi ;
        """

# CENTRIMO_ALL                         = os.path.join(INTERNAL_RESULTS_DIR, "central_enrichment", "selected_motif", "{internal_dataset_ID}.501bp.fa.sites.centrimo.all")
# CENTRIMO_ALL_ANNOT                   = os.path.join(INTERNAL_RESULTS_DIR, "central_enrichment", "selected_motif", "{internal_dataset_ID}.501bp.fa.sites.centrimo.all_annot")
# CENTRIMO_TOP_X_ANNOT                 = os.path.join(INTERNAL_RESULTS_DIR, "central_enrichment", "selected_motif", "{internal_dataset_ID}.501bp.fa.sites.centrimo.best_annot")
# CENTRIMO_ALL_LOGOS_TEX               = os.path.join(INTERNAL_RESULTS_DIR, "central_enrichment", "selected_motif", "{internal_dataset_ID}.501bp.fa.sites.centrimo.all_annot.tex")
# CENTRIMO_ALL_LOGOS_PDF               = os.path.join(INTERNAL_RESULTS_DIR, "central_enrichment", "selected_motif", "{internal_dataset_ID}.501bp.fa.sites.centrimo.all_annot.pdf")
# CENTRIMO_TOP_X_LOGOS_TEX             = os.path.join(INTERNAL_RESULTS_DIR, "central_enrichment", "selected_motif", "{internal_dataset_ID}.501bp.fa.sites.centrimo.best_annot.tex")
# CENTRIMO_TOP_X_LOGOS_PDF             = os.path.join(INTERNAL_RESULTS_DIR, "central_enrichment", "selected_motif", "{internal_dataset_ID}.501bp.fa.sites.centrimo.best_annot.pdf")
# INTERNAL_CURATION_TABLE_ALL                   = os.path.join(INTERNAL_CURATION_DIR, "Annotated_experiments_cat_all.tab")
# INTERNAL_CURATION_TABLE_ALL_PDF               = os.path.join(INTERNAL_CURATION_DIR, "Annotated_experiments_cat_all.pdf")

# rule get_centrimo_for_all_motifs:
#     """
#     5.7. Concatenate the centrimo p-value for all discovered motifs in a dataset. This will be used to generate a PDF with all non-trimmed discovered motifs in a run.
#     This rule is executed for each dataset (experiment).
#     """
#     input:
#         aggregate_motif_ind
#     output:
#         CENTRIMO_ALL
#     params:
#         centrimo_dir = os.path.join(INTERNAL_RESULTS_DIR, "central_enrichment")
#     shell:
#         """
#         for centri_file in {params.centrimo_dir}/*.centrimo; do
#           echo -e -n "$centri_file\t";
#           cat $centri_file;
#         done > {output}
#         """
# rule convert_curation_table_all_to_PDF:
#     """
#     7.2. All logos in tex, pdf format with centrality plots are collected and combined into a single pdf.
#         This document is useful to visualize all motifs and is nice to have during curation.
#     """
#     input:
#         CURATION_TABLE_ALL
#     output:
#         CURATION_TABLE_ALL_PDF
#     message:
#         "; Concatenating PDFs with the motifs to curate "
#     shell:
#         """
#         n_lines=$(wc -l {input} | awk '{{ print $1 }}') ;

#         if [ $n_lines == 0 ]
#         then
#             echo '; No significant motifs to curate.'
#             touch {output} ;
#         else
#             PDF_FILES=` awk -F"\\t" 'FNR>=2 {{ print $30 }}' {input} | uniq | xargs `
#             pdfunite $PDF_FILES {output};
#         fi ;
#         """


# rule collect_individual_internal_motifs_annotations:
#     """
#     7.1. Concatenate the tables with the annotated experiments and the centrality p-value for all discovered motifs.
#     """
#     input:
#         expand(CENTRIMO_ALL_ANNOT,     zip, dataset_ID = internal_dataset_IDs, batch_name = INTERNAL_BATCH_NAME), \
#         expand(CENTRIMO_ALL_LOGOS_TEX, zip, dataset_ID = internal_dataset_IDs, batch_name = INTERNAL_BATCH_NAME), \
#         expand(CENTRIMO_ALL_LOGOS_PDF, zip, dataset_ID = internal_dataset_IDs, batch_name = INTERNAL_BATCH_NAME)
#     output:
#         CURATION_TABLE_ALL
#     message:
#         "; Concatenating the tables with the annotated experiments and the centrality p-value "
#     params:
#         out_dir = os.path.join(OUT_DIR, "internal")
#     shell:
#         """
#         ls {params.out_dir}/{wildcards.internal_batch_name}/*/central_enrichment/selected_motif/*.501bp.fa.sites.centrimo.all_annot \
#         | xargs cat \
#         > {output};
#         """

