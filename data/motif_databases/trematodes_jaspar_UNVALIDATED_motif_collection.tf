AC  Smp_097750_UN0104.1
XX
ID  Smp_097750_UN0104.1
XX
DE  UN0104.1 Smp_097750; from JASPAR
P0       a     c     g     t
1        9     9     9   971
2      971     9     9     9
3        9     9   971     9
4      971     9     9     9
5        9     9     9   971
6      971     9     9     9
7        9     9   971     9
8      971     9     9     9
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
