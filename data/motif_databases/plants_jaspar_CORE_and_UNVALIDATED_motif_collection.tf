AC  AGL3_MA0001.2
XX
ID  AGL3_MA0001.2
XX
DE  MA0001.2 AGL3; from JASPAR
P0       a     c     g     t
1       22    29    34    10
2       16     9    29    41
3       25     8     4    58
4       27    16    13    39
5        0    92     0     3
6        0    79     0    16
7       82     1     2    10
8       40     4     3    48
9       56     0     1    38
10      35     0     0    60
11      65     1     4    25
12      25     4     3    63
13      64     0    28     3
14       0     0    92     3
15      33    14    15    33
16      52     5     7    31
17      45    23    13    14
18      21    24    26    24
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AG_MA0005.2
XX
ID  AG_MA0005.2
XX
DE  MA0005.2 AG; from JASPAR
P0       a     c     g     t
1       21    20     6    19
2        9     3     3    51
3       10     0     1    55
4       29     8     8    21
5        0    66     0     0
6        0    65     0     1
7       31     3     6    26
8       47     2     0    17
9       52     0     1    13
10      25     0     1    40
11      17    15    11    23
12      19     8    20    19
13       7     0    57     2
14       2     0    54    10
15      22    17     5    22
16      45     4     5    12
17      40     6     9    11
18      15    10    16    25
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HAT5_MA0008.3
XX
ID  HAT5_MA0008.3
XX
DE  MA0008.3 HAT5; from JASPAR
P0       a     c     g     t
1       72    32    29    58
2       80    27    18    66
3       50    31    20    90
4       16   148     1    26
5      190     1     0     0
6      190     0     0     1
7        1     0     0   190
8      134     2     2    53
9      189     0     0     2
10       0     0     0   191
11       6     0     4   181
12      13     5   168     5
13      95    20    30    46
14      75    15    29    72
15      62    24    34    71
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Dof2_MA0020.1
XX
ID  Dof2_MA0020.1
XX
DE  MA0020.1 Dof2; from JASPAR
P0       a     c     g     t
1       21     0     0     0
2       21     0     0     0
3       21     0     0     0
4        0     0    21     0
5        3    14     2     2
6        7     6     3     5
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Dof3_MA0021.1
XX
ID  Dof3_MA0021.1
XX
DE  MA0021.1 Dof3; from JASPAR
P0       a     c     g     t
1       21     0     0     0
2       21     0     0     0
3       21     0     0     0
4        0     0    21     0
5        0    10     3     8
6        6     6     9     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Gam1_MA0034.1
XX
ID  Gam1_MA0034.1
XX
DE  MA0034.1 Gam1; from JASPAR
P0       a     c     g     t
1        4     6    11     4
2       10     5     7     3
3        3    13     0     9
4       23     1     1     0
5       25     0     0     0
6        1    24     0     0
7        3    14     6     2
8        6     0    19     0
9       10    11     0     4
10       5    19     1     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MNB1A_MA0053.1
XX
ID  MNB1A_MA0053.1
XX
DE  MA0053.1 MNB1A; from JASPAR
P0       a     c     g     t
1       15     0     0     0
2       15     0     0     0
3       15     0     0     0
4        0     0    15     0
5        3     9     0     3
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  myb.Ph3_MA0054.1
XX
ID  myb.Ph3_MA0054.1
XX
DE  MA0054.1 myb.Ph3; from JASPAR
P0       a     c     g     t
1       19     3     2    46
2       64     1     2     3
3       63     0     2     5
4        4    62     3     1
5       10    27    16    17
6       10     2    53     5
7       13     8     0    49
8        3    17     1    49
9       28     1     0    41
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PBF_MA0064.1
XX
ID  PBF_MA0064.1
XX
DE  MA0064.1 PBF; from JASPAR
P0       a     c     g     t
1       16     0     0     0
2       16     0     0     0
3       16     0     0     0
4        0     0    16     0
5        1     9     1     5
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  squamosa_MA0082.1
XX
ID  squamosa_MA0082.1
XX
DE  MA0082.1 squamosa; from JASPAR
P0       a     c     g     t
1       11    14     1     4
2        0    22     0     8
3       24     0     4     2
4       16     2     1    11
5       23     0     0     7
6       17     0     0    13
7       24     1     0     5
8        8     0     1    21
9       14     1    15     0
10       1     1    28     0
11      14     5     1    10
12      25     5     0     0
13      21     2     3     4
14       7     5     8    10
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  bZIP910_MA0096.1
XX
ID  bZIP910_MA0096.1
XX
DE  MA0096.1 bZIP910; from JASPAR
P0       a     c     g     t
1       15    15     5     0
2        0     0     0    35
3        0     0    35     0
4       35     0     0     0
5        0    35     0     0
6        0     0    35     0
7        0     0     0    35
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  bZIP911_MA0097.1
XX
ID  bZIP911_MA0097.1
XX
DE  MA0097.1 bZIP911; from JASPAR
P0       a     c     g     t
1        1     0    31     1
2       17     0    16     0
3        0     0     0    33
4        0     0    33     0
5       33     0     0     0
6        0    33     0     0
7        0     0    33     0
8        0     0     0    33
9        0     1    32     0
10       1     0    22    10
11      11    20     1     1
12       1    32     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ATHB-5_MA0110.3
XX
ID  ATHB-5_MA0110.3
XX
DE  MA0110.3 ATHB-5; from JASPAR
P0       a     c     g     t
1     3639  1914  1346  3223
2     2718  2228  1336  3840
3     1051  7517   386  1168
4     9632   197    74   219
5     9563   154    50   355
6      230    98    59  9735
7      841  8049   254   978
8     9809    48    51   214
9      648   178   175  9121
10     633   312   394  8783
11    3099  1661  2309  3053
12    3606  1561  1609  3346
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARR10_MA0121.1
XX
ID  ARR10_MA0121.1
XX
DE  MA0121.1 ARR10; from JASPAR
P0       a     c     g     t
1       14     1     0     0
2        0     0    15     0
3       14     0     1     0
4        0     0     0    15
5        4     6     0     5
6        1     8     0     6
7        0     0     9     6
8        3     6     4     2
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  abi4_MA0123.1
XX
ID  abi4_MA0123.1
XX
DE  MA0123.1 abi4; from JASPAR
P0       a     c     g     t
1        0    49     0     0
2       12     0    37     0
3        0    20    29     0
4        0    23     1    25
5        1     3    45     0
6        0    45     4     0
7        5    28     6    10
8        3    25    11    10
9        3    31     5    10
10       4    26     7    12
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PEND_MA0127.1
XX
ID  PEND_MA0127.1
XX
DE  MA0127.1 PEND; from JASPAR
P0       a     c     g     t
1       30     2     8     2
2        6    19     7    10
3        1     0     2    39
4        4     0     4    34
5        3    33     2     4
6        0     2     2    38
7        2     0     1    39
8       32     7     2     1
9        4     3     0    35
10       2     5    14    21
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  EmBP-1_MA0128.1
XX
ID  EmBP-1_MA0128.1
XX
DE  MA0128.1 EmBP-1; from JASPAR
P0       a     c     g     t
1        9     0     0     4
2        1     7     4     1
3       13     0     0     0
4        0    12     0     1
5        0     0    13     0
6        0     0     0    13
7        0     1    12     0
8        0     1    12     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TGA1A_MA0129.1
XX
ID  TGA1A_MA0129.1
XX
DE  MA0129.1 TGA1A; from JASPAR
P0       a     c     g     t
1        4     4     0     7
2       13     0     1     1
3        0    14     0     1
4        2     0    13     0
5        1     0     0    14
6        0    15     0     0
7       13     0     1     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AGL15_MA0548.2
XX
ID  AGL15_MA0548.2
XX
DE  MA0548.2 AGL15; from JASPAR
P0       a     c     g     t
1      133   218   137   111
2       48    33     5   513
3       16     5     3   575
4       84     8    21   486
5        0   596     0     3
6        0   435     0   164
7      200    80    50   269
8       88    85    53   373
9      162     0     4   433
10       7     8     0   584
11      89    45    31   434
12     138    42   168   251
13     125     1   473     0
14       0     0   585    14
15     325    50    28   196
16     543    13     5    38
17     475     7    29    88
18     113   175   185   126
19     157    77    50   315
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZR2_MA0549.1
XX
ID  BZR2_MA0549.1
XX
DE  MA0549.1 BZR2; from JASPAR
P0       a     c     g     t
1       23    45    10    21
2       41     8    50     0
3        0    99     0     0
4       89     0     4     6
5        0    99     0     0
6        0     0    99     0
7        0     0     0    99
8        3     0    96     0
9       16     5    36    42
10      17    33    46     3
11      36    29    33     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZR1_MA0550.2
XX
ID  BZR1_MA0550.2
XX
DE  MA0550.2 BZR1; from JASPAR
P0       a     c     g     t
1       95   260   131   102
2        4   519     4    61
3      351    34   203     0
4        0   575     0    13
5      573     1     4    10
6        0   588     0     0
7        0     0   588     0
8        5     0     0   583
9        0     0   588     0
10      11   277    39   261
11     213    47   280    48
12     198   148    91   151
13     165   169   114   140
14     106   152    78   252
15     111   171   133   173
16     166   144    73   205
17     141   158    98   191
18     150   131   121   186
19     164   152    85   187
20      96   136   126   230
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HY5_MA0551.1
XX
ID  HY5_MA0551.1
XX
DE  MA0551.1 HY5; from JASPAR
P0       a     c     g     t
1      103    60    68    89
2      108    56    62    94
3       52    55    30   183
4        9     8   279    24
5      150   165     1     4
6        0   317     1     2
7      310     1     9     0
8        3   311     5     1
9        1     5   311     3
10       0     9     1   310
11       2     1   317     0
12       4     1   165   150
13      24   279     8     9
14     183    30    55    52
15      94    62    56   108
16      89    68    60   103
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PIF1_MA0552.1
XX
ID  PIF1_MA0552.1
XX
DE  MA0552.1 PIF1; from JASPAR
P0       a     c     g     t
1       37    12    37    28
2       25     0    53    36
3       35    28    29    22
4       53     8    40    13
5       27    27    32    28
6        9    17    73    15
7       38    14    14    48
8        0   103    11     0
9      114     0     0     0
10       0   113     1     0
11       0     0   114     0
12       0     2     0   112
13       0     0   114     0
14      22    23    51    18
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SMZ_MA0553.1
XX
ID  SMZ_MA0553.1
XX
DE  MA0553.1 SMZ; from JASPAR
P0       a     c     g     t
1        0   105     0    42
2        0   119    28     0
3        0     0     0   147
4        0   147     0     0
5        0     0   147     0
6        0     0    25   122
7      135    12     0     0
8        0   147     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SOC1_MA0554.1
XX
ID  SOC1_MA0554.1
XX
DE  MA0554.1 SOC1; from JASPAR
P0       a     c     g     t
1      288   123    99   378
2      172   264   113   339
3       65    27    34   762
4       62     0    51   775
5      117    47    89   635
6       20   855    13     0
7        0   617     9   262
8      459    37    56   336
9      137    78     0   673
10     130     6     0   752
11      22     8     0   858
12      98    24     0   766
13      90    71    86   641
14     181    10   683    14
15      17    23   665   183
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SVP_MA0555.1
XX
ID  SVP_MA0555.1
XX
DE  MA0555.1 SVP; from JASPAR
P0       a     c     g     t
1       28    24    21    19
2       28    11     8    45
3       28     1    10    53
4       33    16    11    32
5        7    85     0     0
6        3    79     1     9
7       58    16     7    11
8       67     5     8    12
9       86     0     0     6
10      77     1     2    12
11      78     8     3     3
12      34    10    14    34
13      16     0    74     2
14       0     0    92     0
15      71    10     0    11
16      84     2     2     4
17      77     7     2     6
18      34     8    39    11
19      50    16     5    21
20      38    21     3    30
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AP3_MA0556.1
XX
ID  AP3_MA0556.1
XX
DE  MA0556.1 AP3; from JASPAR
P0       a     c     g     t
1       92    36    20   143
2       84    33    39   135
3       48   241     0     2
4        0   232     1    58
5      185    61    11    34
6      217    22    11    41
7      283     0     0     8
8      240     1     0    50
9      156    10   104    21
10      95     0    30   166
11      98     2   191     0
12       7     0   284     0
13     183    56     5    47
14     231    18     9    33
15     243     9    19    20
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  FHY3_MA0557.1
XX
ID  FHY3_MA0557.1
XX
DE  MA0557.1 FHY3; from JASPAR
P0       a     c     g     t
1       46    81    40    68
2       75    58    25    77
3        9   210     9     7
4      226     3     2     4
5        2   230     1     2
6        3    16   213     3
7        0   220     0    15
8        4     0   228     3
9       18   208     6     3
10      16    40    17   162
11      63    68    40    64
12      82    44    50    59
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  FLC_MA0558.1
XX
ID  FLC_MA0558.1
XX
DE  MA0558.1 FLC; from JASPAR
P0       a     c     g     t
1      121    44    45    65
2       90    61    63    61
3       95    37    21   122
4       82    26    36   131
5       78    25    66   106
6       54   193    12    16
7        9   185     4    77
8      135    70    37    33
9      198    10    38    29
10     261     0     3    11
11     233     3     3    36
12     193     3    18    61
13      78    11    15   171
14     141     0   134     0
15       0     0   275     0
16     217    21     6    31
17     268     3     4     0
18     257     1    15     2
19      62    43   148    22
20     109    47    17   102
21     103    50    29    93
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PI_MA0559.1
XX
ID  PI_MA0559.1
XX
DE  MA0559.1 PI; from JASPAR
P0       a     c     g     t
1      156   335    48    19
2       57   394    44    63
3      357    79    67    55
4      448    12    83    15
5      551     1     4     2
6      498     0    12    48
7      301     8   244     5
8      312    14    69   163
9      231     0   327     0
10       7     6   545     0
11     446    54     7    51
12     495     7    24    32
13     456    26    60    16
14     256    58   199    45
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PIF3_MA0560.1
XX
ID  PIF3_MA0560.1
XX
DE  MA0560.1 PIF3; from JASPAR
P0       a     c     g     t
1      210    26   148   143
2       58   131   222   116
3       19   399    94    15
4        0   527     0     0
5      527     0     0     0
6        0   409     0   118
7        0     0   527     0
8        0     0     0   527
9        0     0   527     0
10     259    33   137    98
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PIF4_MA0561.1
XX
ID  PIF4_MA0561.1
XX
DE  MA0561.1 PIF4; from JASPAR
P0       a     c     g     t
1        0   335     0     0
2      335     0     0     0
3        0   335     0     0
4       49     0   286     0
5        0     0     0   335
6        0     0   335     0
7        0    99   183    53
8       64   206    65     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PIF5_MA0562.1
XX
ID  PIF5_MA0562.1
XX
DE  MA0562.1 PIF5; from JASPAR
P0       a     c     g     t
1        0    78     0   208
2        0   286     0     0
3      286     0     0     0
4        0   286     0     0
5       91     0   195     0
6        0     0     0   286
7        0     0   286     0
8        0    95   161    30
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SEP3_MA0563.1
XX
ID  SEP3_MA0563.1
XX
DE  MA0563.1 SEP3; from JASPAR
P0       a     c     g     t
1       24    37    29    60
2       19   111     5    15
3        1    97     1    51
4       77     0     0    73
5       12    28     5   105
6       19     0     5   126
7        0     0     0   150
8        9     0    24   117
9       11     3    22   114
10       9     0   138     3
11       0     7   114    29
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ABI3_MA0564.1
XX
ID  ABI3_MA0564.1
XX
DE  MA0564.1 ABI3; from JASPAR
P0       a     c     g     t
1        6    60     9    25
2       15    16    15    53
3        3     7    85     5
4        1    96     2     1
5       97     1     1     1
6        1     1     1    97
7        1     1    96     1
8        6    84     8     2
9       50    15    13    22
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  FUS3_MA0565.2
XX
ID  FUS3_MA0565.2
XX
DE  MA0565.2 FUS3; from JASPAR
P0       a     c     g     t
1      469   152   218   307
2      355   309   143   339
3     1146     0     0     0
4        0  1146     0     0
5     1141     2     2     1
6        3     5     1  1137
7        0     1  1145     0
8        0  1146     0     0
9     1055    18    28    45
10     446   125    98   477
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYC2_MA0566.1
XX
ID  MYC2_MA0566.1
XX
DE  MA0566.1 MYC2; from JASPAR
P0       a     c     g     t
1       25    10    60     5
2        9    91     0     0
3       97     0     2     1
4        0    85     0    15
5       15     0    85     0
6        1     2     0    97
7        0     0    91     9
8        5    60    10    25
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF1B_MA0567.1
XX
ID  ERF1B_MA0567.1
XX
DE  MA0567.1 ERF1B; from JASPAR
P0       a     c     g     t
1       42    54     3     1
2        1     1    98     1
3        0    98     1     1
4        4    96     0     0
5        2     0    97     1
6        2    92     2     4
7        1    98     1     1
8       58     5    24    12
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYC3_MA0568.1
XX
ID  MYC3_MA0568.1
XX
DE  MA0568.1 MYC3; from JASPAR
P0       a     c     g     t
1       25    28    40     7
2       10    89     0     0
3       97     0     2     1
4        0    84     0    16
5       16     0    84     0
6        1     2     0    97
7        0     0    89    10
8        7    40    28    25
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYC4_MA0569.1
XX
ID  MYC4_MA0569.1
XX
DE  MA0569.1 MYC4; from JASPAR
P0       a     c     g     t
1       38    18    36     8
2        5    95     0     0
3       99     0     0     1
4        0    94     0     6
5        2     0    98     0
6        0     1     0    99
7        0     0    98     2
8        2    72     8    17
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ABF1_MA0570.2
XX
ID  ABF1_MA0570.2
XX
DE  MA0570.2 ABF1; from JASPAR
P0       a     c     g     t
1      593   284    60   204
2       83   793   124   141
3     1123     4    11     3
4       10  1099    15    17
5       16     9  1107     9
6        4     8     1  1128
7        9     3  1123     6
8       39    11   707   384
9       81   999    28    33
10     909    36   117    79
11     348   195   234   364
12     358   213   225   345
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANT_MA0571.1
XX
ID  ANT_MA0571.1
XX
DE  MA0571.1 ANT; from JASPAR
P0       a     c     g     t
1        6     4    12    12
2        5     7    13     9
3        8     0    24     2
4        0    32     0     2
5       31     0     2     1
6        0    34     0     0
7       21     0    13     0
8        8     6    15     5
9       15     1     1    17
10       0     3     0    31
11       4    25     0     5
12       0    34     0     0
13       0    33     0     1
14       9    11    13     1
15      34     0     0     0
16       7     1    16    10
17       3     1    29     1
18       2    10     3    19
19       8     6    14     6
20      12    10     4     8
21      19     4     3     8
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ATHB-9_MA0573.1
XX
ID  ATHB-9_MA0573.1
XX
DE  MA0573.1 ATHB-9; from JASPAR
P0       a     c     g     t
1        4     2     1     2
2        5     3     3     3
3        5     4     2     5
4        3     8     2     5
5        0     1    25     0
6        0     6     0    20
7       25     0     1     0
8       26     0     0     0
9        0     0     0    26
10       0     0    26     0
11      26     0     0     0
12       0     0     0    26
13       0     0     3    23
14      17     0     9     0
15       0    25     1     0
16       5     1     4     7
17       2     6     0     4
18       0     3     4     3
19       0     5     3     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB15_MA0574.1
XX
ID  MYB15_MA0574.1
XX
DE  MA0574.1 MYB15; from JASPAR
P0       a     c     g     t
1        4    73     9    14
2       27    18    51     4
3       64    18    18     0
4        4    64    18    14
5        0     0   100     0
6        0     0   100     0
7        0     0     0   100
8       87     4     0     9
9        0     0   100     0
10       0     0   100     0
11       0     0     0   100
12      18     0    78     4
13       4     0    87     9
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB77_MA0575.1
XX
ID  MYB77_MA0575.1
XX
DE  MA0575.1 MYB77; from JASPAR
P0       a     c     g     t
1       33    30    20    17
2       57    17    17     9
3       27     9    37    27
4        6    30    20    44
5        9     6    82     3
6       68     3    23     6
7        0   100     0     0
8       50     0    50     0
9        0     0   100     0
10       0     0     0   100
11       0     0     0   100
12      68     3    26     3
13       3    55    30    12
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RAX3_MA0576.1
XX
ID  RAX3_MA0576.1
XX
DE  MA0576.1 RAX3; from JASPAR
P0       a     c     g     t
1       22    18    42    18
2        0    14    86     0
3       18     4    78     0
4       14     9    55    22
5        0     0   100     0
6        0     0   100     0
7        0     0     0   100
8       78     4     4    14
9        0     0   100     0
10       0     0   100     0
11       0     0     0   100
12       0     0    96     4
13       4    32    60     4
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SPL3_MA0577.3
XX
ID  SPL3_MA0577.3
XX
DE  MA0577.3 SPL3; from JASPAR
P0       a     c     g     t
1      200   288   143   319
2      108   241    85   516
3       11     6   928     5
4        2     3    10   935
5      942     3     3     2
6        7   939     1     3
7       75     6   856    13
8       34     5   872    39
9      760    56    15   119
10     237   275   122   316
11     340   171   226   213
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SPL8_MA0578.1
XX
ID  SPL8_MA0578.1
XX
DE  MA0578.1 SPL8; from JASPAR
P0       a     c     g     t
1       11     1     4    14
2       11     0     6    13
3        9     4     6    11
4       10     1     4    15
5       10     6     4    10
6        3    10     2    15
7        0     0    30     0
8        0     0     0    30
9       30     0     0     0
10       0    30     0     0
11       6    12     1    11
12      10     7     0    13
13       5     8     1    16
14      13     5     0    12
15      10     3     0    17
16      11     6     0    13
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CDC5_MA0579.1
XX
ID  CDC5_MA0579.1
XX
DE  MA0579.1 CDC5; from JASPAR
P0       a     c     g     t
1       28    24    32    15
2       17    26    55     2
3        0    92     4     4
4        2     2     0    96
5        6    85     0     8
6       91     2     2     4
7        2     0    93     4
8        2    93     5     0
9        4     4    91     0
10       2    55    27    16
11      18    23    45    14
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DYT1_MA0580.1
XX
ID  DYT1_MA0580.1
XX
DE  MA0580.1 DYT1; from JASPAR
P0       a     c     g     t
1        4     6     0     1
2        3     1     7     0
3        1     2     1     7
4        2     2     6     1
5       10     1     0     0
6        3     0     7     1
7        4     0     0     7
8        0    11     0     0
9       11     0     0     0
10       0    11     0     0
11       1     0    10     0
12       0     0     0    11
13       0     0    11     0
14      11     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  LEC2_MA0581.1
XX
ID  LEC2_MA0581.1
XX
DE  MA0581.1 LEC2; from JASPAR
P0       a     c     g     t
1      107    86   119   175
2       96   163   165    63
3        2   475     0    10
4      487     0     0     0
5        0     0     0   487
6        0     0   487     0
7        0   487     0     0
8      301    30   156     0
9      115   218    60    94
10     228    86    44   129
11      96    79    62   250
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RAV1_MA0582.1
XX
ID  RAV1_MA0582.1
XX
DE  MA0582.1 RAV1; from JASPAR
P0       a     c     g     t
1       14    17    12     6
2       16    11    10    14
3        7    14    35     1
4        0    62     0     0
5       47    12     9     0
6       62     4     3     0
7        0    69     0     0
8       69     0     0     0
9        9     7    32    21
10      41     4     4    20
11      34     6    10    19
12      27     9    17    16
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RAV1_MA0583.1
XX
ID  RAV1_MA0583.1
XX
DE  MA0583.1 RAV1; from JASPAR
P0       a     c     g     t
1       22    12    10    18
2       13    15     8    28
3        8    49     1     7
4       40     6     8    11
5        1    56     3     5
6        0    65     0     0
7        0     0     0    65
8        0     0    58     0
9       24     7    19     5
10      12    15    18     9
11      12    14    16    10
12       8    20     5    13
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SEP1_MA0584.1
XX
ID  SEP1_MA0584.1
XX
DE  MA0584.1 SEP1; from JASPAR
P0       a     c     g     t
1       14    15     7    15
2       14     6    10    21
3       11     4     6    30
4       19     7     9    16
5        0    51     0     0
6        0    46     0     5
7       39     5     1     6
8       20     2     4    25
9       37     0     0    14
10      25     0     0    26
11      28     4     4    15
12       4     6     3    38
13      26     0    25     0
14       0     0    51     0
15      23    11     3    14
16      40     3     0     8
17      33     5     5     8
18       6     8    17    20
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AGL1_MA0585.1
XX
ID  AGL1_MA0585.1
XX
DE  MA0585.1 AGL1; from JASPAR
P0       a     c     g     t
1       21    11    15    18
2       12     6     9    38
3        4     1     9    51
4       21     5    22    17
5        0    65     0     0
6        1    63     1     0
7       30     6     7    22
8       31     4     8    22
9       42     1     1    21
10      26     3     2    34
11      14    14     9    28
12      15     8    28    14
13       7     0    56     2
14       4     0    56     5
15      26     6     5    28
16      48    10     4     3
17      39     9     8     9
18      18    15    19    13
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SPL14_MA0586.2
XX
ID  SPL14_MA0586.2
XX
DE  MA0586.2 SPL14; from JASPAR
P0       a     c     g     t
1      125     7    74   377
2       23   534     0    26
3        0   583     0     0
4        0     0   583     0
5        0     0     0   583
6      583     0     0     0
7        0   581     0     2
8      302    87   129    65
9      239   105   113   126
10     164   104    83   232
11     148    86   104   245
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP16_MA0587.1
XX
ID  TCP16_MA0587.1
XX
DE  MA0587.1 TCP16; from JASPAR
P0       a     c     g     t
1        3     0    60     1
2        1     3     0    60
3        0     0    64     0
4        0     0    64     0
5       32     8    15     9
6        1    60     2     1
7        0    64     0     0
8        0    64     0     0
9       18    10    29     7
10      12    20    26     6
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TGA1_MA0588.1
XX
ID  TGA1_MA0588.1
XX
DE  MA0588.1 TGA1; from JASPAR
P0       a     c     g     t
1        2     5     6    10
2        3     8    16     2
3       11     9    12     0
4        0     0     0    36
5        0     0    36     0
6       36     0     0     0
7        0    33     0     3
8        2     0    34     0
9        1     3     2    20
10       6     5     6     5
11      11     0     6     7
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY1_MA0589.1
XX
ID  WRKY1_MA0589.1
XX
DE  MA0589.1 WRKY1; from JASPAR
P0       a     c     g     t
1        0     0     0    50
2        0     0     0    50
3        0     0    50     0
4       50     0     0     0
5        0    50     0     0
6        1    47     1     1
7        2     5    40     3
8       32    11     4     3
9        2     7    37     4
10       2    28     5    14
11       8    20     5    13
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  LFY_MA0590.1
XX
ID  LFY_MA0590.1
XX
DE  MA0590.1 LFY; from JASPAR
P0       a     c     g     t
1       32   104    49   199
2      266    13    17    80
3       56    75    12   325
4       20     7   178   360
5      126     0   246     4
6      228    43     2    50
7       10   364     0     8
8        0   358     0    19
9       94     2   273    10
10      35   174   174    35
11      10   273     2    94
12      19     0   358     0
13       8     0   364    10
14      50     2    43   228
15       4   246     0   126
16     360   178     7    20
17     325    12    75    56
18      80    17    13   266
19     199    49   104    32
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ABF3_MA0930.2
XX
ID  ABF3_MA0930.2
XX
DE  MA0930.2 ABF3; from JASPAR
P0       a     c     g     t
1     1414  3728  1502  1948
2     5658   606   908  1420
3      143  8374    36    39
4       51    51  8414    76
5      339   321   381  7551
6      420   296  7845    31
7      781   368  1935  5508
8      662  7781    60    89
9     3106  1283  1779  2424
10    2541  1700  1589  2762
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ABI5_MA0931.1
XX
ID  ABI5_MA0931.1
XX
DE  MA0931.1 ABI5; from JASPAR
P0       a     c     g     t
1      181   257   257   306
2       87    87   660   167
3      608   314     0    78
4        0   922    78     0
5      999     0     0     0
6        0   999     0     0
7        0     0   999     0
8        0     0     0   999
9      152    70   709    70
10     164   164   337   336
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AHL12_MA0932.1
XX
ID  AHL12_MA0932.1
XX
DE  MA0932.1 AHL12; from JASPAR
P0       a     c     g     t
1      772    18   146    64
2      843    35    24    98
3      484    14     5   497
4      332     6     3   660
5      660     3     6   332
6      497     5    14   484
7       98    24    35   843
8       64   146    18   772
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AHL20_MA0933.1
XX
ID  AHL20_MA0933.1
XX
DE  MA0933.1 AHL20; from JASPAR
P0       a     c     g     t
1      774    66    73    88
2      753     6     5   235
3      120     4    13   862
4      250     6     6   738
5      774     4     4   218
6      843     8     1   148
7      648    12     3   337
8       54    28    18   900
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AHL25_MA0934.1
XX
ID  AHL25_MA0934.1
XX
DE  MA0934.1 AHL25; from JASPAR
P0       a     c     g     t
1      872    26    53    49
2      616     4     6   375
3      207     4     3   786
4      225     3     2   770
5      770     2     3   225
6      786     3     4   207
7      375     6     4   616
8       49    53    26   872
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC025_MA0935.1
XX
ID  NAC025_MA0935.1
XX
DE  MA0935.1 NAC025; from JASPAR
P0       a     c     g     t
1        1   421     1   578
2      965     0    35     0
3        0   999     0     0
4        0     0   999     0
5        0   276    69   655
6      827   173     0     0
7      930     0     0    69
8       50   599    50   300
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC046_MA0936.1
XX
ID  NAC046_MA0936.1
XX
DE  MA0936.1 NAC046; from JASPAR
P0       a     c     g     t
1      544   217    80   159
2       66   630    38   267
3      813   120    43    24
4      102   858    24    16
5       94    49   812    45
6      143   565   112   180
7      459   402    48    91
8      805    40    96    59
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC055_MA0937.1
XX
ID  NAC055_MA0937.1
XX
DE  MA0937.1 NAC055; from JASPAR
P0       a     c     g     t
1      796    26    44   134
2       19   774    18   188
3      970    18     4     9
4       22   960     6    12
5       16    16   948    20
6       14   116   134   736
7      878    80     4    37
8      866     9     4   121
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC058_MA0938.2
XX
ID  NAC058_MA0938.2
XX
DE  MA0938.2 NAC058; from JASPAR
P0       a     c     g     t
1      307   175   240   385
2      476   183   145   303
3      187   101   628   191
4      214   105   595   193
5       61     8     3  1035
6      204    15   132   756
7      571    79   189   268
8       17  1084     1     5
9        0     3   524   580
10       0     2     2  1103
11      93    61   936    17
12     133   128    45   801
13     262   153   450   242
14     288   254   269   296
15     241   474   157   235
16     739    57   150   161
17      35   876   112    84
18    1097     3     5     2
19    1009    86    10     2
20       5     1  1090    11
21     284   144   117   562
22     729   121    18   239
23     948     7    14   138
24     265   357   171   314
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC079_MA0939.1
XX
ID  NAC079_MA0939.1
XX
DE  MA0939.1 NAC079; from JASPAR
P0       a     c     g     t
1      665     1   222   112
2        1   691     1   308
3      998     1     1     1
4        1   998     1     1
5        1     1   998     1
6        1   855     1   143
7      784    72     1   143
8      998     1     1     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AP1_MA0940.1
XX
ID  AP1_MA0940.1
XX
DE  MA0940.1 AP1; from JASPAR
P0       a     c     g     t
1      734   799    32    57
2       84   941    39   558
3     1323    46    35   218
4     1134    19   152   317
5     1551    10    15    46
6     1277    15    24   306
7     1191    23   252   156
8      682    21   257   662
9      653    12   930    27
10     133    60  1354    75
11    1146   153   170   153
12    1279   122   168    53
13    1257    22   162   181
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ABF2_MA0941.1
XX
ID  ABF2_MA0941.1
XX
DE  MA0941.1 ABF2; from JASPAR
P0       a     c     g     t
1      231   308   231   231
2      308   231   231   231
3      191   268   191   350
4      274   120   403   202
5      699   177   103    21
6        0   840     0   159
7      999     0     0     0
8        0   917     0    82
9       19    19   942    19
10      19    19    19   942
11     218    59   664    59
12     213   130   287   371
13     229   229   229   312
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF1_MA0942.1
XX
ID  ARF1_MA0942.1
XX
DE  MA0942.1 ARF1; from JASPAR
P0       a     c     g     t
1      722     2   276     1
2        3   993     1     4
3        4   993     2     2
4        4     2   992     1
5      989     3     1     7
6        2   993     2     2
7      901     4    88     8
8      262   220   207   312
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF5_MA0943.1
XX
ID  ARF5_MA0943.1
XX
DE  MA0943.1 ARF5; from JASPAR
P0       a     c     g     t
1      303   160   477    59
2        9   969    15     8
3       97   898     4     2
4       12     1   985     2
5      952     1    46     1
6        2   992     3     3
7      773   224     2     2
8      491    98   218   193
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF8_MA0944.1
XX
ID  ARF8_MA0944.1
XX
DE  MA0944.1 ARF8; from JASPAR
P0       a     c     g     t
1      270   233   204   293
2      225   259   106   410
3        4     0    90   906
4       38     1   900    60
5        0    13     0   987
6       54   863     2    82
7       61    27   798   114
8      121   138   651    89
9      228   337   217   219
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARR1_MA0945.1
XX
ID  ARR1_MA0945.1
XX
DE  MA0945.1 ARR1; from JASPAR
P0       a     c     g     t
1      406   168   208   218
2      172   292   343   192
3      297   466   139    99
4      306    83   564    47
5      423    22   235   320
6      965     0     0    34
7       21     1     1   977
8       56   897     4    43
9      143   156    91   609
10     257   190   181   372
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARR11_MA0946.1
XX
ID  ARR11_MA0946.1
XX
DE  MA0946.1 ARR11; from JASPAR
P0       a     c     g     t
1      625    51    55   269
2      927    11    29    32
3        5     2   982    11
4      989     2     4     4
5        7     3     2   987
6      732    22     4   242
7        3   833     2   162
8       14     5   968    13
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARR14_MA0947.1
XX
ID  ARR14_MA0947.1
XX
DE  MA0947.1 ARR14; from JASPAR
P0       a     c     g     t
1      945     5    26    24
2        2     1   984    13
3      991     1     3     5
4        2     3     2   993
5      506   154     1   339
6        1   960     1    38
7       10     5   960    26
8       83   484   401    32
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARR18_MA0948.1
XX
ID  ARR18_MA0948.1
XX
DE  MA0948.1 ARR18; from JASPAR
P0       a     c     g     t
1      324   225   225   225
2      202   202   202   394
3      274   370   178   178
4      329   186   194   290
5      834    25    25   115
6        0    88   912     0
7      999     0     0     0
8        0     0     0   999
9      728    25    25   222
10      48   673    48   231
11     159    72   697    72
12     341   243   264   151
13     225   225   225   326
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARR2_MA0949.1
XX
ID  ARR2_MA0949.1
XX
DE  MA0949.1 ARR2; from JASPAR
P0       a     c     g     t
1      174   257   394   174
2      338   572    45    45
3      214     0   786     0
4      476     0   214   309
5     1000     0     0     0
6        0     0     0  1000
7        0  1000     0     0
8        0     0     0  1000
9      162    76    76   687
10     205   205   205   386
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ATHB-12_MA0950.1
XX
ID  ATHB-12_MA0950.1
XX
DE  MA0950.1 ATHB-12; from JASPAR
P0       a     c     g     t
1      579   191   107   124
2      668    46    53   233
3       18    35     6   941
4       48   201   633   118
5      965     7    15    13
6        9     8    17   966
7       30    10    19   940
8      128    18   766    88
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ATHB-16_MA0951.1
XX
ID  ATHB-16_MA0951.1
XX
DE  MA0951.1 ATHB-16; from JASPAR
P0       a     c     g     t
1       72   214     1   713
2      999     0     0     0
3      999     0     0     0
4        0     0     0   999
5      545   318   137     0
6      999     0     0     0
7        0     0     0   999
8       67     1    67   865
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ATHB-51_MA0952.1
XX
ID  ATHB-51_MA0952.1
XX
DE  MA0952.1 ATHB-51; from JASPAR
P0       a     c     g     t
1      691   253     4    51
2      985     5     4     7
3        2     8     1   988
4      411     6     3   580
5      990     2     5     3
6       10     2     4   984
7        5    18    12   965
8      105    18   777   100
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ATHB-6_MA0953.1
XX
ID  ATHB-6_MA0953.1
XX
DE  MA0953.1 ATHB-6; from JASPAR
P0       a     c     g     t
1      177   252   287   284
2      149   524    85   243
3      745    89    73    92
4      886    71     1    42
5       20     6     1   972
6      356   310   128   206
7      924     5    38    32
8      106    66   130   698
9      234   111   266   389
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ATHB-7_MA0954.2
XX
ID  ATHB-7_MA0954.2
XX
DE  MA0954.2 ATHB-7; from JASPAR
P0       a     c     g     t
1       92    58    68   382
2       53   389    92    66
3      473    62    11    54
4      560     3     0    37
5        0     1     1   598
6        2     2   464   132
7      600     0     0     0
8        0     0     0   600
9        0     0     2   598
10      37     3   551     9
11     324    46   181    49
12     174    57    98   271
13     128    91    93   288
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  POPTR_0002s00440g_MA0955.1
XX
ID  POPTR_0002s00440g_MA0955.1
XX
DE  MA0955.1 POPTR_0002s00440g; from JASPAR
P0       a     c     g     t
1      201   266   325   209
2        9    29   954     7
3        8    11    99   882
4      951    43     5     0
5      153   706    29   112
6       41    80   859    19
7      188    50   696    67
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BEE2_MA0956.1
XX
ID  BEE2_MA0956.1
XX
DE  MA0956.1 BEE2; from JASPAR
P0       a     c     g     t
1      256   253   236   255
2      211   229   342   218
3      136   722    38   104
4      893     0    33    74
5       23   820     0   157
6      157     0   820    23
7       74    33     0   893
8      104    38   722   136
9      218   342   229   211
10     255   236   253   256
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BHLH3_MA0957.1
XX
ID  BHLH3_MA0957.1
XX
DE  MA0957.1 BHLH3; from JASPAR
P0       a     c     g     t
1      258    94   603    44
2       45   930    14    11
3      950     7    20    22
4       10   937    12    42
5       42    12   937    10
6       22    20     7   950
7       11    14   930    45
8       44   603    94   258
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BHLH13_MA0958.1
XX
ID  BHLH13_MA0958.1
XX
DE  MA0958.1 BHLH13; from JASPAR
P0       a     c     g     t
1      295   116   569    20
2       53   945     1     1
3      992     2     3     4
4        1   952     1    46
5       15     1   982     1
6        4     4     1   991
7        1     2   987    10
8       38   696    65   201
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AIB_MA0959.1
XX
ID  AIB_MA0959.1
XX
DE  MA0959.1 AIB; from JASPAR
P0       a     c     g     t
1       76    14   901     8
2       59   939     1     2
3      966     1    28     5
4        0   903     2    94
5       94     2   903     0
6        5    28     1   966
7        2     1   939    59
8        8   901    14    76
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BHLH104_MA0960.1
XX
ID  BHLH104_MA0960.1
XX
DE  MA0960.1 BHLH104; from JASPAR
P0       a     c     g     t
1      130    26   822    22
2       15    74   866    45
3        6   994     0     0
4      996     0     4     0
5        0   999     0     1
6        1     0   999     0
7        0     4     0   996
8        0     0   994     6
9       45   866    74    15
10      22   822    26   130
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BHLH112_MA0961.1
XX
ID  BHLH112_MA0961.1
XX
DE  MA0961.1 BHLH112; from JASPAR
P0       a     c     g     t
1      221   250   306   222
2        0  1000     0     0
3      177   823     0     0
4     1000     0     0     0
5        0  1000     0     0
6        0     0     0  1000
7        0     0     0  1000
8        0     0  1000     0
9      218   343   217   223
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BHLH34_MA0962.1
XX
ID  BHLH34_MA0962.1
XX
DE  MA0962.1 BHLH34; from JASPAR
P0       a     c     g     t
1      154    39   653   154
2        0   999     0     0
3      727     0   242    31
4        0   848     0   152
5        0     0   999     0
6        0     0     0   999
7        0     0   999     0
8       96   143   476   286
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BHLH78_MA0963.1
XX
ID  BHLH78_MA0963.1
XX
DE  MA0963.1 BHLH78; from JASPAR
P0       a     c     g     t
1       10    10   971    10
2       10   971    10    10
3      971    10    10    10
4       10   971    10    10
5       10    10   971    10
6       10    10    10   971
7       10    10   971    10
8       10   971    10    10
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BIM1_MA0964.2
XX
ID  BIM1_MA0964.2
XX
DE  MA0964.2 BIM1; from JASPAR
P0       a     c     g     t
1      433   255   283   417
2      433   271   535   149
3       96    91   190  1011
4        5  1377     3     3
5     1380     1     0     7
6        1  1376     0    11
7       11     0  1376     1
8        7     0     1  1380
9        3     3  1377     5
10    1011   190    91    96
11     149   535   271   433
12     417   283   255   433
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BIM2_MA0965.2
XX
ID  BIM2_MA0965.2
XX
DE  MA0965.2 BIM2; from JASPAR
P0       a     c     g     t
1     3104  2178  2211  2885
2     3238  2012  2323  2805
3     3383  1958  1799  3238
4     2362  1874  4148  1994
5     2197  1902  3272  3007
6       28 10294    20    36
7    10091    36   166    85
8       16 10174    23   165
9      162    23 10179    14
10      84   165    36 10093
11      36    20 10294    28
12    3003  3275  1899  2201
13    1996  4150  1874  2358
14    3238  1796  1964  3380
15    2808  2323  2010  3237
16    2882  2214  2174  3108
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BIM3_MA0966.1
XX
ID  BIM3_MA0966.1
XX
DE  MA0966.1 BIM3; from JASPAR
P0       a     c     g     t
1      294   203   270   233
2      159   165   433   243
3       74   821     8    96
4      942     0    25    33
5       15   918     0    67
6       67     0   918    15
7       33    25     0   942
8       96     8   821    74
9      243   433   165   159
10     233   270   203   294
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZIP60_MA0967.1
XX
ID  BZIP60_MA0967.1
XX
DE  MA0967.1 BZIP60; from JASPAR
P0       a     c     g     t
1       18    64    24   893
2       26    59   840    74
3      892    17    43    49
4       13   799    13   175
5      175    13   799    13
6       49    43    17   892
7       74   840    59    26
8      893    24    64    18
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZIP68_MA0968.2
XX
ID  BZIP68_MA0968.2
XX
DE  MA0968.2 BZIP68; from JASPAR
P0       a     c     g     t
1       51   126    42   381
2        2     0   502    96
3      106   493     1     0
4        0   598     0     2
5      600     0     0     0
6        1   589     4     6
7        2     4   594     0
8        0     0     1   599
9       58   306   234     2
10     327     1   143   129
11      93   161   212   134
12     154   344    28    74
13     285    76    68   171
14     171   122    82   225
15     157   191    66   186
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CAMTA2_MA0969.1
XX
ID  CAMTA2_MA0969.1
XX
DE  MA0969.1 CAMTA2; from JASPAR
P0       a     c     g     t
1      313   203   220   264
2      327   237   176   260
3      393   121   243   243
4      220   438   306    36
5       26   955     8    11
6       18    11   970     1
7       31   682     7   280
8        8     7   970    15
9        7    11    54   928
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CAMTA3_MA0970.1
XX
ID  CAMTA3_MA0970.1
XX
DE  MA0970.1 CAMTA3; from JASPAR
P0       a     c     g     t
1      233   540   184    43
2       14   974     5     8
3       32     0   962     6
4       68   664     1   267
5       37     0   963     0
6        1     1     0   998
7      231   274   211   285
8      290   247   250   213
9      241   258   251   251
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DREB1A_MA0971.1
XX
ID  DREB1A_MA0971.1
XX
DE  MA0971.1 DREB1A; from JASPAR
P0       a     c     g     t
1      590   118   126   166
2      139   159   137   565
3        2     0   997     1
4       10    14     3   973
5        5   991     4     1
6        0     0   994     6
7       48     0   948     3
8       16   504    19   461
9      341   240   279   140
10     322   205   322   152
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CCA1_MA0972.1
XX
ID  CCA1_MA0972.1
XX
DE  MA0972.1 CCA1; from JASPAR
P0       a     c     g     t
1      934     1    10    54
2      960     1    37     2
3      985     4    10     1
4       35     0     5   960
5      992     5     1     2
6        1    12     1   986
7        5   991     0     4
8       21    91    14   874
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CDF2_MA0973.1
XX
ID  CDF2_MA0973.1
XX
DE  MA0973.1 CDF2; from JASPAR
P0       a     c     g     t
1      334   284   235   148
2      608    92   150   150
3      693    16    41   250
4      980    10     4     5
5      979    10     2     9
6      868    29    81    22
7       17     7   969     6
8       53   295   154   498
9      328    77   395   200
10     299   282   155   264
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CDF3_MA0974.2
XX
ID  CDF3_MA0974.2
XX
DE  MA0974.2 CDF3; from JASPAR
P0       a     c     g     t
1      396   166   213   382
2      337   214   256   350
3      879    63    79   136
4      964    24    29   140
5     1143     2     7     5
6     1133     4    11     9
7     1125     3    19    10
8        1     0  1154     2
9       29    44    37  1047
10      86     7  1038    26
11     430   210   104   413
12     519   186   130   322
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CRF2_MA0975.1
XX
ID  CRF2_MA0975.1
XX
DE  MA0975.1 CRF2; from JASPAR
P0       a     c     g     t
1       44   478   348   131
2      108   783    81    27
3        0     0   999     0
4        0   977    23     0
5        0   999     0     0
6        0     0   999     0
7       23   931     0    46
8      118   735    59    88
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CRF4_MA0976.2
XX
ID  CRF4_MA0976.2
XX
DE  MA0976.2 CRF4; from JASPAR
P0       a     c     g     t
1      165  1167   247   321
2      249  1377   111   163
3       89     4  1744    63
4        4  1865    22     9
5       10  1866    15     9
6      113     2  1734    51
7       10  1794     9    87
8       55  1804    16    25
9     1104    12   653   131
10      67  1659    40   134
11     331  1002   198   369
12     612   168   757   363
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF2.5_MA0977.1
XX
ID  DOF2.5_MA0977.1
XX
DE  MA0977.1 DOF2.5; from JASPAR
P0       a     c     g     t
1      756    26   182    36
2      598     1    12   389
3      990     1     4     5
4      986     1     8     6
5      744     1   254     1
6        4     1   977    18
7        8   198    65   729
8      238    44   557   160
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DREB1E_MA0978.1
XX
ID  DREB1E_MA0978.1
XX
DE  MA0978.1 DREB1E; from JASPAR
P0       a     c     g     t
1      978     2    15     4
2        9    15     3   973
3        6     1   991     3
4        3     3     2   992
5        3   992     1     4
6        2     2   992     4
7      169     0   828     2
8        1   790     1   208
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF008_MA0979.1
XX
ID  ERF008_MA0979.1
XX
DE  MA0979.1 ERF008; from JASPAR
P0       a     c     g     t
1      148   787    24    41
2      335    15   634    17
3       27   889    58    26
4       39   934    19     8
5       66    44   865    26
6      320   524    56   101
7       70   828    68    34
8      100   417   329   154
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RAP2-10_MA0980.2
XX
ID  RAP2-10_MA0980.2
XX
DE  MA0980.2 RAP2-10; from JASPAR
P0       a     c     g     t
1      477   694   291  1106
2      467   733   280  1088
3     1041    48  1374   105
4       16  2516     6    30
5        9  2544     9     6
6       16     6  2534    12
7     2503    19    10    36
8       10  2539     7    12
9     2443    29    65    31
10    1212   510   306   540
11     933   458   409   768
12     906   381   545   736
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF1.8_MA0981.1
XX
ID  DOF1.8_MA0981.1
XX
DE  MA0981.1 DOF1.8; from JASPAR
P0       a     c     g     t
1      251   259   256   234
2      321   197   250   233
3      415   100   135   350
4      812    72    38    77
5      831    64     3   102
6      658   233    27    83
7       65    23   846    65
8      170   241   231   359
9      268   189   270   273
10     288   209   269   234
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF2.4_MA0982.1
XX
ID  DOF2.4_MA0982.1
XX
DE  MA0982.1 DOF2.4; from JASPAR
P0       a     c     g     t
1      381   171   233   215
2      426    90   127   356
3      807    71    18   104
4      815    66     1   118
5      658   252    16    75
6       81    73   762    84
7      177   242   210   371
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF5.6_MA0983.1
XX
ID  DOF5.6_MA0983.1
XX
DE  MA0983.1 DOF5.6; from JASPAR
P0       a     c     g     t
1      254   257   262   228
2      364   170   227   239
3      432    84   108   376
4      955    35     1    10
5      928    40     1    32
6      613   308    56    24
7       50    28   884    38
8      152   247   213   388
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF5.7_MA0984.1
XX
ID  DOF5.7_MA0984.1
XX
DE  MA0984.1 DOF5.7; from JASPAR
P0       a     c     g     t
1      448    86   224   241
2      684    12    61   243
3      970     4    10    16
4      899     3    79    19
5      363    42   589     6
6      485    61   443    11
7      102   191   460   247
8      174   215   408   204
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DREB2C_MA0986.1
XX
ID  DREB2C_MA0986.1
XX
DE  MA0986.1 DREB2C; from JASPAR
P0       a     c     g     t
1      110   757   121    13
2      809     3   185     3
3        4   985     3     8
4        7   986     3     4
5        8     3   984     4
6      776   158    47    19
7        5   977     5    13
8      806    50    27   117
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHYPADRAFT_140773_MA0987.1
XX
ID  PHYPADRAFT_140773_MA0987.1
XX
DE  MA0987.1 PHYPADRAFT_140773; from JASPAR
P0       a     c     g     t
1      243   266   253   238
2      302   215   260   222
3      396   111   173   321
4      888    43    10    60
5      892    46     1    61
6      715   205    28    52
7       72    22   839    67
8      182   258   229   331
9      271   193   271   265
10     280   218   272   229
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHYPADRAFT_143875_MA0988.1
XX
ID  PHYPADRAFT_143875_MA0988.1
XX
DE  MA0988.1 PHYPADRAFT_143875; from JASPAR
P0       a     c     g     t
1      250   272   222   256
2      208   231   392   169
3      106   844    12    38
4      923     0    10    66
5       22   940     0    39
6       39     0   940    22
7       66    10     0   923
8       38    12   844   106
9      169   392   231   208
10     256   222   272   250
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHYPADRAFT_153324_MA0989.1
XX
ID  PHYPADRAFT_153324_MA0989.1
XX
DE  MA0989.1 PHYPADRAFT_153324; from JASPAR
P0       a     c     g     t
1      253   264   261   222
2      334   193   255   218
3      482    71   118   330
4      861    62    13    65
5      926    36     0    38
6      672   147    52   129
7       71    27   898     5
8      139   288   195   378
9      285   158   297   260
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HDG11_MA0990.1
XX
ID  HDG11_MA0990.1
XX
DE  MA0990.1 HDG11; from JASPAR
P0       a     c     g     t
1      209   392   115   285
2      582   256    67    94
3      495    27    70   409
4       57    11     9   923
5      315    55    33   598
6      922    28    43     7
7      833    39    23   105
8      135    26     1   839
9       81    25   756   137
10     220   504    49   227
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF4_MA0992.2
XX
ID  ERF4_MA0992.2
XX
DE  MA0992.2 ERF4; from JASPAR
P0       a     c     g     t
1      227   109   368   319
2       88   644   165   126
3       11  1005     2     5
4       24     1   979    19
5        5   992     9    17
6        5  1015     2     1
7        9     0  1006     8
8       34   349    13   627
9       42   900    25    56
10     827    19    91    86
11     253   306   101   363
12     318   287   113   305
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF7_MA0993.1
XX
ID  ERF7_MA0993.1
XX
DE  MA0993.1 ERF7; from JASPAR
P0       a     c     g     t
1      263   442   140   156
2       77    14   874    35
3       48   868    84     0
4      129   770    36    65
5       90    25   875    10
6      146   456   132   266
7      212   593    66   128
8      419   174   210   198
9      276   208   198   317
10     279   209   201   312
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF8_MA0994.2
XX
ID  ERF8_MA0994.2
XX
DE  MA0994.2 ERF8; from JASPAR
P0       a     c     g     t
1      195   222   125   201
2      198    79   286   180
3      249    91   273   130
4      116   180    33   414
5      103    50   491    99
6      426    29   252    36
7       32   649     5    57
8        3     3   728     9
9       16     9   714     4
10      11   701     0    31
11       6     2   728     7
12      16    46   667    14
13     108   537    11    87
14      21     9   674    39
15     268    90   321    64
16     191   213   109   230
17     168    58   354   163
18     175    93   321   154
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF039_MA0995.2
XX
ID  ERF039_MA0995.2
XX
DE  MA0995.2 ERF039; from JASPAR
P0       a     c     g     t
1      282   574   258   530
2      285   875   168   316
3      679    23   864    78
4        3  1617    23     1
5       11  1605    10    18
6       19    13  1605     7
7     1381   137    45    81
8        1  1615    15    13
9     1521    21    72    30
10     495   533   236   380
11     484   420   278   462
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF069_MA0997.1
XX
ID  ERF069_MA0997.1
XX
DE  MA0997.1 ERF069; from JASPAR
P0       a     c     g     t
1      193   294   315   197
2      197   471   175   157
3       18     0   982     0
4       61   828    86    24
5       98   680    76   146
6      179     0   820     0
7      138   419   212   230
8      224   404   193   180
9      351   209   231   209
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF096_MA0998.1
XX
ID  ERF096_MA0998.1
XX
DE  MA0998.1 ERF096; from JASPAR
P0       a     c     g     t
1      193   300   280   227
2      289   468   167    76
3       34    13   939    13
4       32   905    36    27
5       97   850    21    32
6       43    10   928    19
7      129   591   131   149
8       35   903    24    38
9      384   176   240   200
10     233   254   223   290
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF105_MA1000.2
XX
ID  ERF105_MA1000.2
XX
DE  MA1000.2 ERF105; from JASPAR
P0       a     c     g     t
1      144    57   278   107
2      149   181    36   220
3      168    44   257   117
4      189    37   312    48
5       50   307     0   229
6        0     0   586     0
7       21     0   565     0
8        1   569     0    16
9        0     0   586     0
10       0     1   585     0
11      21   544     0    21
12       1     0   523    62
13      78   109   382    17
14     211   204    60   111
15     123    27   330   106
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF11_MA1001.3
XX
ID  ERF11_MA1001.3
XX
DE  MA1001.3 ERF11; from JASPAR
P0       a     c     g     t
1      138   830   181   224
2      193  1008    63   109
3       59     6  1270    38
4        4  1347    13     9
5       16  1346     5     6
6       76     2  1271    24
7        9  1281     9    74
8       40  1304    11    18
9      852     5   435    81
10      85  1086    45   157
11     290   653   144   286
12     435   126   522   290
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF13_MA1004.1
XX
ID  ERF13_MA1004.1
XX
DE  MA1004.1 ERF13; from JASPAR
P0       a     c     g     t
1      217   478   283    22
2        0    49   951     0
3       16   951    32     0
4        0   983    16     0
5        0    16   983     0
6       97   645   145   113
7        0   999     0     0
8      461   103   282   154
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF3_MA1005.2
XX
ID  ERF3_MA1005.2
XX
DE  MA1005.2 ERF3; from JASPAR
P0       a     c     g     t
1      519   229   644   491
2      162  1145   257   319
3      113  1713    16    41
4      186     8  1658    31
5        0  1878     3     2
6       30  1844     1     8
7      149     2  1697    35
8        2  1852     7    22
9       12  1852     1    18
10     658    48   925   252
11     239   887   194   563
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF6_MA1006.1
XX
ID  ERF6_MA1006.1
XX
DE  MA1006.1 ERF6; from JASPAR
P0       a     c     g     t
1      291   352   179   179
2       78   221   166   535
3        0     0  1000     0
4        0  1000     0     0
5        0  1000     0     0
6        0     0  1000     0
7        0    88   912     0
8        0  1000     0     0
9      159   156   530   155
10     172   278   257   294
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHYPADRAFT_173530_MA1007.1
XX
ID  PHYPADRAFT_173530_MA1007.1
XX
DE  MA1007.1 PHYPADRAFT_173530; from JASPAR
P0       a     c     g     t
1       72     1   286   642
2        0     0   999     0
3       50     0     0   949
4        0   999     0     0
5        0     0   999     0
6        0     0   999     0
7        0   150     0   849
8       84     1   665   250
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHYPADRAFT_182268_MA1008.1
XX
ID  PHYPADRAFT_182268_MA1008.1
XX
DE  MA1008.1 PHYPADRAFT_182268; from JASPAR
P0       a     c     g     t
1      292   180   180   348
2      144    79   288   489
3        0     0  1000     0
4        0     0     0  1000
5        0  1000     0     0
6        0     0  1000     0
7        0     0  1000     0
8        0    76     0   924
9       70    70   789    70
10     292   246   291   171
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF3_MA1009.1
XX
ID  ARF3_MA1009.1
XX
DE  MA1009.1 ARF3; from JASPAR
P0       a     c     g     t
1        2     4    77   917
2        2     3   991     3
3        1    16     2   981
4        2   990     1     6
5        2     8   980    10
6        3    24   964    10
7      618   124   212    46
8      575    95   191   139
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHYPADRAFT_64121_MA1010.1
XX
ID  PHYPADRAFT_64121_MA1010.1
XX
DE  MA1010.1 PHYPADRAFT_64121; from JASPAR
P0       a     c     g     t
1      264   273   180   283
2       41    41    41   877
3       20    20   853   107
4      107    20   125   748
5       20   941    20    20
6        0    87   912     0
7       87     0   912     0
8        0     0     0  1000
9      608    70   158   164
10     288   209   209   294
11     309   230   230   230
12     309   230   230   230
13     309   230   230   230
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHYPADRAFT_72483_MA1011.1
XX
ID  PHYPADRAFT_72483_MA1011.1
XX
DE  MA1011.1 PHYPADRAFT_72483; from JASPAR
P0       a     c     g     t
1      264   238   308   190
2      210   181   272   337
3       69   849    17    64
4      806     0   106    87
5       55   800     0   146
6      146     0   800    55
7       87   106     0   806
8       64    17   849    69
9      337   272   181   210
10     190   308   238   264
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AGL27_MA1012.1
XX
ID  AGL27_MA1012.1
XX
DE  MA1012.1 AGL27; from JASPAR
P0       a     c     g     t
1       25    78    25    14
2        1     4     0   137
3        0     0     0   142
4       14     0     1   127
5        0   142     0     0
6        0    55     0    87
7      108     0     1    33
8       32     2     0   108
9        9     0     0   133
10       2     0     0   140
11      15    21     2   104
12      17    15    42    68
13      33     0   109     0
14      11     2    92    37
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GATA10_MA1013.1
XX
ID  GATA10_MA1013.1
XX
DE  MA1013.1 GATA10; from JASPAR
P0       a     c     g     t
1      100    67   133   699
2      841    27   132     0
3        0     0   999     0
4      999     0     0     0
5        0     0     0   999
6        0   999     0     0
7        0   111    84   805
8      111   111   740    37
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GATA11_MA1014.1
XX
ID  GATA11_MA1014.1
XX
DE  MA1014.1 GATA11; from JASPAR
P0       a     c     g     t
1      167    42   167   624
2      902     0    97     0
3        0     0   999     0
4      999     0     0     0
5        0     0     0   999
6        0   999     0     0
7        0     0     0   999
8       44   131   695   131
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GATA12_MA1015.1
XX
ID  GATA12_MA1015.1
XX
DE  MA1015.1 GATA12; from JASPAR
P0       a     c     g     t
1      190   282   146   382
2      599   112   230    59
3      189    67   738     6
4      968     2    22     7
5        7    22     2   968
6        6   738    67   189
7       59   230   112   599
8      382   146   282   190
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GATA15_MA1016.1
XX
ID  GATA15_MA1016.1
XX
DE  MA1016.1 GATA15; from JASPAR
P0       a     c     g     t
1      206   281   217   296
2      308   200   284   208
3      182   288    66   465
4       40    38   903    19
5      980     0     0    19
6        3     1     0   996
7       55   663   156   126
8      324   201   268   207
9      269   239   280   212
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GATA8_MA1017.1
XX
ID  GATA8_MA1017.1
XX
DE  MA1017.1 GATA8; from JASPAR
P0       a     c     g     t
1      207   319   261   214
2      476   241   152   131
3       59    29   899    13
4      991     1     2     6
5        2    21     0   977
6       27   649   132   192
7       60   379   108   453
8      384   158   369    89
9      314   187   320   179
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GATA9_MA1018.1
XX
ID  GATA9_MA1018.1
XX
DE  MA1018.1 GATA9; from JASPAR
P0       a     c     g     t
1      227   227   227   318
2      161   516   161   161
3      204   291    51   453
4      925     0    74     0
5        0     0  1000     0
6     1000     0     0     0
7        0     0     0  1000
8        0  1000     0     0
9       23    97    23   858
10     294    89   452   165
11     296   199   199   305
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Glyma19g26560.1_MA1019.1
XX
ID  Glyma19g26560.1_MA1019.1
XX
DE  MA1019.1 Glyma19g26560.1; from JASPAR
P0       a     c     g     t
1      104   199   480   218
2       19    14   908    59
3       86    19   861    33
4      109   307   440   144
5       42   882    27    49
6        3   996     1     0
7        4   976     0    20
8      896    17    76    11
9       17   844    50    89
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GT-1_MA1020.1
XX
ID  GT-1_MA1020.1
XX
DE  MA1020.1 GT-1; from JASPAR
P0       a     c     g     t
1        0   150   649   200
2        0    67     0   932
3       30     0     0   970
4      999     0     0     0
5      971    29     0     0
6       30   970     0     0
7        0   939     0    61
8      624   167   125    84
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHYPADRAFT_48267_MA1021.1
XX
ID  PHYPADRAFT_48267_MA1021.1
XX
DE  MA1021.1 PHYPADRAFT_48267; from JASPAR
P0       a     c     g     t
1      154   461   154   231
2        1   998     1     1
3      998     1     1     1
4        1   998     1     1
5       53     1   946     1
6        1     1     1   998
7        1     1   998     1
8      143   642    72   143
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHYPADRAFT_38837_MA1022.1
XX
ID  PHYPADRAFT_38837_MA1022.1
XX
DE  MA1022.1 PHYPADRAFT_38837; from JASPAR
P0       a     c     g     t
1      251   264   257   228
2      343   186   254   217
3      463    85   129   323
4      828    68    24    81
5      855    62     1    82
6      644   216    54    86
7       61    27   860    52
8      150   269   207   374
9      259   174   307   259
10     283   262   227   227
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHYPADRAFT_28324_MA1023.1
XX
ID  PHYPADRAFT_28324_MA1023.1
XX
DE  MA1023.1 PHYPADRAFT_28324; from JASPAR
P0       a     c     g     t
1       72    72   286   571
2        0     0   999     0
3        0     0     0   999
4        0   999     0     0
5        0     0   999     0
6        0     0   999     0
7        0   217     0   782
8      118     1   705   177
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HAT1_MA1024.1
XX
ID  HAT1_MA1024.1
XX
DE  MA1024.1 HAT1; from JASPAR
P0       a     c     g     t
1      149   374   328   149
2       69   506    69   356
3     1000     0     0     0
4     1000     0     0     0
5        0     0     0  1000
6        0  1000     0     0
7     1000     0     0     0
8       98     0     0   902
9      101   101   286   512
10     349   181   289   181
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HBI1_MA1025.1
XX
ID  HBI1_MA1025.1
XX
DE  MA1025.1 HBI1; from JASPAR
P0       a     c     g     t
1      229   313   229   229
2      229   229   313   229
3      132   240   315   313
4       24   106   762   108
5        0  1000     0     0
6     1000     0     0     0
7        0   904     0    95
8        0     0  1000     0
9       21    21    21   936
10      21    21   936    21
11     202   485   195   118
12     226   226   226   322
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ATHB-15_MA1026.2
XX
ID  ATHB-15_MA1026.2
XX
DE  MA1026.2 ATHB-15; from JASPAR
P0       a     c     g     t
1      265    43   158   124
2      363    31    84   112
3      268    35    60   227
4      212    63   131   184
5      199    46   329    16
6       13    97     0   480
7      518     1    68     3
8      590     0     0     0
9        0     0     0   590
10      27    82   462    19
11     590     0     0     0
12       0     0     0   590
13      10     0   218   362
14     494     1    87     8
15      97   248    63   182
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  KAN1_MA1027.1
XX
ID  KAN1_MA1027.1
XX
DE  MA1027.1 KAN1; from JASPAR
P0       a     c     g     t
1      433   129   237   200
2      259   213   285   242
3      618    16    43   323
4      128   192    58   623
5      898    14    68    20
6       54    87    19   840
7       36   147   169   648
8       63   876    18    44
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  KAN4_MA1028.1
XX
ID  KAN4_MA1028.1
XX
DE  MA1028.1 KAN4; from JASPAR
P0       a     c     g     t
1       11    10   965    14
2      941    12    26    21
3      905     2     2    92
4        2   102     3   893
5      893     3   102     2
6       92     2     2   905
7       21    26    12   941
8       14   965    10    11
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  P0510F09.23_MA1030.1
XX
ID  P0510F09.23_MA1030.1
XX
DE  MA1030.1 P0510F09.23; from JASPAR
P0       a     c     g     t
1      416   113    60   411
2      535    24   369    72
3      416   562     7    15
4       26   962     1    11
5       41   928    27     4
6       10     1     0   989
7      832    32    83    54
8      373   132   389   106
9      266   258   226   250
10     241   256   220   283
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  OJ1581_H09.2_MA1031.1
XX
ID  OJ1581_H09.2_MA1031.1
XX
DE  MA1031.1 OJ1581_H09.2; from JASPAR
P0       a     c     g     t
1      175   131   414   279
2       74   146   335   445
3        0     0   929    71
4        0     0  1000     0
5      127   101   661   112
6      112   661   101   127
7        0  1000     0     0
8       71   929     0     0
9      445   335   146    74
10     279   414   131   175
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DREB1G_MA1032.1
XX
ID  DREB1G_MA1032.1
XX
DE  MA1032.1 DREB1G; from JASPAR
P0       a     c     g     t
1      314   229   229   229
2      119   226   119   535
3      180     0   820     0
4        0   999     0     0
5        0   999     0     0
6        0     0   999     0
7      884     0   115     0
8        0   999     0     0
9      845    22    22   111
10     131   219   221   430
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  OJ1058_F05.8_MA1033.1
XX
ID  OJ1058_F05.8_MA1033.1
XX
DE  MA1033.1 OJ1058_F05.8; from JASPAR
P0       a     c     g     t
1      661   332     3     3
2        3   990     3     3
3      990     3     3     3
4        3   990     3     3
5        3     3   990     3
6        3     3     3   990
7        3     3   990     3
8        5     5   495   495
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Os05g0497200_MA1034.1
XX
ID  Os05g0497200_MA1034.1
XX
DE  MA1034.1 Os05g0497200; from JASPAR
P0       a     c     g     t
1        3   990     3     3
2        3     3   990     3
3        3   661   332     3
4        3   990     3     3
5        3     3   990     3
6        3   990     3     3
7        3   990     3     3
8      495     5   495     5
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP4_MA1035.1
XX
ID  TCP4_MA1035.1
XX
DE  MA1035.1 TCP4; from JASPAR
P0       a     c     g     t
1      112     1   775   112
2        1     1   998     1
3        1     1   998     1
4      831   167     1     1
5        1   998     1     1
6        1   998     1     1
7      997     1     1     1
8        1   996     1     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB111_MA1036.1
XX
ID  MYB111_MA1036.1
XX
DE  MA1036.1 MYB111; from JASPAR
P0       a     c     g     t
1      229     4   757    10
2        5     2   636   357
3        3    12     4   981
4      874     7    10   109
5       18     4   976     2
6        6     8   858   128
7        6     4     5   985
8      525    30   390    55
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB24_MA1037.1
XX
ID  MYB24_MA1037.1
XX
DE  MA1037.1 MYB24; from JASPAR
P0       a     c     g     t
1      293   213   260   235
2      369   158   243   231
3      287    20   596    97
4       25     0   253   722
5       50    60     0   889
6      542   206    60   192
7       53    26   879    42
8       51     5   860    84
9       39   360    19   582
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB3_MA1038.1
XX
ID  MYB3_MA1038.1
XX
DE  MA1038.1 MYB3; from JASPAR
P0       a     c     g     t
1      285   176   279   260
2       59    59   823    59
3       20    20   771   188
4        0     0     0  1000
5      929     0     0    71
6        0     0  1000     0
7        0     0  1000     0
8        0     0     0  1000
9      650    74   202    74
10     273   191   345   191
11     311   230   230   230
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB4_MA1039.1
XX
ID  MYB4_MA1039.1
XX
DE  MA1039.1 MYB4; from JASPAR
P0       a     c     g     t
1      253    36   674    36
2        9     0   766   225
3        0     9     0   991
4      565     9    35   391
5       26     0   974     0
6       18    18   777   188
7       10     0     0   990
8      319    73   536    73
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB46_MA1040.1
XX
ID  MYB46_MA1040.1
XX
DE  MA1040.1 MYB46; from JASPAR
P0       a     c     g     t
1       81     3   911     5
2        3     2   387   608
3        6     7     1   985
4      836    30     6   128
5        4     4   991     2
6        5     3   898    95
7        2    33     1   964
8      580    24   358    38
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB55_MA1041.1
XX
ID  MYB55_MA1041.1
XX
DE  MA1041.1 MYB55; from JASPAR
P0       a     c     g     t
1      980     1    16     3
2       48   946     2     4
3        2   989     2     6
4      227    49    23   701
5      979     1    16     4
6      267   730     1     2
7        2   958     1    39
8      118    54   756    72
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB59_MA1042.1
XX
ID  MYB59_MA1042.1
XX
DE  MA1042.1 MYB59; from JASPAR
P0       a     c     g     t
1      357    14   548    80
2       17     7   114   862
3       31    14     6   949
4      874    39    17    71
5       17     8   956    19
6       24    11   948    18
7       39    40     5   916
8      552   267    23   158
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC083_MA1043.1
XX
ID  NAC083_MA1043.1
XX
DE  MA1043.1 NAC083; from JASPAR
P0       a     c     g     t
1      427   124   124   325
2       25   327    25   623
3     1000     0     0     0
4        0  1000     0     0
5        0     0  1000     0
6        0   598     0   402
7     1000     0     0     0
8     1000     0     0     0
9      126   522   126   226
10     225   325   225   225
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC92_MA1044.1
XX
ID  NAC92_MA1044.1
XX
DE  MA1044.1 NAC92; from JASPAR
P0       a     c     g     t
1      227   227   227   318
2      318   227   227   227
3      559    80   282    80
4       22   935    22    22
5      909     0     0    91
6      430   480     0    91
7        0     0  1000     0
8      224   497    92   186
9      247   607    23   123
10     932    23    23    23
11     274   299   256   170
12     314   229   229   229
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC043_MA1045.1
XX
ID  NAC043_MA1045.1
XX
DE  MA1045.1 NAC043; from JASPAR
P0       a     c     g     t
1      444   185   185   185
2       46    46    46   863
3      442     0    83   475
4      719     0   280     0
5       76   924     0     0
6        0     0  1000     0
7        0   245     0   755
8     1000     0     0     0
9      806    65    65    65
10     310   205   205   280
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NTL9_MA1046.1
XX
ID  NTL9_MA1046.1
XX
DE  MA1046.1 NTL9; from JASPAR
P0       a     c     g     t
1       10    10    10   971
2        5     5     5   985
3      985     5     5     5
4      985     5     5     5
5        5     5   985     5
6        5     5     5   985
7      985     5     5     5
8      985     5     5     5
9       10    10    10   971
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TGA5_MA1047.2
XX
ID  TGA5_MA1047.2
XX
DE  MA1047.2 TGA5; from JASPAR
P0       a     c     g     t
1     1135   511   554   912
2      818   560   537  1197
3      648   382  1574   508
4     1934   540   474   164
5       19    57    10  3026
6       41    26  2769   276
7     3036    11    23    42
8        4  2802    22   284
9      288    16  2804     4
10      28    23    16  3045
11     273  2650   140    49
12    2907     9   147    49
13     206   973   612  1321
14     669  1257   443   743
15    1121   530   603   858
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF018_MA1048.1
XX
ID  ERF018_MA1048.1
XX
DE  MA1048.1 ERF018; from JASPAR
P0       a     c     g     t
1      581     1   412     6
2        1   993     2     4
3        6   990     2     1
4        2     2   993     2
5      718     8    65   208
6        3   991     1     5
7       44   943     1    12
8      735    10    70   184
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF094_MA1049.1
XX
ID  ERF094_MA1049.1
XX
DE  MA1049.1 ERF094; from JASPAR
P0       a     c     g     t
1      346   461   192     0
2        0     0   999     0
3        0   999     0     0
4      122   877     0     0
5       25     0   975     0
6        0   951    49     0
7        0   999     0     0
8      241   104   482   173
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  OsI_08196_MA1050.1
XX
ID  OsI_08196_MA1050.1
XX
DE  MA1050.1 OsI_08196; from JASPAR
P0       a     c     g     t
1        1     1   946    53
2       46     0   953     0
3       91    91   726    91
4        0   863    46    91
5        0   999     0     0
6        0   999     0     0
7      899    50    50     0
8        1   855    72    72
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RAP2-3_MA1051.1
XX
ID  RAP2-3_MA1051.1
XX
DE  MA1051.1 RAP2-3; from JASPAR
P0       a     c     g     t
1      119   232   543   107
2      147   761    33    58
3        7     6   982     5
4        6   746   244     4
5       16   977     4     3
6        6     3   976    15
7      109   780    26    85
8      257   664     6    73
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF109_MA1053.1
XX
ID  ERF109_MA1053.1
XX
DE  MA1053.1 ERF109; from JASPAR
P0       a     c     g     t
1       38   287   661    13
2       50   936     8     6
3        3     5   989     3
4        3   827   168     2
5       13   982     2     3
6        4    13   978     5
7       38   862    13    86
8      207   773     2    18
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARALYDRAFT_897773_MA1054.1
XX
ID  ARALYDRAFT_897773_MA1054.1
XX
DE  MA1054.1 ARALYDRAFT_897773; from JASPAR
P0       a     c     g     t
1       92   278   167   463
2       27    79   491   402
3       36     4   877    83
4       17     7   957    19
5       40   125   826     9
6      756   215     7    22
7        2   949    47     2
8        1   995     2     1
9      862     2   132     4
10      20   907     6    67
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SPL1_MA1055.2
XX
ID  SPL1_MA1055.2
XX
DE  MA1055.2 SPL1; from JASPAR
P0       a     c     g     t
1     1193   663   652  1155
2     1027   707   660  1269
3      422   522   347  2372
4      140    43  3426    54
5       19    49    19  3576
6     3606    30    10    17
7       54  3558    11    40
8      126    50  3382   105
9      227    45  3054   337
10    2954   216    77   416
11     989   773   428  1473
12    1216   736   748   963
13    1188   757   657  1061
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SPL11_MA1056.1
XX
ID  SPL11_MA1056.1
XX
DE  MA1056.1 SPL11; from JASPAR
P0       a     c     g     t
1      229   229   229   314
2      314   229   229   229
3      172   318   172   339
4      147   624    63   166
5        0     0  1000     0
6        0     0     0  1000
7     1000     0     0     0
8        0  1000     0     0
9       21    21   936    21
10      21    21   773   185
11     572    78    78   271
12     188   271   188   353
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SPL12_MA1057.1
XX
ID  SPL12_MA1057.1
XX
DE  MA1057.1 SPL12; from JASPAR
P0       a     c     g     t
1      272   182   211   335
2      143    49   761    47
3        8    38    12   942
4      947    46     5     2
5        5   994     0     1
6       76    78   744   102
7      249    53   538   159
8      278   262   151   310
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SPL4_MA1058.1
XX
ID  SPL4_MA1058.1
XX
DE  MA1058.1 SPL4; from JASPAR
P0       a     c     g     t
1      233   327   198   242
2      208    54   636   102
3       44    83    26   847
4      934    51    11     3
5       44   893     5    57
6      197   106   557   140
7      264   104   414   218
8      252   252   205   290
9      269   258   195   277
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SPL5_MA1059.2
XX
ID  SPL5_MA1059.2
XX
DE  MA1059.2 SPL5; from JASPAR
P0       a     c     g     t
1      244   107   109   138
2      273    73   123   129
3      102   150    89   257
4       25   153    36   384
5        0     0   598     0
6        0     0     0   598
7      598     0     0     0
8        0   598     0     0
9        0     0   598     0
10       0     0   598     0
11     546     8     1    43
12     170   259    19   150
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SPL7_MA1060.1
XX
ID  SPL7_MA1060.1
XX
DE  MA1060.1 SPL7; from JASPAR
P0       a     c     g     t
1       51   777   123    49
2       42    26   912    20
3       43    12    89   856
4      885    62    22    31
5       39   922    14    25
6       65   106   798    31
7      286   208   365   142
8      118   614    41   227
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SPT_MA1061.1
XX
ID  SPT_MA1061.1
XX
DE  MA1061.1 SPT; from JASPAR
P0       a     c     g     t
1      202   427   208   163
2       47   568   255   129
3        0   961     0    39
4      885     0    99    16
5        9   910     0    80
6       88     0   912     0
7        0    12     0   988
8       84   115   724    76
9      221   301   237   242
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP15_MA1062.2
XX
ID  TCP15_MA1062.2
XX
DE  MA1062.2 TCP15; from JASPAR
P0       a     c     g     t
1      280   141   171   235
2      254   182   159   232
3      366   127   128   206
4      286   141   158   242
5       99    43   660    25
6       11    41    10   765
7       77     4   736    10
8        2     1   823     1
9       24     3   788    12
10     317   134   135   241
11      11   785     4    27
12       3   821     1     2
13       8   732     5    82
14     762     6    48    11
15      27   654    46   100
16     246   157   137   287
17     212   133   116   366
18     246   156   170   255
19     237   175   140   275
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP19_MA1063.1
XX
ID  TCP19_MA1063.1
XX
DE  MA1063.1 TCP19; from JASPAR
P0       a     c     g     t
1      148   221   181   450
2      137   151   506   206
3       32     7   846   115
4      112    57   778    53
5      190   283   409   118
6      109   739    22   130
7       13   961     1    25
8        0   999     0     1
9      841    29   128     2
10       0   812   106    81
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP2_MA1064.1
XX
ID  TCP2_MA1064.1
XX
DE  MA1064.1 TCP2; from JASPAR
P0       a     c     g     t
1      217   168   429   186
2       88   194   154   564
3       35     0   844   121
4        0     0   981    19
5       68    78   521   333
6      333   521    78    68
7       19   981     0     0
8      121   844     0    35
9      564   154   194    88
10     186   429   168   217
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP20_MA1065.2
XX
ID  TCP20_MA1065.2
XX
DE  MA1065.2 TCP20; from JASPAR
P0       a     c     g     t
1      328   170   215   257
2      303   228   187   252
3      406   165   156   243
4      332   176   198   264
5      106    52   767    45
6       19    53    12   886
7       77     7   877     9
8        4     1   962     3
9       37     3   914    16
10     379   158   159   274
11      13   915     4    38
12       4   962     0     4
13       9   876     4    81
14     887     8    61    14
15      35   769    60   106
16     358   181   172   259
17     272   145   157   396
18     277   181   215   297
19     271   222   158   319
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP23_MA1066.1
XX
ID  TCP23_MA1066.1
XX
DE  MA1066.1 TCP23; from JASPAR
P0       a     c     g     t
1        5     3   966    26
2       10     5   980     5
3       37    88   835    41
4        7   971     8    13
5        4   984     8     4
6       12   980     3     6
7      954    10    31     5
8       10   959    11    21
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP5_MA1067.1
XX
ID  TCP5_MA1067.1
XX
DE  MA1067.1 TCP5; from JASPAR
P0       a     c     g     t
1        2     2   993     2
2        2     2   995     2
3        2     2   995     2
4      995     2     2     2
5        2   995     2     2
6        2   995     2     2
7      995     2     2     2
8        2   745     2   250
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TGA2_MA1068.2
XX
ID  TGA2_MA1068.2
XX
DE  MA1068.2 TGA2; from JASPAR
P0       a     c     g     t
1      459   220   214   337
2      327   235   211   457
3      290   130   613   197
4      655   258   297    20
5        2     4     3  1221
6        3     1  1152    74
7     1221     3     3     3
8        3  1182     5    40
9       39     2  1188     1
10       1     2     3  1224
11      63  1154     6     7
12    1224     0     5     1
13      28   360   264   578
14     222   573   136   299
15     448   216   231   335
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TGA6_MA1069.2
XX
ID  TGA6_MA1069.2
XX
DE  MA1069.2 TGA6; from JASPAR
P0       a     c     g     t
1      702   297   361   543
2      541   356   324   682
3      449   225   856   373
4     1192   284   298   129
5       21    10     4  1868
6       21    10  1827    45
7     1869     9     9    16
8       18   945    30   910
9       25    24  1846     8
10     108    18    14  1763
11      53  1803    23    24
12    1862     3    23    15
13     111   296   233  1263
14     389   721   331   462
15     702   304   385   512
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TGA7_MA1070.2
XX
ID  TGA7_MA1070.2
XX
DE  MA1070.2 TGA7; from JASPAR
P0       a     c     g     t
1      659   295   313   492
2      453   314   289   703
3      367   183   909   300
4      982   371   352    54
5        7    13     3  1736
6        7     6  1611   135
7     1734     5     8    12
8        5  1651     9    94
9      107     3  1646     3
10       5     7     4  1743
11     114  1593    41    11
12    1709     1    39    10
13      65   509   402   783
14     358   795   208   398
15     658   290   321   490
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF5.3_MA1071.1
XX
ID  DOF5.3_MA1071.1
XX
DE  MA1071.1 DOF5.3; from JASPAR
P0       a     c     g     t
1      247   247   262   244
2      327   213   228   233
3      396   128   146   331
4      842    61    17    81
5      850    79     0    70
6      611   329    14    47
7       66    52   767   116
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  UNE10_MA1074.1
XX
ID  UNE10_MA1074.1
XX
DE  MA1074.1 UNE10; from JASPAR
P0       a     c     g     t
1      214   499    72   214
2        0   999     0     0
3      999     0     0     0
4        0   999     0     0
5        0     0   999     0
6        0     0     0   999
7        0     0   999     0
8      267   533    67   134
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY12_MA1075.1
XX
ID  WRKY12_MA1075.1
XX
DE  MA1075.1 WRKY12; from JASPAR
P0       a     c     g     t
1      172   707    76    46
2       70    33   819    78
3       20    62     5   913
4       10    15    29   946
5        8     6   954    33
6      971     6    10    14
7       29   949     5    16
8       25   722   101   151
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY15_MA1076.2
XX
ID  WRKY15_MA1076.2
XX
DE  MA1076.2 WRKY15; from JASPAR
P0       a     c     g     t
1      403   201   271   365
2      395   254   197   394
3      426   250   201   363
4      489   192   175   384
5      521   127   163   429
6      583    58   515    84
7       15     7  1210     8
8        2     8     3  1227
9        9  1222     2     7
10    1221     8     4     7
11    1218     3    11     8
12      65  1088     8    79
13     100    88   893   159
14     330   246   338   326
15     309   305   189   437
16     350   182   296   412
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY18_MA1077.1
XX
ID  WRKY18_MA1077.1
XX
DE  MA1077.1 WRKY18; from JASPAR
P0       a     c     g     t
1      290   262   204   244
2      258   279   152   312
3      392    16   585     8
4        1     1   988    11
5        0     0     2   997
6        1   997     0     2
7      985     8     2     5
8      964    10    11    16
9      186   372   289   153
10     212   254   398   135
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY2_MA1078.1
XX
ID  WRKY2_MA1078.1
XX
DE  MA1078.1 WRKY2; from JASPAR
P0       a     c     g     t
1      115   393   246   246
2      277    11   713     0
3        0     0  1000     0
4        0     0     0  1000
5        0  1000     0     0
6      961    19    19     0
7      806     0   163    31
8      271   528    72   129
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY21_MA1079.2
XX
ID  WRKY21_MA1079.2
XX
DE  MA1079.2 WRKY21; from JASPAR
P0       a     c     g     t
1       79    51    44    65
2       77    34    54    74
3       87    58    33    61
4      137    42    24    36
5      198     8    17    16
6      203     3    12    21
7      220     1    11     7
8        2     0   236     1
9        0     0     0   239
10       0   238     0     1
11     237     0     1     1
12     239     0     0     0
13      28   197     0    14
14      33    16   153    37
15      66    82    42    49
16      71    71    28    69
17      86    37    43    73
18      89    35    42    73
19      80    40    46    73
20      80    44    49    66
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY23_MA1080.1
XX
ID  WRKY23_MA1080.1
XX
DE  MA1080.1 WRKY23; from JASPAR
P0       a     c     g     t
1      698     1   300     1
2        1     1   998     1
3        1     1     1   998
4        1   998     1     1
5      998     1     1     1
6      998     1     1     1
7      167   831     1     1
8      143   143   712     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY25_MA1081.2
XX
ID  WRKY25_MA1081.2
XX
DE  MA1081.2 WRKY25; from JASPAR
P0       a     c     g     t
1      393   181   277   377
2      416   248   210   354
3      423   236   218   351
4      427   212   129   460
5      328   179   131   590
6      444    24   731    29
7        9     7  1202    10
8        3     7     0  1218
9        4  1217     4     3
10    1215     4     4     5
11    1201     5    13     9
12     130  1002    18    78
13     149    53   927    99
14     348   227   320   333
15     326   259   181   462
16     382   212   254   380
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY30_MA1083.2
XX
ID  WRKY30_MA1083.2
XX
DE  MA1083.2 WRKY30; from JASPAR
P0       a     c     g     t
1     1030   507   432   585
2     1385   269   366   534
3     1530    65   885    74
4       26    11  2498    19
5        7     7     3  2537
6       16  2526     6     6
7     2524     9     5    16
8     2521    11     8    14
9       56  2387    19    92
10     125   176  1992   261
11     556  1009   481   508
12     631   659   316   948
13     731   437   579   807
14     823   440   497   794
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY38_MA1084.1
XX
ID  WRKY38_MA1084.1
XX
DE  MA1084.1 WRKY38; from JASPAR
P0       a     c     g     t
1      152   672    73   104
2       69    28   849    55
3       26    30    21   923
4        8    23    39   930
5        9    16   961    14
6      955    17    10    18
7       16   951    13    20
8       15   679    17   289
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY40_MA1085.2
XX
ID  WRKY40_MA1085.2
XX
DE  MA1085.2 WRKY40; from JASPAR
P0       a     c     g     t
1       89    49    34    62
2      107    34    38    55
3      234     0     0     0
4        0     0   234     0
5        0     0     0   234
6        0   234     0     0
7      230     0     0     4
8      232     0     0     2
9       83    54    46    51
10      76    48    50    60
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY43_MA1086.1
XX
ID  WRKY43_MA1086.1
XX
DE  MA1086.1 WRKY43; from JASPAR
P0       a     c     g     t
1      374   213   169   245
2      509    92   342    58
3       53    18   877    52
4       74    14     4   908
5       87   892    11    11
6      798    62    61    79
7      765    32   131    71
8      256   460   108   176
9      332   196   310   163
10     231   311   218   240
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY45_MA1087.2
XX
ID  WRKY45_MA1087.2
XX
DE  MA1087.2 WRKY45; from JASPAR
P0       a     c     g     t
1      739   377   475   603
2      878   445   339   532
3     1345   339   218   292
4     1914    75    85   120
5     1960    34    51   149
6     2087    16    59    32
7       16     3  2159    16
8        4     5     7  2178
9       12  2166     6    10
10    2160    16     2    16
11    2155     8    15    16
12     721  1211    56   206
13     586   297   936   375
14     708   485   424   577
15     684   451   314   745
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY48_MA1088.1
XX
ID  WRKY48_MA1088.1
XX
DE  MA1088.1 WRKY48; from JASPAR
P0       a     c     g     t
1      262   237   265   235
2      263   231   252   255
3      326   154   410   111
4       21     0   869   110
5       21     0     0   978
6       63   930     1     6
7      662   145    66   127
8      538    76   228   159
9      269   386   155   190
10     234   232   313   221
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY57_MA1089.1
XX
ID  WRKY57_MA1089.1
XX
DE  MA1089.1 WRKY57; from JASPAR
P0       a     c     g     t
1      398   177   202   223
2      408   173   161   258
3      499    76   312   114
4       38    17   870    75
5       30     0     0   969
6       52   914     7    27
7      879    48    23    50
8      817    14   113    56
9      260   508    87   145
10     199   207   386   207
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY60_MA1090.1
XX
ID  WRKY60_MA1090.1
XX
DE  MA1090.1 WRKY60; from JASPAR
P0       a     c     g     t
1      270   278   269   183
2      106   409   106   380
3      136    22   820    22
4        0     0  1000     0
5        0     0     0  1000
6        0  1000     0     0
7     1000     0     0     0
8     1000     0     0     0
9      258   608    67    67
10     145   246   465   145
11     228   228   228   316
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY62_MA1091.1
XX
ID  WRKY62_MA1091.1
XX
DE  MA1091.1 WRKY62; from JASPAR
P0       a     c     g     t
1      126   126     1   748
2      308     1   691     1
3        1     1   998     1
4        1     1     1   998
5        1   998     1     1
6      998     1     1     1
7      998     1     1     1
8      273   726     1     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY63_MA1092.1
XX
ID  WRKY63_MA1092.1
XX
DE  MA1092.1 WRKY63; from JASPAR
P0       a     c     g     t
1      200   371   143   286
2      157     0   843     0
3        0     0   999     0
4        0     0     0   999
5        0   999     0     0
6      999     0     0     0
7      981     0     0    19
8      128   513   231   128
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY75_MA1093.1
XX
ID  WRKY75_MA1093.1
XX
DE  MA1093.1 WRKY75; from JASPAR
P0       a     c     g     t
1      302   279   140   279
2      500    17   483     0
3        0     0  1000     0
4        0     0     0  1000
5        0  1000     0     0
6     1000     0     0     0
7     1000     0     0     0
8      260   620     0   120
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY8_MA1094.2
XX
ID  WRKY8_MA1094.2
XX
DE  MA1094.2 WRKY8; from JASPAR
P0       a     c     g     t
1      278   149   203   230
2      360   154   142   204
3      529   122   103   106
4      736    27    54    43
5      781     9    25    45
6      804     6    42     8
7        4     1   851     4
8        1     1     3   855
9        1   854     2     3
10     851     4     1     4
11     850     3     3     4
12      91   728     3    38
13     155    85   504   116
14     266   226   144   224
15     266   175    90   329
16     293   123   139   305
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARALYDRAFT_495258_MA1095.1
XX
ID  ARALYDRAFT_495258_MA1095.1
XX
DE  MA1095.1 ARALYDRAFT_495258; from JASPAR
P0       a     c     g     t
1        2     2   993     2
2      250     2   745     2
3        2   498   498     2
4        2   993     2     2
5        2   993     2     2
6        2   993     2     2
7      993     2     2     2
8        3   990     3     3
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARALYDRAFT_496250_MA1096.1
XX
ID  ARALYDRAFT_496250_MA1096.1
XX
DE  MA1096.1 ARALYDRAFT_496250; from JASPAR
P0       a     c     g     t
1        3     3   990     3
2        2     2   994     2
3        2     2   994     2
4      994     2     2     2
5        2   994     2     2
6        2   994     2     2
7      994     2     2     2
8        3   990     3     3
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARALYDRAFT_493022_MA1097.1
XX
ID  ARALYDRAFT_493022_MA1097.1
XX
DE  MA1097.1 ARALYDRAFT_493022; from JASPAR
P0       a     c     g     t
1        3     3   990     3
2        3     3   990     3
3        3   332   661     3
4      332   661     3     3
5        3   990     3     3
6        3   990     3     3
7      990     3     3     3
8        5   985     5     5
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARALYDRAFT_484486_MA1098.1
XX
ID  ARALYDRAFT_484486_MA1098.1
XX
DE  MA1098.1 ARALYDRAFT_484486; from JASPAR
P0       a     c     g     t
1        2     2   993     2
2      250     2   745     2
3        2   498   498     2
4        2   993     2     2
5        2   993     2     2
6        2   993     2     2
7      993     2     2     2
8        3   990     3     3
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  JKD_MA1156.1
XX
ID  JKD_MA1156.1
XX
DE  MA1156.1 JKD; from JASPAR
P0       a     c     g     t
1      163   108    67   258
2      161    99    72   264
3      162   101    79   254
4      176    63    75   282
5      145    70    74   307
6       83    30    31   452
7        6    24    13   553
8        0     5     4   587
9        0    10     0   586
10       0     0   596     0
11       0     0    13   583
12      11   564     0    21
13      31    62   331   172
14       9    20    42   525
15      57    99    34   406
16      87     0     0   509
17     134    27     8   427
18      71   228   193   104
19      47   135    39   375
20     143    55   209   189
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NUC_MA1157.1
XX
ID  NUC_MA1157.1
XX
DE  MA1157.1 NUC; from JASPAR
P0       a     c     g     t
1      139   103    87   241
2      141   104    90   235
3      151   109    83   227
4      170    77    84   239
5      127    76    85   282
6       70    47    42   411
7        8    33    19   510
8        0     5     8   557
9        0     8     0   562
10       0     0   570     0
11       0     0     5   565
12       0   570     0     0
13      18    51   381   120
14       3    19    33   515
15      63    89    46   372
16      74     0     3   493
17     148    41    15   366
18      56   183   202   129
19      24   148    55   343
20     131    54   227   158
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MGP_MA1158.1
XX
ID  MGP_MA1158.1
XX
DE  MA1158.1 MGP; from JASPAR
P0       a     c     g     t
1      187   203    50   154
2      363    50   139    42
3      143   183   185    83
4      402    15    50   127
5      512     0     2    80
6      421    26    73    74
7      552    21    20     1
8      153   379    52    10
9        0     0   594     0
10     593     0     1     0
11       0   594     0     0
12     578     0    16     0
13     587     3     4     0
14     566     9    16     3
15     484    28    19    63
16     319    80    56   139
17     285    77    68   164
18     269    72   112   141
19     257    80    94   163
20     263    68   104   159
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SGR5_MA1159.1
XX
ID  SGR5_MA1159.1
XX
DE  MA1159.1 SGR5; from JASPAR
P0       a     c     g     t
1      293   126    40   141
2      343    57   160    40
3      340    48   116    96
4      433     0    44   123
5      310    16    27   247
6      486    22    92     0
7      532     4    22    42
8       33     0   566     1
9      591     6     0     3
10       0   599     1     0
11     592     1     7     0
12     598     1     0     1
13     560    10    11    19
14     428    34    21   117
15     285    83    43   189
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  IDD6_MA1160.1
XX
ID  IDD6_MA1160.1
XX
DE  MA1160.1 IDD6; from JASPAR
P0       a     c     g     t
1       61    36    32   113
2       61    32    34   115
3       60    43    32   107
4       72    27    28   115
5       58    23    28   133
6       25    15    11   191
7        0     6     5   231
8        0     2     0   240
9        0     6     1   235
10       0     1   240     1
11       0     2    15   225
12       2   237     0     3
13      13    21   154    54
14       2     9    14   217
15       7    32    13   190
16      35     0     6   201
17      65     5     1   171
18      21    89   108    24
19       4    66     4   168
20      38    15   117    72
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TSO1_MA1161.1
XX
ID  TSO1_MA1161.1
XX
DE  MA1161.1 TSO1; from JASPAR
P0       a     c     g     t
1      136    15    28    85
2       97     9    28   130
3       71    11     8   174
4       54     6     9   195
5       86    38    15   125
6      160    26    41    37
7      228     0    35     1
8      232     1    16    15
9      244     0     0    20
10       0     8     3   253
11       0     0     0   264
12       0    69     0   195
13     214     0    45     5
14     264     0     0     0
15     264     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCX2_MA1162.1
XX
ID  TCX2_MA1162.1
XX
DE  MA1162.1 TCX2; from JASPAR
P0       a     c     g     t
1      257    49    61   232
2      135    23    28   413
3       60    41     1   497
4       54   173    41   331
5      292   132   107    68
6      559     6    26     8
7      554    14     1    30
8      514     0    14    71
9       10     1     7   581
10       0     0     0   599
11       0   318     0   281
12     435     1   163     0
13     599     0     0     0
14     599     0     0     0
15     178    93    10   318
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHL11_MA1163.2
XX
ID  PHL11_MA1163.2
XX
DE  MA1163.2 PHL11; from JASPAR
P0       a     c     g     t
1      835   310   352   616
2      795   337   453   528
3     1171   237   579   126
4       48    21  1980    64
5     1845   101    16   151
6     2079     3     8    23
7        7     9    51  2046
8     2045    52     9     7
9       23     8     3  2079
10     150    16   100  1847
11      67  1977    20    49
12     128   575   237  1173
13     531   451   337   794
14     619   352   309   833
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HHO5_MA1164.1
XX
ID  HHO5_MA1164.1
XX
DE  MA1164.1 HHO5; from JASPAR
P0       a     c     g     t
1      212   168    57   163
2      264    99   111   126
3      304    39   166    91
4      489    19    29    63
5      597     0     0     3
6        0     0   513    87
7      600     0     0     0
8        2     0     0   598
9       14     1     5   580
10       0   600     0     0
11      79   233    70   218
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HHO6_MA1165.2
XX
ID  HHO6_MA1165.2
XX
DE  MA1165.2 HHO6; from JASPAR
P0       a     c     g     t
1     2662  2443  1581  3909
2     2870  2659  1573  3493
3     4865  1624  1608  2498
4     5692  1493  1372  2038
5    10221    76   107   191
6       19    32 10366   178
7    10410    52    58    75
8      435    16    17 10127
9      332    71   105 10087
10      50 10484    21    40
11    1638  1723   865  6369
12    2496  1775  3352  2972
13    3235  2118  1768  3474
14    3441  1763  1741  3650
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHL12_MA1166.1
XX
ID  PHL12_MA1166.1
XX
DE  MA1166.1 PHL12; from JASPAR
P0       a     c     g     t
1       54    14    23    14
2       67     9    12    17
3       80     3    17     5
4       43    16    44     2
5       42    10    53     0
6        4     0   101     0
7       99     0     1     5
8      105     0     0     0
9        0     0     1   104
10      83    22     0     0
11       0     0     0   105
12       3     0     0   102
13       0    86     0    19
14       7    44    14    40
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  EFM_MA1167.1
XX
ID  EFM_MA1167.1
XX
DE  MA1167.1 EFM; from JASPAR
P0       a     c     g     t
1      316     6   229    49
2        0     0   600     0
3      544     0     4    52
4      600     0     0     0
5        0     0     0   600
6      490   110     0     0
7        1     1     0   598
8       95    70    34   401
9       28   529    12    31
10     138   158   149   155
11     206    79   166   149
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYR2_MA1168.1
XX
ID  MYR2_MA1168.1
XX
DE  MA1168.1 MYR2; from JASPAR
P0       a     c     g     t
1      257    84   115   142
2      315    33   103   147
3      206    62   174   156
4      262    97   239     0
5        0     0   597     1
6      586    10     0     2
7      596     0     0     2
8        0     0     0   598
9      288   310     0     0
10      13     0     0   585
11      85    20    55   438
12     108   241    33   216
13     110   131   102   255
14     146    96   131   225
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB105_MA1169.1
XX
ID  MYB105_MA1169.1
XX
DE  MA1169.1 MYB105; from JASPAR
P0       a     c     g     t
1       36     0    13    46
2       18    21     6    50
3       35    29     3    28
4       35     7    22    31
5       44     5     0    46
6       42     3     8    42
7       79     0     1    15
8        0    95     0     0
9        3    85     7     0
10       0     0    95     0
11       0     0     0    95
12       0     0     1    94
13      80     2     6     7
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB118_MA1170.1
XX
ID  MYB118_MA1170.1
XX
DE  MA1170.1 MYB118; from JASPAR
P0       a     c     g     t
1      135    34    40    69
2      129    35    34    80
3      117    33    32    96
4      107    34    51    86
5       51    80    29   118
6      164     7    84    23
7      146     0    57    75
8       78   178    22     0
9        6   251    19     2
10       0     0   278     0
11       0     0     0   278
12       1     0     0   277
13     278     0     0     0
14       2   270     0     6
15     145     5    78    50
16     123    24    23   108
17     138    22    18   100
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB52_MA1171.1
XX
ID  MYB52_MA1171.1
XX
DE  MA1171.1 MYB52; from JASPAR
P0       a     c     g     t
1       27    39    12    31
2       35    10    29    35
3       35     7    17    50
4       32    16    15    46
5      100     0     0     9
6        0   109     0     0
7        0    95    14     0
8        0     0   109     0
9        0     0     0   109
10       0     0     0   109
11      81     0    10    18
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB3R5_MA1172.1
XX
ID  MYB3R5_MA1172.1
XX
DE  MA1172.1 MYB3R5; from JASPAR
P0       a     c     g     t
1      258    77    97   134
2      306    65    65   130
3      339    21    48   158
4      312    22    19   213
5      203    56    40   267
6      199    79   127   161
7       41    59    50   416
8      196     0   224   146
9      389     4   154    19
10      20   546     0     0
11       0   566     0     0
12       0     0   566     0
13       0     2     0   564
14       4     0     0   562
15     238     0   291    37
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB101_MA1173.1
XX
ID  MYB101_MA1173.1
XX
DE  MA1173.1 MYB101; from JASPAR
P0       a     c     g     t
1      327    82    86   105
2      369    53    70   108
3      198   249    47   106
4      140   261    44   155
5      180   120   188   112
6      160    51    47   342
7      596     2     1     1
8      598     0     2     0
9        0   600     0     0
10      54   424    49    73
11      86     4   510     0
12     367    43    27   163
13     407    72     2   119
14     206   112    22   260
15     139   181    68   212
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB56_MA1174.1
XX
ID  MYB56_MA1174.1
XX
DE  MA1174.1 MYB56; from JASPAR
P0       a     c     g     t
1      236    49   114   201
2      169    73    78   280
3      265   106    71   158
4      270    80   103   147
5      333    91    45   131
6        1     3     0   596
7      600     0     0     0
8      600     0     0     0
9        0   600     0     0
10      26    35   417   122
11     143     0   457     0
12     223    41     2   334
13     237   143    13   207
14     194    58    25   323
15     133   135    37   295
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB81_MA1175.1
XX
ID  MYB81_MA1175.1
XX
DE  MA1175.1 MYB81; from JASPAR
P0       a     c     g     t
1      149   163   223    65
2       30    99     0   471
3      600     0     0     0
4      600     0     0     0
5        0   600     0     0
6      118   234   114   134
7       94     0   506     0
8      268   102     0   230
9      264    95     1   240
10     124    59     7   410
11     113   180    39   268
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB119_MA1176.1
XX
ID  MYB119_MA1176.1
XX
DE  MA1176.1 MYB119; from JASPAR
P0       a     c     g     t
1       93   179    78   248
2      287    27   188    96
3      252     0   117   229
4      173   392    33     0
5       16   514    68     0
6        0     0   598     0
7        0     0     0   598
8        0     1     0   597
9      576     0     1    21
10      13   506     7    72
11     316    36   155    91
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB65_MA1177.1
XX
ID  MYB65_MA1177.1
XX
DE  MA1177.1 MYB65; from JASPAR
P0       a     c     g     t
1      284    61   105   149
2      129   250    41   179
3      105   218    65   211
4      183   121   205    90
5       75    75     0   449
6      599     0     0     0
7      599     0     0     0
8        0   599     0     0
9       38   354   118    89
10      76     0   523     0
11     178   153     4   264
12     280   219     2    98
13     266    57    75   201
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB3R1_MA1178.2
XX
ID  MYB3R1_MA1178.2
XX
DE  MA1178.2 MYB3R1; from JASPAR
P0       a     c     g     t
1     1259   682   474  1142
2      999   634   511  1413
3      761  1321   593   882
4      299   745    70  2443
5     3492    10    24    31
6     3524     9    10    14
7       19  3491     9    38
8       47    58  3410    42
9       64    18  3254   221
10     169   193    62  3133
11     694  1927   146   790
12    1795   473   582   707
13    1266   656   515  1120
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB1_MA1179.1
XX
ID  MYB1_MA1179.1
XX
DE  MA1179.1 MYB1; from JASPAR
P0       a     c     g     t
1       48    10    47    39
2       35    17    11    81
3       48     8    26    62
4       95     1    32    16
5        0   144     0     0
6        3    89    52     0
7        0     0   144     0
8        0     0     0   144
9        0     0     0   144
10      97     0     8    39
11      73    16    19    36
12      75    13    18    38
13      44    11    45    44
14      41    36    20    47
15      32    35    15    62
16      19    38    65    22
17      27    25    29    63
18      28    23    17    76
19      60     3    39    42
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB3R4_MA1180.1
XX
ID  MYB3R4_MA1180.1
XX
DE  MA1180.1 MYB3R4; from JASPAR
P0       a     c     g     t
1      261    91    80   163
2      289    48    88   170
3      311    33    31   220
4      180    69    52   294
5      235   102   116   142
6       27    71    41   456
7      258     2   225   110
8      388     1   180    26
9       29   566     0     0
10       0   595     0     0
11       0     0   595     0
12       0     0     0   595
13       0     0     0   595
14     262     3   301    29
15     151   115   202   127
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB113_MA1181.1
XX
ID  MYB113_MA1181.1
XX
DE  MA1181.1 MYB113; from JASPAR
P0       a     c     g     t
1      257    41   130   158
2      426     5    46   109
3      132     0   290   164
4      102     0    44   440
5        1   299     0   286
6      259   195    54    78
7        0     0   586     0
8        0     0     3   583
9        0     0     0   586
10     548     0    38     0
11      40   139    89   318
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RVE8_MA1182.1
XX
ID  RVE8_MA1182.1
XX
DE  MA1182.1 RVE8; from JASPAR
P0       a     c     g     t
1      490     8    38    57
2        0     0   593     0
3      593     0     0     0
4        0     0     0   593
5      590     0     0     3
6        0     2     0   591
7        0     0     0   593
8       50     2     0   541
9      115    44    35   399
10     132   111    92   258
11     167    73   133   220
12     145    96   123   229
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RVE6_MA1183.1
XX
ID  RVE6_MA1183.1
XX
DE  MA1183.1 RVE6; from JASPAR
P0       a     c     g     t
1      521     2    44    29
2        0     0   596     0
3      596     0     0     0
4        0     0     0   596
5      579     1     0    16
6        0     2     0   594
7        0     0     0   596
8       40     0     1   555
9      113    36    20   427
10      90   109    92   305
11     134    91   141   230
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RVE1_MA1184.1
XX
ID  RVE1_MA1184.1
XX
DE  MA1184.1 RVE1; from JASPAR
P0       a     c     g     t
1      234   117   106   143
2      203   147   101   149
3      237   124   142    97
4      332    42    58   168
5      526     0     0    74
6      600     0     0     0
7      586     5     4     5
8        4     0    10   586
9      600     0     0     0
10       0     0     0   600
11       0   600     0     0
12      85    42    12   461
13     205   103    77   215
14     254   130    87   129
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  LHY_MA1185.1
XX
ID  LHY_MA1185.1
XX
DE  MA1185.1 LHY; from JASPAR
P0       a     c     g     t
1      512     1    31    55
2        0     0   599     0
3      599     0     0     0
4        0     0     0   599
5      597     2     0     0
6        0     8     5   586
7        3     0     3   593
8       51     6     0   542
9      172    46    34   347
10     113   144   117   225
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YAB4_MA1186.1
XX
ID  YAB4_MA1186.1
XX
DE  MA1186.1 YAB4; from JASPAR
P0       a     c     g     t
1      189   152    84   174
2      199   103    62   235
3      162   112    63   262
4      325    65    54   155
5      330    20    31   218
6       14   321    31   233
7       89   428    67    15
8       18    12     0   569
9       24    54     0   521
10     598     0     0     1
11       0     0     0   599
12       0   599     0     0
13      81   279     4   235
14     243    49    47   260
15     194   202    24   179
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RVE4_MA1187.1
XX
ID  RVE4_MA1187.1
XX
DE  MA1187.1 RVE4; from JASPAR
P0       a     c     g     t
1      526     0    24    11
2        0     0   558     3
3      561     0     0     0
4        0     1     0   560
5      548     1     0    12
6        0     1     4   556
7        0     1     0   560
8       16     0     0   545
9       79    25    10   447
10      95   100    93   273
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  At3g11280_MA1188.1
XX
ID  At3g11280_MA1188.1
XX
DE  MA1188.1 At3g11280; from JASPAR
P0       a     c     g     t
1      361    46    60   115
2      260    16    28   278
3       29   326    34   193
4      117   360    64    41
5        0    17     0   565
6        4    93     0   485
7      582     0     0     0
8        0     3     6   573
9        0   582     0     0
10     120   144    22   296
11     122    63    52   345
12     189   195    51   147
13     290    15    28   249
14     112   155    30   285
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT5G61620_MA1189.1
XX
ID  AT5G61620_MA1189.1
XX
DE  MA1189.1 AT5G61620; from JASPAR
P0       a     c     g     t
1      205    82   149   163
2      257    18    68   256
3      208    10    10   371
4      175    30   112   282
5      114    58     6   421
6       96     0   503     0
7        0     0   599     0
8      599     0     0     0
9        0     0     0   599
10     559    16     9    15
11     512     0    53    34
12     151    47   310    91
13     264    55   191    89
14     185    71    35   308
15     179    52    62   306
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RVE5_MA1190.1
XX
ID  RVE5_MA1190.1
XX
DE  MA1190.1 RVE5; from JASPAR
P0       a     c     g     t
1      533     4    36    26
2        0     0   599     0
3      599     0     0     0
4        0     0     0   599
5      551     0     0    48
6        0     1     0   598
7        1     0     0   598
8       27     0     0   572
9      116    33    26   424
10      98   107    83   311
11     153   105   120   221
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RVE7L_MA1191.1
XX
ID  RVE7L_MA1191.1
XX
DE  MA1191.1 RVE7L; from JASPAR
P0       a     c     g     t
1      169   143    95   192
2      234   129    93   143
3      207   147    93   152
4      226   120   147   106
5      334    54    51   160
6      540     1     5    53
7      599     0     0     0
8      596     0     3     0
9        0     0     3   596
10     599     0     0     0
11       0     0     0   599
12       0   599     0     0
13     108    78    15   398
14     199   103    79   218
15     287   103    91   118
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DIV1_MA1192.1
XX
ID  DIV1_MA1192.1
XX
DE  MA1192.1 DIV1; from JASPAR
P0       a     c     g     t
1      246    27    25   282
2      187    65   142   186
3      218    47    57   258
4      229     7   313    31
5        0     0   575     5
6      580     0     0     0
7        0     0     0   580
8      579     0     0     1
9      578     0     1     1
10      25    53   443    59
11     226    36   259    59
12     156    62    41   321
13     153    55    65   307
14     232    53   108   187
15     188    63   131   198
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT2G38090_MA1193.1
XX
ID  AT2G38090_MA1193.1
XX
DE  MA1193.1 AT2G38090; from JASPAR
P0       a     c     g     t
1       30    13    11    10
2       38    10     8     8
3       28    10    12    14
4       32     1    21    10
5       24     2    18    20
6       34     1     0    29
7       17     9    13    25
8       26     7     0    31
9       20     0    42     2
10       0     0    64     0
11      64     0     0     0
12       0     0     1    63
13      62     1     1     0
14      63     0     0     1
15      12     3    40     9
16      18    10    24    12
17      42     4     3    15
18       7     3     7    47
19      26     9    24     5
20      30     6    18    10
21      21     0    24    19
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT3G10580_MA1194.1
XX
ID  AT3G10580_MA1194.1
XX
DE  MA1194.1 AT3G10580; from JASPAR
P0       a     c     g     t
1       64    14    33    29
2       61     2    31    46
3       61     4     2    73
4       43    14    30    53
5       65    12    11    52
6       48     5    76    11
7        0     0   132     8
8      139     1     0     0
9        4     0     0   136
10     137     0     0     3
11     138     0     1     1
12      17     6    97    20
13      41    37    53     9
14      63     0     0    77
15      12    22    12    94
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT5G56840_MA1195.1
XX
ID  AT5G56840_MA1195.1
XX
DE  MA1195.1 AT5G56840; from JASPAR
P0       a     c     g     t
1      335    45    57   163
2      377    20    72   131
3       47   324    37   192
4      114   358    59    69
5       11    24     2   563
6       13    19     7   561
7      594     3     0     3
8        0     2     1   597
9        0   600     0     0
10       6   481     0   113
11     432     6    47   115
12     224    86    30   260
13     333     4    10   253
14     254    68    25   253
15     197   147    88   168
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT5G05790_MA1196.1
XX
ID  AT5G05790_MA1196.1
XX
DE  MA1196.1 AT5G05790; from JASPAR
P0       a     c     g     t
1      319    79    59   130
2      295     2    33   257
3       16   340    40   191
4      139   348    67    33
5        0    22     0   565
6        7   112    15   453
7      587     0     0     0
8        0     0     0   587
9        0   587     0     0
10     133   164    15   275
11     119    70    57   341
12     131   200   116   140
13     351     0    26   210
14      92   119    37   339
15     150   256    41   140
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CAMTA1_MA1197.1
XX
ID  CAMTA1_MA1197.1
XX
DE  MA1197.1 CAMTA1; from JASPAR
P0       a     c     g     t
1      301    56   117   124
2      430    46    45    77
3      405     6   132    55
4      172   140   285     1
5        0   598     0     0
6        0     0   598     0
7        0   578     0    20
8        0     0   598     0
9        0     0     3   595
10      63    58   326   151
11     235    33   156   174
12     237    58   170   133
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HAT2_MA1198.1
XX
ID  HAT2_MA1198.1
XX
DE  MA1198.1 HAT2; from JASPAR
P0       a     c     g     t
1      300    23    43   232
2      110   186   112   190
3        0   249     0   349
4      598     0     0     0
5      598     0     0     0
6        0     0     0   598
7        0   541     0    57
8      598     0     0     0
9      113    12     0   473
10     114    42    36   406
11     229    52   131   186
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AGL16_MA1199.1
XX
ID  AGL16_MA1199.1
XX
DE  MA1199.1 AGL16; from JASPAR
P0       a     c     g     t
1       35    19     4   360
2       11     0     3   404
3      100    11    28   279
4        0   418     0     0
5        0   357     0    61
6      123    83    23   189
7       66    77    40   235
8      131     0     0   287
9        0     4     4   410
10      44    59    23   292
11     109    35   135   139
12      62     0   356     0
13       0     0   393    25
14     210    44    12   152
15     358    15     8    37
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AGL42_MA1201.1
XX
ID  AGL42_MA1201.1
XX
DE  MA1201.1 AGL42; from JASPAR
P0       a     c     g     t
1        0   146     0     0
2      146     0     0     0
3        0    12     0   134
4        0   146     0     0
5      146     0     0     0
6        0     0     0   146
7       17    73    16    40
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AGL55_MA1202.1
XX
ID  AGL55_MA1202.1
XX
DE  MA1202.1 AGL55; from JASPAR
P0       a     c     g     t
1        0     5     4    81
2        0    90     0     0
3       90     0     0     0
4        0    90     0     0
5        0    90     0     0
6       90     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AGL63_MA1203.1
XX
ID  AGL63_MA1203.1
XX
DE  MA1203.1 AGL63; from JASPAR
P0       a     c     g     t
1       91     6    39   461
2       94    33   194   276
3        8   579     0    10
4       10   544     0    43
5      444    35    81    37
6      457     9    33    98
7      542     0     0    55
8      456     1     1   139
9      412    28    27   130
10     102   100    24   371
11      64     0   522    11
12       4     0   591     2
13     282   235    20    60
14     546    13     2    36
15     473    10    28    86
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AGL13_MA1204.1
XX
ID  AGL13_MA1204.1
XX
DE  MA1204.1 AGL13; from JASPAR
P0       a     c     g     t
1       59     3     0   157
2        9     0     2   208
3      123     2    25    69
4        2   217     0     0
5        0   161     0    58
6      141    36     7    35
7      169     2     7    41
8      219     0     0     0
9      174     0     0    45
10     174     8     6    31
11      81    12    15   111
12      74     0   145     0
13       0     0   217     2
14     120    15     5    79
15     209     3     0     7
16     191     0     4    24
17     126    14    41    38
18      90    19    20    90
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AGL6_MA1205.1
XX
ID  AGL6_MA1205.1
XX
DE  MA1205.1 AGL6; from JASPAR
P0       a     c     g     t
1       83     8     5   386
2       72     1     8   401
3      200     8    67   207
4       11   465     5     1
5        1   394     0    87
6      253    92    39    98
7      347     8    42    85
8      467     0     2    13
9      422     0     0    60
10     385    18    23    56
11     206    49    34   193
12     128     0   353     1
13       0     0   482     0
14     295    71    13   103
15     441     4     3    34
16     432     4     9    37
17     247    61   113    61
18     256    27    46   153
19     237    41    26   178
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF2_MA1206.1
XX
ID  ARF2_MA1206.1
XX
DE  MA1206.1 ARF2; from JASPAR
P0       a     c     g     t
1      245   116    50   188
2      151    61   193   194
3        0   395   204     0
4      200   384    15     0
5      112     0   487     0
6      559     0    40     0
7        0   599     0     0
8      562    37     0     0
9      271    57   131   140
10     219    87   207    86
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GT-3a_MA1207.1
XX
ID  GT-3a_MA1207.1
XX
DE  MA1207.1 GT-3a; from JASPAR
P0       a     c     g     t
1      145    72    96    58
2      155    69    98    49
3       11   360     0     0
4      371     0     0     0
5        0   328     0    43
6        0     0   371     0
7        0     0     0   371
8        0     0   324    47
9       54    40    76   201
10     106    82    57   126
11     183    15    40   133
12     204    66    14    87
13     259     4     0   108
14     107    64     4   196
15     130     3   104   134
16      80    42    36   213
17      91    42   140    98
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GT-2_MA1208.1
XX
ID  GT-2_MA1208.1
XX
DE  MA1208.1 GT-2; from JASPAR
P0       a     c     g     t
1      171    56    62   309
2      111    56    29   402
3      181     0     0   417
4       35    13    21   529
5        0     0     0   598
6      128    35     0   435
7      598     0     0     0
8        0   593     0     5
9       40   378     4   176
10     136    36   295   131
11      33   186    25   354
12     182   107    95   214
13     173    52   165   208
14     112    97   105   284
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ATHB-20_MA1209.1
XX
ID  ATHB-20_MA1209.1
XX
DE  MA1209.1 ATHB-20; from JASPAR
P0       a     c     g     t
1       58    59    23    90
2       30   115     2    83
3      230     0     0     0
4      230     0     0     0
5        0     0     0   230
6      218     0     9     3
7      230     0     0     0
8        0     0     0   230
9        0     1     0   229
10      71    19   110    30
11     131    27    31    41
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HAT22_MA1210.2
XX
ID  HAT22_MA1210.2
XX
DE  MA1210.2 HAT22; from JASPAR
P0       a     c     g     t
1     1122   540   543  1011
2      950   791   488   987
3      362  2470    86   298
4     3066    41    21    88
5     3127    25     6    58
6       55    23    15  3123
7     2276   309   309   322
8     3128    14    26    48
9       52     9    18  3137
10      83    20    78  3035
11     259    92  2650   215
12    1122   406   761   927
13    1120   459   607  1030
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ATHB-X_MA1211.1
XX
ID  ATHB-X_MA1211.1
XX
DE  MA1211.1 ATHB-X; from JASPAR
P0       a     c     g     t
1      103   191   105   196
2        0   373     0   222
3      584    11     0     0
4      595     0     0     0
5        0     0     0   595
6       45   308     5   237
7      595     0     0     0
8        0     0     0   595
9       27     8    13   547
10     219    15   234   127
11     114    73   216   192
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ATHB-13_MA1212.1
XX
ID  ATHB-13_MA1212.1
XX
DE  MA1212.1 ATHB-13; from JASPAR
P0       a     c     g     t
1      137   147    70   246
2       90   295     0   215
3      600     0     0     0
4      599     0     0     1
5        0     0     0   600
6      484   109     1     6
7      600     0     0     0
8        0     2     0   598
9       63     2    48   487
10     201    70   187   142
11     269    62   109   160
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ZHD3_MA1213.2
XX
ID  ZHD3_MA1213.2
XX
DE  MA1213.2 ZHD3; from JASPAR
P0       a     c     g     t
1     2877  1041  1101  2478
2     2259  1751   784  2703
3      705  5659   189   944
4     7125   133    26   213
5     7415    23     5    54
6       31    19     8  7439
7     1160   247   247  5843
8     7442     7    14    34
9       39    11    29  7418
10     129    30    67  7271
11     897   148  5524   928
12    2341   824  1964  2368
13    2244  1209   943  3101
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ATHB-40_MA1214.1
XX
ID  ATHB-40_MA1214.1
XX
DE  MA1214.1 ATHB-40; from JASPAR
P0       a     c     g     t
1      233    93   118   156
2      213   169    95   123
3      404    25    68   103
4      111   325    68    96
5       21   530     0    49
6      600     0     0     0
7      600     0     0     0
8        0     0     0   600
9      388    18    25   169
10     600     0     0     0
11       0     0     0   600
12      56     5    41   498
13      92     9   397   102
14     255    60   141   144
15     221   104   126   149
16     171   145   133   151
17     192   159    79   170
18     206   121    56   217
19     216    67    70   247
20     261    64    54   221
21     221    85    51   243
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ATHB-53_MA1215.1
XX
ID  ATHB-53_MA1215.1
XX
DE  MA1215.1 ATHB-53; from JASPAR
P0       a     c     g     t
1      163   246    66   124
2      108   405     0    86
3      599     0     0     0
4      599     0     0     0
5        0     0     0   599
6      563     0    14    22
7      599     0     0     0
8        0     0     0   599
9       34     0    34   531
10     130    29   380    60
11     241    57   146   155
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DREB1D_MA1218.1
XX
ID  DREB1D_MA1218.1
XX
DE  MA1218.1 DREB1D; from JASPAR
P0       a     c     g     t
1      113   162    78   244
2      103   130   190   174
3      410    17    72    98
4       34   109    52   402
5        0     0   597     0
6        0     0     0   597
7        0   597     0     0
8        0     0   597     0
9       21     0   576     0
10       0   311     0   286
11     200    95   245    57
12     274    65   165    93
13     214   152    88   143
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF011_MA1219.2
XX
ID  ERF011_MA1219.2
XX
DE  MA1219.2 ERF011; from JASPAR
P0       a     c     g     t
1      732   765   349   600
2      811   393   524   718
3      412   807   326   901
4      139  2130    96    81
5     2305     7    98    36
6       13  2411    10    12
7        8  2427     5     6
8       23    12  2401    10
9     2115    74    47   210
10      10  2394    14    28
11     841  1106   275   224
12    1197   437   268   544
13     733   629   367   717
14     749   508   526   663
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TINY_MA1220.1
XX
ID  TINY_MA1220.1
XX
DE  MA1220.1 TINY; from JASPAR
P0       a     c     g     t
1      146   226    66   158
2      148    98   141   209
3       55   320    53   168
4       41   432    23   100
5      408     0   186     2
6        0   596     0     0
7        0   596     0     0
8        0     0   596     0
9      534    33     0    29
10       0   596     0     0
11     485     2    95    14
12     212   189    82   113
13     203   127    64   202
14     195    66   165   170
15     154   173    95   174
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RAP2-6_MA1221.1
XX
ID  RAP2-6_MA1221.1
XX
DE  MA1221.1 RAP2-6; from JASPAR
P0       a     c     g     t
1      103   139    18   337
2       64    22   326   185
3      187    11   381    18
4        1   574     0    22
5        0     0   597     0
6        1    15   581     0
7        8   567     0    22
8        0     3   579    15
9       15    60   511    11
10     197   310    14    76
11      67     9   454    67
12     146    47   353    51
13     200   173    49   175
14     114    32   337   114
15     107    62   346    82
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF014_MA1222.1
XX
ID  ERF014_MA1222.1
XX
DE  MA1222.1 ERF014; from JASPAR
P0       a     c     g     t
1       61   342    52   140
2       80   395    40    80
3      235    57   139   164
4       76   361    49   109
5       78   405    33    79
6      143    25   206   221
7       23   446    39    87
8        1   592     0     2
9      511     0    73    11
10       0   595     0     0
11       0   595     0     0
12      27     0   568     0
13     164   307     9   115
14       0   591     0     4
15     351     2   137   105
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF035_MA1223.1
XX
ID  ERF035_MA1223.1
XX
DE  MA1223.1 ERF035; from JASPAR
P0       a     c     g     t
1      200    65   177   143
2      197    88   122   178
3      198   104    72   211
4      236    41    75   233
5       74    76   213   222
6        3    16     0   566
7        0     0   585     0
8        0     0     0   585
9        0   585     0     0
10       0     0   585     0
11       0     0   585     0
12       0    78     0   507
13      56    19   472    38
14     161    71   323    30
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF5_MA1225.1
XX
ID  ERF5_MA1225.1
XX
DE  MA1225.1 ERF5; from JASPAR
P0       a     c     g     t
1       59   369    38   114
2       88   385    14    93
3      214    42   180   144
4       32   360    72   116
5       58   494     5    23
6       81     0   370   129
7        3   529    32    16
8       16   564     0     0
9       68     0   503     9
10       4   537    20    19
11       4   576     0     0
12      73     0   465    42
13       3   418    17   142
14      55   424    29    72
15     238    17   219   106
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DREB2G_MA1226.1
XX
ID  DREB2G_MA1226.1
XX
DE  MA1226.1 DREB2G; from JASPAR
P0       a     c     g     t
1       95    41   357    94
2       88    54   371    74
3      137   179    58   213
4       84    49   312   142
5       98    42   386    61
6      304   133    12   138
7      185     0   395     7
8       34     0   531    22
9       21   509     0    57
10       0     0   587     0
11       0     0   587     0
12      25   156     0   406
13       0     0   584     3
14      54    38   491     4
15     237   156    50   144
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF027_MA1227.2
XX
ID  ERF027_MA1227.2
XX
DE  MA1227.2 ERF027; from JASPAR
P0       a     c     g     t
1     2281  2065  1253  2566
2     2124  1440  1886  2715
3     1537  1971   999  3658
4     1018  2217   799  4131
5     2993   257  4656   259
6       59  7829    39   238
7       49  8069    15    32
8       38    30  8067    30
9     7473   249    83   360
10      27  8089    22    27
11    7176   100   562   327
12    2884  1583  1063  2635
13    2750  1629  1362  2424
14    2824  1225  1823  2293
15    2512  1635  1448  2570
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF091_MA1228.1
XX
ID  ERF091_MA1228.1
XX
DE  MA1228.1 ERF091; from JASPAR
P0       a     c     g     t
1      187    52   263    35
2       78   289     0   170
3        0     0   534     3
4       34     0   503     0
5        0   526     0    11
6        0     0   537     0
7        0     0   537     0
8       13   505     0    19
9        6     7   472    52
10      85    69   378     5
11     179   205    32   121
12      91    25   337    84
13     128    48   288    73
14     124   221    72   120
15     104    50   299    84
16     101    68   290    78
17     127   182    83   145
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF034_MA1229.1
XX
ID  ERF034_MA1229.1
XX
DE  MA1229.1 ERF034; from JASPAR
P0       a     c     g     t
1      176   154    66   164
2      192    55   134   179
3       91    89   184   196
4       25    85     2   448
5        0     0   560     0
6       18     0    14   528
7        0   560     0     0
8        0     0   560     0
9        0     0   560     0
10       0   114     0   446
11      74    19   434    33
12     154    71   287    48
13     178   150    71   161
14     145    63   217   135
15     151    78   164   167
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF037_MA1230.1
XX
ID  ERF037_MA1230.1
XX
DE  MA1230.1 ERF037; from JASPAR
P0       a     c     g     t
1       47   307    65   179
2       37   446    31    84
3      443     0   155     0
4        0   598     0     0
5        0   598     0     0
6        0     0   598     0
7      589     3     0     6
8        0   598     0     0
9      529     0    62     7
10     232   199    70    97
11     229   110    39   220
12     188    79   112   219
13     154   142   101   201
14     136   213    62   187
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF15_MA1231.2
XX
ID  ERF15_MA1231.2
XX
DE  MA1231.2 ERF15; from JASPAR
P0       a     c     g     t
1      404   837   296   596
2      487   841   178   627
3      442   248   375  1068
4      206   731   280   916
5     1010   942    94    87
6       30     7  2078    18
7        3  2124     1     5
8       59  2062     3     9
9       51     3  2067    12
10      14  2066    14    39
11      10  2107     8     8
12     579    62   911   581
13     214   929   236   754
14     509   695   348   581
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF054_MA1232.1
XX
ID  ERF054_MA1232.1
XX
DE  MA1232.1 ERF054; from JASPAR
P0       a     c     g     t
1       11     3   546     7
2      153    21   199   194
3        1   564     0     2
4        0     0   567     0
5        0     2   565     0
6        0   213     0   354
7       23    12   484    48
8       77    96   374    20
9      213   190    40   124
10      99    30   330   108
11     179    62   236    90
12     142   178    85   162
13     108    71   275   113
14     132    82   264    89
15     115   158    93   201
16     125    83   282    77
17     121    92   250   104
18     115   153   110   189
19     121    97   245   104
20     144    85   231   107
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF021_MA1233.2
XX
ID  ERF021_MA1233.2
XX
DE  MA1233.2 ERF021; from JASPAR
P0       a     c     g     t
1      723  1062   509   865
2      777  1231   362   789
3      756   363  1265   775
4      424  1691   388   656
5      186  2340   102   531
6      889    15  2078   177
7       38  3024    76    21
8       19  3113    14    13
9       56     8  3076    19
10    2213   500   104   342
11      27  3048    44    40
12    2867    54   158    80
13    1271   837   299   752
14    1020   831   545   763
15    1024   461   885   789
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF017_MA1234.1
XX
ID  ERF017_MA1234.1
XX
DE  MA1234.1 ERF017; from JASPAR
P0       a     c     g     t
1      217    45   183   154
2      127    34    53   385
3       47    19   403   130
4        0     0   599     0
5      291    79    42   187
6        0   599     0     0
7        0     0   599     0
8        0     0   599     0
9       43   185     0   371
10      27     9   524    39
11     160    72   306    61
12     195   157    84   163
13     136    46   292   125
14     192    83   226    98
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AIL7_MA1235.1
XX
ID  AIL7_MA1235.1
XX
DE  MA1235.1 AIL7; from JASPAR
P0       a     c     g     t
1       11   515     1    12
2      114    17   380    28
3      182   103   158    96
4      227    46    30   236
5       86    40    11   402
6       93   249     2   195
7       33   308     0   198
8        0   539     0     0
9      133    18   353    35
10     517     0     0    22
11      32     6   403    98
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF003_MA1236.1
XX
ID  ERF003_MA1236.1
XX
DE  MA1236.1 ERF003; from JASPAR
P0       a     c     g     t
1      148   219   100   129
2       96    48   362    90
3      113    65   368    50
4      186   185    64   161
5      146    26   307   117
6      114    30   331   121
7      178   153    28   237
8       90     7   374   125
9      146    17   385    48
10      12   543     0    41
11       0     0   596     0
12       1     3   592     0
13      29   400     0   167
14       0     0   587     9
15      15    45   524    12
16     270   211    19    96
17      46     9   501    40
18     146    69   347    34
19     233    98    74   191
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF038_MA1238.2
XX
ID  ERF038_MA1238.2
XX
DE  MA1238.2 ERF038; from JASPAR
P0       a     c     g     t
1      575   277   497   561
2      259   811   246   594
3      123  1622    64   101
4     1691     0   197    22
5        7  1892     4     7
6       11  1895     1     3
7       17     5  1882     6
8     1631   120    30   129
9       16  1883     4     7
10    1588    35   202    85
11     673   562   280   395
12     621   465   272   552
13     608   277   494   531
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF104_MA1239.1
XX
ID  ERF104_MA1239.1
XX
DE  MA1239.1 ERF104; from JASPAR
P0       a     c     g     t
1       78   299    75   105
2      151    96   194   116
3       81   317    58   101
4       97   309    56    95
5      165    61   207   124
6       67   293    56   141
7       86   363    33    75
8      125    50   213   169
9       22   369    76    90
10      82   464     6     5
11      36     0   496    25
12       0   555     2     0
13       0   557     0     0
14      27     0   527     3
15       2   466     0    89
16       4   553     0     0
17     214     0   281    62
18      40   274    51   192
19     130   218    60   149
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF10_MA1240.1
XX
ID  ERF10_MA1240.1
XX
DE  MA1240.1 ERF10; from JASPAR
P0       a     c     g     t
1      146   117   138   170
2       79   288    74   130
3      110   273    68   120
4      177    88   177   129
5      107   286    74   104
6       84   311    56   120
7      139    96   175   161
8       90   283    49   149
9      115   324    10   122
10     151    64   173   183
11      35   342    75   119
12      23   538     6     4
13      38     0   448    85
14       0   571     0     0
15       0   571     0     0
16      27     0   544     0
17       5   511     0    55
18      34   528     0     9
19     195     0   323    53
20      44   310    42   175
21     143   182    86   160
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF043_MA1241.1
XX
ID  ERF043_MA1241.1
XX
DE  MA1241.1 ERF043; from JASPAR
P0       a     c     g     t
1      174   184    82   151
2      169    81   153   188
3       29   352    83   127
4       18   537    14    22
5      540     0    51     0
6        0   591     0     0
7        0   591     0     0
8        0     0   591     0
9      588     0     0     3
10       0   591     0     0
11     567     0    23     1
12     215   202    81    93
13     222    97    38   234
14     197    86    97   211
15     161   138    96   196
16     137   191    73   190
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DREB2F_MA1242.1
XX
ID  DREB2F_MA1242.1
XX
DE  MA1242.1 DREB2F; from JASPAR
P0       a     c     g     t
1       39   309    39    91
2       48   347    16    67
3      118    11   217   132
4        0   458     6    14
5        1   477     0     0
6      347     0   131     0
7        0   478     0     0
8        0   475     0     3
9       25     0   453     0
10      21   438     0    19
11      20   346     2   110
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DREB2E_MA1243.1
XX
ID  DREB2E_MA1243.1
XX
DE  MA1243.1 DREB2E; from JASPAR
P0       a     c     g     t
1      132   194   106   165
2      202   102   118   175
3      135   214    92   156
4      154   218    67   158
5      119    86   117   275
6      175   202    70   150
7      257   329     7     4
8      531     0    66     0
9        0   597     0     0
10       0   597     0     0
11       0     0   597     0
12     431   164     0     2
13       0   597     0     0
14     364    31     8   194
15      40   106     3   448
16     240    74    33   250
17     252    65   155   125
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ABR1_MA1244.1
XX
ID  ABR1_MA1244.1
XX
DE  MA1244.1 ABR1; from JASPAR
P0       a     c     g     t
1      122    57   317   104
2      145    75   281    99
3      140   230    96   134
4      100    64   308   128
5      181    79   285    55
6      237   149    79   135
7      225    26   186   163
8      253    18   193   136
9      103    97    14   386
10      59    15   308   218
11     150    27   401    22
12       2   590     0     8
13       0     0   598     2
14       0    12   588     0
15       4   584     0    12
16       5    10   550    35
17      33   105   445    17
18     196   298    30    76
19      66    36   421    77
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF112_MA1245.2
XX
ID  ERF112_MA1245.2
XX
DE  MA1245.2 ERF112; from JASPAR
P0       a     c     g     t
1      641   302   895   657
2      217  1507   361   410
3      168  2246    33    48
4      253    20  2173    49
5        1  2454    37     3
6       38  2449     1     7
7      219     3  2236    37
8        7  2446    14    28
9       38  2406     3    48
10     831    73  1264   327
11     304  1165   243   783
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  LEP_MA1246.1
XX
ID  LEP_MA1246.1
XX
DE  MA1246.1 LEP; from JASPAR
P0       a     c     g     t
1      125    74   128   163
2       40   250    78   122
3       36   413     9    32
4       69    23   226   172
5        4   398    52    36
6        2   488     0     0
7       26     0   456     8
8        0   480     0    10
9        0   489     0     1
10      23     0   460     7
11      12   301    20   157
12      75   303    22    90
13     206    27   146   111
14     119   232    44    95
15     133   189    41   127
16     125    56   187   122
17      61   252    56   121
18     108   254    48    80
19      72    80   222   116
20      88   240    51   111
21      68   278    44   100
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF087_MA1247.1
XX
ID  ERF087_MA1247.1
XX
DE  MA1247.1 ERF087; from JASPAR
P0       a     c     g     t
1      129   178    28   260
2       97    38   374    86
3      212    11   366     6
4        8   577     0    10
5        0     0   595     0
6        6     1   586     2
7        0   586     0     9
8        0     0   594     1
9       15    55   511    14
10     200   314    11    70
11      42    15   485    53
12     165    64   301    65
13     182   165    65   183
14     133    42   294   126
15     125    69   296   105
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF012_MA1248.1
XX
ID  ERF012_MA1248.1
XX
DE  MA1248.1 ERF012; from JASPAR
P0       a     c     g     t
1      108   226    85   149
2      122   257    65   124
3      189   104   132   143
4      114   260    77   117
5      110   274    66   118
6      193    92   134   149
7      106   250    69   143
8      110   277    52   129
9      211    69   136   152
10     106   259    71   132
11     102   306    51   109
12     174    42   143   209
13      26   394    50    98
14       0   568     0     0
15     530     0    36     2
16       0   568     0     0
17       1   567     0     0
18       4     1   563     0
19     299   171     0    98
20       0   558     0    10
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DREB2D_MA1250.1
XX
ID  DREB2D_MA1250.1
XX
DE  MA1250.1 DREB2D; from JASPAR
P0       a     c     g     t
1       59   334    55   109
2       93   340    34    90
3      205    73   150   129
4       50   340    53   114
5       74   360    30    93
6      118    13   205   221
7       19   489    18    31
8        6   551     0     0
9      396     0   160     1
10       0   554     1     2
11       1   556     0     0
12      32     0   519     6
13      74   366    10   107
14      21   395     8   133
15     237    21   127   172
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RAP2-9_MA1251.1
XX
ID  RAP2-9_MA1251.1
XX
DE  MA1251.1 RAP2-9; from JASPAR
P0       a     c     g     t
1      144   196    94   159
2      174   117   125   177
3      144   151   106   192
4      145   182    93   173
5      184   127   137   145
6      147   170   100   176
7      173   187    82   151
8      197    66   147   183
9       44   259    86   204
10       6   582     3     2
11     550     0    42     1
12       0   593     0     0
13       0   593     0     0
14       0     0   593     0
15     566     9     0    18
16       0   593     0     0
17     396   162    14    21
18     278   100     6   209
19     170   134    72   217
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF086_MA1252.1
XX
ID  ERF086_MA1252.1
XX
DE  MA1252.1 ERF086; from JASPAR
P0       a     c     g     t
1       86   245    46   101
2       84   255    26   113
3      148    42   150   138
4       61   254    37   126
5       39   397     0    42
6       59     0   256   163
7        0   397    61    20
8        1   477     0     0
9        8     0   469     1
10       2   466     1     9
11       3   475     0     0
12      13     0   459     6
13       8   260    11   199
14     104   214    34   126
15     194    37   123   124
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF036_MA1253.1
XX
ID  ERF036_MA1253.1
XX
DE  MA1253.1 ERF036; from JASPAR
P0       a     c     g     t
1      163   107    93   198
2      202    64   114   181
3      116    78   170   197
4       38    55    33   435
5       25    32   479    25
6        0     0     5   556
7        0   559     0     2
8        0     0   561     0
9        6     0   555     0
10      27    55    16   463
11      50    20   444    47
12     141    81   268    71
13     171   165    68   157
14     164    72   187   138
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RAP2-12_MA1256.1
XX
ID  RAP2-12_MA1256.1
XX
DE  MA1256.1 RAP2-12; from JASPAR
P0       a     c     g     t
1      127   224   119   127
2      133    64   286   114
3      140    86   276    95
4      159   192    90   156
5      133    83   250   131
6      191    72   250    84
7      244   131    52   170
8      266    14   170   147
9      274    15   184   124
10      68   207     4   318
11      10     6   494    87
12      52     0   542     3
13       0   596     0     1
14       0     0   596     1
15       0     0   596     1
16       7   575     0    15
17       9     6   517    65
18      66   101   402    28
19     228   188    49   132
20      98    42   350   107
21     155    81   252   109
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF9_MA1257.1
XX
ID  ERF9_MA1257.1
XX
DE  MA1257.1 ERF9; from JASPAR
P0       a     c     g     t
1       90   246    79   140
2       89    24   353    89
3      109    50   336    60
4      106   265    69   115
5       83    32   343    97
6      106    63   333    53
7      143   239    39   134
8      104    32   269   150
9      161    37   270    87
10     111   167    17   260
11      73     6   383    93
12     167    15   360    13
13      26   496     0    33
14       0     0   551     4
15      10     0   544     1
16      16   493     0    46
17       4     0   514    37
18      31    34   483     7
19     204   253     6    92
20      52     8   439    56
21     161    71   264    59
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DREB2A_MA1258.1
XX
ID  DREB2A_MA1258.1
XX
DE  MA1258.1 DREB2A; from JASPAR
P0       a     c     g     t
1      166   130   102   183
2      138    82   243   118
3      155    78   204   144
4      103   149    76   253
5      166    54   146   215
6      360     9   162    50
7      219    19    35   308
8        0     0   581     0
9       18     0   226   337
10       2   579     0     0
11       0     0   581     0
12       1     0   580     0
13       0    78     0   503
14      16    25   413   127
15     145    68   267   101
16     212   129    67   173
17     127    48   270   136
18     149    74   258   100
19     160   129   108   184
20     149    87   225   120
21     182    91   206   102
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF023_MA1259.1
XX
ID  ERF023_MA1259.1
XX
DE  MA1259.1 ERF023; from JASPAR
P0       a     c     g     t
1      161    75   222   114
2      171    90   168   143
3      195   116    63   198
4      204    47   124   197
5      103    73   184   212
6       12    64     7   489
7        0     0   572     0
8       40     1    26   505
9        0   572     0     0
10       0     0   572     0
11       0     0   572     0
12       1    46     0   525
13      17     7   540     8
14     162    67   308    35
15     165   149    70   188
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF013_MA1260.1
XX
ID  ERF013_MA1260.1
XX
DE  MA1260.1 ERF013; from JASPAR
P0       a     c     g     t
1      101    80   237   123
2      136    86   191   128
3      139   129   117   156
4      113    77   255    96
5      125    79   209   128
6      128   143    92   178
7      122    46   270   103
8      136    85   194   126
9      147   153    57   184
10     141    53   202   145
11     128    39   233   141
12      59    82    10   390
13       0     0   541     0
14     111     5   133   292
15       0   540     0     1
16       0     0   541     0
17       0     0   541     0
18       4    11     0   526
19       3     0   538     0
20     104    45   351    41
21     198   128    60   155
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF122_MA1261.1
XX
ID  ERF122_MA1261.1
XX
DE  MA1261.1 ERF122; from JASPAR
P0       a     c     g     t
1       34    47    15    42
2       26    20    42    50
3       39     7    57    35
4       40    30    14    54
5       10    35    16    77
6       55     0    80     3
7        0   120     0    18
8        0     0   136     2
9        0     0   138     0
10       0   106     0    32
11       0     3    89    46
12      25     0   106     7
13      41    53     9    35
14       0    10    91    37
15      25    26    67    20
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF2_MA1262.1
XX
ID  ERF2_MA1262.1
XX
DE  MA1262.1 ERF2; from JASPAR
P0       a     c     g     t
1       99   276    73   147
2      117   278    74   126
3      169    82   187   157
4       92   314    74   115
5      114   293    74   114
6      162    83   220   130
7       65   325    63   142
8      102   348    27   118
9      162    46   190   197
10      34   367    85   109
11     118   451     9    17
12      42     0   509    44
13       3   590     0     2
14       7   588     0     0
15      43     0   550     2
16       0   565     0    30
17       0   595     0     0
18     177     0   353    65
19      26   305    52   212
20     122   251    78   144
21     218    29   212   136
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF055_MA1263.1
XX
ID  ERF055_MA1263.1
XX
DE  MA1263.1 ERF055; from JASPAR
P0       a     c     g     t
1      232    21   145   200
2      132    37   133   296
3       50    45   277   226
4        0     0   598     0
5      117    19   171   291
6        0   598     0     0
7        0     0   598     0
8        0     0   597     1
9        0   139     0   459
10       8    18   535    37
11     111    70   378    39
12     198   171    63   166
13     110    54   319   115
14     181    78   246    93
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF095_MA1264.1
XX
ID  ERF095_MA1264.1
XX
DE  MA1264.1 ERF095; from JASPAR
P0       a     c     g     t
1      124   213    47   207
2      137    59   277   118
3      189    45   327    30
4       78   335     2   176
5        4     0   587     0
6       55     2   534     0
7        3   570     0    18
8        0     0   591     0
9        0     4   587     0
10      13   525     0    53
11       5     6   503    77
12      76    85   423     7
13     191   231    43   126
14      90    25   402    74
15     143    51   318    79
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF015_MA1265.2
XX
ID  ERF015_MA1265.2
XX
DE  MA1265.2 ERF015; from JASPAR
P0       a     c     g     t
1      375   146   356   341
2      130   594   153   341
3       26  1145    18    29
4     1121     0    73    24
5        3  1209     2     4
6        6  1209     0     3
7        9     1  1205     3
8      897   141    24   156
9       11  1201     3     3
10    1001    30   132    55
11     438   361   181   238
12     384   330   147   357
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RAP2-11_MA1266.1
XX
ID  RAP2-11_MA1266.1
XX
DE  MA1266.1 RAP2-11; from JASPAR
P0       a     c     g     t
1      106   270   110   102
2      106   236    75   171
3      131    82   212   163
4       69   248    39   232
5       86   342    41   119
6      163    42   220   163
7       15   323    42   208
8       61   373    71    83
9       25     0   539    24
10       0   588     0     0
11       0   588     0     0
12       0     0   588     0
13       0   112   329   147
14       0   560     0    28
15     102   132   193   161
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF5.8_MA1267.1
XX
ID  DOF5.8_MA1267.1
XX
DE  MA1267.1 DOF5.8; from JASPAR
P0       a     c     g     t
1      151   116    72   258
2      138   137    62   260
3      132    95    93   277
4       91   132    49   325
5      105   142    42   308
6       84   104    41   368
7      102   114    33   348
8      146   119    37   295
9      134   149    62   252
10     107   184    32   274
11      64   139    26   368
12      89    39    32   437
13      72    44    35   446
14      20   116    25   436
15     113   105    27   352
16     290    79   107   121
17       1   585    10     1
18       1    21     1   574
19       0     0     0   597
20       0     1     0   596
21     105    22     1   469
22      57    78    86   376
23     122   138   120   217
24     141   142   103   211
25     133   108    61   295
26     124   119    69   285
27     108   107    81   301
28     147   104    54   292
29     182    82    50   283
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CDF5_MA1268.1
XX
ID  CDF5_MA1268.1
XX
DE  MA1268.1 CDF5; from JASPAR
P0       a     c     g     t
1      131   120    57   288
2      138    85    62   311
3       91    51    82   372
4       75    42   113   366
5       68   263    22   243
6      348    72    81    95
7        0   596     0     0
8        0    24     0   572
9        0     4     0   592
10       0     0     0   596
11      89    21     8   478
12      80   103    60   353
13      98   250    59   189
14      80   151    30   335
15      65    80    21   430
16      66    62    25   443
17      84    60    34   418
18      97   109    36   354
19      78   142    60   316
20     114   110    51   321
21      73   100    68   355
22      80    82    56   378
23      99    76    52   369
24     121    99    59   317
25     114    89    81   312
26     134    98    73   291
27     153   122    48   273
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF4.5_MA1269.1
XX
ID  DOF4.5_MA1269.1
XX
DE  MA1269.1 DOF4.5; from JASPAR
P0       a     c     g     t
1      182   108   194   116
2      402    72    47    79
3      349     7    27   217
4      600     0     0     0
5      600     0     0     0
6      600     0     0     0
7        0     0   600     0
8      104   125    57   314
9      282    15   135   168
10     235    54   233    78
11     139   361    45    55
12      91    48    33   428
13      67    31    45   457
14      65    51    24   460
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF3.2_MA1270.1
XX
ID  DOF3.2_MA1270.1
XX
DE  MA1270.1 DOF3.2; from JASPAR
P0       a     c     g     t
1      101    78   143   278
2       75   151    51   323
3      177   111    27   285
4      319   138    85    58
5        0   600     0     0
6        0     0     0   600
7        0     0     1   599
8        0     0     0   600
9      135     0     0   465
10      39    55    10   496
11     129   175   176   120
12     129   211   107   153
13      87   189    57   267
14      83    98    56   363
15      89    55    59   397
16     104   120    48   328
17     126    92    55   327
18     132    92    88   288
19      77   134   115   274
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF2.2_MA1272.2
XX
ID  DOF2.2_MA1272.2
XX
DE  MA1272.2 DOF2.2; from JASPAR
P0       a     c     g     t
1      357   142   190   397
2      203   278   255   350
3     1086     0     0     0
4     1077     0     0     9
5     1086     0     0     0
6     1084     0     0     2
7     1086     0     0     0
8        0     0  1086     0
9      263   154   208   461
10     460   112   195   319
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF4.2_MA1273.1
XX
ID  DOF4.2_MA1273.1
XX
DE  MA1273.1 DOF4.2; from JASPAR
P0       a     c     g     t
1      288    70   106   134
2      281    98    96   123
3      303    73   108   114
4      236    75   145   142
5      214   110   166   108
6      477    38    48    35
7      580     0     0    18
8      598     0     0     0
9      598     0     0     0
10     598     0     0     0
11       0     0   598     0
12     112     0   463    23
13      89   253    28   228
14     346    60    70   122
15     390    38    72    98
16     353    66    50   129
17     303    44   105   146
18     285    75   119   119
19     265    94   120   119
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF3.6_MA1274.1
XX
ID  DOF3.6_MA1274.1
XX
DE  MA1274.1 DOF3.6; from JASPAR
P0       a     c     g     t
1       57   103    91   349
2       60   103     2   435
3      131    92    42   335
4      293    67    72   168
5        1   555     2    42
6        1    23     0   576
7        0    12     0   588
8        1     1     0   598
9       86    21     2   491
10      72   101    35   392
11     137   155    85   223
12     147   150    32   271
13      63   162    53   322
14      62    98    35   405
15      62    86     4   448
16      63   125     1   411
17      98    59    52   391
18      79   124    61   336
19      64   110    66   360
20     115   104    44   337
21     122    84    60   334
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF1.6_MA1275.1
XX
ID  DOF1.6_MA1275.1
XX
DE  MA1275.1 DOF1.6; from JASPAR
P0       a     c     g     t
1      158   144   154   144
2      132   158   204   106
3      343    84    88    85
4      332     2     3   263
5      600     0     0     0
6      600     0     0     0
7      544     0    56     0
8        0     0   600     0
9        0    86    23   491
10     342     1   227    30
11     298    45   155   102
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF3.5_MA1276.1
XX
ID  DOF3.5_MA1276.1
XX
DE  MA1276.1 DOF3.5; from JASPAR
P0       a     c     g     t
1      107   206   187    99
2      467     4   103    25
3      348     0     0   251
4      599     0     0     0
5      599     0     0     0
6      599     0     0     0
7        0     0   599     0
8        2     6   219   372
9      169    31   198   201
10     348    48   136    67
11     276   131    67   125
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF1.7_MA1277.1
XX
ID  DOF1.7_MA1277.1
XX
DE  MA1277.1 DOF1.7; from JASPAR
P0       a     c     g     t
1      277    94    97   127
2      311    71   106   107
3      331    50   117    97
4      316    76   106    97
5      274    68   122   131
6      164   139   202    90
7      388    48   108    51
8      466     0     3   126
9      595     0     0     0
10     595     0     0     0
11     575     0    20     0
12       0     0   595     0
13      96   113    94   292
14     371    18    90   116
15     342    26   177    50
16     381    52    83    79
17     326    69    66   134
18     293    51   135   116
19     288    87   131    89
20     266    78    95   156
21     273    62   151   109
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF3.4_MA1278.1
XX
ID  DOF3.4_MA1278.1
XX
DE  MA1278.1 DOF3.4; from JASPAR
P0       a     c     g     t
1      145   136    80   239
2      130   137   104   229
3       92   169    82   257
4      111   194    57   238
5       80   153    42   325
6       81   107    62   350
7       87   164    34   315
8      102   135    74   289
9      106   119    62   313
10      91   142    51   316
11     113    67    70   350
12      67    83    69   381
13      47   169    17   367
14     161    93    43   303
15     204   146   157    93
16       0   600     0     0
17       0    14     0   586
18       0     7     0   593
19       0     0     0   600
20     145    29     0   426
21      59   105   104   332
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF1.5_MA1279.1
XX
ID  DOF1.5_MA1279.1
XX
DE  MA1279.1 DOF1.5; from JASPAR
P0       a     c     g     t
1      257    82   140   121
2      259    86   118   137
3      333    60   105   102
4      318    62   101   119
5      329    75   121    75
6      329    55   114   102
7      301    61   143    95
8      251    73   147   129
9      184    94   199   123
10     440    34    71    55
11     486     8     1   105
12     599     1     0     0
13     599     0     1     0
14     591     0     9     0
15       0     0   600     0
16      46    35   140   379
17     232    22   273    73
18     325   122    41   112
19     400    81    38    81
20     340    71    54   135
21     308    63   119   110
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF5.4_MA1280.1
XX
ID  DOF5.4_MA1280.1
XX
DE  MA1280.1 DOF5.4; from JASPAR
P0       a     c     g     t
1      142   194   174    90
2      529    17    34    20
3      456     0     3   141
4      599     0     0     1
5      600     0     0     0
6      600     0     0     0
7        0     0   600     0
8       22    37   209   332
9      193    23   127   257
10     387    32   131    50
11     303   102    80   115
12     293    77    52   178
13     262    96    76   166
14     273    87    55   185
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF5.1_MA1281.1
XX
ID  DOF5.1_MA1281.1
XX
DE  MA1281.1 DOF5.1; from JASPAR
P0       a     c     g     t
1      217    86   116    96
2      355    25    90    45
3      412     4    13    86
4      512     0     1     2
5      505     0     0    10
6      464     2    49     0
7        2     0   511     2
8      150    93    82   190
9      306    16    71   122
10     369     0   104    42
11     354    76    58    27
12     320    39    99    57
13     328    20    80    87
14     225    37   149   104
15     261    61    96    97
16     329    16   103    67
17     349     0    73    93
18     345    45   104    21
19     260    27   110   118
20     275    59   140    41
21     263    39   119    94
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP13_MA1282.1
XX
ID  TCP13_MA1282.1
XX
DE  MA1282.1 TCP13; from JASPAR
P0       a     c     g     t
1       17     9    96     8
2        6    11     7   106
3       11     9    97    13
4        0     0   129     1
5        0     0   130     0
6        0     0   130     0
7       99    21     0    10
8        0   129     0     1
9        0   130     0     0
10     120     0    10     0
11       0   117     6     7
12      57    16    12    45
13      50    10    27    43
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP14_MA1283.1
XX
ID  TCP14_MA1283.1
XX
DE  MA1283.1 TCP14; from JASPAR
P0       a     c     g     t
1       31     5     5     6
2       13    16    13     5
3       40     0     4     3
4       14     2    18    13
5       36     2     4     5
6        5     5    28     9
7       10    14    18     5
8       22     2    12    11
9        2    10    16    19
10       0     0    47     0
11       0     0     0    47
12       2     0    45     0
13       0     0    47     0
14       0     0    47     0
15      19     0    18    10
16       0    47     0     0
17       0    47     0     0
18       0    47     0     0
19      43     0     4     0
20       0    47     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP1_MA1284.1
XX
ID  TCP1_MA1284.1
XX
DE  MA1284.1 TCP1; from JASPAR
P0       a     c     g     t
1       36     7    16    36
2        4     7    84     0
3        0     6     5    84
4        0     0    95     0
5        0     0    95     0
6        0     0    95     0
7        8    54     8    25
8        0    94     1     0
9        0    95     0     0
10       9    82     0     4
11      52    26     8     9
12      15    61     2    17
13      16    34    22    23
14      13    22    12    48
15      21    37    15    22
16      20    34    10    31
17      16    28     8    43
18      23    43    11    18
19      25    18    12    40
20      16    31    25    23
21      22    22    17    34
22      23    26    11    35
23      15    13    36    31
24      20    11    40    24
25      15    15    36    29
26      28    36    10    21
27      19    37    15    24
28      22    27    18    28
29      36    19    21    19
30      12    35     8    40
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP9_MA1285.1
XX
ID  TCP9_MA1285.1
XX
DE  MA1285.1 TCP9; from JASPAR
P0       a     c     g     t
1       77   192    60    66
2       85   205    57    48
3      195    79    51    70
4       65    89    49   192
5       50    55    78   212
6      103    58    42   192
7       65    92   151    87
8       92   170    30   103
9       89   166    34   106
10      46    13   332     4
11       1    19     1   374
12      16     0   379     0
13       1     0   394     0
14      24     0   370     1
15      53    97    23   222
16       0   388     0     7
17       2   393     0     0
18       1   371     1    22
19     356     1    38     0
20       2   346    11    36
21     104   178    36    77
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP24_MA1286.1
XX
ID  TCP24_MA1286.1
XX
DE  MA1286.1 TCP24; from JASPAR
P0       a     c     g     t
1      153    81   214   146
2      113   108    94   279
3       74   100   196   224
4       12    11   553    18
5        0     0   593     1
6        0     0   594     0
7      527    24    26    17
8        0   594     0     0
9        0   594     0     0
10     567     0    27     0
11      16   560     9     9
12     195   192    40   167
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP21_MA1287.1
XX
ID  TCP21_MA1287.1
XX
DE  MA1287.1 TCP21; from JASPAR
P0       a     c     g     t
1      188    77    99   234
2       35     5   558     0
3        0    20     0   578
4        0     0   598     0
5        0     0   598     0
6       54     1   520    23
7      154   289    33   122
8        0   597     0     1
9       15   582     0     1
10      62   415    14   107
11     424    37    88    49
12      58   338    65   137
13     153   150   118   177
14     161   104    96   237
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP22_MA1288.1
XX
ID  TCP22_MA1288.1
XX
DE  MA1288.1 TCP22; from JASPAR
P0       a     c     g     t
1      227    93   109   166
2      165    87   112   231
3       12     2   581     0
4        0    25     0   570
5        1     0   594     0
6        0     0   595     0
7       57     3   513    22
8      172   227    58   138
9        0   595     0     0
10       1   594     0     0
11      45   439     9   102
12     441    25    87    42
13      51   321    70   153
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP3_MA1289.1
XX
ID  TCP3_MA1289.1
XX
DE  MA1289.1 TCP3; from JASPAR
P0       a     c     g     t
1      165    69   209   139
2      122    95    77   288
3       58    89   185   250
4        2     2   563    15
5        0     0   582     0
6        0     0   582     0
7      503    51     0    28
8        0   582     0     0
9        0   582     0     0
10     544     0    38     0
11       1   561     6    14
12     215   154    51   162
13     196    74   104   208
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP17_MA1290.1
XX
ID  TCP17_MA1290.1
XX
DE  MA1290.1 TCP17; from JASPAR
P0       a     c     g     t
1        8     6    92     2
2        2     5     0   101
3        0     2   105     1
4        0     0   106     2
5        3     0    20    85
6        1   107     0     0
7        0   108     0     0
8        1   106     1     0
9       17    80     5     6
10      90     6     6     6
11       9    77     9    13
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP7_MA1291.1
XX
ID  TCP7_MA1291.1
XX
DE  MA1291.1 TCP7; from JASPAR
P0       a     c     g     t
1        1     0   587     0
2        0    15     0   573
3       68     0   520     0
4        0     0   588     0
5        4     1   583     0
6      228    92   162   106
7       22   514     0    52
8        2   586     0     0
9       25   556     4     3
10     451    30    75    32
11      42   321    66   159
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB27_MA1292.1
XX
ID  MYB27_MA1292.1
XX
DE  MA1292.1 MYB27; from JASPAR
P0       a     c     g     t
1      278    80   109   129
2      357    55    77   107
3      385    19    71   121
4      469     0     6   121
5      495     0     2    99
6      412     0   178     6
7        0     0     1   595
8        0     0     0   596
9      595     0     0     1
10       0     0   596     0
11       0     0   596     0
12       0     5     0   591
13     512    48    25    11
14     277    66   103   150
15     315    55   139    87
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB57_MA1293.1
XX
ID  MYB57_MA1293.1
XX
DE  MA1293.1 MYB57; from JASPAR
P0       a     c     g     t
1       76   199    53   269
2      109   151    81   256
3        0    20    22   555
4      500     0    97     0
5        0   597     0     0
6        0   597     0     0
7        2     0     0   595
8      597     0     0     0
9      597     0     0     0
10       1   483     0   113
11      44    57     0   496
12      95   127    20   355
13     133   108    56   300
14     171    88    67   271
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB62_MA1294.1
XX
ID  MYB62_MA1294.1
XX
DE  MA1294.1 MYB62; from JASPAR
P0       a     c     g     t
1      198    60    96    86
2      170    88    74   108
3      204    48    76   112
4      213    66    61   100
5      227    23    60   130
6      299    12    58    71
7      112    32   263    33
8        0     0     5   435
9        0     0     0   440
10     411     0     0    29
11       0     0   440     0
12       0     0   440     0
13       5   188     5   242
14     228    47    71    94
15     130    66   134   110
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY20_MA1295.1
XX
ID  WRKY20_MA1295.1
XX
DE  MA1295.1 WRKY20; from JASPAR
P0       a     c     g     t
1      235    55   157   153
2      162   151   156   131
3       34   418    74    74
4       11     3   420   166
5        2     1     0   597
6        0     1     0   599
7        0     0   600     0
8      599     0     1     0
9        1   599     0     0
10       0   270     0   330
11     231    38   108   223
12     160    40   136   264
13     156    68   127   249
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY46_MA1296.1
XX
ID  WRKY46_MA1296.1
XX
DE  MA1296.1 WRKY46; from JASPAR
P0       a     c     g     t
1        2    19     1     0
2        0     0    16     6
3        0     0     0    22
4        0     0     0    22
5        0     0    22     0
6       18     0     4     0
7        0    22     0     0
8        0     3     0    19
9        0     3     1    18
10       3     4     0    15
11       1     4     9     8
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY26_MA1297.1
XX
ID  WRKY26_MA1297.1
XX
DE  MA1297.1 WRKY26; from JASPAR
P0       a     c     g     t
1       53    16     9     6
2       55     7     5    17
3       56    11     3    14
4       71     0    13     0
5        0     0    84     0
6        0     2     1    81
7        0    84     0     0
8       84     0     0     0
9       84     0     0     0
10      13    71     0     0
11      15     5    59     5
12      21    22    24    17
13      18    22     5    39
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY29_MA1298.1
XX
ID  WRKY29_MA1298.1
XX
DE  MA1298.1 WRKY29; from JASPAR
P0       a     c     g     t
1      315   114    65   100
2      403    42    60    89
3      438    15    38   103
4      496     0    81    17
5        0     0   594     0
6        0     1     0   593
7        0   594     0     0
8      593     1     0     0
9      588     0     6     0
10     132   439     0    23
11      67    64   274   189
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY17_MA1299.1
XX
ID  WRKY17_MA1299.1
XX
DE  MA1299.1 WRKY17; from JASPAR
P0       a     c     g     t
1       57    25    23    22
2       67    24    14    22
3       94     9    18     6
4      117     0     6     4
5      125     0     1     1
6        0     0   127     0
7        0     5     1   121
8        0   127     0     0
9      125     0     2     0
10     127     0     0     0
11      20    97     8     2
12      29     9    78    11
13      32    38    32    25
14      28    42    16    41
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY6_MA1300.1
XX
ID  WRKY6_MA1300.1
XX
DE  MA1300.1 WRKY6; from JASPAR
P0       a     c     g     t
1       41   195    21    28
2        1     0   262    22
3        0     0     0   285
4        0     0     0   285
5        0     0   285     0
6      285     0     0     0
7        0   285     0     0
8        0    85     0   200
9       77    38    37   133
10      74    23    43   145
11      58    41    89    97
12      68    29    84   104
13      59    77    19   130
14      75    42    44   124
15     101    23    82    79
16     110    41    49    85
17      63    81    65    76
18      87    79    39    80
19      78    84    34    89
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY33_MA1301.1
XX
ID  WRKY33_MA1301.1
XX
DE  MA1301.1 WRKY33; from JASPAR
P0       a     c     g     t
1      296   145    49   108
2      380    83    35   100
3      372    48    28   150
4      476     0   122     0
5        0     0   598     0
6        0     0     0   598
7        0   598     0     0
8      598     0     0     0
9      598     0     0     0
10     184   400     0    14
11      98    67   350    83
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY65_MA1302.1
XX
ID  WRKY65_MA1302.1
XX
DE  MA1302.1 WRKY65; from JASPAR
P0       a     c     g     t
1      257   122    99   122
2      380    43    84    93
3      418    25    34   123
4      458     6   127     9
5        0     0   600     0
6        0     1     0   599
7        0   600     0     0
8      600     0     0     0
9      599     0     0     1
10      36   542     0    22
11      50    38   419    93
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY22_MA1303.1
XX
ID  WRKY22_MA1303.1
XX
DE  MA1303.1 WRKY22; from JASPAR
P0       a     c     g     t
1      318   116    78    87
2      461    29    50    59
3      518     5    14    62
4      532     0    58     9
5        0     0   599     0
6        0     0     0   599
7        0   599     0     0
8      599     0     0     0
9      599     0     0     0
10      71   508     0    20
11      54    60   314   171
12     182   140   128   149
13     140   135    67   257
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY59_MA1304.1
XX
ID  WRKY59_MA1304.1
XX
DE  MA1304.1 WRKY59; from JASPAR
P0       a     c     g     t
1       56    48    26    43
2      109    12    25    27
3       97    15    16    45
4      103    18     8    44
5      130     0    39     4
6        0     0   173     0
7        0     2     0   171
8        0   173     0     0
9      173     0     0     0
10     173     0     0     0
11      93    62     6    12
12      46    38    58    31
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY55_MA1305.1
XX
ID  WRKY55_MA1305.1
XX
DE  MA1305.1 WRKY55; from JASPAR
P0       a     c     g     t
1      188    73   186   152
2      141   148   167   143
3       71   415    59    54
4       31     2   543    23
5        0     0     0   599
6        0     0     0   599
7        0     0   599     0
8      599     0     0     0
9        0   599     0     0
10       0   128     1   470
11      90    72    48   389
12     126    65   103   305
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY11_MA1306.1
XX
ID  WRKY11_MA1306.1
XX
DE  MA1306.1 WRKY11; from JASPAR
P0       a     c     g     t
1      227    67   173   130
2      149   109   178   161
3      107   358    84    48
4       11     0   512    74
5        0     1     0   596
6        1     0     0   596
7        0     0   597     0
8      595     0     2     0
9        0   597     0     0
10      16    47     0   534
11      59    20     8   510
12      56    42    37   462
13     102    70    93   332
14     144    85   139   229
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY31_MA1307.2
XX
ID  WRKY31_MA1307.2
XX
DE  MA1307.2 WRKY31; from JASPAR
P0       a     c     g     t
1       98    78    41    82
2       83    66    76    74
3       88    67    42   102
4       88    44    41   126
5      134     4   153     8
6        2     0   296     1
7        0     0     0   299
8        2   296     1     0
9      297     1     1     0
10     290     0     3     6
11      16   264     4    15
12      29    11   240    19
13      72   108    49    70
14      64    92    42   101
15      84    47   105    63
16      76    41   107    75
17      68    44    59   128
18      67    74    65    93
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY70_MA1308.1
XX
ID  WRKY70_MA1308.1
XX
DE  MA1308.1 WRKY70; from JASPAR
P0       a     c     g     t
1      210    77   168   144
2      123   165   181   130
3       75   464    32    28
4       14     0   584     1
5        0     0     0   599
6        0     0     0   599
7        0     0   599     0
8      599     0     0     0
9        0   599     0     0
10       2    58     3   536
11      71    24    11   493
12      82    45    37   435
13      99    73   118   309
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY3_MA1309.1
XX
ID  WRKY3_MA1309.1
XX
DE  MA1309.1 WRKY3; from JASPAR
P0       a     c     g     t
1      325   132    63    78
2      404    75    34    85
3      386    62    26   124
4      467     0   131     0
5        0     0   598     0
6        0     0     0   598
7        0   597     0     1
8      598     0     0     0
9      598     0     0     0
10     180   398     1    19
11     106    69   347    76
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY42_MA1310.1
XX
ID  WRKY42_MA1310.1
XX
DE  MA1310.1 WRKY42; from JASPAR
P0       a     c     g     t
1       55    73    70   111
2       71    45    58   135
3       64    34    68   143
4       75    41    92   101
5      125    72    55    57
6      118    92    30    69
7       81    88    39   101
8       75    79    49   106
9       96    51    93    69
10      67    69    89    84
11      51   184    28    46
12       1     0   301     7
13       0     0     0   309
14       0     0     0   309
15       0     0   307     2
16     309     0     0     0
17       0   309     0     0
18       6    90     7   206
19      97    38    37   137
20      82    45    38   144
21      63    48    92   106
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY28_MA1311.2
XX
ID  WRKY28_MA1311.2
XX
DE  MA1311.2 WRKY28; from JASPAR
P0       a     c     g     t
1      929   287   243   633
2      901   240   240   711
3      919    67   924   182
4       21    11  2047    13
5        6     8     1  2077
6       12  2062     8    10
7     2059    17     6    10
8     2050     7    18    17
9      158  1730    55   149
10     596   254   740   502
11     694   436   330   632
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY47_MA1312.1
XX
ID  WRKY47_MA1312.1
XX
DE  MA1312.1 WRKY47; from JASPAR
P0       a     c     g     t
1       46    26     8     8
2       14    38     5    31
3        7    41    17    23
4       13     7    45    23
5       25    17    28    18
6        6    80     2     0
7        2     0    65    21
8        0     0     1    87
9        5     0     0    83
10       0     0    88     0
11      74     0    13     1
12       0    88     0     0
13       1    33     1    53
14      34    13     3    38
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY7_MA1313.1
XX
ID  WRKY7_MA1313.1
XX
DE  MA1313.1 WRKY7; from JASPAR
P0       a     c     g     t
1      220    79   160   135
2      121   116   179   178
3      114   395    55    30
4        3     0   552    39
5        0     0     0   594
6        0     0     0   594
7        0     0   593     1
8      587     0     7     0
9        0   593     0     1
10       3    13     0   578
11      12     3     0   579
12      26    25     4   539
13      61    70    81   382
14     129    78   118   269
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY14_MA1314.1
XX
ID  WRKY14_MA1314.1
XX
DE  MA1314.1 WRKY14; from JASPAR
P0       a     c     g     t
1      286   128    78   106
2      426    38    51    83
3      454    22    24    98
4      509     1    84     4
5        0     0   598     0
6        0     0     0   598
7        0   598     0     0
8      598     0     0     0
9      598     0     0     0
10      36   549     0    13
11      53    54   397    94
12     168   165   142   123
13     144   157    76   221
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY24_MA1315.1
XX
ID  WRKY24_MA1315.1
XX
DE  MA1315.1 WRKY24; from JASPAR
P0       a     c     g     t
1      112   130   158   200
2       42   423    50    85
3       21     2   525    52
4        0     0     0   600
5        2     0     0   598
6        0     0   600     0
7      599     0     1     0
8        1   599     0     0
9        0   114     0   486
10     133    29    27   411
11     101    36    36   427
12     111    79    78   332
13     123   102   103   272
14     143   116   101   240
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY71_MA1316.1
XX
ID  WRKY71_MA1316.1
XX
DE  MA1316.1 WRKY71; from JASPAR
P0       a     c     g     t
1      349   103    69    77
2      475    32    36    55
3      510    13    20    55
4      552     2    40     4
5        0     0   598     0
6        0     0     0   598
7        0   598     0     0
8      598     0     0     0
9      598     0     0     0
10     157   408     4    29
11     128    82   294    94
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY50_MA1317.1
XX
ID  WRKY50_MA1317.1
XX
DE  MA1317.1 WRKY50; from JASPAR
P0       a     c     g     t
1      104   213    88   163
2       57    56   216   239
3        6     1     2   559
4        3     0     0   565
5        0     0   568     0
6      567     0     1     0
7        0   568     0     0
8        2    22     1   543
9       43    10     4   511
10      29    23    14   502
11      51    63    48   406
12     116    88    90   274
13     142   124    87   215
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY27_MA1318.1
XX
ID  WRKY27_MA1318.1
XX
DE  MA1318.1 WRKY27; from JASPAR
P0       a     c     g     t
1      278    65   129   128
2      133   125   167   175
3      171   345    48    36
4        3     1   554    42
5        0     0     0   600
6        0     0     0   600
7        0     0   600     0
8      595     0     3     2
9        0   600     0     0
10       6    20     0   574
11      46    10     2   542
12      43    34     2   521
13      70    64    89   377
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SPL15_MA1320.1
XX
ID  SPL15_MA1320.1
XX
DE  MA1320.1 SPL15; from JASPAR
P0       a     c     g     t
1       91    30    62    63
2      111    33    43    59
3      128    29    28    61
4       39    58    33   116
5        5    45    16   180
6        5     0   241     0
7        0     0     0   246
8      246     0     0     0
9        0   246     0     0
10       0     0   244     2
11       0     0   241     5
12     219     4     3    20
13      69   106    18    53
14      94    44    66    42
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SPL13A_MA1321.1
XX
ID  SPL13A_MA1321.1
XX
DE  MA1321.1 SPL13A; from JASPAR
P0       a     c     g     t
1      262    64    96   156
2      122   118    90   248
3       50    95    65   368
4        0     0   578     0
5        0     0     0   578
6      578     0     0     0
7        0   578     0     0
8        2    19   460    97
9       79     0   438    61
10     346    71    10   151
11     174   163    59   182
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SPL9_MA1322.2
XX
ID  SPL9_MA1322.2
XX
DE  MA1322.2 SPL9; from JASPAR
P0       a     c     g     t
1      514   475   481   646
2      674   396   461   585
3      294    57   169  1596
4      180  1710    51   175
5       94  1844    56   122
6       31    19  2027    39
7       17     7    10  2082
8     2053    20    22    21
9       25  2010    29    52
10     683   396   558   479
11     664   314   522   616
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GATA19_MA1323.1
XX
ID  GATA19_MA1323.1
XX
DE  MA1323.1 GATA19; from JASPAR
P0       a     c     g     t
1       22   101    27    52
2       56    54    51    41
3       10     2   190     0
4      202     0     0     0
5        0     0     0   202
6        2   197     3     0
7        1    55    89    57
8       21     1   180     0
9      159     7    27     9
10      33     1     6   162
11       6    81    13   102
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GATA20_MA1324.1
XX
ID  GATA20_MA1324.1
XX
DE  MA1324.1 GATA20; from JASPAR
P0       a     c     g     t
1       70     0     1     5
2        0     8     0    68
3        2    70     0     4
4       13    25    38     0
5        0     0    76     0
6       76     0     0     0
7        0     0     0    76
8        2    73     0     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GATA14_MA1325.1
XX
ID  GATA14_MA1325.1
XX
DE  MA1325.1 GATA14; from JASPAR
P0       a     c     g     t
1       81   101    89   280
2      100   119   133   199
3       81    87    42   341
4      497     4    26    24
5        0     1   550     0
6      551     0     0     0
7        0     2     0   549
8        4   547     0     0
9        2    97    31   421
10     202    46   296     7
11     189    70   127   165
12     106    55    70   320
13      71   112    83   285
14      86   124    97   244
15      99   125   119   208
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ZHD5_MA1326.1
XX
ID  ZHD5_MA1326.1
XX
DE  MA1326.1 ZHD5; from JASPAR
P0       a     c     g     t
1      206   122   140   132
2      116   217   105   162
3      165    19   371    45
4        0    22     2   576
5      316    90   194     0
6      591     3     6     0
7        0     0     0   600
8        0     0     0   600
9      599     0     0     1
10     335    19   237     9
11      51   170   138   241
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ATHB-23_MA1327.2
XX
ID  ATHB-23_MA1327.2
XX
DE  MA1327.2 ATHB-23; from JASPAR
P0       a     c     g     t
1      660   381   278  1113
2      466   226   103  1637
3     2432     0     0     0
4     2432     0     0     0
5        0     0     0  2432
6        0     0     0  2432
7     2432     0     0     0
8     2432     0     0     0
9      646   310   374  1102
10     670   397   302  1063
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ZHD9_MA1328.1
XX
ID  ZHD9_MA1328.1
XX
DE  MA1328.1 ZHD9; from JASPAR
P0       a     c     g     t
1      241    11    11   337
2      152    45    17   386
3      226    52     4   318
4      332    92    66   110
5      221    76   102   201
6      138   137    55   270
7      174   182    93   151
8      242   175   127    56
9       15   225    19   341
10       4     1     0   595
11     600     0     0     0
12     599     0     1     0
13       4     9     0   587
14       0   222     6   372
15     539    13    32    16
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ZHD1_MA1329.2
XX
ID  ZHD1_MA1329.2
XX
DE  MA1329.2 ZHD1; from JASPAR
P0       a     c     g     t
1      834   351   309   978
2      967   422   333   750
3     1813   246   193   220
4      386  1155   193   738
5       47    24     9  2392
6     2412    14    26    20
7     2417    15    18    22
8       25    29     9  2409
9       19    43    12  2398
10    2382    14    25    51
11    2233    73    60   106
12     562   402   473  1035
13     691   423   370   988
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ZHD6_MA1330.1
XX
ID  ZHD6_MA1330.1
XX
DE  MA1330.1 ZHD6; from JASPAR
P0       a     c     g     t
1      368    45    29   158
2      357    63    54   126
3      260    76    88   176
4      179   153    76   192
5      217    35   242   106
6       12    13     2   573
7      411    14   175     0
8      585     0    13     2
9        0     0     0   600
10       0     0     0   600
11     598     0     2     0
12     394     7   192     7
13      75   108   142   275
14     167    73   160   200
15     286    53    95   166
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BEH4_MA1331.1
XX
ID  BEH4_MA1331.1
XX
DE  MA1331.1 BEH4; from JASPAR
P0       a     c     g     t
1        0   324     4   270
2      157     1   440     0
3        0   598     0     0
4      598     0     0     0
5        0   598     0     0
6        0     0   598     0
7        2     0     0   596
8        0     0   598     0
9       13   132    65   388
10      91    88   365    54
11     191   119   195    93
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BEH2_MA1332.1
XX
ID  BEH2_MA1332.1
XX
DE  MA1332.1 BEH2; from JASPAR
P0       a     c     g     t
1       83   184   103   229
2       36   414    66    83
3      406    72   106    15
4        0   599     0     0
5      598     0     0     1
6        0   599     0     0
7        0     0   599     0
8        0     0     0   599
9        0     0   599     0
10       0   407     1   191
11     234     0   365     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BEH3_MA1333.1
XX
ID  BEH3_MA1333.1
XX
DE  MA1333.1 BEH3; from JASPAR
P0       a     c     g     t
1        3   319    43   225
2      179    28   383     0
3        0   589     0     1
4      590     0     0     0
5        0   590     0     0
6        1     0   589     0
7        1     0     0   589
8        0     0   590     0
9       51    74    60   405
10      60   140   338    52
11     239    91   179    81
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZIP11_MA1334.1
XX
ID  BZIP11_MA1334.1
XX
DE  MA1334.1 BZIP11; from JASPAR
P0       a     c     g     t
1       73    79   105   334
2       16    14   418   143
3      142   428     0    21
4        0   572    19     0
5      591     0     0     0
6        0   588     2     1
7        3     2   586     0
8        0     0     0   591
9       43   534    14     0
10     571     0    19     1
11       2    46   373   170
12      67   501     3    20
13     344    60    38   149
14     191    96    72   232
15     169   182    68   172
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TGA4_MA1335.1
XX
ID  TGA4_MA1335.1
XX
DE  MA1335.1 TGA4; from JASPAR
P0       a     c     g     t
1       44   123     0   433
2       22   117   398    63
3      566     3     5    26
4        0   581     0    19
5       71     2   527     0
6        0     2     0   598
7       42   558     0     0
8      599     1     0     0
9        0    89   195   316
10      18   500    37    45
11     342    62   111    85
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TGA3_MA1336.1
XX
ID  TGA3_MA1336.1
XX
DE  MA1336.1 TGA3; from JASPAR
P0       a     c     g     t
1      182    69   189   158
2      290    61   232    15
3        3     3     0   592
4        0     6   583     9
5      598     0     0     0
6        0   577     0    21
7       26     1   571     0
8        0     1     0   597
9       71   527     0     0
10     593     0     1     4
11       0    83   215   300
12      39   447    51    61
13     306    52   104   136
14     156    67    87   288
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZIP44_MA1337.1
XX
ID  BZIP44_MA1337.1
XX
DE  MA1337.1 BZIP44; from JASPAR
P0       a     c     g     t
1      156    66   161   145
2      210    51   103   164
3      143    37    65   283
4       32     5   423    68
5      153   320    54     1
6        3    24     1   500
7        0     0   479    49
8      528     0     0     0
9        0   520     0     8
10       4     1   523     0
11       2     1     0   525
12       0     0   528     0
13       0     0   425   103
14     124   397     1     6
15     328    79    71    50
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DPBF3_MA1338.2
XX
ID  DPBF3_MA1338.2
XX
DE  MA1338.2 DPBF3; from JASPAR
P0       a     c     g     t
1      836   341   460   792
2      819   393   480   737
3      709   512   414   794
4      317   355   262  1495
5       74    30  2110   215
6      811  1580    17    21
7        6  2407     5    11
8     2419     1     5     4
9        1  2409    14     5
10       9    26  2388     6
11       7     9     1  2412
12      97   269  2041    22
13     307    17   671  1434
14     335  1703   156   235
15     901   790   299   439
16     775   462   390   802
17     711   520   359   839
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZIP43_MA1339.1
XX
ID  BZIP43_MA1339.1
XX
DE  MA1339.1 BZIP43; from JASPAR
P0       a     c     g     t
1        0     0    19     9
2        3    25     0     0
3        0    28     0     0
4       28     0     0     0
5        0    27     0     1
6        0     0    28     0
7        0     0     0    28
8        0    27     1     0
9       27     0     0     1
10       0     0    24     4
11       3    25     0     0
12      18     3     1     6
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZIP3_MA1340.1
XX
ID  BZIP3_MA1340.1
XX
DE  MA1340.1 BZIP3; from JASPAR
P0       a     c     g     t
1      176    69   182   147
2      199    95   109   171
3      150    55    57   312
4       27    10   439    98
5      169   307    75    23
6       38    56     1   479
7        0    70   452    52
8      574     0     0     0
9        0   566     3     5
10       3     8   561     2
11       0     0     0   574
12       0     0   574     0
13       0     3   459   112
14     129   431     2    12
15     314    78   113    69
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZIP53_MA1341.1
XX
ID  BZIP53_MA1341.1
XX
DE  MA1341.1 BZIP53; from JASPAR
P0       a     c     g     t
1      178    67   185   168
2      241    73   105   179
3      152    37    63   346
4       20     1   503    74
5      159   387    50     2
6        0    26     0   572
7        0    14   529    55
8      598     0     0     0
9        0   595     1     2
10       1     2   595     0
11       0     0     0   598
12       0    22   576     0
13      24     0   450   124
14     138   430    14    16
15     348   100    81    69
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZIP52_MA1343.1
XX
ID  BZIP52_MA1343.1
XX
DE  MA1343.1 BZIP52; from JASPAR
P0       a     c     g     t
1      207    68   109   215
2       51    68    46   434
3        0     0   548    51
4      309   290     0     0
5        0   599     0     0
6      599     0     0     0
7        1     0   598     0
8        3   479    85    32
9        0   198     4   397
10      61    78   242   218
11     116     0   332   151
12      98   118   139   244
13     187   204    65   143
14     247   102    76   174
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZIP28_MA1344.1
XX
ID  BZIP28_MA1344.1
XX
DE  MA1344.1 BZIP28; from JASPAR
P0       a     c     g     t
1      177    65   147   159
2      172   108   103   165
3      138    90    91   229
4      125    39   254   130
5      249    69   174    56
6      127    79     3   339
7        1   181   315    51
8      546     0     2     0
9        0   544     0     4
10       4     0   544     0
11       0     0     0   548
12       6     0   542     0
13       0     0   407   141
14      89   431     1    27
15     302    42   118    86
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZIP48_MA1345.1
XX
ID  BZIP48_MA1345.1
XX
DE  MA1345.1 BZIP48; from JASPAR
P0       a     c     g     t
1        7     4   328   114
2       84   369     0     0
3        0   452     0     1
4      453     0     0     0
5        1   449     0     3
6        4     1   448     0
7        0     1     0   452
8       36   417     0     0
9      433     0    16     4
10       5    24   319   105
11      53   384     3    13
12     279    34    33   107
13     130    77    72   174
14     122   145    46   140
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TGA10_MA1346.1
XX
ID  TGA10_MA1346.1
XX
DE  MA1346.1 TGA10; from JASPAR
P0       a     c     g     t
1      193    72   216   112
2      256    90   107   140
3      118   122    54   299
4       45    53   473    22
5      316   200    77     0
6        0     0     0   593
7        4     0   550    39
8      590     0     3     0
9        0   559     1    33
10       6     0   587     0
11      12     5     1   575
12      34   505    48     6
13     524     0    48    21
14      34   258    98   203
15     135   184    83   191
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TGA9_MA1348.1
XX
ID  TGA9_MA1348.1
XX
DE  MA1348.1 TGA9; from JASPAR
P0       a     c     g     t
1       51    51   481     9
2      318   198    76     0
3        8     0     1   583
4        0     0   526    66
5      591     0     1     0
6        0   548     0    44
7       11     0   581     0
8        1     0     0   591
9       16   552    24     0
10     567     0    25     0
11      13   239    76   264
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZIP16_MA1349.1
XX
ID  BZIP16_MA1349.1
XX
DE  MA1349.1 BZIP16; from JASPAR
P0       a     c     g     t
1      205    70   160   159
2      227    52   116   199
3      182    60    75   277
4       52    28   334   180
5       97   174   254    69
6      145   170     0   279
7        0   302   265    27
8      594     0     0     0
9        0   586     7     1
10       0     7   587     0
11       0     0     0   594
12       0     0   594     0
13       0     0   529    65
14      33   561     0     0
15     464    22    94    14
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZIP42_MA1350.1
XX
ID  BZIP42_MA1350.1
XX
DE  MA1350.1 BZIP42; from JASPAR
P0       a     c     g     t
1       13     3     0    51
2        1     0    64     2
3        9    55     3     0
4        0     0     0    67
5        0     0    65     2
6       67     0     0     0
7        0    67     0     0
8        1     0    66     0
9        1     0     0    66
10       0     0    67     0
11       0     0    58     9
12      18    49     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GBF3_MA1351.2
XX
ID  GBF3_MA1351.2
XX
DE  MA1351.2 GBF3; from JASPAR
P0       a     c     g     t
1     1144  4087  2431  1799
2     7456   509   636   860
3      260  8349   340   512
4      341   139  8749   232
5      136   113    27  9185
6      133    60  9137   131
7      385   212  7410  1454
8      828  8130   275   228
9     7429   400   831   801
10    2879  1756  1943  2883
11    2928  1909  1879  2745
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TRP1_MA1352.1
XX
ID  TRP1_MA1352.1
XX
DE  MA1352.1 TRP1; from JASPAR
P0       a     c     g     t
1      253     4     8    42
2       19   250    11    27
3       17   263    14    13
4       30   218    11    48
5       29     0     3   275
6      302     0     2     3
7      304     0     1     2
8      307     0     0     0
9        4   302     0     1
10      13   290     0     4
11       1   296     6     4
12       3     2     2   300
13     296     2     2     7
14     295     9     2     1
15     306     0     0     1
16       3   288     1    15
17       6   292     4     5
18      24   272     0    11
19      13     8     1   285
20     290     4     6     7
21     263     8    28     8
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT1G72740_MA1353.1
XX
ID  AT1G72740_MA1353.1
XX
DE  MA1353.1 AT1G72740; from JASPAR
P0       a     c     g     t
1      187    94   223    96
2      187    71   242   100
3      278    23   230    69
4      254    10    77   259
5        2   107     1   490
6        3     1     0   596
7      595     0     4     1
8        0     1   599     0
9        0     0   597     3
10       0     3   566    31
11       1    28     0   571
12      22     1     0   577
13      77    33    19   471
14     229   120    73   178
15      76    82   281   161
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT4G12670_MA1354.1
XX
ID  AT4G12670_MA1354.1
XX
DE  MA1354.1 AT4G12670; from JASPAR
P0       a     c     g     t
1        0    35     0     7
2        3     3     0    36
3       41     0     0     1
4       41     0     0     1
5       39     1     1     1
6        0    42     0     0
7        4    35     1     2
8        0    41     0     1
9        1     0     1    40
10      38     0     1     3
11      40     0     2     0
12      39     0     3     0
13       1    36     0     5
14       0    39     2     1
15       6    32     1     3
16       1     1     0    40
17      42     0     0     0
18      34     2     1     5
19      36     1     2     3
20       2    35     2     3
21       7    28     0     7
22       0    39     0     3
23       8     0     0    34
24      38     3     0     1
25      40     0     0     2
26      40     0     1     1
27       5    35     0     2
28       0    39     1     2
29       0    32     4     6
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TRB2_MA1355.1
XX
ID  TRB2_MA1355.1
XX
DE  MA1355.1 TRB2; from JASPAR
P0       a     c     g     t
1      275    91    69   163
2      247   122    50   179
3      183   146    50   219
4      120    85    93   300
5      320     8    90   180
6      399     3     1   195
7      353     0   245     0
8       45   553     0     0
9        0   598     0     0
10       0   598     0     0
11       0     0     0   598
12     598     0     0     0
13     505     0    91     2
14     197    92    11   298
15      92   121    28   357
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TRP2_MA1356.1
XX
ID  TRP2_MA1356.1
XX
DE  MA1356.1 TRP2; from JASPAR
P0       a     c     g     t
1      335    34    21    35
2      317    24    18    66
3       44   303    22    56
4       23   344    28    30
5       47   272    15    91
6       72     0     4   349
7      423     0     1     1
8      419     1     2     3
9      422     2     0     1
10       7   416     0     2
11       6   414     2     3
12       0   416     0     9
13       4     3     0   418
14     407     2    13     3
15     422     1     2     0
16     409     4     2    10
17      27   350     1    47
18      18   383     0    24
19      54   303     7    61
20      37    43     7   338
21     337    28    15    45
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  bHLH80_MA1357.1
XX
ID  bHLH80_MA1357.1
XX
DE  MA1357.1 bHLH80; from JASPAR
P0       a     c     g     t
1      132    38   103   322
2      104    18   429    44
3        1   594     0     0
4      595     0     0     0
5      595     0     0     0
6        0     0   595     0
7        0     0     0   595
8        1     0   235   359
9        0     0   595     0
10     146   277    81    91
11     235   140    49   171
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  bHLH130_MA1358.1
XX
ID  bHLH130_MA1358.1
XX
DE  MA1358.1 bHLH130; from JASPAR
P0       a     c     g     t
1       65    14    54    52
2       31    33    68    53
3        0   184     1     0
4       98    87     0     0
5      185     0     0     0
6        1   184     0     0
7        1     0     0   184
8        4     0     0   181
9        0     0   185     0
10      18   105    17    45
11      79    52    19    35
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BPE_MA1359.2
XX
ID  BPE_MA1359.2
XX
DE  MA1359.2 BPE; from JASPAR
P0       a     c     g     t
1      109    83   110    94
2      101    79    74   142
3       90    72   162    72
4       76   162    87    71
5        1   393     2     0
6      396     0     0     0
7        0   388     0     8
8        9     0   387     0
9        0     0     0   396
10       0     2   393     1
11      72    85   162    77
12      73   161    72    90
13     142    75    81    98
14      95   108    85   108
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BHLH74_MA1360.2
XX
ID  BHLH74_MA1360.2
XX
DE  MA1360.2 BHLH74; from JASPAR
P0       a     c     g     t
1      100    96   121   122
2      125    79    97   138
3      119   118   110    92
4       52   111   159   117
5        0   438     0     1
6      439     0     0     0
7        0   436     0     3
8        3     0   436     0
9        0     0     0   439
10       0     0   438     1
11     128   114   148    49
12      89   131    92   127
13     130   102    92   115
14     125   110    94   110
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  bHLH18_MA1361.1
XX
ID  bHLH18_MA1361.1
XX
DE  MA1361.1 bHLH18; from JASPAR
P0       a     c     g     t
1       34   128    20    19
2      109     2    58    32
3        0   143     7    51
4       38     4   153     6
5       28    32     8   133
6       12    13   116    60
7       60    15    29    97
8       55    27    31    88
9       61    61    15    64
10      11   190     0     0
11     193     0     4     4
12       0   199     0     2
13       9     0   192     0
14       7     3     0   191
15       0     0   197     4
16      29    34    79    59
17      39    60    34    68
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  bHLH77_MA1362.1
XX
ID  bHLH77_MA1362.1
XX
DE  MA1362.1 bHLH77; from JASPAR
P0       a     c     g     t
1       21    12    35     7
2       28    13    12    22
3       16     2    32    25
4       31     4    10    30
5       21    13    22    19
6       13     7    40    15
7        0    23    22    30
8        0    75     0     0
9       74     0     1     0
10       0    69     0     6
11       0     0    75     0
12       0     0     0    75
13       0     0    75     0
14      28    36     0    11
15      10    10    28    27
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  LRL2_MA1363.1
XX
ID  LRL2_MA1363.1
XX
DE  MA1363.1 LRL2; from JASPAR
P0       a     c     g     t
1        1    27     5     1
2        6     1    23     4
3        3     2     5    24
4        5     1    25     3
5        6     9     8    11
6        0     5    13    16
7        9     7     2    16
8        0    10    13    11
9        4     8    18     4
10       0    34     0     0
11      34     0     0     0
12       0    34     0     0
13       1     0    33     0
14       0     0     0    34
15       0     1    33     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BHLH72_MA1364.1
XX
ID  BHLH72_MA1364.1
XX
DE  MA1364.1 BHLH72; from JASPAR
P0       a     c     g     t
1       11    33    54    14
2       28    20    22    42
3       19     9    51    33
4       46     8    39    19
5       42    14    32    24
6       28     8    22    54
7        4    29    66    13
8        4    85     4    19
9        0   112     0     0
10     112     0     0     0
11       0   106     0     6
12       0     0   112     0
13       0     0     0   112
14       0     0   110     2
15       5    24    65    18
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT5G47660_MA1365.2
XX
ID  AT5G47660_MA1365.2
XX
DE  MA1365.2 AT5G47660; from JASPAR
P0       a     c     g     t
1     2448   540  1096  1345
2     1466  1149   808  2006
3      549    76  4639   165
4        0     0  5429     0
5        0    14     0  5415
6     5302    30    38    59
7     5236     8    50   135
8     4920    23   104   382
9     4790     1    34   604
10    2233   631   685  1880
11    2014   822   679  1914
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DF1_MA1366.1
XX
ID  DF1_MA1366.1
XX
DE  MA1366.1 DF1; from JASPAR
P0       a     c     g     t
1      292    39   111    38
2      101   225    39   115
3      149    30   292     9
4        0     0   480     0
5        0     0     0   480
6      416     0    14    50
7      480     0     0     0
8      449     3     8    20
9      380     3     0    97
10     316    31    32   101
11     191    71    73   145
12     149    41   141   149
13     164    46    94   176
14     207    47   100   126
15     248    41    53   138
16     226    51    55   148
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT1G76870_MA1367.1
XX
ID  AT1G76870_MA1367.1
XX
DE  MA1367.1 AT1G76870; from JASPAR
P0       a     c     g     t
1      209    94    53   100
2      218    64    59   115
3      173   127    65    91
4       87   173    73   123
5      287    21     3   145
6      436     5     5    10
7      418     0     2    36
8      448     6     0     2
9        6   450     0     0
10       0   452     0     4
11     175     4   257    20
12      41   150   222    43
13     177    71    53   155
14     229    46    35   146
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GT-4_MA1368.2
XX
ID  GT-4_MA1368.2
XX
DE  MA1368.2 GT-4; from JASPAR
P0       a     c     g     t
1      209   130   110   197
2       77   157    89   323
3      111    55    37   443
4      516    20    44    66
5      621     6     4    15
6       10   612     9    15
7        8   181     2   455
8      571    24    28    23
9       20    26    33   567
10      87     1   552     6
11      20     5   618     3
12       9     5     3   629
13      63    36    11   536
14     396    41    63   146
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HDG1_MA1369.1
XX
ID  HDG1_MA1369.1
XX
DE  MA1369.1 HDG1; from JASPAR
P0       a     c     g     t
1      179    56   274    90
2       23   218     0   358
3      518    67     0    14
4      400     0     0   199
5        0     0     0   599
6      126     0     0   473
7      598     0     1     0
8      599     0     0     0
9        0     0     0   599
10      62     4   488    45
11     167   291    23   118
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  IDD5_MA1370.1
XX
ID  IDD5_MA1370.1
XX
DE  MA1370.1 IDD5; from JASPAR
P0       a     c     g     t
1       40    18    20    82
2       29     4     8   119
3        1     7     4   148
4        0     2     7   151
5        0     3     0   157
6        0     0   159     1
7        0     1     0   159
8        0   160     0     0
9        3    14   111    32
10       1     5     8   146
11       3    25    10   122
12      17     4     8   131
13      53     6     0   101
14      13    57    75    15
15       6    44     3   107
16      31    14    78    37
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  IDD4_MA1371.1
XX
ID  IDD4_MA1371.1
XX
DE  MA1371.1 IDD4; from JASPAR
P0       a     c     g     t
1      213   233    42   103
2      429     5   140    17
3       57   250   242    42
4      430     0    14   147
5      510     0     0    81
6      470    29    78    14
7      529    42    16     4
8      186   301    78    26
9       16     0   575     0
10     580    10     0     1
11       0   591     0     0
12     585     0     6     0
13     581     3     7     0
14     547    15    23     6
15     448    24    35    84
16     309    71    61   150
17     264    72    61   194
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ZAT10_MA1372.1
XX
ID  ZAT10_MA1372.1
XX
DE  MA1372.1 ZAT10; from JASPAR
P0       a     c     g     t
1       23   402    14   161
2      431     0    15   154
3       34   325   157    84
4      132   102     0   366
5      158   155   128   159
6      161   153     1   285
7        0   486     0   114
8      600     0     0     0
9        0   444   156     0
10      30    13     0   557
11     173   158   133   136
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GAF1_MA1373.1
XX
ID  GAF1_MA1373.1
XX
DE  MA1373.1 GAF1; from JASPAR
P0       a     c     g     t
1       28    37     2    27
2       64     4    25     1
3        8    34    33    19
4       57     1     2    34
5       87     0     0     7
6       68     6    17     3
7       91     2     1     0
8       24    67     3     0
9        2     0    92     0
10      94     0     0     0
11       5    89     0     0
12      93     0     1     0
13      83    11     0     0
14      91     0     3     0
15      75     7     0    12
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  IDD7_MA1374.1
XX
ID  IDD7_MA1374.1
XX
DE  MA1374.1 IDD7; from JASPAR
P0       a     c     g     t
1      190   232    44   119
2      394    20   153    18
3       95   224   197    69
4      400     3    36   146
5      489     1     0    95
6      424    49    88    24
7      528    31    23     3
8      136   375    57    17
9        2     0   583     0
10     575     9     0     1
11       0   585     0     0
12     574     0    11     0
13     576     2     6     1
14     540    11    29     5
15     441    25    39    80
16     293    89    66   137
17     267    77    62   179
18     244    95   108   138
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANL2_MA1375.1
XX
ID  ANL2_MA1375.1
XX
DE  MA1375.1 ANL2; from JASPAR
P0       a     c     g     t
1      121     3   334   140
2       38   513     1    46
3      598     0     0     0
4        0     1     0   597
5        0     1     0   597
6      473     0     2   123
7      598     0     0     0
8      192     1     0   405
9        8     0    42   548
10     339     8   228    23
11     109   284    35   170
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DEAR3_MA1376.1
XX
ID  DEAR3_MA1376.1
XX
DE  MA1376.1 DEAR3; from JASPAR
P0       a     c     g     t
1      192   143    90   172
2      231    61   140   165
3      223     7   112   255
4       18     6   131   442
5        0     0   597     0
6       14     1    14   568
7        0   597     0     0
8        0     0   597     0
9        0     0   596     1
10       1    71     0   525
11       2     1   592     2
12     179   100   277    41
13     156   180    78   183
14     135    84   209   169
15     157   112   179   149
16     155   160   128   154
17     191    76   187   143
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PLT1_MA1377.1
XX
ID  PLT1_MA1377.1
XX
DE  MA1377.1 PLT1; from JASPAR
P0       a     c     g     t
1       30    13    97    55
2       42     5   111    37
3        3   171     0    21
4      146     9    24    16
5        2   187     3     3
6       29     4   156     6
7       59    41    64    31
8       92     9    16    78
9       37    20     5   133
10      43    82     0    70
11      13   103     1    78
12       0   193     0     2
13      47     9   126    13
14     189     0     6     0
15      16     9   142    28
16      76     3   114     2
17      80    62    10    43
18      80    28    49    38
19      72    33    54    36
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AIL6_MA1378.1
XX
ID  AIL6_MA1378.1
XX
DE  MA1378.1 AIL6; from JASPAR
P0       a     c     g     t
1       11   345    16   159
2      135   351    13    32
3        2    24     3   502
4       33   357    33   108
5        2     7   517     5
6      222     0   278    31
7      167     7   238   119
8      368    16    46   101
9      223    24    41   243
10      96   153    97   185
11      37   372    17   105
12      23    11   473    24
13      83    78    59   311
14      98    14   386    33
15     131   260    24   116
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SOL1_MA1379.1
XX
ID  SOL1_MA1379.1
XX
DE  MA1379.1 SOL1; from JASPAR
P0       a     c     g     t
1      192    17    27   364
2       95    16     6   483
3      167    37     7   389
4      219    69    46   266
5      441    54    70    35
6      543     0    43    14
7      466     3    66    65
8      484     0    23    93
9        0     0     0   600
10       0     0     0   600
11       0   169     0   431
12     482     0   118     0
13     555    45     0     0
14     599     0     1     0
15     268    16     0   316
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCX6_MA1380.1
XX
ID  TCX6_MA1380.1
XX
DE  MA1380.1 TCX6; from JASPAR
P0       a     c     g     t
1      236     0    33   328
2        0     0     0   597
3        0     0    85   512
4        0    82     0   515
5      403     0   194     0
6      597     0     0     0
7      597     0     0     0
8       91    16     0   490
9      152    96    13   336
10       8    30     0   559
11      51    18    65   463
12     139    45    49   364
13     232    16   112   237
14     425     8    20   144
15     409    19    12   157
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT3G46070_MA1381.1
XX
ID  AT3G46070_MA1381.1
XX
DE  MA1381.1 AT3G46070; from JASPAR
P0       a     c     g     t
1       45    53    14    90
2       27    91    15    69
3       78    47     8    69
4       63    55    16    68
5       18    79    14    91
6       48    64    19    71
7        9     0     0   193
8        0   202     0     0
9      200     0     0     2
10       2   200     0     0
11       0     0     0   202
12      14   131    22    35
13      42    44    10   106
14      59    78    13    52
15      63    65    16    58
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  FAR1_MA1382.1
XX
ID  FAR1_MA1382.1
XX
DE  MA1382.1 FAR1; from JASPAR
P0       a     c     g     t
1      234   117    98   142
2      122   191    97   181
3       73   236    58   224
4      126   193    13   259
5        8   550    17    16
6      591     0     0     0
7        0   591     0     0
8        0     0   591     0
9        0   587     0     4
10       0     0   590     1
11       0   586     0     5
12      27   321    50   193
13     112    62   147   270
14     145   212    82   152
15     135   219    66   171
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  KAN2_MA1383.1
XX
ID  KAN2_MA1383.1
XX
DE  MA1383.1 KAN2; from JASPAR
P0       a     c     g     t
1       74    52    16    72
2       45    43    14   112
3       58    44    24    88
4       93    44    49    28
5       30    24   113    47
6      137    18     8    51
7      170     0     0    44
8        0     0     0   214
9      214     0     0     0
10       0     0     0   214
11       2     0     0   212
12       3   210     0     1
13       7    48     0   159
14      24    55    12   123
15      46    40    12   116
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHL7_MA1384.1
XX
ID  PHL7_MA1384.1
XX
DE  MA1384.1 PHL7; from JASPAR
P0       a     c     g     t
1      344    58    90   106
2      349    24    94   131
3      301    87   110   100
4      261   122   215     0
5        0     0   598     0
6      451    86     1    60
7      598     0     0     0
8        0     0     0   598
9      531    67     0     0
10       3     0     0   595
11      88    44    13   453
12      14   542     5    37
13      26   273   114   185
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT2G40260_MA1385.1
XX
ID  AT2G40260_MA1385.1
XX
DE  MA1385.1 AT2G40260; from JASPAR
P0       a     c     g     t
1      139   129    27   305
2      162   121    54   263
3      250    72    67   211
4      250    66   114   170
5      383    35     8   174
6      461     0     2   137
7      148   151    35   266
8      599     0     0     1
9        0     0     0   600
10       1     0     0   599
11       0   600     0     0
12       0   194     9   397
13     134   152    29   285
14      76    83    20   421
15     130    87    78   305
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HHO3_MA1386.2
XX
ID  HHO3_MA1386.2
XX
DE  MA1386.2 HHO3; from JASPAR
P0       a     c     g     t
1      652   436   311   849
2      666   534   321   727
3      777   381   490   600
4     1482   236   174   356
5     2197    12     8    31
6        2     4  2170    72
7     2206    11     9    22
8       79     2     1  2166
9       68    18    32  2130
10       5  2237     4     2
11     247   386   167  1448
12     680   378   474   716
13     684   415   339   810
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HRS1_MA1387.1
XX
ID  HRS1_MA1387.1
XX
DE  MA1387.1 HRS1; from JASPAR
P0       a     c     g     t
1      234    85   129   148
2      126    66   290   114
3      260    70    92   174
4      392    48    45   111
5      101    71    34   390
6      132   302    97    65
7      144   101   250   101
8      423    32    61    80
9      548     0    20    28
10       0     0   405   191
11     596     0     0     0
12       0     0     0   596
13      25    22     4   545
14       0   596     0     0
15      43   250    28   275
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHL2_MA1388.1
XX
ID  PHL2_MA1388.1
XX
DE  MA1388.1 PHL2; from JASPAR
P0       a     c     g     t
1      254   145   201     0
2        0     0   600     0
3      572    22     0     6
4      600     0     0     0
5        0     0     0   600
6      237   363     0     0
7      124     7    10   459
8      174    54   204   168
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHL1_MA1389.1
XX
ID  PHL1_MA1389.1
XX
DE  MA1389.1 PHL1; from JASPAR
P0       a     c     g     t
1      385    87    85    43
2      334    66   105    95
3      325    49   188    38
4       38     2   545    15
5      482    49    25    44
6      600     0     0     0
7        0     1    59   540
8      599     1     0     0
9        0     0     1   599
10      46    14    43   497
11       2   597     1     0
12       0   263   154   183
13     226   122   125   127
14     317    61    44   178
15     154    76   101   269
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HHO2_MA1390.2
XX
ID  HHO2_MA1390.2
XX
DE  MA1390.2 HHO2; from JASPAR
P0       a     c     g     t
1      395   471   237   435
2      469   240   435   394
3     1046   130   123   239
4     1453    14    22    49
5       10     7  1350   171
6     1510     4    16     8
7       37     2     3  1496
8       55    23    14  1446
9       15  1518     1     4
10     155   960   119   304
11     717   181   291   349
12     443   238   258   599
13     506   280   249   503
14     497   237   299   505
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB33_MA1391.2
XX
ID  MYB33_MA1391.2
XX
DE  MA1391.2 MYB33; from JASPAR
P0       a     c     g     t
1      192   147    70   213
2      217   126    75   204
3      341    75    85   121
4       20    11     2   589
5      614     3     3     2
6      613     0     7     2
7        0   615     0     7
8       12   569     6    35
9       31     1   586     4
10     148    35    15   424
11     370    87    16   149
12     268    51    61   242
13     199   135   107   181
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB98_MA1392.2
XX
ID  MYB98_MA1392.2
XX
DE  MA1392.2 MYB98; from JASPAR
P0       a     c     g     t
1      425    76   189   329
2      307    30   178   504
3      529   378    83    29
4        9   991     3    16
5       11     5  1000     3
6        6     1     1  1011
7        5     4     6  1004
8     1004     4     3     8
9        0  1015     2     2
10     407   117   182   313
11     377    95   156   391
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB70_MA1393.2
XX
ID  MYB70_MA1393.2
XX
DE  MA1393.2 MYB70; from JASPAR
P0       a     c     g     t
1      671   278   309   612
2      454   299   181   936
3     1560    29   116   165
4     1708     7   100    55
5        0  1848     6    16
6       66  1736     0    68
7       18     1  1849     2
8      137   112    71  1550
9      164    95    19  1592
10     814   179   377   500
11     594   364   305   607
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB73_MA1394.2
XX
ID  MYB73_MA1394.2
XX
DE  MA1394.2 MYB73; from JASPAR
P0       a     c     g     t
1      298   222   192   354
2      274   268    46   478
3     1056     0     6     4
4     1024    36     1     5
5        5  1047     2    12
6       19   102   847    98
7       19     4  1026    17
8       16    27     4  1019
9      191   614    24   237
10     963    32    36    35
11     411   231   130   294
12     355   199   184   328
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GATA6_MA1396.1
XX
ID  GATA6_MA1396.1
XX
DE  MA1396.1 GATA6; from JASPAR
P0       a     c     g     t
1       45   159    37   176
2      391     0    22     4
3        0     0   417     0
4      417     0     0     0
5        0     0     0   417
6        0   417     0     0
7        0    69     6   342
8      345     0    72     0
9       93    57   244    23
10     298    50    30    39
11      94    47    40   236
12      91   162    53   111
13      99   117    44   157
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT1G74840_MA1397.1
XX
ID  AT1G74840_MA1397.1
XX
DE  MA1397.1 AT1G74840; from JASPAR
P0       a     c     g     t
1      300     8    14   278
2      255    32   114   199
3       46    92     2   460
4       39     0   561     0
5        0     0   600     0
6      600     0     0     0
7        0     0     0   600
8      578     6     5    11
9      549     0    38    13
10     168    60   235   137
11     208    72   228    92
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  KUA1_MA1398.2
XX
ID  KUA1_MA1398.2
XX
DE  MA1398.2 KUA1; from JASPAR
P0       a     c     g     t
1      873   194   273   477
2      328   600   211   678
3       69  1567   112    69
4      127    61    18  1611
5      107    40    41  1629
6     1751    29    11    26
7       14    14     9  1780
8       15  1771    17    14
9       35  1502    27   253
10     943   102   335   437
11     633   377   174   633
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT1G19000_MA1400.1
XX
ID  AT1G19000_MA1400.1
XX
DE  MA1400.1 AT1G19000; from JASPAR
P0       a     c     g     t
1      272    22    73   232
2      319     9    28   243
3      282    27   136   154
4       33    81     1   484
5       29     0   570     0
6        0     0   599     0
7      599     0     0     0
8        0     0     0   599
9      580     8     5     6
10     554     0    33    12
11      82    56   338   123
12     172    61   319    47
13     161    86    48   304
14     169    71    86   273
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RVE7_MA1401.1
XX
ID  RVE7_MA1401.1
XX
DE  MA1401.1 RVE7; from JASPAR
P0       a     c     g     t
1      252    93   164    88
2      421     9    16   151
3      594     0     0     3
4      597     0     0     0
5      597     0     0     0
6        0     0     0   597
7      597     0     0     0
8        0     0     0   597
9        0   597     0     0
10      22    46     3   526
11     154   100    66   277
12     297   102   108    90
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BPC6_MA1402.1
XX
ID  BPC6_MA1402.1
XX
DE  MA1402.1 BPC6; from JASPAR
P0       a     c     g     t
1       24   134     2    28
2       18    19     0   151
3       13   161    10     4
4       15     7     5   161
5       12   142     7    27
6        6     2     5   175
7        0   160     3    25
8        2     6     1   179
9        1   177     2     8
10       0     1     0   187
11       1   168     0    19
12       2     0     0   186
13       2   186     0     0
14       0     1     0   187
15      12   169     0     7
16       4     3     0   181
17      12   168     5     3
18       8     6     0   174
19       6   170     0    12
20       3     2     2   181
21      50   123     1    14
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BPC5_MA1403.1
XX
ID  BPC5_MA1403.1
XX
DE  MA1403.1 BPC5; from JASPAR
P0       a     c     g     t
1       95     0     7     0
2        3     0    99     0
3      102     0     0     0
4        2     0    98     2
5      100     0     0     2
6        1     0    99     2
7       99     0     0     3
8        0     1   100     1
9       99     0     2     1
10       1     1   100     0
11     102     0     0     0
12       0     0   102     0
13     102     0     0     0
14       4     0    97     1
15     101     0     1     0
16       2     0   100     0
17     101     0     1     0
18       1     1   100     0
19      98     0     1     3
20       1     1    99     1
21     101     0     1     0
22       0     1   101     0
23     100     0     0     2
24       1     1    99     1
25      98     0     3     1
26       1     1    99     1
27      98     0     0     4
28       0     4    98     0
29     100     2     0     0
30       5     3    91     3
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BPC1_MA1404.1
XX
ID  BPC1_MA1404.1
XX
DE  MA1404.1 BPC1; from JASPAR
P0       a     c     g     t
1       82    18   421    18
2      483     4    29    23
3        8    15   483    33
4      501     0    18    20
5       67     9   448    15
6      500     0    39     0
7       43    17   461    18
8      478     3    44    14
9       39     0   482    18
10     520     6     2    11
11      27     2   508     2
12     518    11     3     7
13      62     9   450    18
14     460     0    56    23
15      54     0   462    23
16     485     0    54     0
17      41    10   467    21
18     508     0     9    22
19      71     2   459     7
20     478    12    35    14
21      73    19   440     7
22     450    15    39    35
23      47    20   427    45
24     436    22    43    38
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SIZF2_MA1405.1
XX
ID  SIZF2_MA1405.1
XX
DE  MA1405.1 SIZF2; from JASPAR
P0       a     c     g     t
1    16191 20703 39343 23762
2    88380   627  8331  2662
3      264 78711 20496   530
4     3329   918  1251 94502
5     7440  2953 88717   889
6    63395  4073  4720 27813
7      889 88717  2953  7440
8    94502  1251   918  3329
9      530 20496 78711   264
10    2662  8331   627 88380
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ATHB-4_MA1406.1
XX
ID  ATHB-4_MA1406.1
XX
DE  MA1406.1 ATHB-4; from JASPAR
P0       a     c     g     t
1     2392 40381  1915 55312
2    82255  3298   618 13829
3    98348  1291    93   268
4      297   333    76 99294
5     3466 42828 40460 13246
6    99128    66   333   473
7      998   228   422 98352
8     5565   764  2806 90866
9    18045  3599 68409  9947
10   12542 33825 35017 18616
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  FaEOBII_MA1408.1
XX
ID  FaEOBII_MA1408.1
XX
DE  MA1408.1 FaEOBII; from JASPAR
P0       a     c     g     t
1    45035  6949 26996 21020
2    21068   992 73515  4425
3     1124   422  9987 88466
4     2313  1649   767 95271
5    77467  6291  2500 13742
6     1296   865 97032   807
7     1374   865 91609  6153
8     1336 21093   480 77091
9    66987  9932  8318 14763
10   28881 20021 24969 26129
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  OsRR22_MA1409.1
XX
ID  OsRR22_MA1409.1
XX
DE  MA1409.1 OsRR22; from JASPAR
P0       a     c     g     t
1    51257 11381 11771 25591
2    86870  3042  6041  4047
3      486   522 97670  1322
4    97906   287   778  1028
5      495   484   510 98511
6    72395  7724   464 19416
7      444 91648   611  7297
8     1690  2838 92633  2838
9    13926 33145 46756  6173
10   20468 11356  5511 62666
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  StBRC1_MA1410.1
XX
ID  StBRC1_MA1410.1
XX
DE  MA1410.1 StBRC1; from JASPAR
P0       a     c     g     t
1    11498 23683 36598 28222
2     2437  2082 93015  2466
3      419   447 98500   634
4     4402 14768 75089  5741
5     4602 59710 31742  3946
6      925 97660   962   453
7     6571 91694  1031   705
8    30634 52830 11378  5158
9    16590 52087  9765 21559
10   14072 48745 17972 19211
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TSAR1_MA1411.1
XX
ID  TSAR1_MA1411.1
XX
DE  MA1411.1 TSAR1; from JASPAR
P0       a     c     g     t
1    31261 26502 19831 22405
2     3300  5250 91222   228
3    12545 87319    51    84
4    93404    16  5527  1052
5      284 98016   142  1558
6     1558   142 98016   284
7     1052  5527    16 93404
8       84    51 87319 12545
9      228 91222  5250  3300
10   32821 14091 33427 19661
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TSAR2_MA1412.1
XX
ID  TSAR2_MA1412.1
XX
DE  MA1412.1 TSAR2; from JASPAR
P0       a     c     g     t
1    17675 28815 22859 30652
2     2687  2274 94487   552
3     1998 97506   139   358
4    72496   128 26792   585
5      392 95417    60  4131
6     4131    60 95417   392
7      585 26792   128 72496
8      358   139 97506  1998
9      552 94487  2274  2687
10   26626 19532 32839 21003
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  E2FA_MA1414.1
XX
ID  E2FA_MA1414.1
XX
DE  MA1414.1 E2FA; from JASPAR
P0       a     c     g     t
1      394   203   184   670
2      355   339   579   178
3       23    62  1355    11
4       21  1375    22    33
5       35     6  1391    19
6        7  1415    18    11
7        1  1376     1    73
8     1368    10    21    52
9      574   345   121   411
10     535   293   334   289
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  REF6_MA1415.1
XX
ID  REF6_MA1415.1
XX
DE  MA1415.1 REF6; from JASPAR
P0       a     c     g     t
1     1983   209  1144   351
2     2634   137   577   339
3     3458    27    24   178
4     3511    34   118    24
5        1  3661     1    24
6     3622    25    30    10
7        6    21  3625    35
8     3533    21   105    28
9       45    10  3581    51
10     752  1063   884   988
11    2249   473   441   524
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RAMOSA1_MA1416.1
XX
ID  RAMOSA1_MA1416.1
XX
DE  MA1416.1 RAMOSA1; from JASPAR
P0       a     c     g     t
1    10200  3932 56665  4047
2    56235  3491 12116  3002
3     7487  2714 61021  3622
4    61422  2682  8810  1930
5     1100  1543 70293  1908
6    74267   348    83   146
7        9    99 74518   218
8    74320   279    80   165
9        9   119 74498   218
10   74515   168    70    91
11      35   115 74518   176
12   62368  2040  8936  1500
13    7816  2212 61788  3028
14   56980  2591 12358  2915
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  O2_MA1417.1
XX
ID  O2_MA1417.1
XX
DE  MA1417.1 O2; from JASPAR
P0       a     c     g     t
1      306   374  1046   312
2      129   391   151  1367
3       29     9  1895   105
4       56  1959    12    11
5        3  2016    17     2
6     2021     3    12     2
7       18  1485     7   528
8      354   103  1571    10
9        4    13     2  2019
10      19  1991    28     0
11    1961    11    62     4
12      37   282   412  1307
13     124  1672    97   145
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF019_MA1424.1
XX
ID  ERF019_MA1424.1
XX
DE  MA1424.1 ERF019; from JASPAR
P0       a     c     g     t
1      192   468   120   218
2      275    97   328   298
3       63   528   144   263
4       30   949     3    16
5      902     0    97     0
6        0  1000     0     0
7        0  1000     0     0
8        0     0  1000     0
9      796   127     5    70
10       0  1000     0     0
11     741    90   132    35
12     320   359    90   229
13     293   288   152   265
14     285   172   293   248
15     275   330   191   203
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HYH_MA1425.1
XX
ID  HYH_MA1425.1
XX
DE  MA1425.1 HYH; from JASPAR
P0       a     c     g     t
1      201   201   396   201
2      372   152   321   152
3      283    72    72   571
4       23   350   523   102
5      999     0     0     0
6        0   999     0     0
7        0     0   999     0
8        0     0     0   999
9       48   154   747    48
10     203    97   392   306
11     177   466   177   177
12     321   226   226   226
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB124_MA1426.1
XX
ID  MYB124_MA1426.1
XX
DE  MA1426.1 MYB124; from JASPAR
P0       a     c     g     t
1      324   230   221   223
2      200   284   183   330
3      546   266   178     8
4       40   799   124    36
5       36     0   963     0
6        0   999     0     0
7        1     0   680   318
8       61   718   119   100
9       80   610   159   149
10     271   222   197   309
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC028_MA1427.2
XX
ID  NAC028_MA1427.2
XX
DE  MA1427.2 NAC028; from JASPAR
P0       a     c     g     t
1      347   371   214   370
2      481   212   311   298
3       31  1188    34    49
4     1279     4     7    12
5     1202    71    10    19
6       13     2  1281     6
7       97    90    48  1067
8     1103    64     7   128
9     1266     9     3    24
10     198   301   138   665
11     195   824   117   166
12     365   189   230   518
13     420   303   197   382
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCP8_MA1428.1
XX
ID  TCP8_MA1428.1
XX
DE  MA1428.1 TCP8; from JASPAR
P0       a     c     g     t
1      172   240   373   213
2       41    15   801   140
3      143    29   725   101
4      150   255   433   161
5      160   652    63   122
6       32   963     2     0
7       20   902     0    76
8      864     1   113    20
9       32   725    97   145
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TB1_MA1430.1
XX
ID  TB1_MA1430.1
XX
DE  MA1430.1 TB1; from JASPAR
P0       a     c     g     t
1        0     0   999     0
2        0     0   999     0
3       84   308   518    88
4        0   998     0     0
5        0   999     0     0
6       41   958     0     0
7       37   953     0     7
8      153   839     4     3
9      143   648    13   195
10     385   405   119    89
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ABF4_MA1659.1
XX
ID  ABF4_MA1659.1
XX
DE  MA1659.1 ABF4; from JASPAR
P0       a     c     g     t
1     1452  1325  1105  2164
2      970   689  2625  1762
3      660  5169    62   155
4      135  5763    86    62
5     5906    20    47    73
6      175  5666    44   161
7      210   166  5470   200
8      121    58    52  5815
9      610  3358  1825   253
10    2322   583  1360  1781
11    1460  1865   928  1793
12    1630  1901   849  1666
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC013_MA1660.1
XX
ID  NAC013_MA1660.1
XX
DE  MA1660.1 NAC013; from JASPAR
P0       a     c     g     t
1      398   261   175   362
2      303   217   306   370
3      223   130   229   614
4      101    22    24  1049
5      195    29   128   844
6      631   144   182   239
7       16  1172     0     8
8        2     9    81  1104
9        1     2     0  1193
10      41   131   964    60
11     134   180   101   781
12     164   164   224   644
13     207   558   198   233
14     240   293   138   525
15     295   529   197   175
16      23  1125    28    20
17    1195     0     0     1
18    1094    98     1     3
19       9     0  1181     6
20     250   148   120   678
21     875   108    15   198
22    1016    19    16   145
23     380   309   164   343
24     338   352   177   329
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GTL1_MA1661.1
XX
ID  GTL1_MA1661.1
XX
DE  MA1661.1 GTL1; from JASPAR
P0       a     c     g     t
1      492    95   203   283
2      314   253   127   379
3       24     7  1023    19
4       42     5  1020     6
5       48     3    93   929
6      300    17    74   682
7     1038     5    12    18
8     1033    13    10    17
9      996    11    13    53
10     954    18    23    78
11     409   126   138   400
12     356   124   193   400
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT3G10030_MA1662.1
XX
ID  AT3G10030_MA1662.1
XX
DE  MA1662.1 AT3G10030; from JASPAR
P0       a     c     g     t
1      335   254   122   299
2      296   117   316   281
3       66    60    55   829
4       30    12     4   964
5      965    12    22    11
6      972    11    16    11
7       22   958     8    22
8      765    20   199    26
9       16     8   962    24
10     873    53     9    75
11     378   182   234   216
12     300   188   225   297
13     274   212   124   400
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC050_MA1663.2
XX
ID  NAC050_MA1663.2
XX
DE  MA1663.2 NAC050; from JASPAR
P0       a     c     g     t
1     1241  1036   869  1123
2     1140  1204   729  1196
3     1427   778  1097   967
4      108  3913   118   130
5     4208    22    14    25
6     4035   144    38    52
7       41    23  4171    34
8      988   316   238  2727
9     3785   157    43   284
10    4088    40    27   114
11     411  3192   280   386
12    1308  1162   556  1243
13    1316   731   742  1480
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HSFA6B_MA1664.2
XX
ID  HSFA6B_MA1664.2
XX
DE  MA1664.2 HSFA6B; from JASPAR
P0       a     c     g     t
1      205   245   413   293
2      228   412   234   282
3       47    44    14  1051
4       15    18     9  1114
5       13  1108    20    15
6       27    50    16  1063
7      366    78   660    52
8       14    11  1121    10
9     1114     7    12    23
10    1044    15    40    57
11     268   259   364   265
12     287   397   238   234
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HSFB2A_MA1665.2
XX
ID  HSFB2A_MA1665.2
XX
DE  MA1665.2 HSFB2A; from JASPAR
P0       a     c     g     t
1      206   292   154   235
2      179   171    84   453
3      374    77   390    46
4        9     4   866     8
5      878     2     2     5
6      871     1     2    13
7       46    32   713    96
8       96   264   433    94
9       22     2     4   859
10      12     2     2   871
11       7   858     7    15
12      59   261    82   485
13     431    82   181   193
14     228   131   309   219
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HSFB2B_MA1666.2
XX
ID  HSFB2B_MA1666.2
XX
DE  MA1666.2 HSFB2B; from JASPAR
P0       a     c     g     t
1      278   516   236   270
2      277   243   147   633
3      543   151   529    77
4        8    11  1274     7
5     1262     5     8    25
6     1257     5     7    31
7       77    53  1032   138
8      155   352   655   138
9       43     4     6  1247
10      28     7     8  1257
11      10  1264     9    17
12      35   195    83   987
13     736   144   209   211
14     236   164   688   212
15     762   104   174   260
16     580   186   249   285
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HSFC1_MA1667.2
XX
ID  HSFC1_MA1667.2
XX
DE  MA1667.2 HSFC1; from JASPAR
P0       a     c     g     t
1      308   292   613   367
2      358   470   305   447
3       73    45    14  1448
4       28     7     6  1539
5       12  1538    15    15
6       34   765    99   682
7     1476     9    80    15
8       22    22  1521    15
9     1559     1     9    11
10    1479    12    36    53
11     447   269   522   342
12     362   648   299   271
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DREB1B_MA1669.1
XX
ID  DREB1B_MA1669.1
XX
DE  MA1669.1 DREB1B; from JASPAR
P0       a     c     g     t
1      718   486   667   865
2      527   726   396  1087
3      417   789   445  1085
4      756    33  1879    68
5       15  2617    21    83
6       13  2711     4     8
7        8    14  2708     6
8     2607    44    34    51
9       10  2697    12    17
10    2232   108   235   161
11     293   194   102  2147
12     742   879   576   539
13     945   389   746   656
14     817   570   512   837
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DREB1C_MA1670.1
XX
ID  DREB1C_MA1670.1
XX
DE  MA1670.1 DREB1C; from JASPAR
P0       a     c     g     t
1     2439  2044  1220  2749
2     2252  1566  1900  2734
3     1988  1872  1172  3420
4     1562  1719  1265  3906
5     2243    45  6059   105
6       59  7517    35   841
7       43  8357    15    37
8       43    38  8333    38
9     8318    50    48    36
10      28  8363    26    35
11    6267   594   647   944
12    1931   855   561  5105
13    2883  1880  1687  2002
14    2984  1292  1942  2234
15    2781  1613  1517  2541
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF118_MA1671.1
XX
ID  ERF118_MA1671.1
XX
DE  MA1671.1 ERF118; from JASPAR
P0       a     c     g     t
1      703  1819   234   643
2      977   385  1143   894
3      254  2309   354   482
4       30  3343     0    26
5      195     8  3186    10
6        0  3396     2     1
7       19  3370     3     7
8      170     9  3190    30
9        2  3338    11    48
10     593  1903   286   617
11     931   292  1537   639
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GBF2_MA1672.1
XX
ID  GBF2_MA1672.1
XX
DE  MA1672.1 GBF2; from JASPAR
P0       a     c     g     t
1     2246  1574  2262  1840
2     2324  2084   679  2835
3      560  2988  3420   954
4     7479   101   145   197
5      223  7122   221   356
6      319   107  7275   221
7      227   127    77  7491
8      174    83  7553   112
9      333   153  6028  1408
10    1027  6254   282   359
11    5558   596   891   877
12    2457  1420  1653  2392
13    2559  1516  1488  2359
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  LBD18_MA1673.1
XX
ID  LBD18_MA1673.1
XX
DE  MA1673.1 LBD18; from JASPAR
P0       a     c     g     t
1     1364  1402   922  2177
2     1382  2201   833  1449
3      501   245  4833   286
4      122  5323   290   130
5      228  5168   316   153
6      128    40  5531   166
7      105   171  5338   251
8     5373    93   159   240
9     3997   191  1009   668
10    4330   472   384   679
11    2258  1201   671  1735
12    1519  1189  1247  1910
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC017_MA1674.2
XX
ID  NAC017_MA1674.2
XX
DE  MA1674.2 NAC017; from JASPAR
P0       a     c     g     t
1      298   237   240   222
2      340   235   156   266
3      218   116   527   136
4       12   957     9    19
5      990     1     0     6
6      879    92     7    19
7        9     4   978     6
8      127    72    60   738
9      917    24     6    50
10     928     4     6    59
11     255   275   193   274
12     260   284   153   300
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC029_MA1675.1
XX
ID  NAC029_MA1675.1
XX
DE  MA1675.1 NAC029; from JASPAR
P0       a     c     g     t
1      116    93    78   159
2      235    50    54   107
3       41   211    81   113
4      444     1     0     1
5       11   429     4     2
6        6     4   428     8
7       22   294    42    88
8      421    11     5     9
9      422     2     2    20
10       7   388    22    29
11      30    66    16   334
12     103    75    93   175
13     148   106    74   118
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC062_MA1676.2
XX
ID  NAC062_MA1676.2
XX
DE  MA1676.2 NAC062; from JASPAR
P0       a     c     g     t
1      150   108   102   180
2      135   127   106   172
3      129    78   255    78
4       94    76    88   282
5       21    58    35   426
6      533     2     2     3
7      520     5     4    11
8        4     3   528     5
9       51    26    18   445
10     507     8     2    23
11     514     2     4    20
12     112   203    56   169
13     147   157    72   164
14     194    76    91   179
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC078_MA1677.1
XX
ID  NAC078_MA1677.1
XX
DE  MA1677.1 NAC078; from JASPAR
P0       a     c     g     t
1     1698  1538   929  1671
2     1965  1228  1361  1282
3        9  5734     9    84
4     5690    18    58    70
5     5496   255    22    63
6        1     9  5745    81
7     4732   228   142   734
8     5341    98    35   362
9     5581    49    57   149
10    1103  2610   757  1366
11    1772  1650  1053  1361
12    1780   951  1146  1959
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NTL8_MA1678.2
XX
ID  NTL8_MA1678.2
XX
DE  MA1678.2 NTL8; from JASPAR
P0       a     c     g     t
1      324   197   197   264
2      245   165   175   397
3       64   171   669    78
4      958     5    10     9
5      912    12    42    16
6        8     6   956    12
7      795    79    35    73
8      957     4     3    18
9      933    10    16    23
10      45   850    40    47
11     220   468    96   198
12     245   189   142   406
13     324   201   154   303
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RAP2-1_MA1679.1
XX
ID  RAP2-1_MA1679.1
XX
DE  MA1679.1 RAP2-1; from JASPAR
P0       a     c     g     t
1      216   345   123   215
2      266   114   272   247
3       98   389   186   226
4       46   810    17    26
5      810     2    78     9
6        4   890     3     2
7        5   890     0     4
8        5     2   890     2
9      738   106    10    45
10       2   892     1     4
11     739    67    44    49
12     229   237    71   362
13     214   221   121   343
14     254   137   239   269
15     236   265   154   244
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SRM1_MA1681.1
XX
ID  SRM1_MA1681.1
XX
DE  MA1681.1 SRM1; from JASPAR
P0       a     c     g     t
1     1356   254   290  1250
2     1153   375   709   913
3     1274   358   443  1075
4     2050   135   631   334
5       11     4  3125    10
6     3062    44    19    25
7       20    10    13  3107
8     3017    21    25    87
9     3005    27    41    77
10      90   128  2774   158
11     913   246  1692   299
12     743   264   234  1909
13     958   357   390  1445
14    1245   392   543   970
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCX3_MA1682.1
XX
ID  TCX3_MA1682.1
XX
DE  MA1682.1 TCX3; from JASPAR
P0       a     c     g     t
1      314   154    90   413
2      457   127    91   296
3      701    48    63   159
4      751    37    46   137
5      782    10    36   143
6       22     6    11   932
7       24     1     2   944
8        0   971     0     0
9      940     2    11    18
10     943     0     3    25
11     955     2     3    11
12     182    60    16   713
13     316    97    62   496
14     244   154    60   513
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF10_MA1685.1
XX
ID  ARF10_MA1685.1
XX
DE  MA1685.1 ARF10; from JASPAR
P0       a     c     g     t
1     6226 10279  1870  1383
2     5611  5964  2004  6179
3    14080  1960  1922  1796
4     8441  2941  7020  1356
5     5069  5484  3268  5937
6     1319   649 17180   610
7      264   167 19132   195
8      186   250 19192   130
9    13662   268  5695   133
10     388   375 18929    66
11   18959   144   605    50
12     113 19406   147    92
13   14594   443  4553   168
14    6191  8527  1711  3329
15    1672  6272  6594  5220
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF13_MA1686.1
XX
ID  ARF13_MA1686.1
XX
DE  MA1686.1 ARF13; from JASPAR
P0       a     c     g     t
1     5788   794   652  1172
2     3446   543  4009   408
3      569  6725   463   649
4      576   145  7420   265
5      328    29  8001    48
6       55    55  8271    25
7      268    67  8022    49
8      118    86  8175    27
9     7344   111   943     8
10      36  8284    55    31
11    7953    33   393    27
12     749  4283   214  3160
13     532   518  6710   646
14     745   516   875  6270
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF14_MA1687.1
XX
ID  ARF14_MA1687.1
XX
DE  MA1687.1 ARF14; from JASPAR
P0       a     c     g     t
1      862  8235   681  1307
2     2403   571  7563   548
3      498  2616  7532   439
4      417  2651  7664   353
5      702  2221  7827   335
6      350   187 10466    82
7    10113   158   747    67
8       63 10858    94    70
9    10769    60   195    61
10     245  3731    97  7012
11     233    95 10683    74
12     234   291   150 10410
13     233 10042   177   633
14     593  2808  6490  1194
15     995  2579  6910   601
16     719  2592  6957   817
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF16_MA1688.1
XX
ID  ARF16_MA1688.1
XX
DE  MA1688.1 ARF16; from JASPAR
P0       a     c     g     t
1     2470  1473  2135  1754
2     3249  1643  1614  1326
3      980  5307   965   580
4     5651   950   578   653
5     6555   574   511   192
6      302   374  6749   407
7     7230   142   359   101
8      357  6981   283   211
9     7198   220   214   200
10    6756   261   338   477
11     687   565  6061   519
12    5396   697   942   797
13    2328  2087  1543  1874
14    2681  1648  1781  1722
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF18_MA1689.1
XX
ID  ARF18_MA1689.1
XX
DE  MA1689.1 ARF18; from JASPAR
P0       a     c     g     t
1      985   580   588   644
2      700   897   751   449
3       77  2334    50   336
4      346  2297    91    63
5       60    16  2698    23
6     2735    13    39    10
7        9  2746    25    17
8     2672   108    12     5
9     2292   154   209   142
10    2075   277   321   124
11     889   665   746   497
12     998   533   549   717
13     729   603   632   833
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF25_MA1690.1
XX
ID  ARF25_MA1690.1
XX
DE  MA1690.1 ARF25; from JASPAR
P0       a     c     g     t
1     7526  7690  1231  2993
2    13488  1748  1955  2249
3     9458  2342  6525  1115
4     4788  8051  3997  2604
5     1286   638 16922   594
6      490   134 18723    93
7      179   121 19073    67
8     7031   261 11973   175
9      602   171 18527   140
10   18246   140  1021    33
11     119 19053   197    71
12   18584    96   704    56
13    9076  6075   978  3311
14    1349  2535  8454  7102
15    1956  2345  8006  7133
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF27_MA1691.1
XX
ID  ARF27_MA1691.1
XX
DE  MA1691.1 ARF27; from JASPAR
P0       a     c     g     t
1     1524  2266  1789  1251
2     1928  2049  1322  1531
3     2684   860  1857  1429
4      700   671  4962   497
5      173  6134   279   244
6     5932   635   115   148
7      163    93  6412   162
8     6497   133   160    40
9       92  6539    89   110
10    6652    76    55    47
11     782   467   596  4985
12    1645  1498  2355  1332
13    1969  1600  1667  1594
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF29_MA1692.1
XX
ID  ARF29_MA1692.1
XX
DE  MA1692.1 ARF29; from JASPAR
P0       a     c     g     t
1     1242  1455  1310  1234
2     1450  1357  1244  1190
3     1269  1239  1117  1616
4     1475  1102  1278  1386
5      468   468  3994   311
6      200  4649   147   245
7      576  4459   116    90
8       83    16  5091    51
9     5053    43   120    25
10      16  5154    36    35
11    4801   289   121    30
12    3983   329   602   327
13    1531  1370  1531   809
14    1598  1242  1396  1005
15    1622  1206   988  1425
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF34_MA1693.1
XX
ID  ARF34_MA1693.1
XX
DE  MA1693.1 ARF34; from JASPAR
P0       a     c     g     t
1     4678  2573  3827  3172
2     3908  3118  3747  3477
3      388 13025   544   293
4    11877  1396   585   392
5      363   174 13571   142
6    13106   391   622   131
7      146 13756   233   115
8    13693   172   181   204
9     1458   607 10249  1936
10    1230 11608   825   587
11    5728  3751  2708  2063
12    4406  3332  3589  2923
13    3224  3521  4418  3087
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF35_MA1694.1
XX
ID  ARF35_MA1694.1
XX
DE  MA1694.1 ARF35; from JASPAR
P0       a     c     g     t
1     1326  1297  1351  1048
2     1011  1549  1219  1243
3     2144  1043  1014   821
4      744  2700  1101   477
5      250  4409   122   241
6      330  4521   117    54
7       37    15  4934    36
8     4934    16    58    14
9        7  4975    23    17
10    4709   253    39    21
11    3063   556   856   547
12     955   905  2699   463
13    1548  1161  1382   931
14    1377  1285   935  1425
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF36_MA1695.1
XX
ID  ARF36_MA1695.1
XX
DE  MA1695.1 ARF36; from JASPAR
P0       a     c     g     t
1     4333  1368  4470  1079
2     1572  6035  1646  1997
3     1106   795  8866   483
4      438   121 10453   238
5      526    86 10568    70
6     1507   130  9538    75
7      541   120 10495    94
8    10483    87   652    28
9       82 11032    83    53
10   10839    53   316    42
11    3409  4456   595  2790
12     935  1586  7323  1406
13    1087  1444  2452  6267
14    1273  6723   759  2495
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF39_MA1696.1
XX
ID  ARF39_MA1696.1
XX
DE  MA1696.1 ARF39; from JASPAR
P0       a     c     g     t
1      539  2006   641   882
2      720   576  2373   399
3      514    32  3496    26
4       46    27  3983    12
5      302    46  3612   108
6     2101    29  1884    54
7     3833    29   172    34
8       58  3926    62    22
9     3856   106    69    37
10     688  1832   697   851
11     466  1030  2258   314
12     629   517   669  2253
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF4_MA1697.1
XX
ID  ARF4_MA1697.1
XX
DE  MA1697.1 ARF4; from JASPAR
P0       a     c     g     t
1     2014  1950  1732   970
2     1712  2246  1795   913
3     5236   734   356   340
4      375  5738   334   219
5      335   143  6073   115
6     6453    57   112    44
7       35  6524    74    33
8     6158   155   271    82
9     5261   667   243   495
10     316   516  5584   250
11    1927  1481  1918  1340
12    1751  1750  1555  1610
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARF7_MA1698.1
XX
ID  ARF7_MA1698.1
XX
DE  MA1698.1 ARF7; from JASPAR
P0       a     c     g     t
1     1740  1003   752   570
2      775  1727   745   818
3     1321   502   463  1779
4      251   317  3325   172
5      113  3717   135   100
6      141  3801    64    59
7       83   120  3799    63
8     3801    46   176    42
9       18  3917    76    54
10    2657   173  1196    39
11    3150   457   229   229
12     419   529  2904   213
13     771   970  1756   568
14     798  1001   735  1531
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT1G14600_MA1706.1
XX
ID  AT1G14600_MA1706.1
XX
DE  MA1706.1 AT1G14600; from JASPAR
P0       a     c     g     t
1      301   196   307   196
2      359    49    49   544
3      806     0    94    99
4       94   596     0   310
5      906     0     0    94
6        0     0     0   999
7      201     0    94   705
8        0   799     0   201
9      148   261    54   537
10     300   201   201   297
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  E2FD_MA1732.1
XX
ID  E2FD_MA1732.1
XX
DE  MA1732.1 E2FD; from JASPAR
P0       a     c     g     t
1      208     9   246   537
2      104     2    13   882
3       47     4     2   947
4       36     0     7   956
5       15     2     5   978
6        5     4   989     2
7        0     0  1000     0
8        2   998     0     0
9        2     0   996     2
10       2    15   984     0
11       0    11   987     2
12     993     0     7     0
13    1000     0     0     0
14     958    13    11    18
15     774    51    22   153
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ASR3_MA1733.1
XX
ID  ASR3_MA1733.1
XX
DE  MA1733.1 ASR3; from JASPAR
P0       a     c     g     t
1      394   189   240   177
2      466   182   124   229
3      563   130    88   219
4      472    83   159   285
5      344   120   285   250
6      184     0   102   715
7      421     0   579     0
8       60   810     0   130
9        0  1000     0     0
10       0   895     0   105
11      15   104    10   871
12     474    28     0   497
13     649   165    32   154
14     624    78    78   219
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT1G19040_MA1734.1
XX
ID  AT1G19040_MA1734.1
XX
DE  MA1734.1 AT1G19040; from JASPAR
P0       a     c     g     t
1        0  1000     0     0
2        0     0   139   861
3        0     0     0  1000
4       21    33   919    28
5      188   344   208   260
6      239   127   317   317
7      133   513   177   177
8      148   418    93   341
9      167   351   286   196
10       0   948    14    38
11    1000     0     0     0
12     776   224     0     0
13       2     0   993     5
14      59   272    79   590
15     291   146     5   558
16     647    12     0   341
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT2G38300_MA1735.1
XX
ID  AT2G38300_MA1735.1
XX
DE  MA1735.1 AT2G38300; from JASPAR
P0       a     c     g     t
1      647    93    25   235
2      833    20     8   138
3      285   378    63   273
4      995     0     3     2
5        0     0     2   998
6        0     0     0  1000
7        0  1000     0     0
8        0   333     8   658
9      222   285    55   438
10     148   143    15   693
11     230   133   107   530
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT5G04390_MA1736.1
XX
ID  AT5G04390_MA1736.1
XX
DE  MA1736.1 AT5G04390; from JASPAR
P0       a     c     g     t
1      175   404    83   337
2      401   214    58   327
3      382   294   119   205
4      145   441   120   294
5      270   132    88   509
6      270   170     0   559
7        0   985     0    15
8     1000     0     0     0
9        0   963    37     0
10       0     0     0  1000
11     289   277   180   254
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT5G05090_MA1737.1
XX
ID  AT5G05090_MA1737.1
XX
DE  MA1737.1 AT5G05090; from JASPAR
P0       a     c     g     t
1      325   225   225   225
2      249   260    48   443
3      826    25    25   125
4        0     0   999     0
5      999     0     0     0
6        0     0     0   999
7      288   318     0   394
8        0   777     0   223
9       25    25   723   226
10     299   202   297   202
11     323   226   226   226
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ATMYB31_MA1738.1
XX
ID  ATMYB31_MA1738.1
XX
DE  MA1738.1 ATMYB31; from JASPAR
P0       a     c     g     t
1      174   505    44   277
2      769   113    20    98
3      559   285    24   132
4        0   753    10   236
5      166   245    30   559
6      486   302    76   135
7      106   807     0    86
8       27   765     0   208
9     1000     0     0     0
10     736   258     3     2
11       0  1000     0     0
12     199   373    29   399
13     760    46   160    34
14     289   527    19   166
15     264   439     0   297
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BHLH122_MA1739.1
XX
ID  BHLH122_MA1739.1
XX
DE  MA1739.1 BHLH122; from JASPAR
P0       a     c     g     t
1      246   137   201   415
2      203   229   243   325
3      365    75   270   290
4      166   209   355   270
5       12   988     0     0
6      551   436     0    13
7     1000     0     0     0
8        0   995     5     0
9        0     0     2   998
10       2     0     0   998
11       2     0   998     0
12     127   506   137   229
13     353   281   132   233
14     285   248   114   353
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BHLH49_MA1740.1
XX
ID  BHLH49_MA1740.1
XX
DE  MA1740.1 BHLH49; from JASPAR
P0       a     c     g     t
1      304   232   232   232
2      299   135   360   206
3        0  1000     0     0
4     1000     0     0     0
5        0  1000     0     0
6        0     0  1000     0
7        0     0     0  1000
8        0     0  1000     0
9       90   809    83    18
10     212   338   237   212
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BRN2_MA1741.1
XX
ID  BRN2_MA1741.1
XX
DE  MA1741.1 BRN2; from JASPAR
P0       a     c     g     t
1       37   960     0     3
2        0     0   269   731
3        0     0     5   995
4      199   209   537    55
5      390   194   107   309
6      457    72   212   259
7      321   176   166   338
8      169   261   140   430
9      331   154   182   333
10       7   532    48   413
11     998     2     0     0
12     301   699     0     0
13       2     0   925    74
14      50   465    32   453
15     441   135     7   416
16     793     8     0   199
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZIP18_MA1742.1
XX
ID  BZIP18_MA1742.1
XX
DE  MA1742.1 BZIP18; from JASPAR
P0       a     c     g     t
1       90   102    64   744
2        0     0   894   106
3      524   471     5     0
4        0  1000     0     0
5     1000     0     0     0
6       15     0   985     0
7        0   851    95    54
8        0   219     2   779
9       94   111   549   246
10     166     0   499   335
11     219   238   214   328
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZIP2_MA1743.1
XX
ID  BZIP2_MA1743.1
XX
DE  MA1743.1 BZIP2; from JASPAR
P0       a     c     g     t
1      318   106   296   279
2      392   111   180   317
3      265    57   101   577
4       26     2   789   183
5      219   604   152    24
6       75    77     0   848
7        0   127   803    70
8     1000     0     0     0
9        0   991     0     9
10       2     3   995     0
11       0     0     0  1000
12       0     0  1000     0
13       0     0   783   217
14     202   788     2     9
15     639   111   134   116
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZIP30_MA1744.1
XX
ID  BZIP30_MA1744.1
XX
DE  MA1744.1 BZIP30; from JASPAR
P0       a     c     g     t
1      127   288   115   470
2       93     0   719   188
3      571   427     2     0
4        8   972     0    20
5      936    36     3    24
6      213    10   767     9
7       24   682    76   218
8       14    80    33   873
9      188    77   664    70
10      87    60   388   464
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZIP63_MA1745.1
XX
ID  BZIP63_MA1745.1
XX
DE  MA1745.1 BZIP63; from JASPAR
P0       a     c     g     t
1      229   229   229   313
2      203   203   390   203
3      188   437   188   188
4      166   148    21   665
5        0   356   644     0
6     1000     0     0     0
7        0  1000     0     0
8        0     0  1000     0
9       21    21    21   937
10      47   123   783    47
11     139    62   598   201
12     229   313   229   229
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BZIP69_MA1746.1
XX
ID  BZIP69_MA1746.1
XX
DE  MA1746.1 BZIP69; from JASPAR
P0       a     c     g     t
1       84   114    35   767
2       20    15   901    64
3      480   520     0     0
4        0  1000     0     0
5     1000     0     0     0
6        0     0  1000     0
7        0   965     5    30
8        0     5     0   995
9        0     5   990     5
10       5    30   322   644
11     421   376    59   144
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOF4.3_MA1747.1
XX
ID  DOF4.3_MA1747.1
XX
DE  MA1747.1 DOF4.3; from JASPAR
P0       a     c     g     t
1      393   203   186   217
2      434    64   116   386
3     1000     0     0     0
4     1000     0     0     0
5     1000     0     0     0
6        0     0  1000     0
7      302   178    91   429
8      578     2   345    76
9      188   147   571    95
10       3   995     0     2
11       0     5     0   995
12       2     0     0   998
13       0     2     0   998
14     407    66   103   424
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DREB1F_MA1748.1
XX
ID  DREB1F_MA1748.1
XX
DE  MA1748.1 DREB1F; from JASPAR
P0       a     c     g     t
1      189   273   235   302
2      259    68   638    35
3        3   767     7   222
4        0   982    14     4
5        4     1   993     2
6      962    19     4    15
7       20   970     9     0
8      527   149   168   155
9      148   148   130   573
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  E2FC_MA1749.1
XX
ID  E2FC_MA1749.1
XX
DE  MA1749.1 E2FC; from JASPAR
P0       a     c     g     t
1      913    87     0     0
2      348     0   130   522
3      391     0   217   391
4      348     0   217   435
5      261     0   217   522
6      174    43   783     0
7        0    43   783   174
8      217   783     0     0
9        0     0  1000     0
10       0  1000     0     0
11       0  1000     0     0
12     913    87     0     0
13     826    87     0    87
14     913     0    87     0
15     696     0   261    43
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  E2FE_MA1750.1
XX
ID  E2FE_MA1750.1
XX
DE  MA1750.1 E2FE; from JASPAR
P0       a     c     g     t
1      208     9   246   537
2      104     2    13   882
3       47     4     2   947
4       36     0     7   956
5       15     2     5   978
6        5     4   989     2
7        0     0  1000     0
8        2   998     0     0
9        2     0   996     2
10       2    15   984     0
11       0    11   987     2
12     993     0     7     0
13    1000     0     0     0
14     958    13    11    18
15     774    51    22   153
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  EIL3_MA1751.1
XX
ID  EIL3_MA1751.1
XX
DE  MA1751.1 EIL3; from JASPAR
P0       a     c     g     t
1      204   387   204   204
2      696   101   101   101
3       23    23   932    23
4      499     0   501     0
5        0     0     0  1000
6      506     0     0   494
7        0  1000     0     0
8     1000     0     0     0
9       46    46    46   862
10     149   149   149   553
11     227   318   227   227
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF025_MA1752.1
XX
ID  ERF025_MA1752.1
XX
DE  MA1752.1 ERF025; from JASPAR
P0       a     c     g     t
1      245   326   129   301
2      210   341   137   312
3      194   172   257   377
4      129   342    77   452
5      112   356    57   476
6      239     3   758     0
7        0   950     0    50
8        0  1000     0     0
9        0     0  1000     0
10     940    17     0    43
11       2   998     0     0
12     815     3   164    18
13     374   260   174   192
14     349   199   112   341
15     337   147   234   282
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF057_MA1753.1
XX
ID  ERF057_MA1753.1
XX
DE  MA1753.1 ERF057; from JASPAR
P0       a     c     g     t
1       85   637    87   192
2      140   676    38   145
3      367   116   272   246
4       38   642    76   244
5       73   803    31    93
6      270     3   389   337
7       35   862    45    59
8        2   983    12     3
9      410     2   578    10
10       5   995     0     0
11       0  1000     0     0
12      90     3   874    33
13     123   694    12   171
14      52   856     0    92
15     483   130   189   199
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF073_MA1754.1
XX
ID  ERF073_MA1754.1
XX
DE  MA1754.1 ERF073; from JASPAR
P0       a     c     g     t
1      156   440   147   256
2      210   474    91   226
3      311   126   288   275
4      115   480   133   272
5      181   648    28   142
6      169    89   377   366
7       34   742   155    69
8       25   975     0     0
9       32     0   968     0
10       0   993     0     7
11       0  1000     0     0
12      12     0   975    12
13       0   948     0    52
14      94   837    14    55
15     506     0   380   114
16     121   384    83   412
17     275   293    62   369
18     281    76   265   378
19     151   419   108   321
20     242   410   115   233
21     261   178   323   238
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GATA4_MA1755.1
XX
ID  GATA4_MA1755.1
XX
DE  MA1755.1 GATA4; from JASPAR
P0       a     c     g     t
1      287   250   177   286
2      181   300   160   359
3      438   167   256   140
4      164    62   739    34
5      939     3    16    42
6       42    16     3   939
7       34   739    62   164
8      140   256   167   438
9      359   160   300   181
10     286   177   250   287
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GRF9_MA1756.1
XX
ID  GRF9_MA1756.1
XX
DE  MA1756.1 GRF9; from JASPAR
P0       a     c     g     t
1      163   130   185   522
2       33   109    43   815
3        0     0  1000     0
4        0     0     0  1000
5        0   902     0    98
6      967    33     0     0
7        0     0  1000     0
8      761    33    98   109
9      478   163   207   152
10     337   130   304   228
11     457   163   120   261
12     130   250   239   380
13     370   163   130   337
14     239   413   239   109
15     478   217   120   185
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HDG7_MA1757.1
XX
ID  HDG7_MA1757.1
XX
DE  MA1757.1 HDG7; from JASPAR
P0       a     c     g     t
1      272    10   452   265
2       30   902     0    68
3     1000     0     0     0
4        0     0     0  1000
5        0     0     0  1000
6      790     2     8   200
7      995     0     5     0
8      462     0     0   538
9       10     0    70   920
10     452    15   395   138
11     175   382    88   355
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HSFA1E_MA1758.1
XX
ID  HSFA1E_MA1758.1
XX
DE  MA1758.1 HSFA1E; from JASPAR
P0       a     c     g     t
1      662    67   236    35
2       11     0   987     2
3      987     0    13     0
4      939     4    26    30
5      124   200   588    89
6      210   421   204   165
7      124    33    11   833
8       39     0    11   950
9        2   965    33     0
10      48    85    13   855
11     852    39   108     0
12       2     7   980    11
13     978     7     7     9
14     774    30    87   108
15     191   191   423   195
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HSFA4A_MA1759.1
XX
ID  HSFA4A_MA1759.1
XX
DE  MA1759.1 HSFA4A; from JASPAR
P0       a     c     g     t
1      227   200   140   432
2      237   316   199   249
3      285   204   172   339
4      541    93   235   130
5      154    72   689    85
6      796    75    78    50
7      805    28    45   122
8      122   189   482   207
9      107   576   185   132
10      97    18    17   868
11       8     5     0   987
12       3   988     0     8
13       5    73    28   893
14     896     7    90     7
15      10     8   970    12
16     985     0     8     7
17     853    35    28    83
18     195   135   543   127
19     331   376   160   134
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HSFA6A_MA1760.1
XX
ID  HSFA6A_MA1760.1
XX
DE  MA1760.1 HSFA6A; from JASPAR
P0       a     c     g     t
1        3     0   997     0
2      983     0     0    17
3      951     3     0    46
4       80   234   574   111
5      297   323   197   183
6      117    34     0   849
7        6     0     0   994
8        0  1000     0     0
9        0    51    17   931
10     917     6    77     0
11       0     0   997     3
12    1000     0     0     0
13     834    31    54    80
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HSFB3_MA1761.1
XX
ID  HSFB3_MA1761.1
XX
DE  MA1761.1 HSFB3; from JASPAR
P0       a     c     g     t
1      783    38    95    84
2      894     7     0    99
3      117   176   562   145
4       92   596   205   108
5       52     0     9   939
6        2     0     7   991
7        0   989     2     9
8        0   199    48   752
9      899    11    79    11
10     163    18   713   106
11     898     0    61    41
12     803    31    63   104
13     232   189   417   163
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB10_MA1762.1
XX
ID  MYB10_MA1762.1
XX
DE  MA1762.1 MYB10; from JASPAR
P0       a     c     g     t
1      243   251   161   345
2       99   313   116   472
3       75   455    32   438
4       17   708    19   256
5      997     0     2     2
6        9   991     0     0
7        0  1000     0     0
8      350     3     0   646
9     1000     0     0     0
10     333   663     3     0
11      14   824     2   161
12     456   166    51   326
13     354   236    56   354
14     328   214   109   349
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB116_MA1764.1
XX
ID  MYB116_MA1764.1
XX
DE  MA1764.1 MYB116; from JASPAR
P0       a     c     g     t
1      157   268   119   456
2      198   309   218   275
3       55   154   108   684
4      266     2   732     0
5        0  1000     0     0
6        0  1000     0     0
7       14     0     3   983
8     1000     0     0     0
9      978    22     0     0
10       0   910     0    90
11     133   108     5   754
12     148   172    63   616
13     203   184    87   526
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB121_MA1765.1
XX
ID  MYB121_MA1765.1
XX
DE  MA1765.1 MYB121; from JASPAR
P0       a     c     g     t
1      244   225   129   401
2      131   284   103   482
3      309   169   250   272
4      197   101   108   593
5      923     0    77     0
6        3   997     0     0
7        0  1000     0     0
8       94     0    82   824
9     1000     0     0     0
10     735   258     0     7
11      42   719     0   239
12     274   168    33   525
13     297   154    87   462
14     339   143   101   417
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB17_MA1766.1
XX
ID  MYB17_MA1766.1
XX
DE  MA1766.1 MYB17; from JASPAR
P0       a     c     g     t
1       70   440    77   413
2      135   368    61   436
3      102   609    11   278
4      996     0     0     4
5      130   870     0     0
6        0  1000     0     0
7      242    40     0   718
8     1000     0     0     0
9      128   865     5     2
10       2   806     0   193
11     545   117    23   315
12     420   247    58   275
13     305   336    96   263
14     315   228   102   356
15     292   210    98   399
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB23_MA1767.1
XX
ID  MYB23_MA1767.1
XX
DE  MA1767.1 MYB23; from JASPAR
P0       a     c     g     t
1      232   348   121   300
2        0   986     0    14
3     1000     0     0     0
4     1000     0     0     0
5        0  1000     0     0
6      589    10     0   401
7     1000     0     0     0
8      420   343   101   135
9      251   449    92   208
10     309   155    72   464
11     275   261    97   367
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB30_MA1768.1
XX
ID  MYB30_MA1768.1
XX
DE  MA1768.1 MYB30; from JASPAR
P0       a     c     g     t
1      153   306   195   345
2      141   357    84   418
3      261   439    67   232
4      180   537    67   215
5       12   843     0   145
6      983    15     0     2
7      880   103     3    13
8        0  1000     0     0
9       62   525    22   391
10     764    12   173    51
11     167   744     0    89
12      86   682     0   232
13     616    79    57   247
14     402   384    45   168
15     162   613    45   180
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB39_MA1769.1
XX
ID  MYB39_MA1769.1
XX
DE  MA1769.1 MYB39; from JASPAR
P0       a     c     g     t
1      157   169   197   477
2       50   309    94   547
3       67   352    74   508
4       34   670    17   279
5      993     0     7     0
6      138   856     0     5
7        0  1000     0     0
8      169     0     0   831
9     1000     0     0     0
10     554   439     0     7
11      46   470     0   484
12     253    79     0   668
13     224   207    55   515
14     253   219    92   436
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB40_MA1770.1
XX
ID  MYB40_MA1770.1
XX
DE  MA1770.1 MYB40; from JASPAR
P0       a     c     g     t
1       42   350   127   481
2       17   308    38   637
3       25   430     0   544
4      996     0     4     0
5        8   992     0     0
6        0   996     4     0
7      405     0     0   595
8      996     4     0     0
9      401   591     0     8
10      34   578     0   388
11     371   148    89   392
12     241   228    63   468
13     245   262    97   397
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB41_MA1771.1
XX
ID  MYB41_MA1771.1
XX
DE  MA1771.1 MYB41; from JASPAR
P0       a     c     g     t
1      250   109   250   391
2      201   185    87   527
3      310    92   212   386
4      103   141   522   234
5       65   239   201   495
6       76   179    71   674
7      103   457   109   332
8     1000     0     0     0
9       54   946     0     0
10       0  1000     0     0
11      92    16     0   891
12    1000     0     0     0
13     620   370     0    11
14      98   538    22   342
15     217   136    11   636
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB43_MA1772.1
XX
ID  MYB43_MA1772.1
XX
DE  MA1772.1 MYB43; from JASPAR
P0       a     c     g     t
1      133   325   217   325
2        0   361    84   554
3      145   217    48   590
4       24   542    12   422
5     1000     0     0     0
6       24   976     0     0
7        0  1000     0     0
8      349     0     0   651
9     1000     0     0     0
10     398   602     0     0
11       0   663    12   325
12     289   157    24   530
13     241   169    24   566
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB51_MA1773.1
XX
ID  MYB51_MA1773.1
XX
DE  MA1773.1 MYB51; from JASPAR
P0       a     c     g     t
1      268   298   132   302
2      289   199   107   405
3      275   284   130   311
4      262   322   121   295
5      380   195   118   307
6      356   179   174   291
7      342   242   105   311
8      382   195   148   275
9      262   266   161   311
10     286   226   156   333
11      83   373    96   448
12      99   385    90   425
13     105   501    14   380
14     998     2     0     0
15       0  1000     0     0
16       0  1000     0     0
17     454     2     4   541
18     998     0     2     0
19     204   772    14     9
20      83   590    20   307
21     420    90    24   467
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB60_MA1774.1
XX
ID  MYB60_MA1774.1
XX
DE  MA1774.1 MYB60; from JASPAR
P0       a     c     g     t
1      278   314   111   296
2      328   190   152   330
3      251   181   170   398
4      145   362    86   407
5      111   367    90   432
6       86   514    52   348
7        0   975     0    25
8      993     5     2     0
9      871   127     0     2
10       0  1000     0     0
11      66   529     0   405
12     975     0    25     0
13     129   835     0    36
14     127   514     0   360
15     244    86    66   604
16     219   299    72   410
17     247   285   102   367
18     308   186   190   317
19     188   339   133   339
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB61_MA1775.1
XX
ID  MYB61_MA1775.1
XX
DE  MA1775.1 MYB61; from JASPAR
P0       a     c     g     t
1      236   314   162   287
2      171   296   111   422
3      147   434   164   255
4        0   529     7   465
5     1000     0     0     0
6      204   796     0     0
7        0  1000     0     0
8      427    69     0   503
9     1000     0     0     0
10     307   691     0     2
11       0   860     0   140
12     456    93   184   267
13     220   326   122   333
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB67_MA1776.1
XX
ID  MYB67_MA1776.1
XX
DE  MA1776.1 MYB67; from JASPAR
P0       a     c     g     t
1      100   377   119   404
2       82   481   199   239
3        7   549     0   444
4     1000     0     0     0
5       92   905     0     3
6        0  1000     0     0
7      324    42     0   634
8     1000     0     0     0
9      598   401     0     2
10      12   888     0   100
11     446   117   245   192
12     222   339    80   359
13     254   275   102   369
14     362   227   142   269
15     250   307   155   287
16     292   295   129   284
17     297   259   160   284
18     249   332   139   280
19     319   225   134   322
20     287   254   199   260
21     272   304   150   274
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB74_MA1777.1
XX
ID  MYB74_MA1777.1
XX
DE  MA1777.1 MYB74; from JASPAR
P0       a     c     g     t
1      198   218   220   363
2       68   302    94   536
3      123   319    78   480
4       19   568    29   384
5     1000     0     0     0
6       72   925     2     2
7        0  1000     0     0
8      169     0     0   831
9     1000     0     0     0
10     449   551     0     0
11       5   717     0   278
12     370    92    10   527
13     324   230    67   379
14     275   232   116   377
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB80_MA1778.1
XX
ID  MYB80_MA1778.1
XX
DE  MA1778.1 MYB80; from JASPAR
P0       a     c     g     t
1      158   153   318   371
2       62   242   136   560
3      101   297    64   538
4       62   471    66   402
5      998     0     0     2
6      229   771     0     0
7        0  1000     0     0
8      212     8     5   775
9     1000     0     0     0
10     627   371     0     2
11      81   392     8   519
12     217    69     7   708
13     294   195    29   482
14     227   155   168   450
15     257   197   139   407
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB83_MA1779.1
XX
ID  MYB83_MA1779.1
XX
DE  MA1779.1 MYB83; from JASPAR
P0       a     c     g     t
1      130   508    97   265
2      109   811     5    76
3     1000     0     0     0
4      232   759     9     0
5        0   993     0     7
6      849    14     7   130
7      823   125    52     0
8      184   801    14     0
9      291   489    17   203
10     314   253   191   241
11     187   558    66   189
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB92_MA1780.1
XX
ID  MYB92_MA1780.1
XX
DE  MA1780.1 MYB92; from JASPAR
P0       a     c     g     t
1      198   262   150   390
2      209   305   118   369
3      144   380    70   406
4      968    32     0     0
5        0  1000     0     0
6        0  1000     0     0
7      118     0     0   882
8     1000     0     0     0
9      701   299     0     0
10      91   642    64   203
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB94_MA1781.1
XX
ID  MYB94_MA1781.1
XX
DE  MA1781.1 MYB94; from JASPAR
P0       a     c     g     t
1      400   212   106   282
2      295   261    99   346
3      192   302   164   342
4      125   302   109   464
5      187   455   106   252
6      108   571    55   266
7        0   931     0    69
8      998     2     0     0
9      788   212     0     0
10       0  1000     0     0
11      42   453     2   503
12     975     0    18     7
13     125   852     0    23
14      51   582     0   367
15     330    81    44   545
16     268   266    86   379
17     219   362   102   317
18     270   257   138   335
19     309   263   138   291
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC005_MA1783.1
XX
ID  NAC005_MA1783.1
XX
DE  MA1783.1 NAC005; from JASPAR
P0       a     c     g     t
1      222    59    27   692
2      361    78    92   469
3      415   183   271   131
4        0   998     0     2
5        0     0     3   997
6        0     0     0  1000
7      236   376   286   102
8      125   149    93   632
9      190   149   171   490
10     180   346    63   412
11     320   210   115   354
12     495   153   178   175
13      20   493    63   424
14     998     0     2     0
15     995     5     0     0
16       0     0  1000     0
17      63   158   117   663
18     281    66    20   632
19     463    19    20   498
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC011_MA1784.1
XX
ID  NAC011_MA1784.1
XX
DE  MA1784.1 NAC011; from JASPAR
P0       a     c     g     t
1      237    75   548   140
2        4     0     0   996
3       26     0     9   965
4      189    26   544   241
5        0  1000     0     0
6        0     0    48   952
7        0     9     0   991
8      105   430   373    92
9      272   184   136   408
10     281   175   298   246
11     276   373   101   250
12     215   307   132   346
13     373    70   311   246
14       0   794   105   101
15     996     0     4     0
16     794   202     0     4
17       0     0  1000     0
18     162   259   118   461
19     618   110    18   254
20     899     4     4    92
21     118   535    92   254
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC018_MA1785.1
XX
ID  NAC018_MA1785.1
XX
DE  MA1785.1 NAC018; from JASPAR
P0       a     c     g     t
1      591    76   145   189
2       29   609    98   264
3     1000     0     0     0
4        0  1000     0     0
5        0     0  1000     0
6       34   286   195   485
7      657   290     0    54
8      793     0     0   207
9       82   571   111   236
10      72   406    57   465
11     219   120   227   434
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC019_MA1786.1
XX
ID  NAC019_MA1786.1
XX
DE  MA1786.1 NAC019; from JASPAR
P0       a     c     g     t
1      242   260   226   273
2      352   147   279   222
3       41   542   104   313
4      985     2     2    11
5       55   938     1     6
6        7     4   958    31
7      116   273   318   293
8      517   350    34    99
9      660    28    72   240
10     167   348   240   245
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC047_MA1787.1
XX
ID  NAC047_MA1787.1
XX
DE  MA1787.1 NAC047; from JASPAR
P0       a     c     g     t
1      257   127    70   547
2      278   202   148   372
3      262   258   170   310
4      260   207   165   368
5      552    88   122   238
6        7   708    33   252
7     1000     0     0     0
8        0   995     0     5
9        2    10   980     8
10      42   230   152   577
11     522   367     5   107
12     652     0     0   348
13      48   640    97   215
14      67   285    45   603
15     195   137   167   502
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC054_MA1788.1
XX
ID  NAC054_MA1788.1
XX
DE  MA1788.1 NAC054; from JASPAR
P0       a     c     g     t
1        9   991     0     0
2        0     0   300   700
3        0     0     0  1000
4      181   176   621    22
5      233   163   115   489
6      229   216   220   335
7      194   313   189   304
8      335   278   101   286
9      656     9   189   145
10       0   815     0   185
11    1000     0     0     0
12     123   877     0     0
13       0     0   996     4
14      44   379   106   471
15     379   216    13   392
16     885     9     9    97
17     172   419   137   273
18     123   652   101   123
19     194    93   123   590
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC071_MA1789.1
XX
ID  NAC071_MA1789.1
XX
DE  MA1789.1 NAC071; from JASPAR
P0       a     c     g     t
1        5   995     0     0
2        0     3   135   862
3        0     3     0   997
4       45   508   338   108
5      414   133   103   350
6      229   227   126   418
7      184   380   135   301
8      116   310   125   449
9      150   190   123   537
10      29   741   146    84
11    1000     0     0     0
12     633   367     0     0
13       2     0   968    30
14      89   340    66   505
15     707    84     0   209
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC073_MA1790.1
XX
ID  NAC073_MA1790.1
XX
DE  MA1790.1 NAC073; from JASPAR
P0       a     c     g     t
1        3   985     0    12
2        5     0   199   796
3        0     7     2   992
4      134   452   389    25
5      320   102    40   538
6      179   240   189   392
7      186   397   218   199
8      245   260   126   370
9      422    27    47   504
10      20   586   119   275
11     985     8     0     7
12     442   558     0     0
13      67     0   838    95
14      80   575    54   291
15     251   221     3   524
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC087_MA1791.1
XX
ID  NAC087_MA1791.1
XX
DE  MA1791.1 NAC087; from JASPAR
P0       a     c     g     t
1       35     0     0   965
2      250    16   127   607
3      374   143   245   238
4        3   997     0     0
5        0     0   330   670
6        9     2     0   990
7      130   158   694    17
8      172   136    38   654
9      231   195   250   323
10     240   315   163   282
11     207   308   165   320
12     567    40   195   198
13       0   777    10   212
14    1000     0     0     0
15     278   722     0     0
16       0     0   981    19
17      77   283   103   537
18     398   176     9   417
19     847     2     3   148
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC098_MA1792.1
XX
ID  NAC098_MA1792.1
XX
DE  MA1792.1 NAC098; from JASPAR
P0       a     c     g     t
1       17   983     0     0
2        0     0   406   594
3        0     0     0  1000
4       91   117   781    10
5      145   143    46   666
6      217   198   253   332
7      236   291   181   293
8      246   279   148   327
9      535    43   179   243
10       5   731    24   239
11    1000     0     0     0
12     318   678     0     3
13       2     0   981    17
14      64   248   119   570
15     351   224     0   425
16     830     2     5   164
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NID1_MA1793.1
XX
ID  NID1_MA1793.1
XX
DE  MA1793.1 NID1; from JASPAR
P0       a     c     g     t
1      195   388   245   173
2      116   225    29   630
3      132   103    93   673
4      962     4    14    20
5       18    36    15   931
6       23   933    15    29
7       43   637    69   251
8      412   102   282   205
9      245   233   222   300
10     368   162   159   311
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NLP7_MA1794.1
XX
ID  NLP7_MA1794.1
XX
DE  MA1794.1 NLP7; from JASPAR
P0       a     c     g     t
1      447   109   118   326
2      341   129   165   365
3      299    89   183   429
4       10   182     2   807
5       66     0   934     0
6      329   175   339   156
7        0  1000     0     0
8        0   444    40   516
9       18   390    22   570
10      10    81     0   909
11      39    15     0   946
12      99   472   323   106
13     400   113   415    72
14     306   123   351   220
15     346   123   281   250
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RAP2-4_MA1795.1
XX
ID  RAP2-4_MA1795.1
XX
DE  MA1795.1 RAP2-4; from JASPAR
P0       a     c     g     t
1      181   554   129   137
2      584     0   416     0
3        1   933    65     0
4        1   968     0    31
5       93     7   787   113
6      497   242    80   181
7       99   822     0    78
8      271   550    52   127
9      468   141   161   230
10     269   245   189   297
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RAP2-7_MA1796.1
XX
ID  RAP2-7_MA1796.1
XX
DE  MA1796.1 RAP2-7; from JASPAR
P0       a     c     g     t
1        1     1   250   748
2      614   154   231     1
3        1   784   214     1
4        1     1   855   143
5      691     1   308     1
6        1     1   921    77
7        1     1   998     1
8      273     1   182   544
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RAV2_MA1797.1
XX
ID  RAV2_MA1797.1
XX
DE  MA1797.1 RAV2; from JASPAR
P0       a     c     g     t
1      214    72   713     1
2        0   999     0     0
3      908     0    91     0
4      999     0     0     0
5        0   999     0     0
6      999     0     0     0
7        0     0   137   863
8      562   250     1   188
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SPL10_MA1799.1
XX
ID  SPL10_MA1799.1
XX
DE  MA1799.1 SPL10; from JASPAR
P0       a     c     g     t
1      300   233   164   303
2      197   261   149   393
3       34     0   962     3
4        1     5    14   980
5      957    42     0     0
6       10   971     9    10
7       58    44   744   153
8      337    23   596    44
9      490   163   101   246
10     299   323   166   213
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TEM1_MA1800.1
XX
ID  TEM1_MA1800.1
XX
DE  MA1800.1 TEM1; from JASPAR
P0       a     c     g     t
1      220    98   683     0
2       88   912     0     0
3      875    16   109     0
4     1000     0     0     0
5        0  1000     0     0
6     1000     0     0     0
7        0     0   241   758
8      600   125     0   275
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TREE1_MA1801.1
XX
ID  TREE1_MA1801.1
XX
DE  MA1801.1 TREE1; from JASPAR
P0       a     c     g     t
1      331   223   223   223
2      174   174   477   174
3       50   158   242   550
4      191   208    96   504
5      809     0     0   191
6        0     0   901    99
7      191   809     0     0
8       99     0     0   901
9      316    27   433   224
10     386   170   177   267
11     201   201   297   302
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TRP5_MA1802.1
XX
ID  TRP5_MA1802.1
XX
DE  MA1802.1 TRP5; from JASPAR
P0       a     c     g     t
1      190   351   281   178
2      303   239   109   349
3      343    61    57   538
4      845    19   119    17
5      852    24    28    95
6      746   125    99    31
7       44   776     7   173
8       26   931    17    26
9       91   518    70   321
10     191   334   109   366
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  VIP1_MA1803.1
XX
ID  VIP1_MA1803.1
XX
DE  MA1803.1 VIP1; from JASPAR
P0       a     c     g     t
1      341   102   211   346
2       99   109    48   743
3       17     0   850   133
4      508   482    10     0
5        0  1000     0     0
6     1000     0     0     0
7       19     0   981     0
8        0   978    10    12
9        0    34     0   966
10     102    97   753    48
11     162     0   276   562
12     341   262   169   228
13     269   346   102   283
14     404   220   121   254
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WIN1_MA1804.1
XX
ID  WIN1_MA1804.1
XX
DE  MA1804.1 WIN1; from JASPAR
P0       a     c     g     t
1      249   390   267    94
2      137   720   133    10
3      384     1   615     1
4        1   795   187    17
5        1   956     1    43
6      202     9   712    77
7      164   679    79    78
8      348   352   155   145
9      253   163   426   158
10     184   239   172   406
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WRKY53_MA1805.1
XX
ID  WRKY53_MA1805.1
XX
DE  MA1805.1 WRKY53; from JASPAR
P0       a     c     g     t
1      426   115   402    57
2        8     0   980    12
3        0     0     1   998
4       46   950     1     2
5      896    56    10    39
6      896     8    76    21
7       85   706    56   153
8      136   194   486   184
9      204   314   262   219
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ZHD10_MA1807.1
XX
ID  ZHD10_MA1807.1
XX
DE  MA1807.1 ZHD10; from JASPAR
P0       a     c     g     t
1      255   296   160   289
2      307    64   167   463
3        0    28     0   972
4      828     0   172     0
5     1000     0     0     0
6        0     0     0  1000
7        0     0     0  1000
8     1000     0     0     0
9      628     0   372     0
10      92   172   138   598
11     225   160   245   371
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GLYMA-06G314400_MA1808.1
XX
ID  GLYMA-06G314400_MA1808.1
XX
DE  MA1808.1 GLYMA-06G314400; from JASPAR
P0       a     c     g     t
1     4908 11920  5299  5706
2    18522  2293  3696  3322
3     1272 24345  1277   939
4      745   411 25777   900
5      433   530   207 26663
6      595   357 26215   666
7     1246  1261 21101  4225
8     1863 24505   862   603
9    22437  1356  2120  1920
10    7096  6788  6336  7613
11    7296  6274  6535  7728
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GLYMA-07G038400_MA1809.1
XX
ID  GLYMA-07G038400_MA1809.1
XX
DE  MA1809.1 GLYMA-07G038400; from JASPAR
P0       a     c     g     t
1     2069  1964  1378  2122
2     2150  1352  1300  2731
3      298  6804   260   171
4      505   238   127  6663
5      232   167  6779   355
6     6019   729   272   513
7      214  6887   189   243
8     6590   315   162   466
9      211  6694   246   382
10    2179  1453  1704  2197
11    1788  1676  1555  2514
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GLYMA-08G357600_MA1810.1
XX
ID  GLYMA-08G357600_MA1810.1
XX
DE  MA1810.1 GLYMA-08G357600; from JASPAR
P0       a     c     g     t
1       49    38    47    55
2       44    66    33    46
3      120    11    24    34
4       13     7    10   159
5        4    10   172     3
6        5   177     1     6
7      183     2     2     2
8        7     3     1   178
9        2     4   179     4
10       8   163     8    10
11     158     8     9    14
12      42    29    54    64
13      46    41    60    42
14      48    37    46    58
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GLYMA-13G317000_MA1811.1
XX
ID  GLYMA-13G317000_MA1811.1
XX
DE  MA1811.1 GLYMA-13G317000; from JASPAR
P0       a     c     g     t
1     3013  8215  3433  3671
2    12296  1390  2539  2107
3      660 16694   566   412
4      370   182 17411   369
5      310   369   140 17513
6      434   269 17185   444
7      791   895 13599  3047
8     1190 16137   626   379
9    14399   966  1636  1331
10    4529  4702  4253  4848
11    4885  4354  4336  4757
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ASR1_MA1812.1
XX
ID  ASR1_MA1812.1
XX
DE  MA1812.1 ASR1; from JASPAR
P0       a     c     g     t
1       53    36    19    48
2       42    34    35    45
3       33    51    24    48
4      124    10     5    17
5       20     1   135     0
6        1     0   155     0
7        0   153     0     3
8        0   156     0     0
9        0   155     0     1
10     149     1     6     0
11     111     6     7    32
12      41    14     7    94
13      69    19    14    54
14      59    21    34    42
15      46    29    26    55
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  EIL4_MA1813.1
XX
ID  EIL4_MA1813.1
XX
DE  MA1813.1 EIL4; from JASPAR
P0       a     c     g     t
1       85    38    51   307
2      211    29    68   173
3        0   481     0     0
4      415     0    26    40
5      439    11    11    20
6       21    25     4   431
7        1     0   478     2
8      389     6     6    80
9      466     8     0     7
10      75    99    51   256
11     149   105    81   146
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RIN_MA1814.1
XX
ID  RIN_MA1814.1
XX
DE  MA1814.1 RIN; from JASPAR
P0       a     c     g     t
1     1259   576   683  2305
2     1295   689   719  2120
3      682  3258   246   637
4      235   573    85  3930
5     3927   239    87   570
6      248   163    98  4314
7      469    70    55  4229
8      203    33    50  4537
9      357    58    99  4309
10     285    65   143  4330
11     331    32  4270   190
12     265   110  3924   524
13    1089   525  2029  1180
14    1882   765   670  1506
15    1945   689   680  1509
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GRF4_MA1815.1
XX
ID  GRF4_MA1815.1
XX
DE  MA1815.1 GRF4; from JASPAR
P0       a     c     g     t
1       19    24     8    14
2       23     8    16    18
3        7     1    54     3
4        3    59     3     0
5       56     1     5     3
6        2     1    61     1
7        3    60     0     2
8       53     2     0    10
9        2     3    59     1
10       2    59     4     0
11      22    11     6    26
12      19     8    24    14
13      11    28    12    14
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  O11_MA1816.1
XX
ID  O11_MA1816.1
XX
DE  MA1816.1 O11; from JASPAR
P0       a     c     g     t
1     1186  1538  1632  1104
2     1291  1413  1400  1356
3      964  1164  1901  1431
4       22  5216    61   161
5     5442    10     0     8
6        3  5439    15     3
7        4    17  5435     4
8        8     0    12  5440
9      163    60  5212    25
10    1431  1901  1164   964
11    1359  1392  1416  1293
12    1105  1631  1539  1185
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d020267_MA1817.1
XX
ID  Zm00001d020267_MA1817.1
XX
DE  MA1817.1 Zm00001d020267; from JASPAR
P0       a     c     g     t
1     1873  7440  3127  2361
2     1541  8346  2985  1929
3      453   711 13149   488
4      299 13416   683   403
5      271 13624   555   351
6      567   247 13656   331
7      308 13462   632   399
8      277 13749   446   329
9      458   233 13639   471
10     379 13257   657   508
11     354 13267   760   420
12    2204  3064  7641  1892
13    2006  6993  3317  2485
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d052229_MA1818.1
XX
ID  Zm00001d052229_MA1818.1
XX
DE  MA1818.1 Zm00001d052229; from JASPAR
P0       a     c     g     t
1      975  4801  2029  1151
2     1193  1587  5207   969
3        0  8956     0     0
4        0  8956     0     0
5        0     0  8956     0
6        0  8956     0     0
7        0  8956     0     0
8        0     0  8956     0
9      977  5049  1720  1210
10     920  5142  1908   986
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d005892_MA1819.1
XX
ID  Zm00001d005892_MA1819.1
XX
DE  MA1819.1 Zm00001d005892; from JASPAR
P0       a     c     g     t
1     3849  7060 12797  3667
2     3675 11954  6742  5002
3      761 24431  1309   872
4      795   767 25046   765
5      852 23527  1794  1200
6      535 25251   917   670
7      795   435 25421   722
8      885 23482  1696  1310
9      613 25022   990   748
10    1241  1029 24113   990
11    3768 12889  5911  4805
12    3329 13547  6580  3917
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d024324_MA1820.1
XX
ID  Zm00001d024324_MA1820.1
XX
DE  MA1820.1 Zm00001d024324; from JASPAR
P0       a     c     g     t
1     3094  5929 10701  2995
2     2719 10708  5462  3830
3      548 20536   959   676
4      630   627 20748   714
5      475 20683   934   627
6      474 20957   695   593
7      619   350 21141   609
8      508 20319  1100   792
9      499 20733   864   623
10     685   425 20848   761
11     692 19965  1113   949
12    2485 11929  5360  2945
13    3014  5407 11092  3206
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d020595_MA1821.1
XX
ID  Zm00001d020595_MA1821.1
XX
DE  MA1821.1 Zm00001d020595; from JASPAR
P0       a     c     g     t
1      856  1437  3902   869
2      962  1187  4239   676
3        0  7064     0     0
4        0     0  7064     0
5        1     0  7061     2
6        0  7062     1     1
7        0     0  7064     0
8        0     0  7064     0
9        0  7056     3     5
10     759  1193  4405   707
11    1110  1448  3732   774
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d018571_MA1822.1
XX
ID  Zm00001d018571_MA1822.1
XX
DE  MA1822.1 Zm00001d018571; from JASPAR
P0       a     c     g     t
1      144   229   142   199
2      235   164   171   144
3      116   336   118   144
4       69    59   563    23
5       10    50    24   630
6      675    15    16     8
7       13   637    19    45
8       58    25   613    18
9        8    17    14   675
10     630    19    54    11
11      23   580    51    60
12     180   144   261   129
13     148   180   163   223
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d027846_MA1823.1
XX
ID  Zm00001d027846_MA1823.1
XX
DE  MA1823.1 Zm00001d027846; from JASPAR
P0       a     c     g     t
1      430   159   223   175
2      399   157   264   167
3      818    46    86    37
4      893    17    44    33
5      929     6    30    22
6       23    24   866    74
7      896    29    38    24
8      915    11    28    33
9      914    27    33    13
10     752    53   123    59
11     429   154   233   171
12     424   130   261   172
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d005692_MA1824.1
XX
ID  Zm00001d005692_MA1824.1
XX
DE  MA1824.1 Zm00001d005692; from JASPAR
P0       a     c     g     t
1     1156   633   652   815
2     1009   821   681   745
3      440  1879   254   683
4     3130    34    27    65
5     3145    30    24    57
6       31    47    32  3146
7     2697   148   148   263
8     3173    18    28    37
9       72    19    26  3139
10     706   240   520  1790
11     912   699   925   720
12    1010   563   674  1009
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d044409_MA1825.1
XX
ID  Zm00001d044409_MA1825.1
XX
DE  MA1825.1 Zm00001d044409; from JASPAR
P0       a     c     g     t
1     1019   574   629   901
2     1124   488   595   916
3      295  1554   370   904
4       10  2994    92    27
5       20    52   142  2909
6       27     1     2  3093
7     3099     9    14     1
8       25    87    28  2983
9       48  3042    10    23
10     364  1215   302  1242
11     755   762   832   774
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  bHLH145_MA1826.1
XX
ID  bHLH145_MA1826.1
XX
DE  MA1826.1 bHLH145; from JASPAR
P0       a     c     g     t
1      314   634   473   358
2      454   432   582   311
3      193  1148   188   250
4      176   158  1370    75
5       34  1582    42   121
6     1562    47   115    55
7       13  1620    33   113
8      113    34  1619    13
9       57   121    48  1553
10     120    41  1584    34
11      75  1367   160   177
12     247   188  1151   193
13     313   581   433   452
14     356   472   636   315
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d044785_MA1827.1
XX
ID  Zm00001d044785_MA1827.1
XX
DE  MA1827.1 Zm00001d044785; from JASPAR
P0       a     c     g     t
1      165   149   147   142
2      145   129   119   210
3       12   510    29    52
4       19   530    32    22
5       48    28    26   501
6       37    47    31   488
7      511    41    35    16
8        3    10    11   579
9        7   575     4    17
10      69   247   107   180
11     130   150   139   184
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d038683_MA1828.1
XX
ID  Zm00001d038683_MA1828.1
XX
DE  MA1828.1 Zm00001d038683; from JASPAR
P0       a     c     g     t
1      359   604   490   343
2      350   540   521   385
3      135   197  1406    58
4       17    65    50  1664
5       12    42  1727    15
6       10     9  1768     9
7      138    85   111  1462
8        9  1755    28     4
9       24  1746    18     8
10    1121   265   189   221
11     305   569   550   372
12     295   492   654   355
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d035604_MA1829.1
XX
ID  Zm00001d035604_MA1829.1
XX
DE  MA1829.1 Zm00001d035604; from JASPAR
P0       a     c     g     t
1       48    77    77    82
2       63    68    69    84
3       41    35    53   155
4      247     9    17    11
5        8   263     7     6
6        3   276     2     3
7       17    10    14   243
8      268     7     8     1
9      243    28    10     3
10      15   247     3    19
11      63    75    32   114
12      85    77    48    74
13      89    63    64    68
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d015407_MA1830.1
XX
ID  Zm00001d015407_MA1830.1
XX
DE  MA1830.1 Zm00001d015407; from JASPAR
P0       a     c     g     t
1      175   126   110   145
2      185   118   101   152
3      276    56    66   158
4      279   120    54   103
5      389    43    44    80
6      334    24    91   107
7      144    24   351    37
8       45    11   483    17
9      481    20    16    39
10     536     3     6    11
11       8     5    26   517
12     513    17     7    19
13      57     8     6   485
14      20    16    23   497
15      34   477    13    32
16      20    92    16   428
17     105    79    37   335
18      71    56    77   352
19      63    64   112   317
20     124    70    52   310
21     116   192    98   150
22     128   113   146   169
23     158    75   112   211
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d031796_MA1831.1
XX
ID  Zm00001d031796_MA1831.1
XX
DE  MA1831.1 Zm00001d031796; from JASPAR
P0       a     c     g     t
1     1688  6667  3858  1608
2     1680  3534  7250  1357
3    11164   807  1352   498
4      199 13089   257   276
5      309   323 13025   164
6     1454   655 11443   269
7      227 13138   239   217
8      282   514 12856   169
9    10265  1019  2008   529
10     242 12691   634   254
11    2022  2882  7673  1244
12    3416  3467  4782  2156
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d002364_MA1832.1
XX
ID  Zm00001d002364_MA1832.1
XX
DE  MA1832.1 Zm00001d002364; from JASPAR
P0       a     c     g     t
1     2288  9863  4044  2895
2     1747 11808  3334  2201
3     1536  2981 13670   903
4        0 19090     0     0
5        0 19090     0     0
6        0     0 19090     0
7        0 19090     0     0
8        0 19090     0     0
9        0     0 19090     0
10    1785 11009  3546  2750
11    1941 11352  3763  2034
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d049364_MA1833.1
XX
ID  Zm00001d049364_MA1833.1
XX
DE  MA1833.1 Zm00001d049364; from JASPAR
P0       a     c     g     t
1     2350  9280  4348  2628
2     2486  4638  9048  2434
3     1345 13507  2196  1558
4     3673  9771  2975  2187
5      711  1027 16187   681
6      352 17229   601   424
7      464 17025   607   510
8      652   366 17063   525
9      300 17272   593   441
10     465 17013   612   516
11     605   321 17121   559
12     416 16869   777   544
13     461 16513  1008   624
14    4800  2765  8879  2162
15    2247  9179  4110  3070
16    2100  9497  4550  2459
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d034298_MA1834.1
XX
ID  Zm00001d034298_MA1834.1
XX
DE  MA1834.1 Zm00001d034298; from JASPAR
P0       a     c     g     t
1     1331  2050  2146  1075
2     1471  2040  1597  1494
3      190  6091   160   161
4       41  5812   487   262
5     6373   128    17    84
6       55  6223   252    72
7      499   489  5256   358
8      418  5039   401   744
9      192   522  5681   207
10    1361  1886  1893  1462
11    1226  2323  1777  1276
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TFLG2-Zm00001d042777_MA1835.1
XX
ID  TFLG2-Zm00001d042777_MA1835.1
XX
DE  MA1835.1 TFLG2-Zm00001d042777; from JASPAR
P0       a     c     g     t
1      111   175   159   217
2      266   123   165   108
3      103   364    87   108
4       58    68   513    23
5        8    24    24   606
6      613    11    26    12
7        4   625    14    19
8       25    17   616     4
9       10    29    13   610
10     602    25    26     9
11      28   517    65    52
12     137   106   287   132
13     121   169   132   240
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC016_MA2005.1
XX
ID  NAC016_MA2005.1
XX
DE  MA2005.1 NAC016; from JASPAR
P0       a     c     g     t
1      853   869   617   926
2      996   733   819   717
3       17  3090    33   125
4     3073    61    63    68
5     2869   279    33    84
6       24    21  3176    44
7       91  2392    72   710
8     2866   140    72   187
9     2960    68    48   189
10     777   800   707   981
11     932   779   611   943
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC020_MA2006.1
XX
ID  NAC020_MA2006.1
XX
DE  MA2006.1 NAC020; from JASPAR
P0       a     c     g     t
1      263   143   214   302
2      378   151   139   254
3      149    77   550   146
4      161    84   541   136
5       45     7     2   868
6      147    11    78   686
7      198    62   463   199
8       27   885     1     9
9        2     2   448   470
10       1     1     1   919
11      70    66   769    17
12     105    94    49   674
13     228   112   384   198
14     255   210   201   256
15     208   379   147   188
16     578    61   145   138
17      35   734   100    53
18     921     0     1     0
19     849    69     3     1
20       2     1   910     9
21     200   133    89   500
22     585   105    14   218
23     767     6    13   136
24     207   312   143   260
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB107_MA2007.1
XX
ID  MYB107_MA2007.1
XX
DE  MA2007.1 MYB107; from JASPAR
P0       a     c     g     t
1      116   151    97   216
2       89   192    43   256
3       57   441    25    57
4      564     1     7     8
5       32   532     3    13
6        7   563     3     7
7      473    12     3    92
8      571     7     1     1
9      454   106     8    12
10      23   525     5    27
11     227   133    42   178
12     178   155    60   187
13     224   115    85   156
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB99_MA2008.1
XX
ID  MYB99_MA2008.1
XX
DE  MA2008.1 MYB99; from JASPAR
P0       a     c     g     t
1      154   332   106   431
2      168   312    90   453
3        0  1023     0     0
4      987     1    26     9
5        0  1004    11     8
6        0  1022     1     0
7        1     0    12  1010
8     1020     3     0     0
9      535   372    32    84
10     140   471    35   377
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC010_MA2009.1
XX
ID  NAC010_MA2009.1
XX
DE  MA2009.1 NAC010; from JASPAR
P0       a     c     g     t
1      225   105   309   171
2      189    29    27   565
3      523     7    83   197
4      495    67   122   126
5        8   796     0     6
6        4     1    79   726
7        1     1     1   807
8       71    96   626    17
9      112    67    27   604
10     191   125   290   204
11     209   207   205   189
12     186   322   108   194
13     594    30    71   115
14      17   634    87    72
15     802     1     3     4
16     709    97     3     1
17      10     1   792     7
18     144   100    64   502
19     235    72    10   493
20     669    13    14   114
21     112   495    59   144
22     130   436    77   167
23     232   121   142   315
24     266   158   119   267
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SMB_MA2010.1
XX
ID  SMB_MA2010.1
XX
DE  MA2010.1 SMB; from JASPAR
P0       a     c     g     t
1     1022   851   488  1241
2     1139   616   758  1089
3      418  2165   553   466
4     3480    45    35    42
5     3246   251    25    80
6        6    21  3530    45
7        8  3329    33   232
8     3245   115    64   178
9     3387    33    29   153
10     869  1004   716  1013
11     998  1078   740   786
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF010_MA2011.1
XX
ID  ERF010_MA2011.1
XX
DE  MA2011.1 ERF010; from JASPAR
P0       a     c     g     t
1      303   128   285   283
2      118   465   127   289
3       68   858    30    43
4      915     0    71    13
5        2   993     0     4
6        4   992     0     3
7        9     1   984     5
8      719   144    23   113
9       11   983     2     3
10     750   107    78    64
11     305   331    93   270
12     272   308   138   281
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF023_MA2012.1
XX
ID  ERF023_MA2012.1
XX
DE  MA2012.1 ERF023; from JASPAR
P0       a     c     g     t
1       30   155    46    87
2       27   253    16    22
3      284     0    32     2
4        0   317     1     0
5        1   317     0     0
6        5     0   310     3
7      277    18     8    15
8        1   314     1     2
9      297     2     6    13
10     109   108    37    64
11     106    67    38   107
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERF115_MA2013.1
XX
ID  ERF115_MA2013.1
XX
DE  MA2013.1 ERF115; from JASPAR
P0       a     c     g     t
1      198   458    73   176
2      237   139   300   229
3       53   344    53   455
4       43   815    15    32
5       56     8   715   126
6       13   839    35    18
7       23   866     6    10
8       14     1   880    10
9        4   865    12    24
10      13   880     0    12
11      44     3   840    18
12      31   698    55   121
13     480   286    42    97
14     293    71   375   166
15     174   418    92   221
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BAM8_MA2014.1
XX
ID  BAM8_MA2014.1
XX
DE  MA2014.1 BAM8; from JASPAR
P0       a     c     g     t
1      420   498   325   633
2      418   441   207   810
3      116  1278   279   203
4     1383   101   273   119
5        6  1856     4    10
6     1846     2    14    14
7        2  1862     1    11
8        9     1  1863     3
9        9    31     5  1831
10      66     1  1803     6
11     116  1018   140   602
12     320   500   863   193
13     803   222   409   442
14     600   371   456   449
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC002_MA2015.1
XX
ID  NAC002_MA2015.1
XX
DE  MA2015.1 NAC002; from JASPAR
P0       a     c     g     t
1      346   237   229   357
2      501   152   185   331
3      175   531   115   348
4     1143    12     3    11
5       31  1096    14    28
6       38    13  1089    29
7       47   753   103   266
8     1066    58    13    32
9     1070    16    16    67
10      26  1013    64    66
11      69   144    63   893
12     293   243   232   401
13     387   259   195   328
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GRF6_MA2016.1
XX
ID  GRF6_MA2016.1
XX
DE  MA2016.1 GRF6; from JASPAR
P0       a     c     g     t
1      276   305   166   499
2      205   146    54   841
3        0  1246     0     0
4        0     3     3  1240
5        1     0  1245     0
6     1245     1     0     0
7        0  1245     0     1
8     1228     1    17     0
9      391   398   193   264
10     500   185   265   296
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  JUB1_MA2017.1
XX
ID  JUB1_MA2017.1
XX
DE  MA2017.1 JUB1; from JASPAR
P0       a     c     g     t
1      151    79   132   147
2      113   153    65   178
3       96   236   115    62
4      458    30    10    11
5        9   492     0     8
6        9     8   489     3
7        3    12   492     2
8       12   476     0    21
9       33    10   457     9
10     288    36    82   103
11      68   122   196   123
12      67    47   121   274
13     142    91   139   137
14     148   102    98   161
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC69_MA2018.1
XX
ID  NAC69_MA2018.1
XX
DE  MA2018.1 NAC69; from JASPAR
P0       a     c     g     t
1      100    80    88    94
2      124    96    66    76
3      306     7    37    12
4        5   314    40     3
5      353     2     4     3
6      337    18     2     5
7        5     1   349     7
8       10     6   297    49
9       49   273     2    38
10     331     7    13    11
11     136    72    48   106
12     103   103    59    97
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HSFA1B_MA2019.1
XX
ID  HSFA1B_MA2019.1
XX
DE  MA2019.1 HSFA1B; from JASPAR
P0       a     c     g     t
1     1802  1746  1029  3083
2     3819   983  1728  1130
3      155   105  7327    73
4     7359    86    95   120
5     7308    54   124   174
6     3914   804  1940  1002
7      216  6814   401   229
8      345   133    77  7105
9      122   108   101  7329
10      95  7300   122   143
11    1158  1761   990  3751
12    3033  1204  1541  1882
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HSFB4_MA2020.1
XX
ID  HSFB4_MA2020.1
XX
DE  MA2020.1 HSFB4; from JASPAR
P0       a     c     g     t
1       44    11    25     9
2       15     4    64     6
3       81     1     4     3
4       86     1     1     1
5        2     3    83     1
6       10    16     4    59
7        2     1     0    86
8        0     0     0    89
9        0    89     0     0
10       1    62     0    26
11      42     6    30    11
12      25     8    40    16
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  LBD13_MA2021.1
XX
ID  LBD13_MA2021.1
XX
DE  MA2021.1 LBD13; from JASPAR
P0       a     c     g     t
1      184   382   180   309
2      247   494   116   198
3      231    44   145   635
4       17   979    20    39
5       16  1018     4    17
6      809     6   168    72
7        7  1031     8     9
8       10  1021    16     8
9       46    11   980    18
10      57   227   143   628
11     353   369   112   221
12     295   125   306   329
13     270   370   148   267
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  LOB_MA2022.1
XX
ID  LOB_MA2022.1
XX
DE  MA2022.1 LOB; from JASPAR
P0       a     c     g     t
1       65   553    74   103
2      116   569    24    86
3       92    18   552   133
4        7   760    10    18
5       28   739     4    24
6       39     4   729    23
7        4   768     7    16
8       39   721     8    27
9       37     1   733    24
10      14   693    38    50
11     250   398    44   103
12     129    33   532   101
13      73   538    71   113
14     147   331    56   261
15     170    74   373   178
16      96   463    96   140
17     159   382    87   167
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHL4_MA2023.1
XX
ID  PHL4_MA2023.1
XX
DE  MA2023.1 PHL4; from JASPAR
P0       a     c     g     t
1       65    37    40    45
2       90    22    29    46
3       72    39    32    44
4       75    32    75     5
5        1     1   180     5
6      154    16     5    12
7      187     0     0     0
8        1     0     0   186
9      184     2     1     0
10       1     1     0   185
11      16     3    12   156
12       4   183     0     0
13       7   114    27    39
14      49    28    33    77
15      42    29    21    95
16      56    32    34    65
17      51    39    42    55
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB63_MA2024.1
XX
ID  MYB63_MA2024.1
XX
DE  MA2024.1 MYB63; from JASPAR
P0       a     c     g     t
1       18    32    19    50
2       15    53     8    43
3        1   108     2     8
4      115     1     1     2
5        6   111     1     1
6        1   116     0     2
7       32     4     2    81
8      117     0     1     1
9       11   107     0     1
10       1   108     3     7
11      69    15    14    21
12      28    65     4    22
13      33    45     9    32
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB93_MA2025.1
XX
ID  MYB93_MA2025.1
XX
DE  MA2025.1 MYB93; from JASPAR
P0       a     c     g     t
1      112   157    72   192
2      113   196    60   164
3      111   107    32   283
4      516     4     8     5
5       12   511     4     6
6        7   519     0     7
7      400    11    17   105
8      516     2     2    13
9      483    32     7    11
10       6   492     3    32
11     126   245    36   126
12     161   135    78   159
13     205   119    75   134
14     217   115    68   133
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB108_MA2026.1
XX
ID  MYB108_MA2026.1
XX
DE  MA2026.1 MYB108; from JASPAR
P0       a     c     g     t
1       22    27    11    48
2       28    19    18    43
3       15     5    13    75
4       83     0    25     0
5        1   105     0     2
6        0   107     0     1
7       16     0     3    89
8      108     0     0     0
9      104     2     1     1
10       3    84     0    21
11      11     5     3    89
12      20    10     3    75
13      30    15    15    48
14      37    24    15    32
15      31    27    16    34
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB44_MA2027.1
XX
ID  MYB44_MA2027.1
XX
DE  MA2027.1 MYB44; from JASPAR
P0       a     c     g     t
1      202   130   117   224
2      166   168    25   314
3      665     1     2     5
4      638    33     1     1
5        3   663     1     6
6        4    52   559    58
7        9     3   655     6
8        3    20     2   648
9      117   370    19   167
10     612    18    21    22
11     260   143    84   186
12     225   120   121   207
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB49_MA2028.1
XX
ID  MYB49_MA2028.1
XX
DE  MA2028.1 MYB49; from JASPAR
P0       a     c     g     t
1       49    33    51    55
2       37    46    25    80
3       15    32     9   132
4       11    43    17   117
5      182     1     1     4
6        7   175     2     4
7        1   187     0     0
8       12     2     1   173
9      188     0     0     0
10     164    18     1     5
11      12   142     4    30
12      29    12     5   142
13      82    29    10    67
14      56    35    28    69
15      51    44    30    63
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB88_MA2029.1
XX
ID  MYB88_MA2029.1
XX
DE  MA2029.1 MYB88; from JASPAR
P0       a     c     g     t
1       60    54    16    38
2       33    68    36    31
3      150    15     3     0
4        3   159     6     0
5        1     1   165     1
6        0   167     1     0
7        1     1    27   139
8        1   165     2     0
9        0   152     3    13
10      18     6     3   141
11       7   138     6    17
12      38    72    21    37
13      49    42    37    40
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB96_MA2030.1
XX
ID  MYB96_MA2030.1
XX
DE  MA2030.1 MYB96; from JASPAR
P0       a     c     g     t
1       63    20    10    40
2       46    36     8    43
3       11    78    11    33
4       20    50    10    53
5       38    74    10    11
6        8   107     1    17
7        1   123     0     9
8      131     1     0     1
9      113    15     3     2
10       0   132     0     1
11      11    23     1    98
12     116     2     6     9
13      14   116     0     3
14       8   102     0    23
15      90    10     5    28
16      50    43    12    28
17      21    78     8    26
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT5G04760_MA2031.1
XX
ID  AT5G04760_MA2031.1
XX
DE  MA2031.1 AT5G04760; from JASPAR
P0       a     c     g     t
1     1646   661   672  1196
2      867   999   645  1664
3       11  4161     3     0
4      231   100    94  3750
5      176    36    29  3934
6     4053    79    39     4
7      118    55   127  3875
8       67  4094    10     4
9      573  2121   242  1239
10    1314   709   739  1413
11    1276   918   524  1457
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TRB1_MA2032.1
XX
ID  TRB1_MA2032.1
XX
DE  MA2032.1 TRB1; from JASPAR
P0       a     c     g     t
1     1648  1082  1087  1126
2     2956   451   682   854
3     4615   128    96   104
4     4355    40   443   105
5      138  4706    35    64
6       86  4755    36    66
7       71  4767    55    50
8      108    44    57  4734
9     4824    34    44    41
10    2829   372  1303   439
11    1714  1073   474  1682
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC038_MA2033.1
XX
ID  NAC038_MA2033.1
XX
DE  MA2033.1 NAC038; from JASPAR
P0       a     c     g     t
1      195    72    51   112
2       89    53   199    89
3       86    46   222    76
4       30     2     5   393
5       98     9    51   272
6      153    21   165    91
7        6   423     0     1
8        0     2   255   173
9        2     0     0   428
10      35    16   378     1
11      42    44    10   334
12     176    57   103    94
13     117   102   103   108
14      96   161    70   103
15     294    17    58    61
16       6   337    40    47
17     420     2     2     6
18     358    65     5     2
19       1     1   422     6
20      79    51    29   271
21     177    51     7   195
22     384     4     2    40
23     102   148    48   132
24      81   182    70    97
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC057_MA2034.1
XX
ID  NAC057_MA2034.1
XX
DE  MA2034.1 NAC057; from JASPAR
P0       a     c     g     t
1      731   628   443   780
2      971   448   579   584
3       36  2380    22   144
4     2461    36    35    50
5     2158   269    61    94
6       30     8  2524    20
7       33    95    42  2412
8     2146    54    10   372
9     2337    55    35   155
10     571   758   413   840
11     657   825   443   657
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC075_MA2035.1
XX
ID  NAC075_MA2035.1
XX
DE  MA2035.1 NAC075; from JASPAR
P0       a     c     g     t
1      249   151   168   225
2      297   148   162   186
3      404    80   174   135
4      113    41   585    54
5      455    30    30   278
6      591    12    73   117
7      163    40   476   114
8       17   766     2     8
9        1     3   113   676
10       0     0     1   792
11      71   164   543    15
12     177    74    26   516
13     238   145   185   225
14     163   230   241   159
15     237   176   139   241
16     510    23    83   177
17      13   516   188    76
18     788     1     3     1
19     677   112     0     4
20       8     1   770    14
21     110   459    65   159
22     142    83    19   549
23     412    52    68   261
24     147   339    85   222
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC076_MA2036.1
XX
ID  NAC076_MA2036.1
XX
DE  MA2036.1 NAC076; from JASPAR
P0       a     c     g     t
1      914   802   497  1034
2     1056   615   667   909
3      408  1879   539   421
4     3171    22    20    34
5     2963   216    16    52
6        7    18  3193    29
7       16  3023    23   185
8     2965    81    55   146
9     3088    19    17   123
10     775   931   615   926
11     871  1018   682   676
12     960   566   616  1105
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC096_MA2037.1
XX
ID  NAC096_MA2037.1
XX
DE  MA2037.1 NAC096; from JASPAR
P0       a     c     g     t
1      306   290   191   357
2      380   189   260   315
3       51   904    51   138
4     1110    13     7    14
5     1011    84    23    26
6        7     1  1129     7
7        4    30     9  1101
8     1012    12     6   114
9     1089    12    12    31
10     216   352   145   431
11     238   415   183   308
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC103_MA2038.1
XX
ID  NAC103_MA2038.1
XX
DE  MA2038.1 NAC103; from JASPAR
P0       a     c     g     t
1      498   380   209   378
2      456   331   351   327
3       18  1379    18    50
4     1411    16    15    23
5     1318   103    15    29
6       10     2  1442    11
7      102   155    90  1118
8      141    61     8  1255
9     1415    10    10    30
10     264   445   246   510
11     318   509   294   344
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ASIL2_MA2039.1
XX
ID  ASIL2_MA2039.1
XX
DE  MA2039.1 ASIL2; from JASPAR
P0       a     c     g     t
1      480   168   281   470
2      297   210   210   682
3       33  1089    33   244
4       74    10   147  1168
5       12  1367    14     6
6        6  1391     0     2
7       14     2  1382     1
8       63    61  1244    31
9       25  1233     7   134
10      83    59  1217    40
11     853   144   228   174
12     393   419   252   335
13     393   188   449   369
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT5G05550_MA2040.1
XX
ID  AT5G05550_MA2040.1
XX
DE  MA2040.1 AT5G05550; from JASPAR
P0       a     c     g     t
1      845   280   463   710
2      482   390   327  1099
3       41  1863    43   351
4      160    13   328  1797
5       24  2244    15    15
6        7  2283     0     8
7       13     0  2281     4
8      105   119  2017    57
9       59  2038    10   191
10     128    68  2037    65
11    1363   208   428   299
12     597   641   377   683
13     707   289   796   506
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB13_MA2041.1
XX
ID  MYB13_MA2041.1
XX
DE  MA2041.1 MYB13; from JASPAR
P0       a     c     g     t
1      348   300   168   332
2      324   265   214   345
3      349   320   157   322
4      215   472   147   314
5      140   581    70   357
6       60   914    36   138
7     1107     5    11    25
8      107  1022     8    11
9       10  1117     6    15
10     905    46    17   180
11    1106    11    22     9
12      90  1034     5    19
13      35  1016    16    81
14     728   191    95   134
15     390   360   106   292
16     375   313   153   307
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MYB58_MA2042.1
XX
ID  MYB58_MA2042.1
XX
DE  MA2042.1 MYB58; from JASPAR
P0       a     c     g     t
1      563   303   205   480
2      406   402   311   432
3      335   590   202   424
4      294   310   165   782
5      163   861    98   429
6       35  1368    14   134
7     1500     8    21    22
8      163  1361    12    15
9        9  1520     7    15
10    1374    21    23   133
11    1440    59    30    22
12     209  1291    11    40
13      91  1185    35   240
14     466   688   138   259
15     355   798   105   293
16     529   352   213   457
17     498   411   234   408
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC004_MA2043.1
XX
ID  NAC004_MA2043.1
XX
DE  MA2043.1 NAC004; from JASPAR
P0       a     c     g     t
1      279   229   129   288
2      422   120   250   133
3        0   918     1     6
4      898     1    19     7
5      874    28     1    22
6        0     2   912    11
7      648    38    16   223
8      842    14     2    67
9      858    29     2    36
10     173   348   171   233
11     256   259   179   231
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC007_MA2044.1
XX
ID  NAC007_MA2044.1
XX
DE  MA2044.1 NAC007; from JASPAR
P0       a     c     g     t
1      613   525   325   710
2      721   368   473   611
3      285  1236   361   291
4     2117    13    19    24
5     1962   161    11    39
6        4     7  2144    18
7        3  2037     9   124
8     1974    60    40    99
9     2067    15    13    78
10     542   624   413   594
11     607   686   437   443
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC031_MA2045.1
XX
ID  NAC031_MA2045.1
XX
DE  MA2045.1 NAC031; from JASPAR
P0       a     c     g     t
1      269   142   221   281
2      378   169   127   239
3      161    82   513   157
4      200    93   464   156
5       69     5    10   829
6      173    18    95   627
7      261    64   449   139
8       18   882     3    10
9        1     4   599   309
10       0     2     2   909
11     115    56   734     8
12     119    95    40   659
13     406   112   188   207
14     274   200   193   246
15     245   298   138   232
16     635    44    99   135
17      34   665   105   109
18     883     7     4    19
19     750   145    10     8
20      11     1   880    21
21     153   146    96   518
22     382   120    23   388
23     748    19    13   133
24     217   302   134   260
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC035_MA2046.1
XX
ID  NAC035_MA2046.1
XX
DE  MA2046.1 NAC035; from JASPAR
P0       a     c     g     t
1      230   184   148   251
2      191   187   185   250
3      329   143   156   185
4      431   109   112   161
5       41   632    36   104
6      800     4     3     6
7       43   736    24    10
8       10     8   787     8
9       41    29   649    94
10      45   720     7    41
11     755     4    29    25
12     235   253   104   221
13     181   274   112   246
14     195   133   226   259
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC037_MA2047.1
XX
ID  NAC037_MA2047.1
XX
DE  MA2047.1 NAC037; from JASPAR
P0       a     c     g     t
1      543   530   269   645
2      690   347   462   488
3       25  1779    32   151
4     1919    22    17    29
5     1709   217    14    47
6        6    24  1928    29
7        4  1727    32   224
8     1725    85    56   121
9     1827    16    24   120
10     450   601   330   606
11     496   643   438   410
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC045_MA2048.1
XX
ID  NAC045_MA2048.1
XX
DE  MA2048.1 NAC045; from JASPAR
P0       a     c     g     t
1      446   353   263   342
2      531   193   304   376
3      112   367   814   111
4     1389     4     5     6
5     1327    52    16     9
6       23    11  1354    16
7       84  1158    41   121
8     1328    17    11    48
9     1352     3    10    39
10     148   917   142   197
11     287   695   172   250
12     364   238   263   539
13     444   330   250   380
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC053_MA2049.1
XX
ID  NAC053_MA2049.1
XX
DE  MA2049.1 NAC053; from JASPAR
P0       a     c     g     t
1     1157  1073   639  1093
2     1383   799   960   820
3        2  3931     2    27
4     3873     8    38    43
5     3762   161     8    31
6        1     2  3927    32
7     3241   134    50   537
8     3640    56    17   249
9     3814    31    21    96
10     819  1483   640  1020
11    1125  1249   687   901
12    1168   634   812  1348
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC101_MA2050.1
XX
ID  NAC101_MA2050.1
XX
DE  MA2050.1 NAC101; from JASPAR
P0       a     c     g     t
1      896   554   460   739
2      784   628   374   863
3      715   409   941   584
4      248  1815   253   333
5     2593    11    17    28
6     2461   145     7    36
7       32    35  2538    44
8       82  2318    57   192
9     2449    56    42   102
10    2513    14    14   108
11     587   644   405  1013
12     706   813   562   568
13     787   453   465   944
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC68_MA2051.1
XX
ID  NAC68_MA2051.1
XX
DE  MA2051.1 NAC68; from JASPAR
P0       a     c     g     t
1      394   317   204   391
2      395   323   222   366
3     1208    31    42    25
4       39  1163    44    60
5     1292     3     3     8
6     1261    15     6    24
7       25    16  1243    22
8       28    38    39  1201
9      840   134    46   286
10     408    72    81   745
11     380   286   271   369
12     396   255   213   442
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ZAT6_MA2052.1
XX
ID  ZAT6_MA2052.1
XX
DE  MA2052.1 ZAT6; from JASPAR
P0       a     c     g     t
1     2817  1418  1391  2958
2     2604  1840  1459  2681
3     7234   393   310   647
4     7675   170   164   575
5      210    82    45  8247
6      342    40  7885   317
7     8264    56    82   182
8      290    37   173  8084
9      214    77   213  8080
10    1185   410  5912  1077
11    3267  1175  1824  2318
12    2603  1165  1611  3205
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NAC105_MA2053.1
XX
ID  NAC105_MA2053.1
XX
DE  MA2053.1 NAC105; from JASPAR
P0       a     c     g     t
1      333   313   166   389
2      389   209   257   346
3      128   680   214   179
4     1178    10     5     8
5     1122    69     3     7
6        9    12  1176     4
7       73   990    37   101
8     1136    20    14    31
9     1172     4     2    23
10     107   825    87   182
11     239   699   108   155
12     331   180   205   485
13     390   297   186   328
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK00315.1_UN0001.1
XX
ID  PK00315.1_UN0001.1
XX
DE  UN0001.1 PK00315.1; from JASPAR
P0       a     c     g     t
1      226   226   320   226
2      186   260   186   367
3       86   163   664    86
4      839   159     0     0
5        0   922     0    77
6      917     0    82     0
7       77     0   922     0
8       77   840    82     0
9       23    23   106   846
10     317    64   477   141
11     246   164   281   308
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK01144.1_UN0002.1
XX
ID  PK01144.1_UN0002.1
XX
DE  UN0002.1 PK01144.1; from JASPAR
P0       a     c     g     t
1      349   229   253   168
2       23   872     4   100
3       31     0    82   886
4       43     2    25   929
5       43    25    22   909
6      331   133    78   456
7      222   219   213   344
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK01630.1_UN0003.1
XX
ID  PK01630.1_UN0003.1
XX
DE  UN0003.1 PK01630.1; from JASPAR
P0       a     c     g     t
1      166   111   677    44
2      134   864     0     0
3      904    14    43    36
4        0   926     0    73
5       94     0   904     0
6       52    67    82   798
7        8     0   927    64
8      473    53   290   182
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK03025.1_UN0005.1
XX
ID  PK03025.1_UN0005.1
XX
DE  UN0005.1 PK03025.1; from JASPAR
P0       a     c     g     t
1      495     4     4   495
2      985     4     4     4
3        4     4     4   985
4      985     4     4     4
5      985     4     4     4
6      985     4     4     4
7        4     4     4   985
8        4   495     4   495
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK03929.1_UN0007.1
XX
ID  PK03929.1_UN0007.1
XX
DE  UN0007.1 PK03929.1; from JASPAR
P0       a     c     g     t
1      223   275   188   313
2      279   247   161   311
3      430   189   231   147
4       39   908    16    35
5       34    46    80   839
6       31     4    22   941
7       24    10    24   940
8      349   118    73   458
9      207   251   170   371
10     245   240   255   258
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK04914.1_UN0008.1
XX
ID  PK04914.1_UN0008.1
XX
DE  UN0008.1 PK04914.1; from JASPAR
P0       a     c     g     t
1      246    13   547   191
2      151   828    10    10
3        0   990     9     0
4        0     0   999     0
5        0     0    18   980
6        0   307     9   682
7      924     0    10    64
8      421   156   234   187
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK05432.1_UN0010.1
XX
ID  PK05432.1_UN0010.1
XX
DE  UN0010.1 PK05432.1; from JASPAR
P0       a     c     g     t
1      263   231   242   263
2      220   264   220   294
3      384   103   451    60
4        4     1   894   100
5        8     2     2   986
6       34   963     0     1
7      725   132    35   106
8      622    47   201   129
9      314   408   121   154
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK05451.1_UN0011.1
XX
ID  PK05451.1_UN0011.1
XX
DE  UN0011.1 PK05451.1; from JASPAR
P0       a     c     g     t
1       83   582    83   250
2        0   998     0     0
3      998     0     0     0
4        0   998     0     0
5        0     0   998     0
6        0     0     0   998
7        0     0   998     0
8      285   570    71    71
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK05732.1_UN0013.1
XX
ID  PK05732.1_UN0013.1
XX
DE  UN0013.1 PK05732.1; from JASPAR
P0       a     c     g     t
1      469   128   117   285
2      336   339   127   196
3      892    17    39    50
4       37    12     0   949
5      105    58    87   748
6       62   837    33    66
7       78   336   175   410
8      231   276   200   290
9      209   266   160   363
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK06517.1_UN0015.1
XX
ID  PK06517.1_UN0015.1
XX
DE  UN0015.1 PK06517.1; from JASPAR
P0       a     c     g     t
1      167   498   167   167
2        0   997     0     0
3      997     0     0     0
4        0   997     0     0
5        0     0   997     0
6        0     0     0   997
7        0     0   997     0
8      125   747   125     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK07031.1_UN0016.1
XX
ID  PK07031.1_UN0016.1
XX
DE  UN0016.1 PK07031.1; from JASPAR
P0       a     c     g     t
1      355   222   252   169
2       58   816    17   107
3       50    17   243   688
4       74     0    65   859
5       73    21    44   860
6      314   154   110   420
7      213   245   210   330
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK07402.1_UN0017.1
XX
ID  PK07402.1_UN0017.1
XX
DE  UN0017.1 PK07402.1; from JASPAR
P0       a     c     g     t
1      138   346   272   242
2      190   534   166   108
3      608     2   384     5
4        0   993     5     1
5        0   994     0     4
6       10     1   977    10
7      441   411    42   104
8       75   790    31   102
9      367   227    86   319
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK07858.1_UN0019.1
XX
ID  PK07858.1_UN0019.1
XX
DE  UN0019.1 PK07858.1; from JASPAR
P0       a     c     g     t
1      235   294    59   411
2        0   998     0     0
3      998     0     0     0
4        0   998     0     0
5        0     0   998     0
6        0     0     0   998
7        0     0   998     0
8       67   532   200   200
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK08602.1_UN0021.1
XX
ID  PK08602.1_UN0021.1
XX
DE  UN0021.1 PK08602.1; from JASPAR
P0       a     c     g     t
1      200   307   292   200
2       96   621    96   186
3        0   999     0     0
4      999     0     0     0
5        0   999     0     0
6        0   999     0     0
7        0     0   999     0
8        0   999     0     0
9      134   574    49   241
10     335   270   239   154
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK09021.1_UN0022.1
XX
ID  PK09021.1_UN0022.1
XX
DE  UN0022.1 PK09021.1; from JASPAR
P0       a     c     g     t
1      217   253   282   246
2      295   243   282   178
3        0     0     0   999
4        0     0   999     0
5      999     0     0     0
6        0   999     0     0
7        0     0   999     0
8        0     0     0   999
9      265   669    32    32
10     711    71   109   107
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK09702.1_UN0025.1
XX
ID  PK09702.1_UN0025.1
XX
DE  UN0025.1 PK09702.1; from JASPAR
P0       a     c     g     t
1        1   125   871     1
2        0   997     0     0
3      997     0     0     0
4        0   997     0     0
5        0     0   997     0
6        0     0     0   997
7        0     0   997     0
8      332   167   498     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK10344.1_UN0026.1
XX
ID  PK10344.1_UN0026.1
XX
DE  UN0026.1 PK10344.1; from JASPAR
P0       a     c     g     t
1       91     0   725   182
2        0     0   997     0
3        0     0   997     0
4      855   143     0     0
5        0   997     0     0
6        0   997     0     0
7      997     0     0     0
8        1   871     1   125
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK10955.1_UN0027.1
XX
ID  PK10955.1_UN0027.1
XX
DE  UN0027.1 PK10955.1; from JASPAR
P0       a     c     g     t
1      500   136   253   109
2        9   970     3    17
3        8    89    43   857
4        7     0     3   988
5        7     4     8   979
6      308    59    16   615
7      181   187   126   504
8      218   225   246   310
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK12011.1_UN0029.1
XX
ID  PK12011.1_UN0029.1
XX
DE  UN0029.1 PK12011.1; from JASPAR
P0       a     c     g     t
1      138   287    85   488
2      985     0    12     1
3      926    58     7     8
4        3   986     0     9
5       35    80   798    84
6      112    24   701   161
7      213   266   113   405
8      288   312   108   291
9      363   216   180   239
10     265   228   209   296
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK12240.1_UN0030.1
XX
ID  PK12240.1_UN0030.1
XX
DE  UN0030.1 PK12240.1; from JASPAR
P0       a     c     g     t
1        2     2   992     2
2        2     2   992     2
3        2     2   992     2
4        2   745   250     2
5        2   992     2     2
6        2   992     2     2
7      250   745     2     2
8        2   745   250     2
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK13314.1_UN0032.1
XX
ID  PK13314.1_UN0032.1
XX
DE  UN0032.1 PK13314.1; from JASPAR
P0       a     c     g     t
1      135   302   177   384
2      169   211   421   197
3       22    15   862    99
4       41     8   919    30
5      155   167   482   194
6       72   727    52   146
7        0   991     6     1
8       14   908     0    77
9      828    46   105    19
10       6   806    77   110
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK14653.1_UN0034.1
XX
ID  PK14653.1_UN0034.1
XX
DE  UN0034.1 PK14653.1; from JASPAR
P0       a     c     g     t
1       95   137   489   276
2       17     4   923    54
3       57    20   915     6
4      134   207   568    89
5       53   881    11    53
6        8   975     4    12
7        0   989     0    10
8      886    34    73     6
9        1   870    62    65
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK15337.1_UN0036.1
XX
ID  PK15337.1_UN0036.1
XX
DE  UN0036.1 PK15337.1; from JASPAR
P0       a     c     g     t
1      354   244   223   177
2       69   769    33   127
3       51     1   159   788
4       67     6    39   886
5       73    39    43   842
6      325   155    92   426
7      212   241   212   333
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK15505.1_UN0037.1
XX
ID  PK15505.1_UN0037.1
XX
DE  UN0037.1 PK15505.1; from JASPAR
P0       a     c     g     t
1      290   295   173   240
2      190   120   628    59
3        0   999     0     0
4      999     0     0     0
5        0   999     0     0
6        0     0   999     0
7        0     0     0   999
8        0     0   999     0
9       76   255   137   530
10     190   263   286   259
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK16335.1_UN0038.1
XX
ID  PK16335.1_UN0038.1
XX
DE  UN0038.1 PK16335.1; from JASPAR
P0       a     c     g     t
1      233   233   233   298
2      298   233   233   233
3      298   233   233   233
4      158   293   243   304
5       99    99   614   185
6      772   227     0     0
7       64   935     0     0
8      999     0     0     0
9       16    16   951    16
10      16   951    16    16
11      16    16    16   951
12     167    91   649    91
13     150   150   300   399
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK16340.1_UN0039.1
XX
ID  PK16340.1_UN0039.1
XX
DE  UN0039.1 PK16340.1; from JASPAR
P0       a     c     g     t
1       67   812    73    46
2       50     0   329   619
3       68     0    40   890
4       77    10    57   853
5      327   135    85   450
6      237   244   175   343
7      230   245   245   279
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK17878.1_UN0040.1
XX
ID  PK17878.1_UN0040.1
XX
DE  UN0040.1 PK17878.1; from JASPAR
P0       a     c     g     t
1       80   878     0    40
2        0     0   999     0
3        0   965    33     0
4        0   999     0     0
5        0     0   999     0
6       66   632   100   200
7      138   826     0    34
8      649    50   250    50
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK18009.1_UN0041.1
XX
ID  PK18009.1_UN0041.1
XX
DE  UN0041.1 PK18009.1; from JASPAR
P0       a     c     g     t
1      233   233   300   233
2      153   153   419   273
3      749   137     0   112
4       56   831    56    56
5      999     0     0     0
6      112   887     0     0
7       56     0   887    56
8        0     0     0   999
9      185    17   780    17
10     152   152   220   473
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK18401.1_UN0042.1
XX
ID  PK18401.1_UN0042.1
XX
DE  UN0042.1 PK18401.1; from JASPAR
P0       a     c     g     t
1      300   233   233   233
2      233   233   233   300
3      377   180   261   180
4      224   194   530    49
5        0   932     0    67
6      999     0     0     0
7       67   932     0     0
8        0     0   932    67
9       16    16    16   949
10      16    16   949    16
11      69   147   593   189
12     200   311   200   288
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK18474.1_UN0043.1
XX
ID  PK18474.1_UN0043.1
XX
DE  UN0043.1 PK18474.1; from JASPAR
P0       a     c     g     t
1      307   270   210   210
2      456   117   309   117
3        0   999     0     0
4      999     0     0     0
5        0   999     0     0
6        0     0   999     0
7        0     0     0   999
8        0     0   999     0
9       39   743    96   120
10     226   281   253   238
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK19166.1_UN0044.1
XX
ID  PK19166.1_UN0044.1
XX
DE  UN0044.1 PK19166.1; from JASPAR
P0       a     c     g     t
1       29   734   117   117
2        0   956     0    42
3      382   616     0     0
4        0   999     0     0
5        0     0     0   999
6      999     0     0     0
7      615     0    51   333
8       77   230   115   576
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK19363.1_UN0046.1
XX
ID  PK19363.1_UN0046.1
XX
DE  UN0046.1 PK19363.1; from JASPAR
P0       a     c     g     t
1      150   716    75    58
2       25    12   948    12
3       18   874   106     0
4       18   974     6     0
5        0     6   993     0
6       58   627   137   176
7      157   774     7    60
8      488    66   299   144
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK19621.1_UN0047.1
XX
ID  PK19621.1_UN0047.1
XX
DE  UN0047.1 PK19621.1; from JASPAR
P0       a     c     g     t
1      158   413   182   245
2      146   468   228   156
3       93   185    25   695
4      109    64    82   744
5      981     4     8     5
6        9    38    27   925
7       11   963     5    19
8      103   493   102   299
9      310   172   232   284
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK19717.1_UN0048.1
XX
ID  PK19717.1_UN0048.1
XX
DE  UN0048.1 PK19717.1; from JASPAR
P0       a     c     g     t
1      100   115   712    72
2        6     8   955    29
3       49    34   893    22
4      116   401   401    81
5       23   929     2    44
6        8   982     4     4
7       21   930     2    46
8      553   313    72    60
9        0   882    36    80
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK20225.1_UN0049.1
XX
ID  PK20225.1_UN0049.1
XX
DE  UN0049.1 PK20225.1; from JASPAR
P0       a     c     g     t
1      219   287   250   242
2      135   788    33    42
3      757    98    70    73
4        4   819    47   128
5       80     0   897    21
6       46    29     0   923
7       14     0   984     0
8      245   461   171   121
9      199   243   303   253
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK20317.1_UN0050.1
XX
ID  PK20317.1_UN0050.1
XX
DE  UN0050.1 PK20317.1; from JASPAR
P0       a     c     g     t
1       62   868    37    31
2       22     0   330   646
3       45     0    55   898
4       53     0    60   884
5      340   100    81   477
6      245   222   168   363
7      217   252   259   270
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK20392.1_UN0051.1
XX
ID  PK20392.1_UN0051.1
XX
DE  UN0051.1 PK20392.1; from JASPAR
P0       a     c     g     t
1      154   289   184   371
2      148   395    79   376
3      776    73    80    70
4      945    27     4    22
5       13    14     2   968
6      478   330    66   124
7      973     5     9    11
8       63    30    47   858
9      243    61   294   400
10     199   233   377   189
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK20543.1_UN0054.1
XX
ID  PK20543.1_UN0054.1
XX
DE  UN0054.1 PK20543.1; from JASPAR
P0       a     c     g     t
1      105   253   119   521
2      145    60    36   757
3      803    23   107    64
4      980    13     0     5
5        6   961     1    30
6       35   642    15   306
7      332   352   180   134
8      234   231   297   236
9      420   141   278   159
10     181   295   357   165
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK21166.1_UN0057.1
XX
ID  PK21166.1_UN0057.1
XX
DE  UN0057.1 PK21166.1; from JASPAR
P0       a     c     g     t
1      150   483    50   316
2      999     0     0     0
3      123   845    10    20
4        0   979     0    20
5      282    70     0   646
6      999     0     0     0
7      260   739     0     0
8       54   616    13   315
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK23009.1_UN0059.1
XX
ID  PK23009.1_UN0059.1
XX
DE  UN0059.1 PK23009.1; from JASPAR
P0       a     c     g     t
1      237   217   214   330
2      233   189   305   271
3      212   407   107   272
4      420   157   317   103
5       24   890     5    79
6       29   104    54   811
7       52     0    19   927
8       39    12    18   930
9      338    73    25   563
10     176   226   162   434
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK23763.1_UN0060.1
XX
ID  PK23763.1_UN0060.1
XX
DE  UN0060.1 PK23763.1; from JASPAR
P0       a     c     g     t
1      210   276   176   337
2      287   270   143   298
3      461   173   238   127
4       26   933    13    26
5       35    52    71   839
6       30     6    21   941
7       22     8    24   944
8      366    91    53   489
9      199   244   149   406
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK23964.1_UN0061.1
XX
ID  PK23964.1_UN0061.1
XX
DE  UN0061.1 PK23964.1; from JASPAR
P0       a     c     g     t
1      373   117   250   258
2      132   167   272   427
3       45    47   540   366
4      268   716    14     0
5       16   942     1    39
6      960     0    37     1
7       52   787   100    59
8       88    64   831    15
9       50    49    27   873
10     277   218   452    51
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK24205.1_UN0063.1
XX
ID  PK24205.1_UN0063.1
XX
DE  UN0063.1 PK24205.1; from JASPAR
P0       a     c     g     t
1      452   211   133   202
2       86   671   154    86
3      999     0     0     0
4        0   999     0     0
5        0     0   999     0
6        0     0     0   999
7        0     0   999     0
8        0     0   570   429
9      116   651   116   116
10     381   222   232   163
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK24580.1_UN0064.1
XX
ID  PK24580.1_UN0064.1
XX
DE  UN0064.1 PK24580.1; from JASPAR
P0       a     c     g     t
1      272   453   182    91
2        0     0   998     0
3       59   880    59     0
4        0   998     0     0
5        0     0   998     0
6        0   117   880     0
7        0   998     0     0
8      614   230    77    77
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK25034.1_UN0067.1
XX
ID  PK25034.1_UN0067.1
XX
DE  UN0067.1 PK25034.1; from JASPAR
P0       a     c     g     t
1      293   185   217   302
2      223   175   310   289
3      139    98    50   711
4       25    28   929    15
5      963     0     0    35
6       20   929     1    48
7      542   106   253    97
8       91    94   496   316
9      123   381   328   166
10     222   209   159   408
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK25870.1_UN0068.1
XX
ID  PK25870.1_UN0068.1
XX
DE  UN0068.1 PK25870.1; from JASPAR
P0       a     c     g     t
1      303   196   178   321
2      392    12   594     0
3        0     0   969    30
4       20     0    30   949
5       20   949    30     0
6      999     0     0     0
7      999     0     0     0
8      434   376    58   130
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK26523.1_UN0070.1
XX
ID  PK26523.1_UN0070.1
XX
DE  UN0070.1 PK26523.1; from JASPAR
P0       a     c     g     t
1      384   148   196   270
2      558    69    63   309
3      269   349    98   283
4      960     5    20    14
5       27    12    10   949
6       70    28    32   867
7       41   904    14    39
8       32   417   137   413
9      243   286   197   272
10     180   274   164   381
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK27085.1_UN0072.1
XX
ID  PK27085.1_UN0072.1
XX
DE  UN0072.1 PK27085.1; from JASPAR
P0       a     c     g     t
1      971     9     9     9
2      971     9     9     9
3        9     9     9   971
4        9     9     9   971
5        9     9     9   971
6      971     9     9     9
7        9     9     9   971
8        9     9     9   971
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK27109.1_UN0073.1
XX
ID  PK27109.1_UN0073.1
XX
DE  UN0073.1 PK27109.1; from JASPAR
P0       a     c     g     t
1       37    37   703   222
2      743   205     0    51
3        0   926    24    48
4      999     0     0     0
5       48     0   950     0
6        0   950     0    48
7        0     0     0   999
8      185   111   666    37
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK27149.1_UN0074.1
XX
ID  PK27149.1_UN0074.1
XX
DE  UN0074.1 PK27149.1; from JASPAR
P0       a     c     g     t
1       71   143     0   784
2        0   166   610   222
3      999     0     0     0
4        0   999     0     0
5        0     0   972    26
6        0    30     0   968
7       74    74   850     0
8       91     0   544   363
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK27683.1_UN0076.1
XX
ID  PK27683.1_UN0076.1
XX
DE  UN0076.1 PK27683.1; from JASPAR
P0       a     c     g     t
1      340   134   240   284
2      414    58   170   356
3       57    57    30   854
4       61    35    45   856
5       24   606     0   367
6      814    17    85    82
7      895    14    32    57
8      883    25    51    39
9      303   246    75   374
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK27693.1_UN0077.1
XX
ID  PK27693.1_UN0077.1
XX
DE  UN0077.1 PK27693.1; from JASPAR
P0       a     c     g     t
1      199   367   180   251
2      173   394   241   190
3      128   222    39   610
4      138    96    84   679
5      972     3     7    16
6       15    45    36   902
7       14   956    11    17
8      116   436   138   307
9      261   211   231   294
10     220   227   256   295
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK28187.1_UN0078.1
XX
ID  PK28187.1_UN0078.1
XX
DE  UN0078.1 PK28187.1; from JASPAR
P0       a     c     g     t
1        4     4     4   985
2      990     3     3     3
3        3   990     3     3
4        3   990     3     3
5        3     3     3   990
6      990     3     3     3
7        3   990     3     3
8        4   985     4     4
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHL3_UN0080.1
XX
ID  PHL3_UN0080.1
XX
DE  UN0080.1 PHL3; from JASPAR
P0       a     c     g     t
1      389   161   203   245
2      587    81   101   229
3       48    37   518   395
4      929     2    60     7
5       37     0     0   962
6       91    45   139   724
7        0   988     0    11
8       48   364   298   289
9      212   310   223   254
10     216   267   190   326
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CEG01538.1_UN0275.1
XX
ID  CEG01538.1_UN0275.1
XX
DE  UN0275.1 CEG01538.1; from JASPAR
P0       a     c     g     t
1       69   241    34   654
2      112   112   687    87
3      999     0     0     0
4        0     0     0   999
5        0     0     0   999
6        0     0   999     0
7      111    49     0   839
8      138   103    17   741
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PNRC2_UN0276.1
XX
ID  PNRC2_UN0276.1
XX
DE  UN0276.1 PNRC2; from JASPAR
P0       a     c     g     t
1      725   182    91     0
2      873     0     0   125
3      686     0     0   312
4      312   686     0     0
5        0   998     0     0
6        0   935     0    62
7       62     0     0   935
8      997     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARID6_UN0351.1
XX
ID  ARID6_UN0351.1
XX
DE  UN0351.1 ARID6; from JASPAR
P0       a     c     g     t
1      297    71    78   292
2      328    58    69   283
3      246    39    57   396
4      199    47    43   449
5       49    10    14   665
6       55    19    24   640
7      686     1    20    31
8      704     5     9    20
9       13     5     4   716
10      21    14     4   699
11      18     0   701    19
12     642    10    27    59
13     587    23    32    96
14     288    60    56   334
15     272    72    57   337
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT3G11100_UN0353.1
XX
ID  AT3G11100_UN0353.1
XX
DE  UN0353.1 AT3G11100; from JASPAR
P0       a     c     g     t
1      479  1021   300   696
2      615   330  1002   549
3     1432   652   254   158
4       16  2413     6    61
5       24    13  2445    14
6       57    26  2394    19
7      102  2298    12    84
8       73    55  2311    57
9     2127    58   198   113
10     355   653   886   602
11     760   289   875   572
12     810   396   698   592
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT3G42860_UN0354.1
XX
ID  AT3G42860_UN0354.1
XX
DE  UN0354.1 AT3G42860; from JASPAR
P0       a     c     g     t
1      626   198   199   264
2      804   100   170   213
3     1072    26   166    23
4        9     6  1268     4
5        0     2     2  1283
6       10  1271     1     5
7     1277     5     2     3
8     1279     1     3     4
9       66  1136     6    79
10     122   114   871   180
11     339   305   301   342
12     334   357   165   431
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT3G49930_UN0355.1
XX
ID  AT3G49930_UN0355.1
XX
DE  UN0355.1 AT3G49930; from JASPAR
P0       a     c     g     t
1      222   290   146   466
2      294   286   131   413
3       44    90    39   951
4     1068    10    17    29
5       35  1013    42    34
6       23  1078    11    12
7       40     5    30  1049
8     1077     7    14    26
9      887   139    19    79
10     234   362    74   454
11     325   205   119   475
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT5G23930_UN0357.1
XX
ID  AT5G23930_UN0357.1
XX
DE  UN0357.1 AT5G23930; from JASPAR
P0       a     c     g     t
1      228  1033   293   520
2      535  1323    91   125
3        0     0  2074     0
4        0  2074     0     0
5        0  2074     0     0
6        0     0  2074     0
7        0  2074     0     0
8        0  2074     0     0
9      600    43  1135   296
10     191  1036   249   598
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BHLH28_UN0359.1
XX
ID  BHLH28_UN0359.1
XX
DE  UN0359.1 BHLH28; from JASPAR
P0       a     c     g     t
1      806   581   525   836
2      740   657   481   870
3      716   514   610   908
4      688   548   559   953
5      734   287  1141   586
6      394    51   291  2012
7      117  2536    12    83
8       22  2660    16    50
9       16     5  2716    11
10      10     4     9  2725
11    2714    12    12    10
12       8  2699    19    22
13    1731   261   509   247
14    1043   455   581   669
15     823   489   436  1000
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  EICBP.B_UN0362.1
XX
ID  EICBP.B_UN0362.1
XX
DE  UN0362.1 EICBP.B; from JASPAR
P0       a     c     g     t
1      302   175   186   243
2      291   180   169   266
3      224   292   224   166
4      644    65    84   113
5      762    30    48    66
6      791    23    47    45
7      610    97   182    17
8        7   888     3     8
9       28     9   861     8
10      18   839     6    43
11       9     3   889     5
12      10    10    21   865
13      95    53   596   162
14     530    52   143   181
15     343   153   200   210
16     281   168   225   232
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  S1FA3_UN0379.1
XX
ID  S1FA3_UN0379.1
XX
DE  UN0379.1 S1FA3; from JASPAR
P0       a     c     g     t
1      410   371   703   496
2      476   573   418   513
3       97    81    34  1768
4       39     9     5  1927
5       17  1915    23    25
6       58  1078   118   726
7     1842    16   102    20
8       38    30  1892    20
9     1943     4    15    18
10    1800    19    69    92
11     513   379   627   461
12     462   759   365   394
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AGL95_UN0386.1
XX
ID  AGL95_UN0386.1
XX
DE  UN0386.1 AGL95; from JASPAR
P0       a     c     g     t
1      603    71   247    79
2       37    12   934    17
3      945    10    25    20
4      911     7    22    61
5      103   195   556   146
6      244   405   215   136
7      141    34    15   810
8       37     5    17   941
9       13   945    18    24
10      50   141    74   734
11     763    39   176    22
12      13    15   955    17
13     913    25    22    40
14     724    47   104   124
15     229   218   393   160
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARID3_UN0388.1
XX
ID  ARID3_UN0388.1
XX
DE  UN0388.1 ARID3; from JASPAR
P0       a     c     g     t
1      703    64   108   126
2      714    90    42   154
3      564    64    13   359
4      269    57    44   630
5      220   196    66   518
6      381   159   275   185
7      797    70     0   132
8      976     7    18     0
9      980     0     0    20
10       0     0     0  1000
11       0   262     0   738
12     775     0   225     0
13     934    11    55     0
14     535     0    22   443
15     269    31    18   683
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT1G66420_UN0389.1
XX
ID  AT1G66420_UN0389.1
XX
DE  UN0389.1 AT1G66420; from JASPAR
P0       a     c     g     t
1      736    38    75   151
2      566    75   208   151
3      189   660    57    94
4      321   528   151     0
5        0    19     0   981
6      113   189     0   698
7      981     0     0    19
8        0     0     0  1000
9        0   981     0    19
10      19   981     0     0
11     774     0   132    94
12     132   132    38   698
13     528    38     0   434
14     340    19    57   585
15     283   434    75   208
16      75   226   113   585
17     208    57   170   566
18     453   170   245   132
19     208    94   340   358
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT2G31460_UN0390.1
XX
ID  AT2G31460_UN0390.1
XX
DE  UN0390.1 AT2G31460; from JASPAR
P0       a     c     g     t
1      335   104   293   268
2      476    85   159   280
3      457    49   226   268
4       18    18   957     6
5      988     0    12     0
6     1000     0     0     0
7     1000     0     0     0
8        6     0     0   994
9        0     0   994     6
10     945    37     6    12
11     366    37    30   567
12     390    79   329   201
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT3G58630_UN0391.1
XX
ID  AT3G58630_UN0391.1
XX
DE  UN0391.1 AT3G58630; from JASPAR
P0       a     c     g     t
1      234   359   115   292
2      187   216   364   232
3       97   150    59   694
4       38   804    45   114
5      139     2   859     0
6       29   950     0    22
7        0  1000     0     0
8        0     0  1000     0
9        0     0  1000     0
10     793   101    29    77
11     241    27   701    31
12     575   105   114   207
13     360   178    74   387
14     272   115   252   360
15     182   238   207   373
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT4G00390_UN0392.1
XX
ID  AT4G00390_UN0392.1
XX
DE  UN0392.1 AT4G00390; from JASPAR
P0       a     c     g     t
1      285   191   332   191
2      679    75   171    75
3       25   924    25    25
4       25   924    25    25
5        0     0  1000     0
6        0    86   914     0
7        0    99    86   815
8        0   914    86     0
9      254   149   453   144
10     377   175   273   175
11     225   326   225   225
12     225   326   225   225
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT4G09450_UN0393.1
XX
ID  AT4G09450_UN0393.1
XX
DE  UN0393.1 AT4G09450; from JASPAR
P0       a     c     g     t
1      168   374   180   277
2      165   398   227   210
3      111   184    17   688
4       63    34    68   835
5      972     7    10    12
6        9    31    21   938
7       16   968     7     9
8       75   559    80   286
9      279   164   209   348
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AtPLATZ6_UN0394.1
XX
ID  AtPLATZ6_UN0394.1
XX
DE  UN0394.1 AtPLATZ6; from JASPAR
P0       a     c     g     t
1      580    70   308    42
2        5     7   988     0
3      968     5     3    23
4      922     8    17    53
5       75   238   550   137
6      288   337   188   187
7       98    42    12   848
8       13     0     7   980
9        3   985     5     7
10       0   142    47   812
11     778    37   183     2
12       2    10   988     0
13     960     7     8    25
14     700    57   110   133
15     248   187   378   187
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CRC_UN0396.1
XX
ID  CRC_UN0396.1
XX
DE  UN0396.1 CRC; from JASPAR
P0       a     c     g     t
1       35   143    80   742
2      587    48     2   363
3      995     0     0     5
4       28     0     0   972
5        0   988    12     0
6     1000     0     0     0
7        0     0     5   995
8      598    73    60   268
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  EDF3_UN0397.1
XX
ID  EDF3_UN0397.1
XX
DE  UN0397.1 EDF3; from JASPAR
P0       a     c     g     t
1      428     1   570     1
2        1   997     1     1
3      997     1     1     1
4      997     1     1     1
5        1   997     1     1
6      997     1     1     1
7        1     1   333   665
8      854     1     1   143
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ENY_UN0398.1
XX
ID  ENY_UN0398.1
XX
DE  UN0398.1 ENY; from JASPAR
P0       a     c     g     t
1      294   113   129   464
2      259   139   168   434
3      114    50    50   785
4        8    34    13   945
5        0     5     7   988
6        0     3     0   997
7        0     0  1000     0
8        0     2     5   993
9        0  1000     0     0
10      27    96   627   250
11       3    40    45   911
12     145   133    62   661
13     175     2     3   820
14     239    92    32   637
15     108   324   304   264
16      94   239   129   538
17     249   109   353   289
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GEBP_UN0399.1
XX
ID  GEBP_UN0399.1
XX
DE  UN0399.1 GEBP; from JASPAR
P0       a     c     g     t
1      143    36   607   214
2       27   972     0     0
3        0     0   999     0
4        0     0   949    50
5        0   999     0     0
6        0   974     0    25
7       54     0   810   135
8      160    40   560   240
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HMGB15_UN0400.1
XX
ID  HMGB15_UN0400.1
XX
DE  UN0400.1 HMGB15; from JASPAR
P0       a     c     g     t
1       70    83    35   813
2      695    13     0   292
3       48     0     0   952
4      981     6     6     6
5      343    83    79   495
6      206    60    41   692
7      908    57    22    13
8        0     3     0   997
9     1000     0     0     0
10     273     0     0   727
11     641     0   229   130
12     130    60   787    22
13      10     6     0   984
14     225    63    19   692
15     102    63    54   781
16     254    70   105   571
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HMGB9_UN0401.1
XX
ID  HMGB9_UN0401.1
XX
DE  UN0401.1 HMGB9; from JASPAR
P0       a     c     g     t
1      491   101    71   337
2      288    85   136   491
3      430   150   107   314
4     1000     0     0     0
5      308     0     0   692
6        0     0     0  1000
7        0     0     0  1000
8     1000     0     0     0
9     1000     0     0     0
10       0     0     0  1000
11     211   128   195   465
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  IDD11_UN0402.1
XX
ID  IDD11_UN0402.1
XX
DE  UN0402.1 IDD11; from JASPAR
P0       a     c     g     t
1      285   371   113   231
2      621    87   209    84
3      210   332   323   135
4      680    12    72   236
5      846     2     2   150
6      641    70   142   147
7      915    39    41     5
8      209   682    75    34
9        7     0   990     3
10     991     9     0     0
11       0  1000     0     0
12    1000     0     0     0
13     959    12    24     5
14     885    31    63    21
15     682    70    77   171
16     448   144   118   291
17     482   116   109   292
18     369   152   227   251
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  JGL_UN0403.1
XX
ID  JGL_UN0403.1
XX
DE  UN0403.1 JGL; from JASPAR
P0       a     c     g     t
1      853    25    82    40
2      415   427   108    50
3       37   458     7   498
4      207    40   190   563
5      115   267   237   382
6       13   815    87    85
7      963     7    30     0
8        0    12   988     0
9        7     0     0   993
10       3    47    27   923
11     397   313    78   212
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  KHZ1_UN0404.1
XX
ID  KHZ1_UN0404.1
XX
DE  UN0404.1 KHZ1; from JASPAR
P0       a     c     g     t
1      480   127   222   172
2      420   173   187   220
3      372   137   230   262
4      168   362   307   163
5      717    98   118    67
6      762    22     0   217
7     1000     0     0     0
8     1000     0     0     0
9      985     0    15     0
10       0     0  1000     0
11      98   130   347   425
12     273   105   172   450
13     585    15   298   102
14     477   173   148   202
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MGH6.1_UN0405.1
XX
ID  MGH6.1_UN0405.1
XX
DE  UN0405.1 MGH6.1; from JASPAR
P0       a     c     g     t
1        0   443     0   557
2       82     0   814   103
3        0   619   361    21
4        0   969     0    31
5        0     0   979    21
6        0     0   268   732
7      258   237   351   155
8      454   103   268   175
9      423   155   196   227
10     258   103   278   361
11     320   124   216   340
12     237   371    62   330
13     165   247   361   227
14     670   330     0     0
15       0   990    10     0
16       0     0  1000     0
17      31   206   443   320
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NLP4_UN0406.1
XX
ID  NLP4_UN0406.1
XX
DE  UN0406.1 NLP4; from JASPAR
P0       a     c     g     t
1      159   241   244   356
2      230   255   356   159
3      205   395   121   279
4      238   252   142   367
5      170   192   436   203
6      115   537   129   219
7        3    11     0   986
8        0     0  1000     0
9        0   992     0     8
10       0    14     0   986
11       8     0   992     0
12     129   627   112   132
13     274   132   118   477
14     263   151   323   263
15     255   334   153   258
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  REM1_UN0407.1
XX
ID  REM1_UN0407.1
XX
DE  UN0407.1 REM1; from JASPAR
P0       a     c     g     t
1      329   121   121   429
2        0    95   802   103
3      409     0   191   400
4        0     0     0  1000
5        0     0  1000     0
6        0     0     0  1000
7     1000     0     0     0
8        0     0  1000     0
9      418   324   129   129
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  REM21_UN0408.1
XX
ID  REM21_UN0408.1
XX
DE  UN0408.1 REM21; from JASPAR
P0       a     c     g     t
1      306   327   102   265
2      211   211   143   435
3      218   279   163   340
4      224   204   190   381
5      170   177   259   395
6      224   361   122   293
7        0     0     0  1000
8        0     0     0  1000
9        0   993     7     0
10    1000     0     0     0
11       0     0     0  1000
12       0  1000     0     0
13     381   129   177   313
14     197   238   150   415
15     190   401   163   245
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RKD2_UN0409.1
XX
ID  RKD2_UN0409.1
XX
DE  UN0409.1 RKD2; from JASPAR
P0       a     c     g     t
1      247   210   351   193
2      206   286   124   383
3      184     2   797    17
4      610   135    65   191
5        0   995     0     5
6        3   194   453   349
7      129   264   201   405
8       15    17     0   968
9       15    51     0   934
10      22   763   148    66
11     528    68   397     7
12     240   111   244   405
13     181   687    31   102
14       0     0    15   985
15      70   165    15   750
16      19   952     9    20
17      20   726     7   247
18     157   370    90   383
19     227   358   155   261
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  STKL2_UN0410.1
XX
ID  STKL2_UN0410.1
XX
DE  UN0410.1 STKL2; from JASPAR
P0       a     c     g     t
1      492   128   103   277
2      552    52   144   252
3       88   541    81   291
4      191   622   105    83
5        8    56     0   936
6       14     8    24   954
7     1000     0     0     0
8        0     0     0  1000
9        0  1000     0     0
10       0   919     0    81
11     730     2   201    68
12     277   213    52   458
13     412    30    20   537
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCX5_UN0411.1
XX
ID  TCX5_UN0411.1
XX
DE  UN0411.1 TCX5; from JASPAR
P0       a     c     g     t
1      324   225   225   225
2      637    22   225   115
3        0     0     0  1000
4        0     0     0  1000
5        0   776     0   224
6      906     0    94     0
7     1000     0     0     0
8     1000     0     0     0
9      130   251    25   594
10     228   228   228   316
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  VFP5_UN0412.1
XX
ID  VFP5_UN0412.1
XX
DE  UN0412.1 VFP5; from JASPAR
P0       a     c     g     t
1      302   208   102   388
2      131   435   126   309
3      362   119   357   161
4      101   193    45   661
5        2   911    29    59
6      168     0   832     0
7       42   757   114    87
8        0  1000     0     0
9        0     0  1000     0
10       0     0   998     2
11     634   206    15   144
12     388     5   596    12
13     614    76   104   206
14     378   164   136   322
15     322   119   255   304
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WOX11_UN0413.1
XX
ID  WOX11_UN0413.1
XX
DE  UN0413.1 WOX11; from JASPAR
P0       a     c     g     t
1      734     0   122   144
2      312   118   392   179
3        0    53     0   947
4        0     0     0  1000
5     1000     0     0     0
6      954     0    46     0
7        0   110     0   890
8        0    23    53   924
9      798     0   202     0
10     479   202   319     0
11     243   122   163   471
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WOX13_UN0414.1
XX
ID  WOX13_UN0414.1
XX
DE  UN0414.1 WOX13; from JASPAR
P0       a     c     g     t
1      101   200   101   599
2       72     1     1   927
3      134     1   799    67
4      998     1     1     1
5        1     1     1   998
6        1     1     1   998
7      154     1   844     1
8      816    91    91     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YAB1_UN0415.1
XX
ID  YAB1_UN0415.1
XX
DE  UN0415.1 YAB1; from JASPAR
P0       a     c     g     t
1      796     2     2   200
2      997     1     1     1
3        1     1     1   997
4      665   112   112   112
5      775     1     1   222
6      112     1     1   886
7      554   112     1   333
8      996     1     1     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YAB5_UN0416.1
XX
ID  YAB5_UN0416.1
XX
DE  UN0416.1 YAB5; from JASPAR
P0       a     c     g     t
1      305   198   299   198
2      765    78    78    78
3      147    51    51   752
4      625   121   227    26
5      903    96     0     0
6       94     0    96   809
7       95     0     0   904
8      905    94     0     0
9      348    52   153   447
10     283   276   172   270
11     200   297   303   200
12     328   224   224   224
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ZAT9_UN0417.1
XX
ID  ZAT9_UN0417.1
XX
DE  UN0417.1 ZAT9; from JASPAR
P0       a     c     g     t
1       38   522    20   420
2        0   145     0   855
3      997     0     3     0
4        0  1000     0     0
5        0     0     0  1000
6      522    33     0   445
7      340   413   117   130
8      383   183    55   378
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GLYMA-04G093300_UN0418.1
XX
ID  GLYMA-04G093300_UN0418.1
XX
DE  UN0418.1 GLYMA-04G093300; from JASPAR
P0       a     c     g     t
1     2384  1814  1220  2467
2     2594  1764  1184  2343
3        0  7885     0     0
4        0     0   119  7766
5     7816     1     3    65
6     7649   102    82    52
7       26     3    24  7832
8        0  7885     0     0
9     2961  1883   800  2241
10    2718  1780   847  2540
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GLYMA-08G357600_UN0420.1
XX
ID  GLYMA-08G357600_UN0420.1
XX
DE  UN0420.1 GLYMA-08G357600; from JASPAR
P0       a     c     g     t
1     1134  2600  1008  1671
2     3654   593  1107  1059
3       17  6396     0     0
4        0     1  6412     0
5      217   547   124  5525
6      302   191  5839    81
7      200   134   525  5554
8      288  6073     0    52
9     1824  1368  1558  1663
10    1670  1654  1209  1880
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GLYMA-13G317000_UN0421.1
XX
ID  GLYMA-13G317000_UN0421.1
XX
DE  UN0421.1 GLYMA-13G317000; from JASPAR
P0       a     c     g     t
1     4020  3522  3422  4955
2     3343  2447  6249  3880
3    15158   355   177   229
4       11 15390    93   425
5    13931   118  1340   530
6        0 15919     0     0
7        0     0 15919     0
8      290   365    65 15199
9     3774  2289  7596  2260
10    3204  2479  4529  5707
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  LEC1-A_UN0422.1
XX
ID  LEC1-A_UN0422.1
XX
DE  UN0422.1 LEC1-A; from JASPAR
P0       a     c     g     t
1     3294  7023  2759  3467
2     9508  1783  2775  2477
3      942 14394   501   706
4      609   207 15158   569
5      452   435   174 15482
6      567   401 15073   502
7      892   820 12672  2159
8     1110 14415   672   346
9    12833   887  1497  1326
10    4101  4154  3848  4440
11    4425  3805  3775  4538
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  LEC1-A_UN0423.1
XX
ID  LEC1-A_UN0423.1
XX
DE  UN0423.1 LEC1-A; from JASPAR
P0       a     c     g     t
1     1925  1271  2338  2083
2     3228  1636  1564  1189
3        0  7614     0     3
4     7037     2   293   285
5     6966   650     1     0
6        0     0  7617     0
7       40   108     0  7469
8        4     0  7613     0
9     1334  1200  2100  2983
10    2166  2225  1436  1790
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  LEC1-A_UN0424.1
XX
ID  LEC1-A_UN0424.1
XX
DE  UN0424.1 LEC1-A; from JASPAR
P0       a     c     g     t
1     6705  3845  3654  4745
2     3892  6859  2900  5298
3    16947   590   713   699
4     1298  4176   821 12654
5      858   508 16842   741
6      615   357   455 17522
7      652   540 17337   420
8      824   422   766 16937
9      784 17815   141   209
10    6548  3454  3307  5640
11    4900  4587  3447  6015
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TAGL1_UN0426.1
XX
ID  TAGL1_UN0426.1
XX
DE  UN0426.1 TAGL1; from JASPAR
P0       a     c     g     t
1     2178   657   789  1227
2     2082   806   785  1178
3     2930   676   401   844
4     1091   351  2722   687
5     4576    61   106   108
6     4641    47    85    78
7     4647    23    83    98
8     4590    14   109   138
9     4562    37    85   167
10     320    46  4328   157
11     461    83  4129   178
12    2263   587   893  1108
13    2526   536   797   992
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NF-YB1_UN0427.1
XX
ID  NF-YB1_UN0427.1
XX
DE  UN0427.1 NF-YB1; from JASPAR
P0       a     c     g     t
1       50   170   116    80
2       50   229    79    58
3        6    25   371    14
4       11   358    32    15
5        6   393    15     2
6        7   321    79     9
7       17    23   356    20
8        2   397     6    11
9        9     5   396     6
10       5   383    19     9
11       8   384    19     5
12      57   112   198    49
13      65   146   129    76
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ONAC127_UN0428.1
XX
ID  ONAC127_UN0428.1
XX
DE  UN0428.1 ONAC127; from JASPAR
P0       a     c     g     t
1        1    11     3     5
2        7     4     2     7
3        0     0    20     0
4        0    20     0     0
5        0     0     0    20
6        0     0    20     0
7        0    20     0     0
8        0    20     0     0
9        3    11     4     2
10       3    10     3     4
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ONAC127_UN0429.1
XX
ID  ONAC127_UN0429.1
XX
DE  UN0429.1 ONAC127; from JASPAR
P0       a     c     g     t
1       21     9    15    10
2       11    10    27     7
3       49     0     1     5
4        0     3    46     6
5        1     0    48     6
6       50     0     0     5
7       53     1     1     0
8        1     1    52     1
9       51     0     2     2
10      12    15    11    17
11      14    15    21     5
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ONAC127_UN0430.1
XX
ID  ONAC127_UN0430.1
XX
DE  UN0430.1 ONAC127; from JASPAR
P0       a     c     g     t
1       20     8    20    20
2       16    16    22    14
3       65     0     0     3
4       67     0     0     1
5        4     0    62     2
6       57     2     7     2
7       61     5     1     1
8       63     0     5     0
9       58     1     3     6
10       3     7    56     2
11      16    14    30     8
12      25     9    19    15
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  bHLH172_UN0431.1
XX
ID  bHLH172_UN0431.1
XX
DE  UN0431.1 bHLH172; from JASPAR
P0       a     c     g     t
1     1263  1217  1242   582
2      629  1708   990   977
3        0     0  4304     0
4        0  4304     0     0
5     4304     0     0     0
6        0  4304     0     0
7        0     0  4304     0
8        0  4304     0     0
9     1428   876  1512   488
10     663  1791  1118   732
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d042907_UN0432.1
XX
ID  Zm00001d042907_UN0432.1
XX
DE  UN0432.1 Zm00001d042907; from JASPAR
P0       a     c     g     t
1      255   282   246   137
2      123   244   160   393
3       18    56   827    19
4      798    62    36    24
5       13   864    22    21
6       13    18   879    10
7       39   677    27   177
8       27   835    42    16
9      860    14    36    10
10      37   774    43    66
11     157   300   291   172
12     202   264   300   154
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d024644_UN0433.1
XX
ID  Zm00001d024644_UN0433.1
XX
DE  UN0433.1 Zm00001d024644; from JASPAR
P0       a     c     g     t
1      327   175   191   217
2      236   161   171   342
3       61   555   107   187
4       38   787    45    40
5       73    58    31   748
6       23    35    27   825
7      868    14    11    17
8        4    18     4   884
9        6   896     3     5
10      41   708    25   136
11     483   137   130   160
12     116   181   112   501
13     243   233   138   296
14     175   269   211   255
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d013777_UN0434.1
XX
ID  Zm00001d013777_UN0434.1
XX
DE  UN0434.1 Zm00001d013777; from JASPAR
P0       a     c     g     t
1       99   534   264   207
2      111   527   277   189
3       15   113    30   946
4       40   992    59    13
5       19    32    22  1031
6       12  1069    10    13
7       21    24   990    69
8       19   945    30   110
9       11  1061    14    18
10      32   149    73   850
11     112   588   254   150
12     103   416   344   241
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NGA4_UN0687.1
XX
ID  NGA4_UN0687.1
XX
DE  UN0687.1 NGA4; from JASPAR
P0       a     c     g     t
1       80    72    42    93
2       62    43    47   135
3       32    54    79   122
4       23    19    29   216
5        6     6    10   265
6        5   276     4     2
7      280     3     1     3
8        3     2   280     2
9       16     1   267     3
10       3     6     2   276
11     157     5    91    34
12      91    58    53    85
13      89    45    49   104
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BHLH10_UN0688.1
XX
ID  BHLH10_UN0688.1
XX
DE  UN0688.1 BHLH10; from JASPAR
P0       a     c     g     t
1      187    97   154   169
2      127   219    84   177
3      106   303    59   139
4      133    62   142   270
5       48   412    42   105
6       31   492    17    67
7      308     1   293     5
8        9   589     7     2
9        3   601     2     1
10      10     1   593     3
11     487    78    16    26
12       7   591     4     5
13     537     7    48    15
14     111   360    62    74
15     139   181    51   236
16     178    79   174   176
17     134   185   105   183
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NFYB2_UN0689.1
XX
ID  NFYB2_UN0689.1
XX
DE  UN0689.1 NFYB2; from JASPAR
P0       a     c     g     t
1     2030  1475  1388  2546
2     1465  1097  2724  2153
3     6742   224   211   262
4        0  7151    21   267
5     6665    27   498   249
6        0  7439     0     0
7        0     0  7439     0
8      181   289    46  6923
9     1887   993  3708   851
10    1452  1000  1918  3069
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NFYC2_UN0690.1
XX
ID  NFYC2_UN0690.1
XX
DE  UN0690.1 NFYC2; from JASPAR
P0       a     c     g     t
1      633  2373   837   837
2     3455   259   443   523
3      169  4264    87   160
4      116    31  4428   105
5       79   105    23  4473
6      120    53  4421    86
7      236   103  3438   903
8      343  4053   174   110
9     3557   184   508   431
10    1466   882   948  1384
11    1473   966   876  1365
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
