AC  br_MA0010.1
XX
ID  br_MA0010.1
XX
DE  MA0010.1 br; from JASPAR
P0       a     c     g     t
1        3     1     4     1
2        1     1     1     6
3        5     2     1     1
4        7     0     1     1
5        3     0     0     6
6        6     0     0     3
7        4     1     4     0
8        7     0     1     1
9        1     8     0     0
10       9     0     0     0
11       8     0     1     0
12       5     0     3     1
13       4     0     0     5
14       2     3     2     2
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  br_MA0011.1
XX
ID  br_MA0011.1
XX
DE  MA0011.1 br; from JASPAR
P0       a     c     g     t
1        3     1     1     7
2        5     2     1     4
3        0    10     0     2
4        0     1     0    11
5       12     0     0     0
6        1     1     2     8
7        2     0     1     9
8        1     2     1     8
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  br_MA0012.1
XX
ID  br_MA0012.1
XX
DE  MA0012.1 br; from JASPAR
P0       a     c     g     t
1        3     1     1     7
2        9     2     1     0
3       10     0     0     2
4       12     0     0     0
5        0    10     1     1
6        4     0     0     8
7       10     0     0     2
8        6     0     4     2
9        6     1     2     3
10       4     3     2     3
11       2     3     5     2
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  br_MA0013.1
XX
ID  br_MA0013.1
XX
DE  MA0013.1 br; from JASPAR
P0       a     c     g     t
1        1     1     0     4
2        4     0     0     2
3        2     0     4     0
4        1     0     0     5
5        5     0     1     0
6        6     0     0     0
7        5     0     1     0
8        1     3     1     1
9        3     0     1     2
10       5     0     0     1
11       3     1     0     2
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Cf2_MA0015.1
XX
ID  Cf2_MA0015.1
XX
DE  MA0015.1 Cf2; from JASPAR
P0       a     c     g     t
1       25    13    40     2
2        1     2     1    76
3       74     2     4     0
4        0     9     4    67
5       78     2     0     0
6        1     4     1    74
7       41     2    36     1
8        2     9     1    68
9       53     3    20     4
10      12    29    15    24
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  usp_MA0016.1
XX
ID  usp_MA0016.1
XX
DE  MA0016.1 usp; from JASPAR
P0       a     c     g     t
1        0     1    37     0
2        1     0    36     1
3        0     0    38     0
4        0     0    38     0
5        0     0     0    38
6        0    36     1     1
7       35     0     3     0
8        5    25     3     5
9        5     8    22     3
10       6    10    16     6
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  dl_MA0022.1
XX
ID  dl_MA0022.1
XX
DE  MA0022.1 dl; from JASPAR
P0       a     c     g     t
1        0     5     6     2
2        0     0    12     1
3        0     1    11     1
4        0     0    10     3
5        1     1     3     8
6        1     0     2    10
7        1     0     0    12
8        0     0     0    13
9        0     3     0    10
10       1     9     0     3
11       1     9     1     2
12       3     5     5     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  dl_MA0023.1
XX
ID  dl_MA0023.1
XX
DE  MA0023.1 dl; from JASPAR
P0       a     c     g     t
1        0     1     7     1
2        0     0     7     2
3        0     0     9     0
4        0     0     8     1
5        4     1     0     4
6        2     1     0     6
7        0     0     0     9
8        0     1     0     8
9        0     9     0     0
10       1     8     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Eip74EF_MA0026.1
XX
ID  Eip74EF_MA0026.1
XX
DE  MA0026.1 Eip74EF; from JASPAR
P0       a     c     g     t
1        2    12     2     1
2        5    12     0     0
3        0     0    17     0
4        0     0    17     0
5       17     0     0     0
6       17     0     0     0
7        5     1    10     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  hb_MA0049.1
XX
ID  hb_MA0049.1
XX
DE  MA0049.1 hb; from JASPAR
P0       a     c     g     t
1        1     5     8     2
2        6     8     2     0
3        9     3     4     0
4        4     3     1     8
5       13     1     0     2
6       16     0     0     0
7       16     0     0     0
8       14     0     2     0
9       15     1     0     0
10       9     2     2     3
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  sna_MA0086.2
XX
ID  sna_MA0086.2
XX
DE  MA0086.2 sna; from JASPAR
P0       a     c     g     t
1      241   200   309   250
2      128   324   264   284
3      454    57   387   102
4      835     7   114    44
5        5   984     6     5
6      984     4     7     5
7       87     4   903     6
8        5     4   986     5
9        5     4     7   984
10      10     5   976    10
11      42   455   184   319
12     535   139   147   179
13     290   244   230   236
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Ubx_MA0094.2
XX
ID  Ubx_MA0094.2
XX
DE  MA0094.2 Ubx; from JASPAR
P0       a     c     g     t
1        3     5     3     9
2        0     0     0    20
3        0     0     0    20
4       17     0     0     3
5       20     0     0     0
6        0     0     0    20
7        0     0     6    14
8       14     0     6     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ovo_MA0126.1
XX
ID  ovo_MA0126.1
XX
DE  MA0126.1 ovo; from JASPAR
P0       a     c     g     t
1       10     4     0     7
2        1     4    14     2
3        4     2     2    13
4       20     0     0     1
5       21     0     0     0
6        0    21     0     0
7        7     5     3     6
8        0     0    21     0
9        5     4     2    10
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Abd-B_MA0165.1
XX
ID  Abd-B_MA0165.1
XX
DE  MA0165.1 Abd-B; from JASPAR
P0       a     c     g     t
1        1     0     0    20
2        0     0     0    21
3        5     0     0    16
4       21     0     0     0
5        0     3     0    18
6        3     0    11     7
7       13     0     5     3
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Antp_MA0166.1
XX
ID  Antp_MA0166.1
XX
DE  MA0166.1 Antp; from JASPAR
P0       a     c     g     t
1        1     1     0    14
2        0     0     0    16
3       16     0     0     0
4       16     0     0     0
5        0     0     0    16
6        0     0     9     7
7       15     0     1     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Awh_MA0167.1
XX
ID  Awh_MA0167.1
XX
DE  MA0167.1 Awh; from JASPAR
P0       a     c     g     t
1        3    16     0    21
2        1     0     0    39
3       33     0     7     0
4       40     0     0     0
5        0     0     1    39
6        0     0     8    32
7       34     0     4     2
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  B-H1_MA0168.1
XX
ID  B-H1_MA0168.1
XX
DE  MA0168.1 B-H1; from JASPAR
P0       a     c     g     t
1        4     4     0    13
2        0     0     0    21
3       21     0     0     0
4       21     0     0     0
5        8     0     0    13
6        1     7     0    13
7        1     0    20     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  B-H2_MA0169.1
XX
ID  B-H2_MA0169.1
XX
DE  MA0169.1 B-H2; from JASPAR
P0       a     c     g     t
1        6     2     1    12
2        0     0     0    21
3       21     0     0     0
4       21     0     0     0
5        5     0     0    16
6        3     1     2    15
7        0     0    21     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  C15_MA0170.1
XX
ID  C15_MA0170.1
XX
DE  MA0170.1 C15; from JASPAR
P0       a     c     g     t
1        1     1     0    17
2        0     0     0    19
3       17     0     0     2
4       19     0     0     0
5        6     2     0    11
6        0     3     6    10
7       10     0     9     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CG11085_MA0171.1
XX
ID  CG11085_MA0171.1
XX
DE  MA0171.1 CG11085; from JASPAR
P0       a     c     g     t
1        0     3     0    10
2        0     0     0    13
3       13     0     0     0
4       13     0     0     0
5        1     0     0    12
6        2     1     2     8
7        1     0    12     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CG11294_MA0172.1
XX
ID  CG11294_MA0172.1
XX
DE  MA0172.1 CG11294; from JASPAR
P0       a     c     g     t
1        0     3     0    12
2        0     2     0    13
3       15     0     0     0
4       15     0     0     0
5        0     0     0    15
6        0     0     0    15
7       15     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CG11617_MA0173.1
XX
ID  CG11617_MA0173.1
XX
DE  MA0173.1 CG11617; from JASPAR
P0       a     c     g     t
1        1     0     0    16
2        0     0     0    17
3       10     0     3     4
4       17     0     0     0
5        0    17     0     0
6       17     0     0     0
7        0     0     0    17
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Dbx_MA0174.1
XX
ID  Dbx_MA0174.1
XX
DE  MA0174.1 Dbx; from JASPAR
P0       a     c     g     t
1        2     0     0    14
2        2     0     0    14
3        6     0     2     8
4       16     0     0     0
5        0     0     0    16
6        0     2     6     8
7       13     0     3     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  lms_MA0175.1
XX
ID  lms_MA0175.1
XX
DE  MA0175.1 lms; from JASPAR
P0       a     c     g     t
1        2     7     1    11
2        0     0     0    21
3       21     0     0     0
4       21     0     0     0
5        0     0     0    21
6        3     0     4    14
7        8     0    13     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CG15696-RA_MA0176.1
XX
ID  CG15696-RA_MA0176.1
XX
DE  MA0176.1 CG15696-RA; from JASPAR
P0       a     c     g     t
1        2     3     3    24
2        2     3     0    27
3       21     0     6     5
4       32     0     0     0
5        0     0     0    32
6        0     0     2    30
7       16     0    16     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CG18599_MA0177.1
XX
ID  CG18599_MA0177.1
XX
DE  MA0177.1 CG18599; from JASPAR
P0       a     c     g     t
1        4    10     1    10
2        0     0     0    25
3       25     0     0     0
4       25     0     0     0
5        0     0     0    25
6        0     1     8    16
7       23     0     2     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CG32105_MA0178.1
XX
ID  CG32105_MA0178.1
XX
DE  MA0178.1 CG32105; from JASPAR
P0       a     c     g     t
1        2     4     0    13
2        2     0     0    17
3       17     0     0     2
4       19     0     0     0
5        0     0     1    18
6        0     0     1    18
7       15     0     4     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CG32532_MA0179.1
XX
ID  CG32532_MA0179.1
XX
DE  MA0179.1 CG32532; from JASPAR
P0       a     c     g     t
1        2     6     2    13
2        0     0     0    23
3       23     0     0     0
4       23     0     0     0
5        0     0     0    23
6        0     0     2    21
7       12     0    10     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Vsx2_MA0180.1
XX
ID  Vsx2_MA0180.1
XX
DE  MA0180.1 Vsx2; from JASPAR
P0       a     c     g     t
1        1     1     5     6
2        2     2     0     9
3        0     0     0    13
4       13     0     0     0
5       13     0     0     0
6        0     0     0    13
7        0     0     0    13
8       10     0     3     0
9        1     2    10     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Vsx1_MA0181.1
XX
ID  Vsx1_MA0181.1
XX
DE  MA0181.1 Vsx1; from JASPAR
P0       a     c     g     t
1        1     8     1    12
2        0     0     0    22
3       22     0     0     0
4       22     0     0     0
5        0     0     0    22
6        2     0     3    17
7       15     0     6     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CG4328-RA_MA0182.1
XX
ID  CG4328-RA_MA0182.1
XX
DE  MA0182.1 CG4328-RA; from JASPAR
P0       a     c     g     t
1       12     2     3    13
2        7     1     0    22
3       13     0     0    17
4       30     0     0     0
5        0     0     0    30
6        0     0     6    24
7       15     0    15     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HHEX_MA0183.1
XX
ID  HHEX_MA0183.1
XX
DE  MA0183.1 HHEX; from JASPAR
P0       a     c     g     t
1        7     3     2    14
2        0     7     0    19
3        0     6     7    13
4       21     0     2     3
5       26     0     0     0
6        0     3     3    20
7        7     0     1    18
8       22     0     4     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CG9876_MA0184.1
XX
ID  CG9876_MA0184.1
XX
DE  MA0184.1 CG9876; from JASPAR
P0       a     c     g     t
1        2     9     3     6
2        0     0     0    20
3       18     0     0     2
4       20     0     0     0
5        0     0     0    20
6        0     0     1    19
7       14     0     6     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Deaf1_MA0185.1
XX
ID  Deaf1_MA0185.1
XX
DE  MA0185.1 Deaf1; from JASPAR
P0       a     c     g     t
1        0     3     0     7
2        0     0     0    10
3        0    10     0     0
4        0     0    10     0
5        0     0     5     5
6        1     3     4     2
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Dfd_MA0186.1
XX
ID  Dfd_MA0186.1
XX
DE  MA0186.1 Dfd; from JASPAR
P0       a     c     g     t
1        1     4     3    16
2        0     0     0    24
3       24     0     0     0
4       24     0     0     0
5        0     0     0    24
6        1     0    19     4
7       21     0     3     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Dll_MA0187.1
XX
ID  Dll_MA0187.1
XX
DE  MA0187.1 Dll; from JASPAR
P0       a     c     g     t
1        1     0     0    22
2       22     0     1     0
3       23     0     0     0
4        0     0     0    23
5        3     0     3    17
6       10     0     9     4
7        1    12     4     6
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Dr_MA0188.1
XX
ID  Dr_MA0188.1
XX
DE  MA0188.1 Dr; from JASPAR
P0       a     c     g     t
1        1    13     6     1
2        0    16     0     5
3       21     0     0     0
4       21     0     0     0
5        0     0     0    21
6        0     0     0    21
7       21     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  E5_MA0189.1
XX
ID  E5_MA0189.1
XX
DE  MA0189.1 E5; from JASPAR
P0       a     c     g     t
1        5    15     6    17
2        3     0     0    40
3       43     0     0     0
4       42     0     0     1
5        1     0     1    41
6        5     0    16    22
7       34     0     9     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Gsc_MA0190.1
XX
ID  Gsc_MA0190.1
XX
DE  MA0190.1 Gsc; from JASPAR
P0       a     c     g     t
1        0     0     0    22
2       22     0     0     0
3       22     0     0     0
4        0     0     0    22
5        0    22     0     0
6        0    14     2     6
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HGTX_MA0191.1
XX
ID  HGTX_MA0191.1
XX
DE  MA0191.1 HGTX; from JASPAR
P0       a     c     g     t
1        4     2     3    11
2        0     0     0    20
3       20     0     0     0
4       20     0     0     0
5        0     0     0    20
6        1     0     6    13
7       15     0     5     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Hmx_MA0192.1
XX
ID  Hmx_MA0192.1
XX
DE  MA0192.1 Hmx; from JASPAR
P0       a     c     g     t
1        1     3     0    16
2        0     0     0    20
3       20     0     0     0
4       20     0     0     0
5        1     0     0    19
6        0     3     0    17
7        4     0    16     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  schlank_MA0193.1
XX
ID  schlank_MA0193.1
XX
DE  MA0193.1 schlank; from JASPAR
P0       a     c     g     t
1        0    19     0     0
2        0     9     0    10
3       17     2     0     0
4        1    18     0     0
5        0    12     0     7
6       18     0     1     0
7       12     0     4     3
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Lim1_MA0194.1
XX
ID  Lim1_MA0194.1
XX
DE  MA0194.1 Lim1; from JASPAR
P0       a     c     g     t
1        0     2     0    16
2        0     0     0    18
3       18     0     0     0
4       18     0     0     0
5        0     0     0    18
6        0     0     0    18
7       17     0     1     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Lim3_MA0195.1
XX
ID  Lim3_MA0195.1
XX
DE  MA0195.1 Lim3; from JASPAR
P0       a     c     g     t
1        3     7     2     8
2        4     0     0    16
3       16     0     3     1
4       20     0     0     0
5        0     0     0    20
6        0     2     4    14
7       16     0     4     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NK7.1_MA0196.1
XX
ID  NK7.1_MA0196.1
XX
DE  MA0196.1 NK7.1; from JASPAR
P0       a     c     g     t
1        5     5     1    24
2        0     0     0    35
3       33     0     2     0
4       35     0     0     0
5        4     0     5    26
6        6     0     7    22
7       13     0    22     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  nub_MA0197.2
XX
ID  nub_MA0197.2
XX
DE  MA0197.2 nub; from JASPAR
P0       a     c     g     t
1        2     1     1    25
2       29     0     0     0
3        0     0     0    29
4        0     0    25     4
5        0    19     1     9
6       24     0     0     5
7       28     0     1     0
8       28     0     0     1
9        4     1     0    24
10       6     5     8    10
11      19     5     1     4
12       4     3    15     7
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  OdsH_MA0198.1
XX
ID  OdsH_MA0198.1
XX
DE  MA0198.1 OdsH; from JASPAR
P0       a     c     g     t
1        0    11     4     7
2        0     0     0    22
3       22     0     0     0
4       22     0     0     0
5        0     0     0    22
6        0     0     0    22
7       13     0     8     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Optix_MA0199.1
XX
ID  Optix_MA0199.1
XX
DE  MA0199.1 Optix; from JASPAR
P0       a     c     g     t
1        0     5     0    22
2        0     0    27     0
3       27     0     0     0
4        0     0     0    27
5       26     0     1     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Pph13_MA0200.1
XX
ID  Pph13_MA0200.1
XX
DE  MA0200.1 Pph13; from JASPAR
P0       a     c     g     t
1        6     8     3     4
2        0     0     0    21
3       21     0     0     0
4       21     0     0     0
5        0     0     0    21
6        0     1     0    20
7       13     1     6     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Ptx1_MA0201.1
XX
ID  Ptx1_MA0201.1
XX
DE  MA0201.1 Ptx1; from JASPAR
P0       a     c     g     t
1        0     5     1    14
2        0     0     0    20
3       20     0     0     0
4       20     0     0     0
5        0     0     0    20
6        0    20     0     0
7        0    19     0     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Rx_MA0202.1
XX
ID  Rx_MA0202.1
XX
DE  MA0202.1 Rx; from JASPAR
P0       a     c     g     t
1        3    13     0    11
2        0     0     0    27
3       27     0     0     0
4       27     0     0     0
5        0     0     0    27
6        0     0     0    27
7       17     0    10     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Scr_MA0203.1
XX
ID  Scr_MA0203.1
XX
DE  MA0203.1 Scr; from JASPAR
P0       a     c     g     t
1        3     7     0    15
2        0     0     0    25
3       25     0     0     0
4       25     0     0     0
5        0     0     0    25
6        0     0    18     7
7       23     0     2     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Six4_MA0204.1
XX
ID  Six4_MA0204.1
XX
DE  MA0204.1 Six4; from JASPAR
P0       a     c     g     t
1        0     0     0    20
2        0     0    20     0
3       20     0     0     0
4        0     7     4     9
5       20     0     0     0
6        1    19     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Trl_MA0205.2
XX
ID  Trl_MA0205.2
XX
DE  MA0205.2 Trl; from JASPAR
P0       a     c     g     t
1     2210  1330  2687   631
2     2618  1348  1972   920
3     4107   597  1902   252
4     6613    47    44   154
5       91    54  6535   178
6     6743    21    44    50
7       10    38  6783    27
8     6603   182    45    28
9      141    29  6638    50
10    5105   845   495   413
11    1888   621  3800   549
12    2984   870  2179   825
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  abd-A_MA0206.1
XX
ID  abd-A_MA0206.1
XX
DE  MA0206.1 abd-A; from JASPAR
P0       a     c     g     t
1        1     3     0    14
2        0     0     0    18
3       16     0     0     2
4       18     0     0     0
5        1     0     0    17
6        0     0     6    12
7       15     1     2     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  achi_MA0207.1
XX
ID  achi_MA0207.1
XX
DE  MA0207.1 achi; from JASPAR
P0       a     c     g     t
1        0     0     0    23
2        0     0    23     0
3       23     0     0     0
4        0    22     0     1
5       23     0     0     0
6        3     0    12     2
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  al_MA0208.1
XX
ID  al_MA0208.1
XX
DE  MA0208.1 al; from JASPAR
P0       a     c     g     t
1        0     0     0    20
2       20     0     0     0
3       20     0     0     0
4        0     0     0    20
5        0     0     0    20
6       17     0     3     0
7       18     0     2     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ap_MA0209.1
XX
ID  ap_MA0209.1
XX
DE  MA0209.1 ap; from JASPAR
P0       a     c     g     t
1        2    10     0     7
2        1     0     0    18
3       19     0     0     0
4       19     0     0     0
5        0     0     0    19
6        0     0     6    13
7       16     0     3     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ara_MA0210.1
XX
ID  ara_MA0210.1
XX
DE  MA0210.1 ara; from JASPAR
P0       a     c     g     t
1       14     0     5    15
2       23     0     0    11
3       34     0     0     0
4        0    34     0     0
5       34     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  bap_MA0211.1
XX
ID  bap_MA0211.1
XX
DE  MA0211.1 bap; from JASPAR
P0       a     c     g     t
1        0     0     1    22
2        0     0     0    23
3       23     0     0     0
4       23     0     0     0
5        0     0    23     0
6        1     0     0    22
7        7     0    16     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  bcd_MA0212.1
XX
ID  bcd_MA0212.1
XX
DE  MA0212.1 bcd; from JASPAR
P0       a     c     g     t
1        0     0     0    22
2       20     0     0     2
3       22     0     0     0
4        0     0     1    21
5        0    22     0     0
6        0    21     0     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  brk_MA0213.1
XX
ID  brk_MA0213.1
XX
DE  MA0213.1 brk; from JASPAR
P0       a     c     g     t
1        1     5     4     0
2        0     4     0     6
3        0     0    10     0
4        0     0    10     0
5        0    10     0     0
6        1     0     9     0
7        1     8     0     1
8        0     5     1     4
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  bsh_MA0214.1
XX
ID  bsh_MA0214.1
XX
DE  MA0214.1 bsh; from JASPAR
P0       a     c     g     t
1        1     3     2    10
2        0     0     0    16
3       16     0     0     0
4       16     0     0     0
5        0     1     0    15
6        0     3     6     7
7        7     0     9     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  btn_MA0215.1
XX
ID  btn_MA0215.1
XX
DE  MA0215.1 btn; from JASPAR
P0       a     c     g     t
1        5     1     4    13
2        0     0     0    23
3       22     0     1     0
4       23     0     0     0
5        0     1     0    22
6        0     0    15     8
7       19     0     3     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  cad_MA0216.2
XX
ID  cad_MA0216.2
XX
DE  MA0216.2 cad; from JASPAR
P0       a     c     g     t
1      854     0  1143   306
2      575   341  1387     0
3        0  1481     0   822
4      745  1475     0    83
5     2117     0   186     0
6        0    95     0  2208
7     2236     0     0    67
8     2303     0     0     0
9     2303     0     0     0
10    1637   115   147   404
11    1046   755     0   502
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  caup_MA0217.1
XX
ID  caup_MA0217.1
XX
DE  MA0217.1 caup; from JASPAR
P0       a     c     g     t
1        4     1     2    12
2       14     0     2     3
3       19     0     0     0
4        0    19     0     0
5       19     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ct_MA0218.1
XX
ID  ct_MA0218.1
XX
DE  MA0218.1 ct; from JASPAR
P0       a     c     g     t
1        0     6     0    14
2        1     2     0    17
3        9     0    11     0
4       20     0     0     0
5       17     0     1     2
6        0    19     1     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ems_MA0219.1
XX
ID  ems_MA0219.1
XX
DE  MA0219.1 ems; from JASPAR
P0       a     c     g     t
1        3     7     1     9
2        3     0     0    17
3       19     0     0     1
4       19     0     1     0
5        0     0     0    20
6        0     1    10     9
7       14     0     6     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  en_MA0220.1
XX
ID  en_MA0220.1
XX
DE  MA0220.1 en; from JASPAR
P0       a     c     g     t
1        2     8     2    11
2        0     0     0    23
3       23     0     0     0
4       23     0     0     0
5        0     0     0    23
6        0     0     2    21
7       13     0    10     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  eve_MA0221.1
XX
ID  eve_MA0221.1
XX
DE  MA0221.1 eve; from JASPAR
P0       a     c     g     t
1        3    10     0     9
2        1     0     0    21
3       22     0     0     0
4       22     0     0     0
5        0     2     1    19
6        0     2    11     9
7       17     0     4     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  exd_MA0222.1
XX
ID  exd_MA0222.1
XX
DE  MA0222.1 exd; from JASPAR
P0       a     c     g     t
1        4     4     4     5
2        1     0     2    14
3        0     0     0    17
4        0     0     0    17
5        0     0    17     0
6       17     0     0     0
7        0    12     0     5
8       11     0     6     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  exex_MA0224.1
XX
ID  exex_MA0224.1
XX
DE  MA0224.1 exex; from JASPAR
P0       a     c     g     t
1        0     7    10     6
2        0     0     0    23
3       23     0     0     0
4       23     0     0     0
5        0     0     0    23
6        1     1     0    21
7       20     0     3     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ftz_MA0225.1
XX
ID  ftz_MA0225.1
XX
DE  MA0225.1 ftz; from JASPAR
P0       a     c     g     t
1        1     2     0    15
2        1     0     0    17
3       18     0     0     0
4       18     0     0     0
5        0     0     0    18
6        0     0     9     9
7       14     0     4     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  hbn_MA0226.1
XX
ID  hbn_MA0226.1
XX
DE  MA0226.1 hbn; from JASPAR
P0       a     c     g     t
1        2     2     2    11
2        0     0     0    17
3       17     0     0     0
4       17     0     0     0
5        0     0     0    17
6        0     0     0    17
7        9     0     7     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  hth_MA0227.1
XX
ID  hth_MA0227.1
XX
DE  MA0227.1 hth; from JASPAR
P0       a     c     g     t
1        0     0     0    17
2        0     0    17     0
3       17     0     0     0
4        0    17     0     0
5       14     0     3     0
6        0     0     8     4
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ind_MA0228.1
XX
ID  ind_MA0228.1
XX
DE  MA0228.1 ind; from JASPAR
P0       a     c     g     t
1        0    12     0     9
2        0     0     0    21
3       21     0     0     0
4       21     0     0     0
5        0     0     1    20
6        0     0     8    13
7       19     0     2     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  inv_MA0229.1
XX
ID  inv_MA0229.1
XX
DE  MA0229.1 inv; from JASPAR
P0       a     c     g     t
1        4     0     3     9
2        1     9     0     6
3        0     0     0    16
4       16     0     0     0
5       16     0     0     0
6        0     0     0    16
7        0     0     0    16
8       11     0     5     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  lab_MA0230.1
XX
ID  lab_MA0230.1
XX
DE  MA0230.1 lab; from JASPAR
P0       a     c     g     t
1        1     3     0    12
2        0     0     0    16
3       16     0     0     0
4       16     0     0     0
5        1     0     0    15
6        0     0     6    10
7       16     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  lbe_MA0231.1
XX
ID  lbe_MA0231.1
XX
DE  MA0231.1 lbe; from JASPAR
P0       a     c     g     t
1        0     0     0    22
2       22     0     0     0
3       22     0     0     0
4        1    10     2     9
5        4     5     3    10
6       19     0     3     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  lbl_MA0232.1
XX
ID  lbl_MA0232.1
XX
DE  MA0232.1 lbl; from JASPAR
P0       a     c     g     t
1        0     0     0    23
2       23     0     0     0
3       23     0     0     0
4        0     5     0    18
5        1     3     7    12
6       17     0     6     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  mirr_MA0233.1
XX
ID  mirr_MA0233.1
XX
DE  MA0233.1 mirr; from JASPAR
P0       a     c     g     t
1       20     1     1    19
2       29     0     2    10
3       41     0     0     0
4        0    41     0     0
5       41     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  oc_MA0234.1
XX
ID  oc_MA0234.1
XX
DE  MA0234.1 oc; from JASPAR
P0       a     c     g     t
1        0     0     0    19
2       19     0     0     0
3       19     0     0     0
4        0     0     2    17
5        0    19     0     0
6        0    17     1     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  onecut_MA0235.1
XX
ID  onecut_MA0235.1
XX
DE  MA0235.1 onecut; from JASPAR
P0       a     c     g     t
1        1     2     0    12
2        0     0     0    15
3        0     0    15     0
4       15     0     0     0
5        0     0     0    15
6        0     0     0    15
7        5     0     4     6
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  otp_MA0236.1
XX
ID  otp_MA0236.1
XX
DE  MA0236.1 otp; from JASPAR
P0       a     c     g     t
1        1     6     2    11
2        0     1     0    19
3       20     0     0     0
4       20     0     0     0
5        0     0     0    20
6        0     1     4    15
7       15     0     3     2
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  pan_MA0237.2
XX
ID  pan_MA0237.2
XX
DE  MA0237.2 pan; from JASPAR
P0       a     c     g     t
1        0     0    34    37
2        0    71     0     0
3        0     0    71     0
4        0    20    40    11
5        7    43     3    18
6       11    10    19    31
7        7    40     0    24
8        0    30    15    26
9        0     0     0    71
10       0     0     0    71
11       0     0    11    60
12      11     9    41    10
13      34     0    21    16
14      22     0     0    49
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  pb_MA0238.1
XX
ID  pb_MA0238.1
XX
DE  MA0238.1 pb; from JASPAR
P0       a     c     g     t
1        4     9     0    11
2        0     0     0    24
3       24     0     0     0
4       24     0     0     0
5        0     0     0    24
6        0     0    11    13
7       24     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  prd_MA0239.1
XX
ID  prd_MA0239.1
XX
DE  MA0239.1 prd; from JASPAR
P0       a     c     g     t
1       10     4     0     7
2        1     4    14     2
3        4     2     2    13
4       20     0     0     1
5       21     0     0     0
6        0    21     0     0
7        7     5     3     6
8        0     0    21     0
9        5     4     2    10
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  repo_MA0240.1
XX
ID  repo_MA0240.1
XX
DE  MA0240.1 repo; from JASPAR
P0       a     c     g     t
1        1     4     4    19
2        1     0     0    27
3       27     0     0     1
4       28     0     0     0
5        0     0     0    28
6        0     0     0    28
7       21     0     7     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ro_MA0241.1
XX
ID  ro_MA0241.1
XX
DE  MA0241.1 ro; from JASPAR
P0       a     c     g     t
1        1    12     3     7
2        0     0     0    23
3       23     0     0     0
4       23     0     0     0
5        0     0     0    23
6        0     0     3    20
7       19     0     4     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Bgb::run_MA0242.1
XX
ID  Bgb::run_MA0242.1
XX
DE  MA0242.1 Bgb::run; from JASPAR
P0       a     c     g     t
1       12     2     0    15
2       27     0     1     1
3       28     0     1     0
4        0    29     0     0
5        0    28     1     0
6        5     1    23     0
7        0    29     0     0
8       28     0     0     1
9       20     0     9     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  sd_MA0243.1
XX
ID  sd_MA0243.1
XX
DE  MA0243.1 sd; from JASPAR
P0       a     c     g     t
1        3     0     8     3
2        8     1     1     4
3        4     8     1     1
4       14     0     0     0
5        1     1     0    12
6        3     0     1    10
7        0     8     0     6
8        3     5     3     3
9        4     1     2     7
10       5     6     3     0
11       5     1     7     1
12       4     3     4     3
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  slbo_MA0244.1
XX
ID  slbo_MA0244.1
XX
DE  MA0244.1 slbo; from JASPAR
P0       a     c     g     t
1        9     1     1     1
2        0     1     0    11
3        0     0     3     9
4        3     2     7     0
5        2    10     0     0
6        7     2     2     1
7        6     5     1     0
8       11     1     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  slou_MA0245.1
XX
ID  slou_MA0245.1
XX
DE  MA0245.1 slou; from JASPAR
P0       a     c     g     t
1        3     5     1    13
2        0     1     0    21
3       22     0     0     0
4       21     0     1     0
5        0     0     0    22
6        2     1     5    14
7       11     0    11     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  so_MA0246.1
XX
ID  so_MA0246.1
XX
DE  MA0246.1 so; from JASPAR
P0       a     c     g     t
1        0     0     1    26
2        0     0    27     0
3       27     0     0     0
4        0     0     1    26
5       27     0     0     0
6        0    10     0     3
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  tin_MA0247.2
XX
ID  tin_MA0247.2
XX
DE  MA0247.2 tin; from JASPAR
P0       a     c     g     t
1      136    54    80   245
2        0   188   137   190
3        0    51     0   464
4        0   425    34    56
5      326     0   189     0
6      515     0     0     0
7        0     0   515     0
8        0     0     0   515
9        0     0   515     0
10      13   157   301    44
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  tup_MA0248.1
XX
ID  tup_MA0248.1
XX
DE  MA0248.1 tup; from JASPAR
P0       a     c     g     t
1        4     2     1     9
2        1     0     0    15
3       16     0     0     0
4       16     0     0     0
5        0     0     2    14
6        1     0     7     8
7        2     0    13     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  twi_MA0249.2
XX
ID  twi_MA0249.2
XX
DE  MA0249.2 twi; from JASPAR
P0       a     c     g     t
1     1056   519   801   520
2      694   805   534   863
3     2583   166    69    78
4       15  2804    24    53
5     2844    26    11    15
6       16  2025   154   701
7     2723    57    83    33
8       31    69    46  2750
9      137    54  2671    34
10     305   519   604  1468
11     927   588   718   663
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  unc-4_MA0250.1
XX
ID  unc-4_MA0250.1
XX
DE  MA0250.1 unc-4; from JASPAR
P0       a     c     g     t
1        1     5     2    13
2        0     0     0    21
3       21     0     0     0
4       21     0     0     0
5        0     0     0    21
6        1     0     0    20
7        6     1    14     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  unpg_MA0251.1
XX
ID  unpg_MA0251.1
XX
DE  MA0251.1 unpg; from JASPAR
P0       a     c     g     t
1        1     6     4    10
2        1     0     0    20
3       21     0     0     0
4       21     0     0     0
5        0     0     0    21
6        0     0     2    19
7       14     0     7     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  vis_MA0252.1
XX
ID  vis_MA0252.1
XX
DE  MA0252.1 vis; from JASPAR
P0       a     c     g     t
1        0     0     0    22
2        0     0    22     0
3       22     0     0     0
4        0    22     0     0
5       22     0     0     0
6        2     4     8     2
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  vnd_MA0253.1
XX
ID  vnd_MA0253.1
XX
DE  MA0253.1 vnd; from JASPAR
P0       a     c     g     t
1        4     0     2    13
2        1     8     1     9
3        0     0     0    19
4        0    15     2     2
5       19     0     0     0
6       19     0     0     0
7        0     0    19     0
8        2     0     0    17
9        9     0    10     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  vvl_MA0254.1
XX
ID  vvl_MA0254.1
XX
DE  MA0254.1 vvl; from JASPAR
P0       a     c     g     t
1        0     2     0     9
2       11     0     0     0
3        2     2     0     7
4        0     0     6     5
5        3     8     0     0
6       11     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  z_MA0255.1
XX
ID  z_MA0255.1
XX
DE  MA0255.1 z; from JASPAR
P0       a     c     g     t
1       11     3     5    22
2        1     7     3    30
3        1     0    40     0
4       41     0     0     0
5        1     0    40     0
6        8    10     0    23
7        2     6    29     4
8       18     7    11     5
9       10     5    12    14
10      14     6     2    19
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  zen_MA0256.1
XX
ID  zen_MA0256.1
XX
DE  MA0256.1 zen; from JASPAR
P0       a     c     g     t
1        2     9     0     5
2        0     0     0    16
3       16     0     0     0
4       16     0     0     0
5        0     0     0    16
6        0     0    11     5
7       16     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  zen2_MA0257.1
XX
ID  zen2_MA0257.1
XX
DE  MA0257.1 zen2; from JASPAR
P0       a     c     g     t
1        3     7     5    11
2        1     0     0    25
3       26     0     0     0
4       26     0     0     0
5        0     1     0    25
6        0     0    10    16
7       22     0     4     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  btd_MA0443.1
XX
ID  btd_MA0443.1
XX
DE  MA0443.1 btd; from JASPAR
P0       a     c     g     t
1       14     3     7     6
2       10     0    20     0
3        4     0    18     8
4        2     0    28     0
5        0     0    30     0
6        0     0    30     0
7        2    28     0     0
8        0     0    30     0
9        0     0    20    10
10      15     1    10     4
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CG34031_MA0444.1
XX
ID  CG34031_MA0444.1
XX
DE  MA0444.1 CG34031; from JASPAR
P0       a     c     g     t
1        0     1     1    23
2        0     0     0    25
3       22     0     0     3
4       25     0     0     0
5        2     0     0    23
6        7     0     1    17
7        3     0    22     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  D_MA0445.1
XX
ID  D_MA0445.1
XX
DE  MA0445.1 D; from JASPAR
P0       a     c     g     t
1        1     8     7    13
2        0    25     0     4
3        0    17     0    12
4       20     0     0     9
5        0     0     0    29
6        0     0     3    26
7        2     0    27     0
8        0     0     0    29
9        1     2     4    22
10       4    10     6     9
11       6     1     1    21
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  fkh_MA0446.1
XX
ID  fkh_MA0446.1
XX
DE  MA0446.1 fkh; from JASPAR
P0       a     c     g     t
1        3     0     0    24
2        5     0    22     0
3        0     0     0    27
4        0     0     0    27
5        0     0     1    26
6       13     0    14     0
7        4    13     2     8
8        6     7     3    11
9        0    11     2    14
10      23     0     1     3
11      15     4     4     4
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  gt_MA0447.1
XX
ID  gt_MA0447.1
XX
DE  MA0447.1 gt; from JASPAR
P0       a     c     g     t
1       28     5    25     2
2        0     0     0    60
3        1     1     3    55
4       54     0     6     0
5        0    53     0     7
6        7     0    53     0
7        0     6     0    54
8       55     3     1     1
9       60     0     0     0
10       2    25     5    28
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  H2.0_MA0448.1
XX
ID  H2.0_MA0448.1
XX
DE  MA0448.1 H2.0; from JASPAR
P0       a     c     g     t
1        6     3     4    19
2        2     1     0    29
3       16     1     1    14
4       31     0     0     1
5        0     0     0    32
6        8     0     9    15
7       24     0     7     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  h_MA0449.1
XX
ID  h_MA0449.1
XX
DE  MA0449.1 h; from JASPAR
P0       a     c     g     t
1        1     2    30     1
2        3     7    17     7
3        1    31     1     1
4       22     1    10     1
5        1    30     1     2
6        2     1    30     1
7        1    10     1    22
8        1     1    31     1
9        7    17     7     3
10       1    30     2     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  hkb_MA0450.1
XX
ID  hkb_MA0450.1
XX
DE  MA0450.1 hkb; from JASPAR
P0       a     c     g     t
1        3     0    18    11
2        8     0    24     0
3        0     0    32     0
4        0     0    32     0
5        0    32     0     0
6        0     0    32     0
7        0     0     3    29
8        0     0    31     1
9       24     3     2     3
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  kni_MA0451.1
XX
ID  kni_MA0451.1
XX
DE  MA0451.1 kni; from JASPAR
P0       a     c     g     t
1       19     1     2     4
2       25     1     0     0
3       16     0     0    10
4        5     9     6     6
5        0     4     1    21
6       21     0     5     0
7        0     0    26     0
8       17     0     8     1
9        1     3    18     4
10       0    26     0     0
11      25     0     1     0
12       5    12     7     2
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Kr_MA0452.2
XX
ID  Kr_MA0452.2
XX
DE  MA0452.2 Kr; from JASPAR
P0       a     c     g     t
1      179   422   238   457
2      205   189   218   684
3      151   386    73   686
4     1248     0     0    48
5     1080   216     0     0
6       11  1170     0   115
7        0  1177     0   119
8        0  1296     0     0
9        0   202     0  1094
10       0     0     0  1296
11       0   113     0  1183
12     113   399   231   553
13     318   322   285   371
14     211   434   219   432
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  odd_MA0454.1
XX
ID  odd_MA0454.1
XX
DE  MA0454.1 odd; from JASPAR
P0       a     c     g     t
1        7     5     0     3
2       10     5     0     0
3        0    14     1     0
4       12     0     3     0
5        0     0    15     0
6        0     0     0    15
7       15     0     0     0
8        0     0    15     0
9        0    15     0     0
10      11     3     1     0
11       5     4     6     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  opa_MA0456.1
XX
ID  opa_MA0456.1
XX
DE  MA0456.1 opa; from JASPAR
P0       a     c     g     t
1        0     1    15     2
2       10     7     1     0
3        1    16     0     1
4        1    15     1     1
5        0    16     1     1
6        0    18     0     0
7        0    18     0     0
8        0    14     0     4
9        4     0    13     1
10       0    14     2     2
11       3     1     4    10
12       2     0    16     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHDP_MA0457.1
XX
ID  PHDP_MA0457.1
XX
DE  MA0457.1 PHDP; from JASPAR
P0       a     c     g     t
1        4     6     0     7
2        0     0     0    17
3       16     0     0     1
4       17     0     0     0
5        0     0     0    17
6        1     0     1    15
7        8     1     4     4
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  slp1_MA0458.1
XX
ID  slp1_MA0458.1
XX
DE  MA0458.1 slp1; from JASPAR
P0       a     c     g     t
1        8     3    14    16
2        0     0     0    41
3        3     0    38     0
4        0     0     0    41
5        0     0     1    40
6        0     0     2    39
7       27     0     4    10
8        1    22     7    11
9       17     8    15     1
10       4    13     3    21
11      15     3     4    19
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  tll_MA0459.1
XX
ID  tll_MA0459.1
XX
DE  MA0459.1 tll; from JASPAR
P0       a     c     g     t
1       20     4     4     5
2       29     0     4     0
3       33     0     0     0
4       31     1     1     0
5        0     0    33     0
6        0     2     0    31
7        0    33     0     0
8       33     0     0     0
9       31     0     1     1
10      17    10     2     4
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ttk_MA0460.1
XX
ID  ttk_MA0460.1
XX
DE  MA0460.1 ttk; from JASPAR
P0       a     c     g     t
1       10     8     3     1
2       22     0     0     0
3        0     0    22     0
4        0     0    22     0
5       22     0     0     0
6        0     5     0    17
7       21     0     0     1
8       22     0     0     0
9        1     5     3    13
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BEAF-32_MA0529.2
XX
ID  BEAF-32_MA0529.2
XX
DE  MA0529.2 BEAF-32; from JASPAR
P0       a     c     g     t
1     1772  1048  1556  1126
2     1612  1825   711  1354
3      539   460    97  4406
4     5151    51    96   204
5       98   112    79  5213
6       26  5423    13    40
7       40    13  5423    26
8     5213    79   112    98
9      204    96    51  5151
10    4406    97   460   539
11    1354   711  1825  1612
12    1126  1556  1048  1772
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  cnc::maf-S_MA0530.1
XX
ID  cnc::maf-S_MA0530.1
XX
DE  MA0530.1 cnc::maf-S; from JASPAR
P0       a     c     g     t
1      142   113   159    60
2      340    20   114     0
3        0     0     0   474
4        0     0   474     0
5      407    46    21     0
6        0   292    80   102
7       80     0   169   225
8       78   215    90    91
9      136    33   163   142
10      24     0   422    28
11       0   474     0     0
12     368     0   106     0
13     133   128    99   114
14     221    63     0   190
15     214    12     0   248
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CTCF_MA0531.1
XX
ID  CTCF_MA0531.1
XX
DE  MA0531.1 CTCF; from JASPAR
P0       a     c     g     t
1      306   876   403   317
2      313  1147   219   223
3      457   383   826   236
4      676   784   350    92
5      257   714    87   844
6     1534     1   192   175
7      202     0  1700     0
8      987     0   912     3
9        2     4   311  1585
10       0     0  1902     0
11       2     0  1652   248
12     124  1645     3   130
13       1     0  1807    94
14      79  1514     8   301
15     231   773   144   754
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Stat92E_MA0532.1
XX
ID  Stat92E_MA0532.1
XX
DE  MA0532.1 Stat92E; from JASPAR
P0       a     c     g     t
1       15    50    24    29
2       24    22    59    13
3       27    35    41    15
4       91     1    17     9
5       47     0    27    44
6        0     1     1   116
7        0     6     0   112
8        0   106     0    12
9        0    67    14    37
10      38    21    30    29
11      41     2    70     5
12       0     1   117     0
13     113     0     2     3
14     118     0     0     0
15      63     9    10    36
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  EcR::usp_MA0534.1
XX
ID  EcR::usp_MA0534.1
XX
DE  MA0534.1 EcR::usp; from JASPAR
P0       a     c     g     t
1       29    36    39     0
2       60    12    32     0
3       19    10    75     0
4        5     3    35    61
5        9     0     0    95
6       12    66     0    26
7       74     0    30     0
8       34    11     5    54
9        0     0     0   104
10       0     0   104     0
11      66    33     0     5
12      49    55     0     0
13      21    57     0    26
14       0    34     0    70
15      24    28     0    52
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Mad_MA0535.1
XX
ID  Mad_MA0535.1
XX
DE  MA0535.1 Mad; from JASPAR
P0       a     c     g     t
1        0    48    47     7
2       35    26    13    28
3        0    14    88     0
4       35     0    56    11
5        0   102     0     0
6        0     0   102     0
7       23    65     0    14
8        0    71    31     0
9       29     0    73     0
10      29    39    34     0
11      11    53    25    13
12      29     0    73     0
13      12    48    27    15
14      32    34    16    20
15      11    28    63     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  pnr_MA0536.1
XX
ID  pnr_MA0536.1
XX
DE  MA0536.1 pnr; from JASPAR
P0       a     c     g     t
1       82   180     5   602
2      802     0     0    67
3        0    32     1   836
4        0   869     0     0
5        0     0   869     0
6      869     0     0     0
7        0     0     0   869
8      580     0    87   202
9      123     0   530   216
10      58   278   194   339
11      91   273   110   395
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  dve_MA0915.1
XX
ID  dve_MA0915.1
XX
DE  MA0915.1 dve; from JASPAR
P0       a     c     g     t
1      276   379   138   207
2        0     0     0   999
3      999     0     0     0
4      999     0     0     0
5        0     0     0   999
6        0   999     0     0
7        0   971     0    29
8      348   217   348    87
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Ets21C_MA0916.1
XX
ID  Ets21C_MA0916.1
XX
DE  MA0916.1 Ets21C; from JASPAR
P0       a     c     g     t
1        1   996     1     1
2        1   997     1     1
3        1     1   997     1
4        1     1   997     1
5      997     1     1     1
6      897     1     1   101
7      333     1   665     1
8        1   286     1   712
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  gcm2_MA0917.1
XX
ID  gcm2_MA0917.1
XX
DE  MA0917.1 gcm2; from JASPAR
P0       a     c     g     t
1      730   111   159     0
2        0    12     0   988
3      235    24   741     0
4       47   800   141    12
5       71     0   753   177
6        0     0  1000     0
7        0     0  1000     0
8       56   352    93   500
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  dmrt99B_MA1455.1
XX
ID  dmrt99B_MA1455.1
XX
DE  MA1455.1 dmrt99B; from JASPAR
P0       a     c     g     t
1      289   150   115   445
2      224   201   142   431
3       92    38   808    60
4      341   288     9   360
5      348    85    39   526
6      979     4     8     7
7       10   955    10    23
8      856     8    10   124
9      934    26    39     0
10     137    21    11   829
11      41    12   933    12
12       3     4     2   989
13     618    35    66   279
14     356    11   260   371
15      63   825    27    82
16     479   155   200   164
17     529   117   128   224
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Dref_MA1456.1
XX
ID  Dref_MA1456.1
XX
DE  MA1456.1 Dref; from JASPAR
P0       a     c     g     t
1      814   612  1095   591
2      791  1028   357   936
3        0     0     0  3112
4     3112     0     0     0
5        0     0     0  3112
6        0  3112     0     0
7        0     0  3112     0
8     3112     0     0     0
9      367   166   122  2457
10    1825   188   459   640
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  grh_MA1457.1
XX
ID  grh_MA1457.1
XX
DE  MA1457.1 grh; from JASPAR
P0       a     c     g     t
1     1170   730   883   549
2     1915   477   535   405
3     3090    27    89   126
4     3211    20    57    44
5       29  3265    18    20
6      187  2825   199   121
7     1914   194   838   386
8       20    57  3221    34
9       49    34    23  3226
10      58    96    23  3155
11     358   569   407  1998
12     546   786   818  1182
13     754   933   783   862
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Hsf_MA1458.1
XX
ID  Hsf_MA1458.1
XX
DE  MA1458.1 Hsf; from JASPAR
P0       a     c     g     t
1      647   366   423   686
2      533   444   421   724
3       74    57    43  1948
4       43    38    49  1992
5       11  2070    22    19
6       54  1297   279   492
7     1981    51    49    41
8       33    37  2036    16
9     1996    66    29    31
10    1936    41    50    95
11     668   449   469   536
12     658   443   384   637
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  M1BP_MA1459.1
XX
ID  M1BP_MA1459.1
XX
DE  MA1459.1 M1BP; from JASPAR
P0       a     c     g     t
1      372   417   438   234
2      451   441   433   136
3      342   323   207   589
4      678   346   310   127
5       54   827    54   526
6       36     5  1412     8
7        6     3  1429    23
8        2   165    61  1233
9       69  1372     5    15
10    1382    29    17    33
11       1  1384     4    72
12    1308    15    18   120
13       1  1393     2    65
14      58   130    12  1261
15     291    72   939   159
16     380   481   299   301
17     365   392   282   422
18     368   458   305   330
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  pho_MA1460.1
XX
ID  pho_MA1460.1
XX
DE  MA1460.1 pho; from JASPAR
P0       a     c     g     t
1     2293  1738  1276  1644
2     2256  1182  2050  1463
3     6191   297   268   195
4      104   154   124  6569
5      143   318  6303   187
6      116   150  6516   169
7      299  6080   274   298
8      340  5236   923   452
9      498   179  5995   279
10    1866  2406  1235  1444
11    1325  2511  1500  1615
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  sv_MA1461.1
XX
ID  sv_MA1461.1
XX
DE  MA1461.1 sv; from JASPAR
P0       a     c     g     t
1      187   314   238   259
2        2   273   444   279
3       58   734     4   202
4      644    15   312    26
5      152   284   383   179
6        1   323     5   669
7        2   640   356     1
8      934     2    59     4
9      375    29    68   526
10       1   129   868     1
11       1   982     3    12
12     106     1   890     1
13       4     2     8   983
14     128     1   868     1
15     945     2    13    38
16      25   971     2     1
17      71   213   669    45
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  vfl_MA1462.1
XX
ID  vfl_MA1462.1
XX
DE  MA1462.1 vfl; from JASPAR
P0       a     c     g     t
1     2978  3107  2712  2934
2     2925  2757  2740  3309
3     2283  1953  5193  2302
4       52 10668   180   831
5    11435    28   163   105
6       43    58 11602    28
7       91    53 11545    42
8       93   689    37 10912
9    11460   125    94    52
10    2143  1746  6863   979
11    3270  2868  3309  2284
12    3157  3169  2364  3041
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Clamp_MA1700.1
XX
ID  Clamp_MA1700.1
XX
DE  MA1700.1 Clamp; from JASPAR
P0       a     c     g     t
1      332   250  1888   181
2     1071   455   378   747
3      189   175  2178   109
4      713  1322   354   262
5       74    29  2532    16
6     2510    32    43    66
7       62    11  2562    16
8      325  1998   174   154
9       31    12  2600     8
10    2451    28   155    17
11      71    36  2517    27
12    2038   332   118   163
13     234   502  1515   400
14    1251   327   960   113
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Pdp1_MA1702.1
XX
ID  Pdp1_MA1702.1
XX
DE  MA1702.1 Pdp1; from JASPAR
P0       a     c     g     t
1     1468   759  1141  1448
2     1848   765  1362   841
3      126    46    64  4580
4      110    49   119  4538
5     4468    35   218    95
6      109   340    74  4293
7      331    76  4221   188
8      154  3282    79  1301
9     4598    88    71    59
10    4606    60    68    82
11     914  1263   694  1945
12    1407  1183   802  1424
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  dsx_MA1836.1
XX
ID  dsx_MA1836.1
XX
DE  MA1836.1 dsx; from JASPAR
P0       a     c     g     t
1     1836  1527   802  1651
2     1566   920  1085  2245
3     5460    97    26   233
4        0  5444   124   248
5     5236   120   151   309
6     4573   247   249   747
7      736   151   121  4808
8      330   103  5383     0
9      233    22   137  5424
10    2306  1071   999  1440
11    1682   790  1520  1824
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Elba1_MA1837.1
XX
ID  Elba1_MA1837.1
XX
DE  MA1837.1 Elba1; from JASPAR
P0       a     c     g     t
1      914   790   988  1359
2      999  1776   465   811
3       80  3594    88   289
4     3723    96   101   131
5     3717    86   127   121
6      174   106    74  3697
7      978   304    66  2703
8     3739   112   182    18
9       25    68  3882    76
10    1817   682   610   942
11    1482   867   686  1016
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Elba2_MA1838.1
XX
ID  Elba2_MA1838.1
XX
DE  MA1838.1 Elba2; from JASPAR
P0       a     c     g     t
1     1144   899   963  1424
2     1113  1539   652  1126
3      142  3857   122   309
4     3977   150   127   176
5     3958   115   171   186
6      231   131   131  3937
7      726   298    99  3307
8     4033   133   234    30
9       40   114  4148   128
10    1759   777   780  1114
11    1453   932   841  1204
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GATAd_MA1839.1
XX
ID  GATAd_MA1839.1
XX
DE  MA1839.1 GATAd; from JASPAR
P0       a     c     g     t
1      406   385   305   501
2      414   337   329   517
3       65  1438    62    32
4       81    46    36  1434
5       74    54    42  1427
6     1483    68    37     9
7       57    49    37  1454
8       38  1460    31    68
9     1168    51   241   137
10     391   378   450   378
11     403   380   259   555
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  gcm_MA1840.1
XX
ID  gcm_MA1840.1
XX
DE  MA1840.1 gcm; from JASPAR
P0       a     c     g     t
1     2485  2711  2405  2499
2     2163  3398  2168  2371
3     4797   878  3207  1218
4       83  9705   110   202
5      162  9704    53   181
6      549  9113   164   274
7      619   384  8643   454
8      110  9382   132   476
9     9478   180   115   327
10     308  1335   818  7639
11    2562  4295  1362  1881
12    2462  2803  1696  3139
13    2721  2502  2117  2760
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  kn_MA1841.1
XX
ID  kn_MA1841.1
XX
DE  MA1841.1 kn; from JASPAR
P0       a     c     g     t
1       45    83    75    78
2       65    62    83    71
3       94    30    91    66
4       36    50   136    59
5       16    18    11   236
6        1   277     1     2
7        6   252     5    18
8        4   259     9     9
9       47   174    22    38
10     153    25    54    49
11      12     6   258     5
12      27     7   243     4
13       2     5   272     2
14     227     8    27    19
15      64   130    46    41
16      52   101    37    91
17      75    72    57    77
18      63    78    80    60
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  msl-1_MA1842.1
XX
ID  msl-1_MA1842.1
XX
DE  MA1842.1 msl-1; from JASPAR
P0       a     c     g     t
1     1007   765  1283   826
2     1016  1327   470  1068
3        0     0    21  3860
4     3863    18     0     0
5        0    45     0  3836
6       49  3832     0     0
7        0     0  3809    72
8     3759     0    92    30
9      539   284   266  2792
10    2111   339   578   853
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
