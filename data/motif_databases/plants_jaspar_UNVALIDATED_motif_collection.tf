AC  PK00315.1_UN0001.1
XX
ID  PK00315.1_UN0001.1
XX
DE  UN0001.1 PK00315.1; from JASPAR
P0       a     c     g     t
1      226   226   320   226
2      186   260   186   367
3       86   163   664    86
4      839   159     0     0
5        0   922     0    77
6      917     0    82     0
7       77     0   922     0
8       77   840    82     0
9       23    23   106   846
10     317    64   477   141
11     246   164   281   308
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK01144.1_UN0002.1
XX
ID  PK01144.1_UN0002.1
XX
DE  UN0002.1 PK01144.1; from JASPAR
P0       a     c     g     t
1      349   229   253   168
2       23   872     4   100
3       31     0    82   886
4       43     2    25   929
5       43    25    22   909
6      331   133    78   456
7      222   219   213   344
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK01630.1_UN0003.1
XX
ID  PK01630.1_UN0003.1
XX
DE  UN0003.1 PK01630.1; from JASPAR
P0       a     c     g     t
1      166   111   677    44
2      134   864     0     0
3      904    14    43    36
4        0   926     0    73
5       94     0   904     0
6       52    67    82   798
7        8     0   927    64
8      473    53   290   182
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK03025.1_UN0005.1
XX
ID  PK03025.1_UN0005.1
XX
DE  UN0005.1 PK03025.1; from JASPAR
P0       a     c     g     t
1      495     4     4   495
2      985     4     4     4
3        4     4     4   985
4      985     4     4     4
5      985     4     4     4
6      985     4     4     4
7        4     4     4   985
8        4   495     4   495
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK03929.1_UN0007.1
XX
ID  PK03929.1_UN0007.1
XX
DE  UN0007.1 PK03929.1; from JASPAR
P0       a     c     g     t
1      223   275   188   313
2      279   247   161   311
3      430   189   231   147
4       39   908    16    35
5       34    46    80   839
6       31     4    22   941
7       24    10    24   940
8      349   118    73   458
9      207   251   170   371
10     245   240   255   258
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK04914.1_UN0008.1
XX
ID  PK04914.1_UN0008.1
XX
DE  UN0008.1 PK04914.1; from JASPAR
P0       a     c     g     t
1      246    13   547   191
2      151   828    10    10
3        0   990     9     0
4        0     0   999     0
5        0     0    18   980
6        0   307     9   682
7      924     0    10    64
8      421   156   234   187
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK05432.1_UN0010.1
XX
ID  PK05432.1_UN0010.1
XX
DE  UN0010.1 PK05432.1; from JASPAR
P0       a     c     g     t
1      263   231   242   263
2      220   264   220   294
3      384   103   451    60
4        4     1   894   100
5        8     2     2   986
6       34   963     0     1
7      725   132    35   106
8      622    47   201   129
9      314   408   121   154
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK05451.1_UN0011.1
XX
ID  PK05451.1_UN0011.1
XX
DE  UN0011.1 PK05451.1; from JASPAR
P0       a     c     g     t
1       83   582    83   250
2        0   998     0     0
3      998     0     0     0
4        0   998     0     0
5        0     0   998     0
6        0     0     0   998
7        0     0   998     0
8      285   570    71    71
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK05732.1_UN0013.1
XX
ID  PK05732.1_UN0013.1
XX
DE  UN0013.1 PK05732.1; from JASPAR
P0       a     c     g     t
1      469   128   117   285
2      336   339   127   196
3      892    17    39    50
4       37    12     0   949
5      105    58    87   748
6       62   837    33    66
7       78   336   175   410
8      231   276   200   290
9      209   266   160   363
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK06517.1_UN0015.1
XX
ID  PK06517.1_UN0015.1
XX
DE  UN0015.1 PK06517.1; from JASPAR
P0       a     c     g     t
1      167   498   167   167
2        0   997     0     0
3      997     0     0     0
4        0   997     0     0
5        0     0   997     0
6        0     0     0   997
7        0     0   997     0
8      125   747   125     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK07031.1_UN0016.1
XX
ID  PK07031.1_UN0016.1
XX
DE  UN0016.1 PK07031.1; from JASPAR
P0       a     c     g     t
1      355   222   252   169
2       58   816    17   107
3       50    17   243   688
4       74     0    65   859
5       73    21    44   860
6      314   154   110   420
7      213   245   210   330
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK07402.1_UN0017.1
XX
ID  PK07402.1_UN0017.1
XX
DE  UN0017.1 PK07402.1; from JASPAR
P0       a     c     g     t
1      138   346   272   242
2      190   534   166   108
3      608     2   384     5
4        0   993     5     1
5        0   994     0     4
6       10     1   977    10
7      441   411    42   104
8       75   790    31   102
9      367   227    86   319
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK07858.1_UN0019.1
XX
ID  PK07858.1_UN0019.1
XX
DE  UN0019.1 PK07858.1; from JASPAR
P0       a     c     g     t
1      235   294    59   411
2        0   998     0     0
3      998     0     0     0
4        0   998     0     0
5        0     0   998     0
6        0     0     0   998
7        0     0   998     0
8       67   532   200   200
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK08602.1_UN0021.1
XX
ID  PK08602.1_UN0021.1
XX
DE  UN0021.1 PK08602.1; from JASPAR
P0       a     c     g     t
1      200   307   292   200
2       96   621    96   186
3        0   999     0     0
4      999     0     0     0
5        0   999     0     0
6        0   999     0     0
7        0     0   999     0
8        0   999     0     0
9      134   574    49   241
10     335   270   239   154
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK09021.1_UN0022.1
XX
ID  PK09021.1_UN0022.1
XX
DE  UN0022.1 PK09021.1; from JASPAR
P0       a     c     g     t
1      217   253   282   246
2      295   243   282   178
3        0     0     0   999
4        0     0   999     0
5      999     0     0     0
6        0   999     0     0
7        0     0   999     0
8        0     0     0   999
9      265   669    32    32
10     711    71   109   107
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK09702.1_UN0025.1
XX
ID  PK09702.1_UN0025.1
XX
DE  UN0025.1 PK09702.1; from JASPAR
P0       a     c     g     t
1        1   125   871     1
2        0   997     0     0
3      997     0     0     0
4        0   997     0     0
5        0     0   997     0
6        0     0     0   997
7        0     0   997     0
8      332   167   498     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK10344.1_UN0026.1
XX
ID  PK10344.1_UN0026.1
XX
DE  UN0026.1 PK10344.1; from JASPAR
P0       a     c     g     t
1       91     0   725   182
2        0     0   997     0
3        0     0   997     0
4      855   143     0     0
5        0   997     0     0
6        0   997     0     0
7      997     0     0     0
8        1   871     1   125
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK10955.1_UN0027.1
XX
ID  PK10955.1_UN0027.1
XX
DE  UN0027.1 PK10955.1; from JASPAR
P0       a     c     g     t
1      500   136   253   109
2        9   970     3    17
3        8    89    43   857
4        7     0     3   988
5        7     4     8   979
6      308    59    16   615
7      181   187   126   504
8      218   225   246   310
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK12011.1_UN0029.1
XX
ID  PK12011.1_UN0029.1
XX
DE  UN0029.1 PK12011.1; from JASPAR
P0       a     c     g     t
1      138   287    85   488
2      985     0    12     1
3      926    58     7     8
4        3   986     0     9
5       35    80   798    84
6      112    24   701   161
7      213   266   113   405
8      288   312   108   291
9      363   216   180   239
10     265   228   209   296
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK12240.1_UN0030.1
XX
ID  PK12240.1_UN0030.1
XX
DE  UN0030.1 PK12240.1; from JASPAR
P0       a     c     g     t
1        2     2   992     2
2        2     2   992     2
3        2     2   992     2
4        2   745   250     2
5        2   992     2     2
6        2   992     2     2
7      250   745     2     2
8        2   745   250     2
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK13314.1_UN0032.1
XX
ID  PK13314.1_UN0032.1
XX
DE  UN0032.1 PK13314.1; from JASPAR
P0       a     c     g     t
1      135   302   177   384
2      169   211   421   197
3       22    15   862    99
4       41     8   919    30
5      155   167   482   194
6       72   727    52   146
7        0   991     6     1
8       14   908     0    77
9      828    46   105    19
10       6   806    77   110
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK14653.1_UN0034.1
XX
ID  PK14653.1_UN0034.1
XX
DE  UN0034.1 PK14653.1; from JASPAR
P0       a     c     g     t
1       95   137   489   276
2       17     4   923    54
3       57    20   915     6
4      134   207   568    89
5       53   881    11    53
6        8   975     4    12
7        0   989     0    10
8      886    34    73     6
9        1   870    62    65
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK15337.1_UN0036.1
XX
ID  PK15337.1_UN0036.1
XX
DE  UN0036.1 PK15337.1; from JASPAR
P0       a     c     g     t
1      354   244   223   177
2       69   769    33   127
3       51     1   159   788
4       67     6    39   886
5       73    39    43   842
6      325   155    92   426
7      212   241   212   333
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK15505.1_UN0037.1
XX
ID  PK15505.1_UN0037.1
XX
DE  UN0037.1 PK15505.1; from JASPAR
P0       a     c     g     t
1      290   295   173   240
2      190   120   628    59
3        0   999     0     0
4      999     0     0     0
5        0   999     0     0
6        0     0   999     0
7        0     0     0   999
8        0     0   999     0
9       76   255   137   530
10     190   263   286   259
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK16335.1_UN0038.1
XX
ID  PK16335.1_UN0038.1
XX
DE  UN0038.1 PK16335.1; from JASPAR
P0       a     c     g     t
1      233   233   233   298
2      298   233   233   233
3      298   233   233   233
4      158   293   243   304
5       99    99   614   185
6      772   227     0     0
7       64   935     0     0
8      999     0     0     0
9       16    16   951    16
10      16   951    16    16
11      16    16    16   951
12     167    91   649    91
13     150   150   300   399
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK16340.1_UN0039.1
XX
ID  PK16340.1_UN0039.1
XX
DE  UN0039.1 PK16340.1; from JASPAR
P0       a     c     g     t
1       67   812    73    46
2       50     0   329   619
3       68     0    40   890
4       77    10    57   853
5      327   135    85   450
6      237   244   175   343
7      230   245   245   279
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK17878.1_UN0040.1
XX
ID  PK17878.1_UN0040.1
XX
DE  UN0040.1 PK17878.1; from JASPAR
P0       a     c     g     t
1       80   878     0    40
2        0     0   999     0
3        0   965    33     0
4        0   999     0     0
5        0     0   999     0
6       66   632   100   200
7      138   826     0    34
8      649    50   250    50
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK18009.1_UN0041.1
XX
ID  PK18009.1_UN0041.1
XX
DE  UN0041.1 PK18009.1; from JASPAR
P0       a     c     g     t
1      233   233   300   233
2      153   153   419   273
3      749   137     0   112
4       56   831    56    56
5      999     0     0     0
6      112   887     0     0
7       56     0   887    56
8        0     0     0   999
9      185    17   780    17
10     152   152   220   473
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK18401.1_UN0042.1
XX
ID  PK18401.1_UN0042.1
XX
DE  UN0042.1 PK18401.1; from JASPAR
P0       a     c     g     t
1      300   233   233   233
2      233   233   233   300
3      377   180   261   180
4      224   194   530    49
5        0   932     0    67
6      999     0     0     0
7       67   932     0     0
8        0     0   932    67
9       16    16    16   949
10      16    16   949    16
11      69   147   593   189
12     200   311   200   288
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK18474.1_UN0043.1
XX
ID  PK18474.1_UN0043.1
XX
DE  UN0043.1 PK18474.1; from JASPAR
P0       a     c     g     t
1      307   270   210   210
2      456   117   309   117
3        0   999     0     0
4      999     0     0     0
5        0   999     0     0
6        0     0   999     0
7        0     0     0   999
8        0     0   999     0
9       39   743    96   120
10     226   281   253   238
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK19166.1_UN0044.1
XX
ID  PK19166.1_UN0044.1
XX
DE  UN0044.1 PK19166.1; from JASPAR
P0       a     c     g     t
1       29   734   117   117
2        0   956     0    42
3      382   616     0     0
4        0   999     0     0
5        0     0     0   999
6      999     0     0     0
7      615     0    51   333
8       77   230   115   576
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK19363.1_UN0046.1
XX
ID  PK19363.1_UN0046.1
XX
DE  UN0046.1 PK19363.1; from JASPAR
P0       a     c     g     t
1      150   716    75    58
2       25    12   948    12
3       18   874   106     0
4       18   974     6     0
5        0     6   993     0
6       58   627   137   176
7      157   774     7    60
8      488    66   299   144
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK19621.1_UN0047.1
XX
ID  PK19621.1_UN0047.1
XX
DE  UN0047.1 PK19621.1; from JASPAR
P0       a     c     g     t
1      158   413   182   245
2      146   468   228   156
3       93   185    25   695
4      109    64    82   744
5      981     4     8     5
6        9    38    27   925
7       11   963     5    19
8      103   493   102   299
9      310   172   232   284
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK19717.1_UN0048.1
XX
ID  PK19717.1_UN0048.1
XX
DE  UN0048.1 PK19717.1; from JASPAR
P0       a     c     g     t
1      100   115   712    72
2        6     8   955    29
3       49    34   893    22
4      116   401   401    81
5       23   929     2    44
6        8   982     4     4
7       21   930     2    46
8      553   313    72    60
9        0   882    36    80
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK20225.1_UN0049.1
XX
ID  PK20225.1_UN0049.1
XX
DE  UN0049.1 PK20225.1; from JASPAR
P0       a     c     g     t
1      219   287   250   242
2      135   788    33    42
3      757    98    70    73
4        4   819    47   128
5       80     0   897    21
6       46    29     0   923
7       14     0   984     0
8      245   461   171   121
9      199   243   303   253
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK20317.1_UN0050.1
XX
ID  PK20317.1_UN0050.1
XX
DE  UN0050.1 PK20317.1; from JASPAR
P0       a     c     g     t
1       62   868    37    31
2       22     0   330   646
3       45     0    55   898
4       53     0    60   884
5      340   100    81   477
6      245   222   168   363
7      217   252   259   270
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK20392.1_UN0051.1
XX
ID  PK20392.1_UN0051.1
XX
DE  UN0051.1 PK20392.1; from JASPAR
P0       a     c     g     t
1      154   289   184   371
2      148   395    79   376
3      776    73    80    70
4      945    27     4    22
5       13    14     2   968
6      478   330    66   124
7      973     5     9    11
8       63    30    47   858
9      243    61   294   400
10     199   233   377   189
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK20543.1_UN0054.1
XX
ID  PK20543.1_UN0054.1
XX
DE  UN0054.1 PK20543.1; from JASPAR
P0       a     c     g     t
1      105   253   119   521
2      145    60    36   757
3      803    23   107    64
4      980    13     0     5
5        6   961     1    30
6       35   642    15   306
7      332   352   180   134
8      234   231   297   236
9      420   141   278   159
10     181   295   357   165
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK21166.1_UN0057.1
XX
ID  PK21166.1_UN0057.1
XX
DE  UN0057.1 PK21166.1; from JASPAR
P0       a     c     g     t
1      150   483    50   316
2      999     0     0     0
3      123   845    10    20
4        0   979     0    20
5      282    70     0   646
6      999     0     0     0
7      260   739     0     0
8       54   616    13   315
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK23009.1_UN0059.1
XX
ID  PK23009.1_UN0059.1
XX
DE  UN0059.1 PK23009.1; from JASPAR
P0       a     c     g     t
1      237   217   214   330
2      233   189   305   271
3      212   407   107   272
4      420   157   317   103
5       24   890     5    79
6       29   104    54   811
7       52     0    19   927
8       39    12    18   930
9      338    73    25   563
10     176   226   162   434
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK23763.1_UN0060.1
XX
ID  PK23763.1_UN0060.1
XX
DE  UN0060.1 PK23763.1; from JASPAR
P0       a     c     g     t
1      210   276   176   337
2      287   270   143   298
3      461   173   238   127
4       26   933    13    26
5       35    52    71   839
6       30     6    21   941
7       22     8    24   944
8      366    91    53   489
9      199   244   149   406
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK23964.1_UN0061.1
XX
ID  PK23964.1_UN0061.1
XX
DE  UN0061.1 PK23964.1; from JASPAR
P0       a     c     g     t
1      373   117   250   258
2      132   167   272   427
3       45    47   540   366
4      268   716    14     0
5       16   942     1    39
6      960     0    37     1
7       52   787   100    59
8       88    64   831    15
9       50    49    27   873
10     277   218   452    51
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK24205.1_UN0063.1
XX
ID  PK24205.1_UN0063.1
XX
DE  UN0063.1 PK24205.1; from JASPAR
P0       a     c     g     t
1      452   211   133   202
2       86   671   154    86
3      999     0     0     0
4        0   999     0     0
5        0     0   999     0
6        0     0     0   999
7        0     0   999     0
8        0     0   570   429
9      116   651   116   116
10     381   222   232   163
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK24580.1_UN0064.1
XX
ID  PK24580.1_UN0064.1
XX
DE  UN0064.1 PK24580.1; from JASPAR
P0       a     c     g     t
1      272   453   182    91
2        0     0   998     0
3       59   880    59     0
4        0   998     0     0
5        0     0   998     0
6        0   117   880     0
7        0   998     0     0
8      614   230    77    77
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK25034.1_UN0067.1
XX
ID  PK25034.1_UN0067.1
XX
DE  UN0067.1 PK25034.1; from JASPAR
P0       a     c     g     t
1      293   185   217   302
2      223   175   310   289
3      139    98    50   711
4       25    28   929    15
5      963     0     0    35
6       20   929     1    48
7      542   106   253    97
8       91    94   496   316
9      123   381   328   166
10     222   209   159   408
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK25870.1_UN0068.1
XX
ID  PK25870.1_UN0068.1
XX
DE  UN0068.1 PK25870.1; from JASPAR
P0       a     c     g     t
1      303   196   178   321
2      392    12   594     0
3        0     0   969    30
4       20     0    30   949
5       20   949    30     0
6      999     0     0     0
7      999     0     0     0
8      434   376    58   130
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK26523.1_UN0070.1
XX
ID  PK26523.1_UN0070.1
XX
DE  UN0070.1 PK26523.1; from JASPAR
P0       a     c     g     t
1      384   148   196   270
2      558    69    63   309
3      269   349    98   283
4      960     5    20    14
5       27    12    10   949
6       70    28    32   867
7       41   904    14    39
8       32   417   137   413
9      243   286   197   272
10     180   274   164   381
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK27085.1_UN0072.1
XX
ID  PK27085.1_UN0072.1
XX
DE  UN0072.1 PK27085.1; from JASPAR
P0       a     c     g     t
1      971     9     9     9
2      971     9     9     9
3        9     9     9   971
4        9     9     9   971
5        9     9     9   971
6      971     9     9     9
7        9     9     9   971
8        9     9     9   971
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK27109.1_UN0073.1
XX
ID  PK27109.1_UN0073.1
XX
DE  UN0073.1 PK27109.1; from JASPAR
P0       a     c     g     t
1       37    37   703   222
2      743   205     0    51
3        0   926    24    48
4      999     0     0     0
5       48     0   950     0
6        0   950     0    48
7        0     0     0   999
8      185   111   666    37
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK27149.1_UN0074.1
XX
ID  PK27149.1_UN0074.1
XX
DE  UN0074.1 PK27149.1; from JASPAR
P0       a     c     g     t
1       71   143     0   784
2        0   166   610   222
3      999     0     0     0
4        0   999     0     0
5        0     0   972    26
6        0    30     0   968
7       74    74   850     0
8       91     0   544   363
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK27683.1_UN0076.1
XX
ID  PK27683.1_UN0076.1
XX
DE  UN0076.1 PK27683.1; from JASPAR
P0       a     c     g     t
1      340   134   240   284
2      414    58   170   356
3       57    57    30   854
4       61    35    45   856
5       24   606     0   367
6      814    17    85    82
7      895    14    32    57
8      883    25    51    39
9      303   246    75   374
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK27693.1_UN0077.1
XX
ID  PK27693.1_UN0077.1
XX
DE  UN0077.1 PK27693.1; from JASPAR
P0       a     c     g     t
1      199   367   180   251
2      173   394   241   190
3      128   222    39   610
4      138    96    84   679
5      972     3     7    16
6       15    45    36   902
7       14   956    11    17
8      116   436   138   307
9      261   211   231   294
10     220   227   256   295
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PK28187.1_UN0078.1
XX
ID  PK28187.1_UN0078.1
XX
DE  UN0078.1 PK28187.1; from JASPAR
P0       a     c     g     t
1        4     4     4   985
2      990     3     3     3
3        3   990     3     3
4        3   990     3     3
5        3     3     3   990
6      990     3     3     3
7        3   990     3     3
8        4   985     4     4
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHL3_UN0080.1
XX
ID  PHL3_UN0080.1
XX
DE  UN0080.1 PHL3; from JASPAR
P0       a     c     g     t
1      389   161   203   245
2      587    81   101   229
3       48    37   518   395
4      929     2    60     7
5       37     0     0   962
6       91    45   139   724
7        0   988     0    11
8       48   364   298   289
9      212   310   223   254
10     216   267   190   326
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CEG01538.1_UN0275.1
XX
ID  CEG01538.1_UN0275.1
XX
DE  UN0275.1 CEG01538.1; from JASPAR
P0       a     c     g     t
1       69   241    34   654
2      112   112   687    87
3      999     0     0     0
4        0     0     0   999
5        0     0     0   999
6        0     0   999     0
7      111    49     0   839
8      138   103    17   741
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PNRC2_UN0276.1
XX
ID  PNRC2_UN0276.1
XX
DE  UN0276.1 PNRC2; from JASPAR
P0       a     c     g     t
1      725   182    91     0
2      873     0     0   125
3      686     0     0   312
4      312   686     0     0
5        0   998     0     0
6        0   935     0    62
7       62     0     0   935
8      997     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARID6_UN0351.1
XX
ID  ARID6_UN0351.1
XX
DE  UN0351.1 ARID6; from JASPAR
P0       a     c     g     t
1      297    71    78   292
2      328    58    69   283
3      246    39    57   396
4      199    47    43   449
5       49    10    14   665
6       55    19    24   640
7      686     1    20    31
8      704     5     9    20
9       13     5     4   716
10      21    14     4   699
11      18     0   701    19
12     642    10    27    59
13     587    23    32    96
14     288    60    56   334
15     272    72    57   337
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT3G11100_UN0353.1
XX
ID  AT3G11100_UN0353.1
XX
DE  UN0353.1 AT3G11100; from JASPAR
P0       a     c     g     t
1      479  1021   300   696
2      615   330  1002   549
3     1432   652   254   158
4       16  2413     6    61
5       24    13  2445    14
6       57    26  2394    19
7      102  2298    12    84
8       73    55  2311    57
9     2127    58   198   113
10     355   653   886   602
11     760   289   875   572
12     810   396   698   592
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT3G42860_UN0354.1
XX
ID  AT3G42860_UN0354.1
XX
DE  UN0354.1 AT3G42860; from JASPAR
P0       a     c     g     t
1      626   198   199   264
2      804   100   170   213
3     1072    26   166    23
4        9     6  1268     4
5        0     2     2  1283
6       10  1271     1     5
7     1277     5     2     3
8     1279     1     3     4
9       66  1136     6    79
10     122   114   871   180
11     339   305   301   342
12     334   357   165   431
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT3G49930_UN0355.1
XX
ID  AT3G49930_UN0355.1
XX
DE  UN0355.1 AT3G49930; from JASPAR
P0       a     c     g     t
1      222   290   146   466
2      294   286   131   413
3       44    90    39   951
4     1068    10    17    29
5       35  1013    42    34
6       23  1078    11    12
7       40     5    30  1049
8     1077     7    14    26
9      887   139    19    79
10     234   362    74   454
11     325   205   119   475
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT5G23930_UN0357.1
XX
ID  AT5G23930_UN0357.1
XX
DE  UN0357.1 AT5G23930; from JASPAR
P0       a     c     g     t
1      228  1033   293   520
2      535  1323    91   125
3        0     0  2074     0
4        0  2074     0     0
5        0  2074     0     0
6        0     0  2074     0
7        0  2074     0     0
8        0  2074     0     0
9      600    43  1135   296
10     191  1036   249   598
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BHLH28_UN0359.1
XX
ID  BHLH28_UN0359.1
XX
DE  UN0359.1 BHLH28; from JASPAR
P0       a     c     g     t
1      806   581   525   836
2      740   657   481   870
3      716   514   610   908
4      688   548   559   953
5      734   287  1141   586
6      394    51   291  2012
7      117  2536    12    83
8       22  2660    16    50
9       16     5  2716    11
10      10     4     9  2725
11    2714    12    12    10
12       8  2699    19    22
13    1731   261   509   247
14    1043   455   581   669
15     823   489   436  1000
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  EICBP.B_UN0362.1
XX
ID  EICBP.B_UN0362.1
XX
DE  UN0362.1 EICBP.B; from JASPAR
P0       a     c     g     t
1      302   175   186   243
2      291   180   169   266
3      224   292   224   166
4      644    65    84   113
5      762    30    48    66
6      791    23    47    45
7      610    97   182    17
8        7   888     3     8
9       28     9   861     8
10      18   839     6    43
11       9     3   889     5
12      10    10    21   865
13      95    53   596   162
14     530    52   143   181
15     343   153   200   210
16     281   168   225   232
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  S1FA3_UN0379.1
XX
ID  S1FA3_UN0379.1
XX
DE  UN0379.1 S1FA3; from JASPAR
P0       a     c     g     t
1      410   371   703   496
2      476   573   418   513
3       97    81    34  1768
4       39     9     5  1927
5       17  1915    23    25
6       58  1078   118   726
7     1842    16   102    20
8       38    30  1892    20
9     1943     4    15    18
10    1800    19    69    92
11     513   379   627   461
12     462   759   365   394
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AGL95_UN0386.1
XX
ID  AGL95_UN0386.1
XX
DE  UN0386.1 AGL95; from JASPAR
P0       a     c     g     t
1      603    71   247    79
2       37    12   934    17
3      945    10    25    20
4      911     7    22    61
5      103   195   556   146
6      244   405   215   136
7      141    34    15   810
8       37     5    17   941
9       13   945    18    24
10      50   141    74   734
11     763    39   176    22
12      13    15   955    17
13     913    25    22    40
14     724    47   104   124
15     229   218   393   160
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARID3_UN0388.1
XX
ID  ARID3_UN0388.1
XX
DE  UN0388.1 ARID3; from JASPAR
P0       a     c     g     t
1      703    64   108   126
2      714    90    42   154
3      564    64    13   359
4      269    57    44   630
5      220   196    66   518
6      381   159   275   185
7      797    70     0   132
8      976     7    18     0
9      980     0     0    20
10       0     0     0  1000
11       0   262     0   738
12     775     0   225     0
13     934    11    55     0
14     535     0    22   443
15     269    31    18   683
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT1G66420_UN0389.1
XX
ID  AT1G66420_UN0389.1
XX
DE  UN0389.1 AT1G66420; from JASPAR
P0       a     c     g     t
1      736    38    75   151
2      566    75   208   151
3      189   660    57    94
4      321   528   151     0
5        0    19     0   981
6      113   189     0   698
7      981     0     0    19
8        0     0     0  1000
9        0   981     0    19
10      19   981     0     0
11     774     0   132    94
12     132   132    38   698
13     528    38     0   434
14     340    19    57   585
15     283   434    75   208
16      75   226   113   585
17     208    57   170   566
18     453   170   245   132
19     208    94   340   358
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT2G31460_UN0390.1
XX
ID  AT2G31460_UN0390.1
XX
DE  UN0390.1 AT2G31460; from JASPAR
P0       a     c     g     t
1      335   104   293   268
2      476    85   159   280
3      457    49   226   268
4       18    18   957     6
5      988     0    12     0
6     1000     0     0     0
7     1000     0     0     0
8        6     0     0   994
9        0     0   994     6
10     945    37     6    12
11     366    37    30   567
12     390    79   329   201
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT3G58630_UN0391.1
XX
ID  AT3G58630_UN0391.1
XX
DE  UN0391.1 AT3G58630; from JASPAR
P0       a     c     g     t
1      234   359   115   292
2      187   216   364   232
3       97   150    59   694
4       38   804    45   114
5      139     2   859     0
6       29   950     0    22
7        0  1000     0     0
8        0     0  1000     0
9        0     0  1000     0
10     793   101    29    77
11     241    27   701    31
12     575   105   114   207
13     360   178    74   387
14     272   115   252   360
15     182   238   207   373
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT4G00390_UN0392.1
XX
ID  AT4G00390_UN0392.1
XX
DE  UN0392.1 AT4G00390; from JASPAR
P0       a     c     g     t
1      285   191   332   191
2      679    75   171    75
3       25   924    25    25
4       25   924    25    25
5        0     0  1000     0
6        0    86   914     0
7        0    99    86   815
8        0   914    86     0
9      254   149   453   144
10     377   175   273   175
11     225   326   225   225
12     225   326   225   225
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AT4G09450_UN0393.1
XX
ID  AT4G09450_UN0393.1
XX
DE  UN0393.1 AT4G09450; from JASPAR
P0       a     c     g     t
1      168   374   180   277
2      165   398   227   210
3      111   184    17   688
4       63    34    68   835
5      972     7    10    12
6        9    31    21   938
7       16   968     7     9
8       75   559    80   286
9      279   164   209   348
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AtPLATZ6_UN0394.1
XX
ID  AtPLATZ6_UN0394.1
XX
DE  UN0394.1 AtPLATZ6; from JASPAR
P0       a     c     g     t
1      580    70   308    42
2        5     7   988     0
3      968     5     3    23
4      922     8    17    53
5       75   238   550   137
6      288   337   188   187
7       98    42    12   848
8       13     0     7   980
9        3   985     5     7
10       0   142    47   812
11     778    37   183     2
12       2    10   988     0
13     960     7     8    25
14     700    57   110   133
15     248   187   378   187
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CRC_UN0396.1
XX
ID  CRC_UN0396.1
XX
DE  UN0396.1 CRC; from JASPAR
P0       a     c     g     t
1       35   143    80   742
2      587    48     2   363
3      995     0     0     5
4       28     0     0   972
5        0   988    12     0
6     1000     0     0     0
7        0     0     5   995
8      598    73    60   268
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  EDF3_UN0397.1
XX
ID  EDF3_UN0397.1
XX
DE  UN0397.1 EDF3; from JASPAR
P0       a     c     g     t
1      428     1   570     1
2        1   997     1     1
3      997     1     1     1
4      997     1     1     1
5        1   997     1     1
6      997     1     1     1
7        1     1   333   665
8      854     1     1   143
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ENY_UN0398.1
XX
ID  ENY_UN0398.1
XX
DE  UN0398.1 ENY; from JASPAR
P0       a     c     g     t
1      294   113   129   464
2      259   139   168   434
3      114    50    50   785
4        8    34    13   945
5        0     5     7   988
6        0     3     0   997
7        0     0  1000     0
8        0     2     5   993
9        0  1000     0     0
10      27    96   627   250
11       3    40    45   911
12     145   133    62   661
13     175     2     3   820
14     239    92    32   637
15     108   324   304   264
16      94   239   129   538
17     249   109   353   289
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GEBP_UN0399.1
XX
ID  GEBP_UN0399.1
XX
DE  UN0399.1 GEBP; from JASPAR
P0       a     c     g     t
1      143    36   607   214
2       27   972     0     0
3        0     0   999     0
4        0     0   949    50
5        0   999     0     0
6        0   974     0    25
7       54     0   810   135
8      160    40   560   240
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HMGB15_UN0400.1
XX
ID  HMGB15_UN0400.1
XX
DE  UN0400.1 HMGB15; from JASPAR
P0       a     c     g     t
1       70    83    35   813
2      695    13     0   292
3       48     0     0   952
4      981     6     6     6
5      343    83    79   495
6      206    60    41   692
7      908    57    22    13
8        0     3     0   997
9     1000     0     0     0
10     273     0     0   727
11     641     0   229   130
12     130    60   787    22
13      10     6     0   984
14     225    63    19   692
15     102    63    54   781
16     254    70   105   571
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HMGB9_UN0401.1
XX
ID  HMGB9_UN0401.1
XX
DE  UN0401.1 HMGB9; from JASPAR
P0       a     c     g     t
1      491   101    71   337
2      288    85   136   491
3      430   150   107   314
4     1000     0     0     0
5      308     0     0   692
6        0     0     0  1000
7        0     0     0  1000
8     1000     0     0     0
9     1000     0     0     0
10       0     0     0  1000
11     211   128   195   465
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  IDD11_UN0402.1
XX
ID  IDD11_UN0402.1
XX
DE  UN0402.1 IDD11; from JASPAR
P0       a     c     g     t
1      285   371   113   231
2      621    87   209    84
3      210   332   323   135
4      680    12    72   236
5      846     2     2   150
6      641    70   142   147
7      915    39    41     5
8      209   682    75    34
9        7     0   990     3
10     991     9     0     0
11       0  1000     0     0
12    1000     0     0     0
13     959    12    24     5
14     885    31    63    21
15     682    70    77   171
16     448   144   118   291
17     482   116   109   292
18     369   152   227   251
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  JGL_UN0403.1
XX
ID  JGL_UN0403.1
XX
DE  UN0403.1 JGL; from JASPAR
P0       a     c     g     t
1      853    25    82    40
2      415   427   108    50
3       37   458     7   498
4      207    40   190   563
5      115   267   237   382
6       13   815    87    85
7      963     7    30     0
8        0    12   988     0
9        7     0     0   993
10       3    47    27   923
11     397   313    78   212
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  KHZ1_UN0404.1
XX
ID  KHZ1_UN0404.1
XX
DE  UN0404.1 KHZ1; from JASPAR
P0       a     c     g     t
1      480   127   222   172
2      420   173   187   220
3      372   137   230   262
4      168   362   307   163
5      717    98   118    67
6      762    22     0   217
7     1000     0     0     0
8     1000     0     0     0
9      985     0    15     0
10       0     0  1000     0
11      98   130   347   425
12     273   105   172   450
13     585    15   298   102
14     477   173   148   202
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MGH6.1_UN0405.1
XX
ID  MGH6.1_UN0405.1
XX
DE  UN0405.1 MGH6.1; from JASPAR
P0       a     c     g     t
1        0   443     0   557
2       82     0   814   103
3        0   619   361    21
4        0   969     0    31
5        0     0   979    21
6        0     0   268   732
7      258   237   351   155
8      454   103   268   175
9      423   155   196   227
10     258   103   278   361
11     320   124   216   340
12     237   371    62   330
13     165   247   361   227
14     670   330     0     0
15       0   990    10     0
16       0     0  1000     0
17      31   206   443   320
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NLP4_UN0406.1
XX
ID  NLP4_UN0406.1
XX
DE  UN0406.1 NLP4; from JASPAR
P0       a     c     g     t
1      159   241   244   356
2      230   255   356   159
3      205   395   121   279
4      238   252   142   367
5      170   192   436   203
6      115   537   129   219
7        3    11     0   986
8        0     0  1000     0
9        0   992     0     8
10       0    14     0   986
11       8     0   992     0
12     129   627   112   132
13     274   132   118   477
14     263   151   323   263
15     255   334   153   258
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  REM1_UN0407.1
XX
ID  REM1_UN0407.1
XX
DE  UN0407.1 REM1; from JASPAR
P0       a     c     g     t
1      329   121   121   429
2        0    95   802   103
3      409     0   191   400
4        0     0     0  1000
5        0     0  1000     0
6        0     0     0  1000
7     1000     0     0     0
8        0     0  1000     0
9      418   324   129   129
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  REM21_UN0408.1
XX
ID  REM21_UN0408.1
XX
DE  UN0408.1 REM21; from JASPAR
P0       a     c     g     t
1      306   327   102   265
2      211   211   143   435
3      218   279   163   340
4      224   204   190   381
5      170   177   259   395
6      224   361   122   293
7        0     0     0  1000
8        0     0     0  1000
9        0   993     7     0
10    1000     0     0     0
11       0     0     0  1000
12       0  1000     0     0
13     381   129   177   313
14     197   238   150   415
15     190   401   163   245
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RKD2_UN0409.1
XX
ID  RKD2_UN0409.1
XX
DE  UN0409.1 RKD2; from JASPAR
P0       a     c     g     t
1      247   210   351   193
2      206   286   124   383
3      184     2   797    17
4      610   135    65   191
5        0   995     0     5
6        3   194   453   349
7      129   264   201   405
8       15    17     0   968
9       15    51     0   934
10      22   763   148    66
11     528    68   397     7
12     240   111   244   405
13     181   687    31   102
14       0     0    15   985
15      70   165    15   750
16      19   952     9    20
17      20   726     7   247
18     157   370    90   383
19     227   358   155   261
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  STKL2_UN0410.1
XX
ID  STKL2_UN0410.1
XX
DE  UN0410.1 STKL2; from JASPAR
P0       a     c     g     t
1      492   128   103   277
2      552    52   144   252
3       88   541    81   291
4      191   622   105    83
5        8    56     0   936
6       14     8    24   954
7     1000     0     0     0
8        0     0     0  1000
9        0  1000     0     0
10       0   919     0    81
11     730     2   201    68
12     277   213    52   458
13     412    30    20   537
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TCX5_UN0411.1
XX
ID  TCX5_UN0411.1
XX
DE  UN0411.1 TCX5; from JASPAR
P0       a     c     g     t
1      324   225   225   225
2      637    22   225   115
3        0     0     0  1000
4        0     0     0  1000
5        0   776     0   224
6      906     0    94     0
7     1000     0     0     0
8     1000     0     0     0
9      130   251    25   594
10     228   228   228   316
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  VFP5_UN0412.1
XX
ID  VFP5_UN0412.1
XX
DE  UN0412.1 VFP5; from JASPAR
P0       a     c     g     t
1      302   208   102   388
2      131   435   126   309
3      362   119   357   161
4      101   193    45   661
5        2   911    29    59
6      168     0   832     0
7       42   757   114    87
8        0  1000     0     0
9        0     0  1000     0
10       0     0   998     2
11     634   206    15   144
12     388     5   596    12
13     614    76   104   206
14     378   164   136   322
15     322   119   255   304
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WOX11_UN0413.1
XX
ID  WOX11_UN0413.1
XX
DE  UN0413.1 WOX11; from JASPAR
P0       a     c     g     t
1      734     0   122   144
2      312   118   392   179
3        0    53     0   947
4        0     0     0  1000
5     1000     0     0     0
6      954     0    46     0
7        0   110     0   890
8        0    23    53   924
9      798     0   202     0
10     479   202   319     0
11     243   122   163   471
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  WOX13_UN0414.1
XX
ID  WOX13_UN0414.1
XX
DE  UN0414.1 WOX13; from JASPAR
P0       a     c     g     t
1      101   200   101   599
2       72     1     1   927
3      134     1   799    67
4      998     1     1     1
5        1     1     1   998
6        1     1     1   998
7      154     1   844     1
8      816    91    91     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YAB1_UN0415.1
XX
ID  YAB1_UN0415.1
XX
DE  UN0415.1 YAB1; from JASPAR
P0       a     c     g     t
1      796     2     2   200
2      997     1     1     1
3        1     1     1   997
4      665   112   112   112
5      775     1     1   222
6      112     1     1   886
7      554   112     1   333
8      996     1     1     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YAB5_UN0416.1
XX
ID  YAB5_UN0416.1
XX
DE  UN0416.1 YAB5; from JASPAR
P0       a     c     g     t
1      305   198   299   198
2      765    78    78    78
3      147    51    51   752
4      625   121   227    26
5      903    96     0     0
6       94     0    96   809
7       95     0     0   904
8      905    94     0     0
9      348    52   153   447
10     283   276   172   270
11     200   297   303   200
12     328   224   224   224
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ZAT9_UN0417.1
XX
ID  ZAT9_UN0417.1
XX
DE  UN0417.1 ZAT9; from JASPAR
P0       a     c     g     t
1       38   522    20   420
2        0   145     0   855
3      997     0     3     0
4        0  1000     0     0
5        0     0     0  1000
6      522    33     0   445
7      340   413   117   130
8      383   183    55   378
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GLYMA-04G093300_UN0418.1
XX
ID  GLYMA-04G093300_UN0418.1
XX
DE  UN0418.1 GLYMA-04G093300; from JASPAR
P0       a     c     g     t
1     2384  1814  1220  2467
2     2594  1764  1184  2343
3        0  7885     0     0
4        0     0   119  7766
5     7816     1     3    65
6     7649   102    82    52
7       26     3    24  7832
8        0  7885     0     0
9     2961  1883   800  2241
10    2718  1780   847  2540
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GLYMA-08G357600_UN0420.1
XX
ID  GLYMA-08G357600_UN0420.1
XX
DE  UN0420.1 GLYMA-08G357600; from JASPAR
P0       a     c     g     t
1     1134  2600  1008  1671
2     3654   593  1107  1059
3       17  6396     0     0
4        0     1  6412     0
5      217   547   124  5525
6      302   191  5839    81
7      200   134   525  5554
8      288  6073     0    52
9     1824  1368  1558  1663
10    1670  1654  1209  1880
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GLYMA-13G317000_UN0421.1
XX
ID  GLYMA-13G317000_UN0421.1
XX
DE  UN0421.1 GLYMA-13G317000; from JASPAR
P0       a     c     g     t
1     4020  3522  3422  4955
2     3343  2447  6249  3880
3    15158   355   177   229
4       11 15390    93   425
5    13931   118  1340   530
6        0 15919     0     0
7        0     0 15919     0
8      290   365    65 15199
9     3774  2289  7596  2260
10    3204  2479  4529  5707
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  LEC1-A_UN0422.1
XX
ID  LEC1-A_UN0422.1
XX
DE  UN0422.1 LEC1-A; from JASPAR
P0       a     c     g     t
1     3294  7023  2759  3467
2     9508  1783  2775  2477
3      942 14394   501   706
4      609   207 15158   569
5      452   435   174 15482
6      567   401 15073   502
7      892   820 12672  2159
8     1110 14415   672   346
9    12833   887  1497  1326
10    4101  4154  3848  4440
11    4425  3805  3775  4538
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  LEC1-A_UN0423.1
XX
ID  LEC1-A_UN0423.1
XX
DE  UN0423.1 LEC1-A; from JASPAR
P0       a     c     g     t
1     1925  1271  2338  2083
2     3228  1636  1564  1189
3        0  7614     0     3
4     7037     2   293   285
5     6966   650     1     0
6        0     0  7617     0
7       40   108     0  7469
8        4     0  7613     0
9     1334  1200  2100  2983
10    2166  2225  1436  1790
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  LEC1-A_UN0424.1
XX
ID  LEC1-A_UN0424.1
XX
DE  UN0424.1 LEC1-A; from JASPAR
P0       a     c     g     t
1     6705  3845  3654  4745
2     3892  6859  2900  5298
3    16947   590   713   699
4     1298  4176   821 12654
5      858   508 16842   741
6      615   357   455 17522
7      652   540 17337   420
8      824   422   766 16937
9      784 17815   141   209
10    6548  3454  3307  5640
11    4900  4587  3447  6015
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TAGL1_UN0426.1
XX
ID  TAGL1_UN0426.1
XX
DE  UN0426.1 TAGL1; from JASPAR
P0       a     c     g     t
1     2178   657   789  1227
2     2082   806   785  1178
3     2930   676   401   844
4     1091   351  2722   687
5     4576    61   106   108
6     4641    47    85    78
7     4647    23    83    98
8     4590    14   109   138
9     4562    37    85   167
10     320    46  4328   157
11     461    83  4129   178
12    2263   587   893  1108
13    2526   536   797   992
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NF-YB1_UN0427.1
XX
ID  NF-YB1_UN0427.1
XX
DE  UN0427.1 NF-YB1; from JASPAR
P0       a     c     g     t
1       50   170   116    80
2       50   229    79    58
3        6    25   371    14
4       11   358    32    15
5        6   393    15     2
6        7   321    79     9
7       17    23   356    20
8        2   397     6    11
9        9     5   396     6
10       5   383    19     9
11       8   384    19     5
12      57   112   198    49
13      65   146   129    76
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ONAC127_UN0428.1
XX
ID  ONAC127_UN0428.1
XX
DE  UN0428.1 ONAC127; from JASPAR
P0       a     c     g     t
1        1    11     3     5
2        7     4     2     7
3        0     0    20     0
4        0    20     0     0
5        0     0     0    20
6        0     0    20     0
7        0    20     0     0
8        0    20     0     0
9        3    11     4     2
10       3    10     3     4
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ONAC127_UN0429.1
XX
ID  ONAC127_UN0429.1
XX
DE  UN0429.1 ONAC127; from JASPAR
P0       a     c     g     t
1       21     9    15    10
2       11    10    27     7
3       49     0     1     5
4        0     3    46     6
5        1     0    48     6
6       50     0     0     5
7       53     1     1     0
8        1     1    52     1
9       51     0     2     2
10      12    15    11    17
11      14    15    21     5
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ONAC127_UN0430.1
XX
ID  ONAC127_UN0430.1
XX
DE  UN0430.1 ONAC127; from JASPAR
P0       a     c     g     t
1       20     8    20    20
2       16    16    22    14
3       65     0     0     3
4       67     0     0     1
5        4     0    62     2
6       57     2     7     2
7       61     5     1     1
8       63     0     5     0
9       58     1     3     6
10       3     7    56     2
11      16    14    30     8
12      25     9    19    15
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  bHLH172_UN0431.1
XX
ID  bHLH172_UN0431.1
XX
DE  UN0431.1 bHLH172; from JASPAR
P0       a     c     g     t
1     1263  1217  1242   582
2      629  1708   990   977
3        0     0  4304     0
4        0  4304     0     0
5     4304     0     0     0
6        0  4304     0     0
7        0     0  4304     0
8        0  4304     0     0
9     1428   876  1512   488
10     663  1791  1118   732
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d042907_UN0432.1
XX
ID  Zm00001d042907_UN0432.1
XX
DE  UN0432.1 Zm00001d042907; from JASPAR
P0       a     c     g     t
1      255   282   246   137
2      123   244   160   393
3       18    56   827    19
4      798    62    36    24
5       13   864    22    21
6       13    18   879    10
7       39   677    27   177
8       27   835    42    16
9      860    14    36    10
10      37   774    43    66
11     157   300   291   172
12     202   264   300   154
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d024644_UN0433.1
XX
ID  Zm00001d024644_UN0433.1
XX
DE  UN0433.1 Zm00001d024644; from JASPAR
P0       a     c     g     t
1      327   175   191   217
2      236   161   171   342
3       61   555   107   187
4       38   787    45    40
5       73    58    31   748
6       23    35    27   825
7      868    14    11    17
8        4    18     4   884
9        6   896     3     5
10      41   708    25   136
11     483   137   130   160
12     116   181   112   501
13     243   233   138   296
14     175   269   211   255
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Zm00001d013777_UN0434.1
XX
ID  Zm00001d013777_UN0434.1
XX
DE  UN0434.1 Zm00001d013777; from JASPAR
P0       a     c     g     t
1       99   534   264   207
2      111   527   277   189
3       15   113    30   946
4       40   992    59    13
5       19    32    22  1031
6       12  1069    10    13
7       21    24   990    69
8       19   945    30   110
9       11  1061    14    18
10      32   149    73   850
11     112   588   254   150
12     103   416   344   241
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NGA4_UN0687.1
XX
ID  NGA4_UN0687.1
XX
DE  UN0687.1 NGA4; from JASPAR
P0       a     c     g     t
1       80    72    42    93
2       62    43    47   135
3       32    54    79   122
4       23    19    29   216
5        6     6    10   265
6        5   276     4     2
7      280     3     1     3
8        3     2   280     2
9       16     1   267     3
10       3     6     2   276
11     157     5    91    34
12      91    58    53    85
13      89    45    49   104
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BHLH10_UN0688.1
XX
ID  BHLH10_UN0688.1
XX
DE  UN0688.1 BHLH10; from JASPAR
P0       a     c     g     t
1      187    97   154   169
2      127   219    84   177
3      106   303    59   139
4      133    62   142   270
5       48   412    42   105
6       31   492    17    67
7      308     1   293     5
8        9   589     7     2
9        3   601     2     1
10      10     1   593     3
11     487    78    16    26
12       7   591     4     5
13     537     7    48    15
14     111   360    62    74
15     139   181    51   236
16     178    79   174   176
17     134   185   105   183
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NFYB2_UN0689.1
XX
ID  NFYB2_UN0689.1
XX
DE  UN0689.1 NFYB2; from JASPAR
P0       a     c     g     t
1     2030  1475  1388  2546
2     1465  1097  2724  2153
3     6742   224   211   262
4        0  7151    21   267
5     6665    27   498   249
6        0  7439     0     0
7        0     0  7439     0
8      181   289    46  6923
9     1887   993  3708   851
10    1452  1000  1918  3069
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NFYC2_UN0690.1
XX
ID  NFYC2_UN0690.1
XX
DE  UN0690.1 NFYC2; from JASPAR
P0       a     c     g     t
1      633  2373   837   837
2     3455   259   443   523
3      169  4264    87   160
4      116    31  4428   105
5       79   105    23  4473
6      120    53  4421    86
7      236   103  3438   903
8      343  4053   174   110
9     3557   184   508   431
10    1466   882   948  1384
11    1473   966   876  1365
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
