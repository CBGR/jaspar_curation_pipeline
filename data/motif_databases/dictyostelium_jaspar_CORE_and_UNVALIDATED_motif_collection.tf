AC  gtaJ_UN0271.1
XX
ID  gtaJ_UN0271.1
XX
DE  UN0271.1 gtaJ; from JASPAR
P0       a     c     g     t
1      125   357   134   383
2      698   106   134    60
3       13     5   912    67
4      959    15     0    24
5       25    13    23   937
6        5   876    46    70
7       45    32   230   692
8      341   134   404   119
9      269   243   171   315
10     350   175   221   252
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  gtaS_UN0272.1
XX
ID  gtaS_UN0272.1
XX
DE  UN0272.1 gtaS; from JASPAR
P0       a     c     g     t
1        9     9     9   971
2      971     9     9     9
3        9     9   971     9
4      971     9     9     9
5        9     9     9   971
6        9   971     9     9
7        9     9     9   971
8      971     9     9     9
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
