AC  ABF1_MA0265.2
XX
ID  ABF1_MA0265.2
XX
DE  MA0265.2 ABF1; from JASPAR
P0       a     c     g     t
1      123    71    77   156
2      104    86    70   167
3      101    66    60   200
4       69    72    10   276
5        5   421     1     0
6        4     0   418     5
7        5     0     3   419
8      188    66    95    78
9       87    91    65   184
10     228    38    72    89
11     155    48    64   160
12     291    48    54    34
13     126     1   297     3
14       4    41     6   376
15       1     2   420     4
16     419     3     1     4
17       3   169    15   240
18     229    52    67    79
19     106    92    78   151
20     141    57    88   141
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ABF2_MA0266.1
XX
ID  ABF2_MA0266.1
XX
DE  MA0266.1 ABF2; from JASPAR
P0       a     c     g     t
1       18    39    25    18
2        9     9     2    80
3        0   100     0     0
4        0     0     0   100
5      100     0     0     0
6        0     0   100     0
7       95     2     2     2
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ACE2_MA0267.1
XX
ID  ACE2_MA0267.1
XX
DE  MA0267.1 ACE2; from JASPAR
P0       a     c     g     t
1       45    28    21     5
2        1    92     6     1
3        0   100     0     0
4       90    10     0     0
5        0     0   100     0
6        0   100     0     0
7       49    20    17    14
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ADR1_MA0268.1
XX
ID  ADR1_MA0268.1
XX
DE  MA0268.1 ADR1; from JASPAR
P0       a     c     g     t
1       41    24    13    22
2        0   100     0     0
3        0    85     0    15
4        0   100     0     0
5        0   100     0     0
6       64     6    20    10
7       34    51     6     9
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AFT1_MA0269.1
XX
ID  AFT1_MA0269.1
XX
DE  MA0269.1 AFT1; from JASPAR
P0       a     c     g     t
1      106   205   231   456
2      155   249   220   374
3      362   197   221   218
4      179   396   222   200
5      426   231   154   187
6      136    78   142   641
7      716    25    31   226
8      145    31   288   535
9       34    47    39   878
10     160    10   812    16
11      10   966    11    10
12     961     9    15    14
13       8   972     9    10
14      25   955     2    16
15      19   740    13   225
16     181   257   494    65
17     265   252   241   240
18     287   155   183   373
19     276   180    68   474
20     123   225   357   292
21     154   236   458   150
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AFT2_MA0270.1
XX
ID  AFT2_MA0270.1
XX
DE  MA0270.1 AFT2; from JASPAR
P0       a     c     g     t
1        8    45    27    20
2       70     0    30     0
3        0   100     0     0
4      100     0     0     0
5        0   100     0     0
6        0   100     0     0
7        0   100     0     0
8        9    42    27    21
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARG80_MA0271.1
XX
ID  ARG80_MA0271.1
XX
DE  MA0271.1 ARG80; from JASPAR
P0       a     c     g     t
1       89     0     0    69
2        0     0   301     0
3      161     0     0     0
4        0   301     0     0
5       44     0    79    69
6        0   256     0    20
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARG81_MA0272.1
XX
ID  ARG81_MA0272.1
XX
DE  MA0272.1 ARG81; from JASPAR
P0       a     c     g     t
1       59    12    83    44
2        6     0     4   154
3       20     0   239     0
4      169     0     0     0
5        0   271     0     1
6        1     0     0   164
7        0   279     0     0
8       59    59     4    64
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARO80_MA0273.1
XX
ID  ARO80_MA0273.1
XX
DE  MA0273.1 ARO80; from JASPAR
P0       a     c     g     t
1      102   155   531   210
2      330   193   301   174
3      126   232   317   323
4      407   253   203   136
5      390   193   229   186
6      310   198   135   355
7       83   154   336   426
8       40   744    30   185
9        5   231     8   753
10       5   987     1     5
11       1    22   975     1
12       1     5   990     2
13      15   359   295   329
14     670     4     6   318
15     690    68    60   181
16     105   222   372   299
17     269   119   193   418
18     353   210   205   230
19     257   217   178   347
20     236   103   349   310
21     278   252   170   298
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ARR1_MA0274.1
XX
ID  ARR1_MA0274.1
XX
DE  MA0274.1 ARR1; from JASPAR
P0       a     c     g     t
1      160     0     0     0
2       24    75    29    73
3       30    70     0    87
4        0     0     0   161
5       73     0   161     0
6      129    41    12     0
7      137     0    29     3
8       11     0    24   129
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ASG1_MA0275.1
XX
ID  ASG1_MA0275.1
XX
DE  MA0275.1 ASG1; from JASPAR
P0       a     c     g     t
1        0    76     0    24
2        0   100     0     0
3        0     0   100     0
4        0     0   100     0
5       72    11    16     0
6       53     0     0    47
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ASH1_MA0276.1
XX
ID  ASH1_MA0276.1
XX
DE  MA0276.1 ASH1; from JASPAR
P0       a     c     g     t
1        0   220     8    35
2        0   291     0     3
3       61    21   133    10
4       58    54   101    12
5      130     0    54     0
6        0    11     8   147
7       33   150     8    35
8       80     0    92    26
9        0     8   249    19
10       0     0   256    18
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  AZF1_MA0277.1
XX
ID  AZF1_MA0277.1
XX
DE  MA0277.1 AZF1; from JASPAR
P0       a     c     g     t
1       63    13    13    13
2      100     0     0     0
3      100     0     0     0
4       88    13     0     0
5       75     0    25     0
6        0     0   100     0
7       78    16     3     3
8       81     6     6     6
9       63    13    13    13
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  BAS1_MA0278.1
XX
ID  BAS1_MA0278.1
XX
DE  MA0278.1 BAS1; from JASPAR
P0       a     c     g     t
1       64   217   425   292
2      104   552   150   192
3      484   111   114   288
4       78   401   186   333
5      455    51   370   122
6      248    34   670    46
7       98   724   143    33
8       52   918     7    20
9      348   346   280    24
10      12     3   977     6
11     966     5    23     4
12      26     6   962     4
13       9    10     4   975
14      47   930     7    15
15     892    42    16    49
16     487   123   320    68
17     288   140   317   254
18     373   110    81   434
19     178   367   184   268
20     402   140   341   114
21     435   229   241    94
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CAD1_MA0279.2
XX
ID  CAD1_MA0279.2
XX
DE  MA0279.2 CAD1; from JASPAR
P0       a     c     g     t
1       29    18    15    25
2       15    17    20    35
3       12    14    11    50
4        5     5    68     9
5       26    58     3     0
6        1     1     0    85
7        1     0    18    68
8       85     0     1     1
9        6    64    11     6
10      10     0     3    74
11      82     3     0     2
12      85     0     1     1
13       5     1    14    67
14      25    31    14    17
15      29    23    19    16
16      28    13    23    23
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CAT8_MA0280.1
XX
ID  CAT8_MA0280.1
XX
DE  MA0280.1 CAT8; from JASPAR
P0       a     c     g     t
1        0   100     0     0
2        0   100     0     0
3        0     0   100     0
4        0     0   100     0
5       66    13    21     0
6       32    18    28    22
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CBF1_MA0281.2
XX
ID  CBF1_MA0281.2
XX
DE  MA0281.2 CBF1; from JASPAR
P0       a     c     g     t
1       73    44    46    93
2       75    33    85    63
3      137    14    73    32
4       12    20    55   169
5        1   254     1     0
6      253     0     2     1
7        1   243     2    10
8        7     0   249     0
9        0     0     0   256
10       0     1   255     0
11     215    24    11     6
12      17   146     9    84
13      67    93    21    75
14      83    50    47    76
15      79    33    66    78
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CEP3_MA0282.1
XX
ID  CEP3_MA0282.1
XX
DE  MA0282.1 CEP3; from JASPAR
P0       a     c     g     t
1        4    56     4    36
2        0     0     0   100
3        0   100     0     0
4        0     0   100     0
5        0     0   100     0
6       91     1     1     7
7       92     3     3     3
8       42    21    21    16
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CHA4_MA0283.1
XX
ID  CHA4_MA0283.1
XX
DE  MA0283.1 CHA4; from JASPAR
P0       a     c     g     t
1       22     5    68     5
2        0     0   100     0
3        0   100     0     0
4        0     0   100     0
5        0     0   100     0
6       71     0    28     0
7       22     1    76     1
8       48    10    10    31
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CIN5_MA0284.2
XX
ID  CIN5_MA0284.2
XX
DE  MA0284.2 CIN5; from JASPAR
P0       a     c     g     t
1      110    75    47   113
2       95    32   139    79
3      240    46    25    34
4        3     5     5   332
5        3     2    13   327
6      334     1     3     7
7        4   273    10    58
8      223    17    88    17
9        1     1     1   342
10     321    19     3     2
11     329     3     1    12
12      25    18    58   244
13      76   180    33    56
14     145    38    74    88
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CRZ1_MA0285.1
XX
ID  CRZ1_MA0285.1
XX
DE  MA0285.1 CRZ1; from JASPAR
P0       a     c     g     t
1       20    48    18    14
2       28    22    17    32
3       37    30    22    11
4       63    36     1     1
5        0     0   100     0
6        0   100     0     0
7        0    99     0     0
8       42    30     7    21
9        7    76     8     8
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CST6_MA0286.1
XX
ID  CST6_MA0286.1
XX
DE  MA0286.1 CST6; from JASPAR
P0       a     c     g     t
1       47    11    32    11
2        1     1     1    96
3        1     1    91     7
4      100     0     0     0
5        0   100     0     0
6        5     0    95     0
7        0     0     0   100
8       45    28     8    18
9       46    14    20    20
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CUP2_MA0287.1
XX
ID  CUP2_MA0287.1
XX
DE  MA0287.1 CUP2; from JASPAR
P0       a     c     g     t
1        0     7     0     2
2        9     0     0     0
3        0     0     9     0
4        2     7     0     0
5        6     0     3     0
6        4     1     4     0
7        9     0     0     0
8        9     0     0     0
9        7     0     2     0
10       4     0     0     5
11       2     0     7     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CUP9_MA0288.1
XX
ID  CUP9_MA0288.1
XX
DE  MA0288.1 CUP9; from JASPAR
P0       a     c     g     t
1        7    10     3    79
2        0     0   100     0
3       84     0     9     7
4        1    99     0     0
5       91     0     9     0
6        0    64     9    27
7       75    15     0    10
8       40     0     0    60
9       39    13     3    45
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DAL80_MA0289.1
XX
ID  DAL80_MA0289.1
XX
DE  MA0289.1 DAL80; from JASPAR
P0       a     c     g     t
1       11    67     6    17
2        0     0   100     0
3      100     0     0     0
4        0     0     0   100
5      100     0     0     0
6       90     1     1     7
7        3    19    75     3
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DAL81_MA0290.1
XX
ID  DAL81_MA0290.1
XX
DE  MA0290.1 DAL81; from JASPAR
P0       a     c     g     t
1      203     0     0     0
2      203     0     0     0
3      203     0     0     0
4      203     0     0     0
5        0     0   203     0
6        0   203     0     0
7        0   203     0     0
8        0     0   203     0
9        0   203     0     0
10       0     0   203     0
11       0     0   203     0
12       0     0   203     0
13       0   203     0     0
14       0     0   203     0
15       0     0   203     0
16       0     0   203     0
17     203     0     0     0
18       0     0     0   203
19       0     0     0   203
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DAL82_MA0291.1
XX
ID  DAL82_MA0291.1
XX
DE  MA0291.1 DAL82; from JASPAR
P0       a     c     g     t
1       56    14    14    14
2       53    11     5    32
3       39     3     3    55
4       26    11    42    21
5        0    26     0    74
6        0     0   100     0
7        0   100     0     0
8        0     0   100     0
9       11    68    11    11
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ECM22_MA0292.1
XX
ID  ECM22_MA0292.1
XX
DE  MA0292.1 ECM22; from JASPAR
P0       a     c     g     t
1       28    36     7    28
2       14     7     7    71
3        0   100     0     0
4        0   100     0     0
5        0    14    86     0
6        0     0    86    14
7       93     0     7     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ECM23_MA0293.1
XX
ID  ECM23_MA0293.1
XX
DE  MA0293.1 ECM23; from JASPAR
P0       a     c     g     t
1       31    25    23    21
2       33    25    22    20
3       72    11    10     7
4       10     2    86     2
5      100     0     0     0
6        3     0     0    97
7        1    90     3     7
8       14    19    14    53
9       30    23    21    26
10      32    25    25    19
11      30    26    22    21
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  EDS1_MA0294.1
XX
ID  EDS1_MA0294.1
XX
DE  MA0294.1 EDS1; from JASPAR
P0       a     c     g     t
1       14    52    14    19
2        0     0   100     0
3        0     0   100     0
4      100     0     0     0
5       76     0     0    24
6       48    24    10    19
7      100     0     0     0
8       82     1     1    15
9       15    20    11    54
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  FHL1_MA0295.1
XX
ID  FHL1_MA0295.1
XX
DE  MA0295.1 FHL1; from JASPAR
P0       a     c     g     t
1        0    10    90     0
2      100     0     0     0
3        0   100     0     0
4        0     0   100     0
5        0   100     0     0
6       90     0     0    10
7       48    28    18     8
8       55    15    15    15
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  FKH1_MA0296.1
XX
ID  FKH1_MA0296.1
XX
DE  MA0296.1 FKH1; from JASPAR
P0       a     c     g     t
1      177   287   207   326
2      247   192   334   225
3      520    84    98   297
4      526    76   129   267
5      365    73   150   411
6      119   146   133   600
7      146     4   847     1
8        7     3     0   987
9      959    37     0     1
10     977    11     1     9
11     990     1     2     5
12       1   925     1    71
13     990     1     3     4
14     874    39    16    69
15     671    49    70   208
16     248   191   407   152
17     374   158   296   170
18     284   184   330   200
19     205   116   452   225
20     421   192   248   137
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  FKH2_MA0297.1
XX
ID  FKH2_MA0297.1
XX
DE  MA0297.1 FKH2; from JASPAR
P0       a     c     g     t
1       13     0    88     0
2        0     0     0   100
3      100     0     0     0
4      100     0     0     0
5      100     0     0     0
6        0   100     0     0
7      100     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GAL4_MA0299.1
XX
ID  GAL4_MA0299.1
XX
DE  MA0299.1 GAL4; from JASPAR
P0       a     c     g     t
1        0  2779     0     0
2        0     0  2779     0
3        0   438  2273    97
4      778   655   927   717
5      292  1137  1151   498
6     1019   810  1002   390
7     2339     0   538    98
8      567  1296   810   194
9     1913   335     0   737
10     659   592  1744   204
11      96   767   494  1786
12    1170   517  1105   404
13       0  1437   729   824
14     235   392   419  2001
15       0  2779     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GAT1_MA0300.1
XX
ID  GAT1_MA0300.1
XX
DE  MA0300.1 GAT1; from JASPAR
P0       a     c     g     t
1       13    44    19    25
2       17    42     5    36
3        0     0   100     0
4      100     0     0     0
5        0     0     0   100
6      100     0     0     0
7      100     0     0     0
8        3     9    84     3
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GAT3_MA0301.1
XX
ID  GAT3_MA0301.1
XX
DE  MA0301.1 GAT3; from JASPAR
P0       a     c     g     t
1       65     9    20     6
2        8     2    88     1
3       92     0     0     8
4       12     0     1    87
5        3    94     1     2
6        6    27     4    63
7       51    19    18    12
8       28    33    21    17
9       32    25    21    21
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GAT4_MA0302.1
XX
ID  GAT4_MA0302.1
XX
DE  MA0302.1 GAT4; from JASPAR
P0       a     c     g     t
1       36    21    24    20
2       31    26    18    26
3       80     4    12     4
4        3     0    96     0
5       98     0     0     2
6        0     0     0    99
7        0    99     0     0
8        5    28     3    64
9       40    20    23    17
10      28    26    23    22
11      33    24    21    21
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GCN4_MA0303.2
XX
ID  GCN4_MA0303.2
XX
DE  MA0303.2 GCN4; from JASPAR
P0       a     c     g     t
1       30    15    26    19
2       45    15    19    11
3        0     0     0    90
4        1     0    84     5
5       87     0     2     1
6        0    62    28     0
7        1     0     2    87
8        7    78     5     0
9       82     3     2     3
10       6    20    13    51
11      16    31    13    30
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GCR1_MA0304.1
XX
ID  GCR1_MA0304.1
XX
DE  MA0304.1 GCR1; from JASPAR
P0       a     c     g     t
1      719    31    77  1822
2        0   142  2318     0
3        0    22  2438     0
4     2460     0     0     0
5     2157    10     0   294
6        9    13  2326   121
7      553  1703     0   240
8        0  1932    15   651
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GCR2_MA0305.1
XX
ID  GCR2_MA0305.1
XX
DE  MA0305.1 GCR2; from JASPAR
P0       a     c     g     t
1       33    33   149    16
2        0   279     0     0
3       11     0     1   152
4        0     0     0   169
5        0   260     0     8
6        0   269     1     0
7       61    52     6    64
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GIS1_MA0306.1
XX
ID  GIS1_MA0306.1
XX
DE  MA0306.1 GIS1; from JASPAR
P0       a     c     g     t
1       41    22    12    25
2        0    97     0     3
3        0   100     0     0
4        0   100     0     0
5        0    87     0    13
6        0     0     0   100
7       49     4    15    32
8       46    11    16    27
9       32    19    18    30
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GLN3_MA0307.1
XX
ID  GLN3_MA0307.1
XX
DE  MA0307.1 GLN3; from JASPAR
P0       a     c     g     t
1        0     0   100     0
2      100     0     0     0
3        0     0     0   100
4       74     0     0    26
5       61     0    27    12
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GSM1_MA0308.1
XX
ID  GSM1_MA0308.1
XX
DE  MA0308.1 GSM1; from JASPAR
P0       a     c     g     t
1      322   207   190   279
2      160   172   229   438
3      219   289   168   322
4      461   124   184   229
5      349   201   233   216
6      342   144   281   231
7      566    22    17   393
8      849     9    20   120
9      400   164   182   252
10       3   916    10    69
11       4   177    38   778
12       3   985     1     9
13       0   972    25     0
14       0    30   967     1
15     136     4   849     9
16     708    60   197    32
17     289   183   438    87
18     164   172   205   458
19     373   101   232   292
20     225   168   299   306
21     322   212   195   268
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  GZF3_MA0309.1
XX
ID  GZF3_MA0309.1
XX
DE  MA0309.1 GZF3; from JASPAR
P0       a     c     g     t
1       18    39     2    41
2        0     0   100     0
3      100     0     0     0
4        0     0     0   100
5      100     0     0     0
6       73     9     0    18
7        4    38    59     0
8       39    18    27    16
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HAC1_MA0310.1
XX
ID  HAC1_MA0310.1
XX
DE  MA0310.1 HAC1; from JASPAR
P0       a     c     g     t
1       24     6    56    15
2       68    32     0     0
3        0   100     0     0
4      100     0     0     0
5        0   100     0     0
6        0     0   100     0
7        0     0     0   100
8       27    18    41    14
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HAL9_MA0311.1
XX
ID  HAL9_MA0311.1
XX
DE  MA0311.1 HAL9; from JASPAR
P0       a     c     g     t
1        0    99     0     0
2        0     0    99     0
3        0     0    99     0
4       99     0     0     0
5       62     0    37     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HAP1_MA0312.2
XX
ID  HAP1_MA0312.2
XX
DE  MA0312.2 HAP1; from JASPAR
P0       a     c     g     t
1       16     4    21    15
2       12    11    20    13
3        1    55     0     0
4        0    56     0     0
5        0     0    56     0
6       56     0     0     0
7        0     0     0    56
8       56     0     0     0
9       27     7     4    18
10      31     9     6    10
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HAP2_MA0313.1
XX
ID  HAP2_MA0313.1
XX
DE  MA0313.1 HAP2; from JASPAR
P0       a     c     g     t
1        0     0     0  3939
2        0     0    40  3897
3       22     0  3922     0
4        0     0  3932     7
5      675  1619   131  1847
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HAP3_MA0314.2
XX
ID  HAP3_MA0314.2
XX
DE  MA0314.2 HAP3; from JASPAR
P0       a     c     g     t
1        9    18    32    56
2       14    61    19    21
3        4    17     0    94
4        2    20    84     9
5      111     1     0     3
6        0     0     0   115
7        0     2     0   113
8        0     1   114     0
9        0     0   115     0
10       6    67     4    38
11       5    42    18    50
12      28    49    26    12
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HAP5_MA0316.1
XX
ID  HAP5_MA0316.1
XX
DE  MA0316.1 HAP5; from JASPAR
P0       a     c     g     t
1      641   935  1033  1789
2      200  1914  2108    97
3      113  1513  2485   433
4      826  1137  2155   453
5     1817  1451   440  1002
6      127   637  1273  2389
7       98  3199   677   624
8        0   700     0  3713
9      537  1308  1904   793
10    4156     0     0     0
11       0     0     0  4156
12       0     0   234  3922
13     387     0  3788     0
14     361   497  3123   262
15       0  1730     0  2808
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HCM1_MA0317.1
XX
ID  HCM1_MA0317.1
XX
DE  MA0317.1 HCM1; from JASPAR
P0       a     c     g     t
1       51    16    25     8
2       27     8     4    61
3       67    32     1     1
4       95     5     0     0
5      100     0     0     0
6        1    86     0    13
7       95     2     2     2
8       57    17    11    14
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HMRA2_MA0318.1
XX
ID  HMRA2_MA0318.1
XX
DE  MA0318.1 HMRA2; from JASPAR
P0       a     c     g     t
1        6    67     6    21
2       54     0    46     0
3        4     0     0    96
4        0     0   100     0
5        0     0     0   100
6       93     0     0     7
7       88     2     2     9
8       40     8     8    44
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HSF1_MA0319.1
XX
ID  HSF1_MA0319.1
XX
DE  MA0319.1 HSF1; from JASPAR
P0       a     c     g     t
1       69    13    13     6
2        2     2     2    95
3       19     0    81     0
4        0     0   100     0
5      100     0     0     0
6      100     0     0     0
7        6    56    19    19
8       48    11    23    17
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  IME1_MA0320.1
XX
ID  IME1_MA0320.1
XX
DE  MA0320.1 IME1; from JASPAR
P0       a     c     g     t
1        0   301     0     0
2        0   232    61     0
3        0     0   301     0
4        0   301     0     0
5        0   189    75    15
6        0     0   301     0
7      161     0     0     0
8       16     0   266     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  INO2_MA0321.1
XX
ID  INO2_MA0321.1
XX
DE  MA0321.1 INO2; from JASPAR
P0       a     c     g     t
1       18    29   198     4
2        0   275     0     0
3      155     7     0     4
4        0    25     0   150
5        7     0   258     0
6        8     0    11   147
7        0    34   241     0
8      141    16    23     0
9      134     2     5    24
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  INO4_MA0322.1
XX
ID  INO4_MA0322.1
XX
DE  MA0322.1 INO4; from JASPAR
P0       a     c     g     t
1       29    46   162     5
2        0   236     0    19
3      138    14     0    17
4        0     0     0   168
5       10     0   256     0
6        0     0     0   167
7       17     0   241     0
8      146     7    23     0
9      142     7    26     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  IXR1_MA0323.1
XX
ID  IXR1_MA0323.1
XX
DE  MA0323.1 IXR1; from JASPAR
P0       a     c     g     t
1      161     0     0     0
2      121     0     0    37
3       68     0   112    28
4       28   228     0     6
5       59   187     0     0
6       59     0   187     0
7       28     0   169    37
8      121     0    71     0
9      121     0     0    37
10       0     0   301     0
11       0   301     0     0
12      28     0   244     0
13       6     0   285     0
14       0     0   112    99
15       0     0   244    28
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  LEU3_MA0324.1
XX
ID  LEU3_MA0324.1
XX
DE  MA0324.1 LEU3; from JASPAR
P0       a     c     g     t
1        0   100     0     0
2        0   100     0     0
3        0     0    56    43
4       20     0    35    45
5       26    34     0    41
6       47    12    16    25
7        0    61    39     0
8        0   100     0     0
9        0     0   100     0
10       0    32    68     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  LYS14_MA0325.1
XX
ID  LYS14_MA0325.1
XX
DE  MA0325.1 LYS14; from JASPAR
P0       a     c     g     t
1       21    49    21    10
2        4    82    10     4
3        0     0   100     0
4        0     0   100     0
5      100     0     0     0
6      100     0     0     0
7        6     0     0    94
8       13    13     7    68
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MAC1_MA0326.1
XX
ID  MAC1_MA0326.1
XX
DE  MA0326.1 MAC1; from JASPAR
P0       a     c     g     t
1      108     0     0  1632
2       89     0   219  1451
3        0     0     0  1714
4      115     0  1599     0
5        0  1714     0     0
6        0     0     0  1714
7        0  1714     0     0
8      958     0   655   113
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HMRA1_MA0327.1
XX
ID  HMRA1_MA0327.1
XX
DE  MA0327.1 HMRA1; from JASPAR
P0       a     c     g     t
1      101     0   101     0
2        0   203     0     0
3      203     0     0     0
4        0   203     0     0
5      203     0     0     0
6      203     0     0     0
7        0     0     0   203
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MATALPHA2_MA0328.2
XX
ID  MATALPHA2_MA0328.2
XX
DE  MA0328.2 MATALPHA2; from JASPAR
P0       a     c     g     t
1        0   815     0   184
2      455     0   535    10
3        0    62     0   938
4       26     0   973     0
5        0     0     0  1000
6      910     0     9    80
7      639    72    41   247
8      394    28     0   577
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MBP1_MA0329.1
XX
ID  MBP1_MA0329.1
XX
DE  MA0329.1 MBP1; from JASPAR
P0       a     c     g     t
1       55    10    18    17
2        0   100     0     0
3       15     0    85     0
4        0    88     0    12
5        0     2    98     0
6       35    17     9    39
7       33    30    18    18
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MBP1::SWI6_MA0330.1
XX
ID  MBP1::SWI6_MA0330.1
XX
DE  MA0330.1 MBP1::SWI6; from JASPAR
P0       a     c     g     t
1    15153     0   405  6881
2       82 21942   101   179
3       45   182 21983    23
4       31 19078   188  3065
5       59   182 21982    21
6      726  1491  1506 18880
7      365  9107   882 13437
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MCM1_MA0331.1
XX
ID  MCM1_MA0331.1
XX
DE  MA0331.1 MCM1; from JASPAR
P0       a     c     g     t
1     1692 11356   405   368
2       97 12733     0   971
3     3002  5897  3023  4309
4     6037  2220  3432  3681
5     7789   195   642  5363
6      936   237     0 12586
7     1175  3860   542  9116
8     6619  1176  5947  1957
9      612    32 13102     0
10     117   134 13351    16
11   10592   966   319  2201
12   12477   399   350   475
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MET28_MA0332.1
XX
ID  MET28_MA0332.1
XX
DE  MA0332.1 MET28; from JASPAR
P0       a     c     g     t
1        0   203     0     0
2        0     0     0   203
3        0     0   203     0
4        0     0     0   203
5        0     0   203     0
6        0     0   203     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MET31_MA0333.1
XX
ID  MET31_MA0333.1
XX
DE  MA0333.1 MET31; from JASPAR
P0       a     c     g     t
1       38    10    38    15
2       20    28    30    20
3       14    14    18    55
4        0     0   100     0
5        0     0     0   100
6        0     0   100     0
7        0     0   100     0
8        0   100     0     0
9       10     0    69    20
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MET32_MA0334.1
XX
ID  MET32_MA0334.1
XX
DE  MA0334.1 MET32; from JASPAR
P0       a     c     g     t
1       34    46    10    10
2       11     5    77     7
3        0    99     0     0
4        0   100     0     0
5       93     3     0     3
6        0    99     0     1
7       64    15    12     9
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MET4_MA0335.1
XX
ID  MET4_MA0335.1
XX
DE  MA0335.1 MET4; from JASPAR
P0       a     c     g     t
1      161     0     0     0
2      106    47    48     0
3        0   199     0    51
4        0     0     0   161
5        0     0   301     0
6        0     0     0   161
7        0     0   301     0
8        0     0   301     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MGA1_MA0336.1
XX
ID  MGA1_MA0336.1
XX
DE  MA0336.1 MGA1; from JASPAR
P0       a     c     g     t
1      384    91   206   317
2      305   124   376   194
3      240   268   318   172
4      367   171   306   154
5      252   280   194   273
6      116   204   230   448
7      787     3   198    11
8      192   110   166   531
9      701   197    98     2
10       5     2   989     2
11     990     2     3     2
12     990     2     2     5
13      12   815    15   156
14     844    39    82    32
15      68   505    70   355
16     203   180   195   419
17     301   353    84   260
18     291   268    97   342
19     441    70   229   257
20     440   168   105   285
21     342   295   216   144
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MIG1_MA0337.1
XX
ID  MIG1_MA0337.1
XX
DE  MA0337.1 MIG1; from JASPAR
P0       a     c     g     t
1       32    35    15    18
2        1    97     1     1
3        0   100     0     0
4        0   100     0     0
5        0   100     0     0
6       46     3    50     1
7        5    57    30     7
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MIG2_MA0338.1
XX
ID  MIG2_MA0338.1
XX
DE  MA0338.1 MIG2; from JASPAR
P0       a     c     g     t
1        1    96     2     1
2        0   100     0     0
3        0   100     0     0
4        0   100     0     0
5       36    11    52     1
6        5    77    13     6
7       36    24    24    15
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MIG3_MA0339.1
XX
ID  MIG3_MA0339.1
XX
DE  MA0339.1 MIG3; from JASPAR
P0       a     c     g     t
1        1    98     1     1
2        0    94     0     6
3        0   100     0     0
4        0   100     0     0
5       41     2    55     1
6        5    69    17     9
7       41    22    23    14
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MOT3_MA0340.1
XX
ID  MOT3_MA0340.1
XX
DE  MA0340.1 MOT3; from JASPAR
P0       a     c     g     t
1       67    67     0    67
2      203     0     0     0
3        0     0   203     0
4        0     0   203     0
5        0   101     0   101
6      203     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MSN2_MA0341.1
XX
ID  MSN2_MA0341.1
XX
DE  MA0341.1 MSN2; from JASPAR
P0       a     c     g     t
1       60     7    32     0
2        0     0   100     0
3        0     0   100     0
4        0     0   100     0
5        0     0   100     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MSN4_MA0342.1
XX
ID  MSN4_MA0342.1
XX
DE  MA0342.1 MSN4; from JASPAR
P0       a     c     g     t
1       83     0    16     0
2        0     0    99     0
3        0     0    99     0
4        0     0    99     0
5        0     0    99     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NDT80_MA0343.1
XX
ID  NDT80_MA0343.1
XX
DE  MA0343.1 NDT80; from JASPAR
P0       a     c     g     t
1      289   282   118   309
2      197   276   160   364
3      169   213   172   443
4      215   403   140   239
5      201   430   176   191
6       99   114   495   290
7      188    20   675   116
8      444   483    41    30
9       12   948     4    34
10     940     7    45     6
11      18   967     6     6
12     965     4     9    20
13     970     5    15     8
14     945     3    14    37
15     713    24   236    24
16     496   178    82   241
17     123   630    95   149
18     127   329   440   102
19     314   330   277    78
20     360   177   230   231
21     477   192   174   154
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NHP10_MA0344.1
XX
ID  NHP10_MA0344.1
XX
DE  MA0344.1 NHP10; from JASPAR
P0       a     c     g     t
1       20    10    60    10
2        8    78     8     8
3        0   100     0     0
4        0     0   100     0
5        0     0   100     0
6        0     0   100     0
7        0     0   100     0
8       68     8    18     8
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NRG1_MA0347.2
XX
ID  NRG1_MA0347.2
XX
DE  MA0347.2 NRG1; from JASPAR
P0       a     c     g     t
1       14     7    12    15
2       17    11     7    13
3       27    17     1     3
4       45     2     1     0
5        0     0    48     0
6        0     0    48     0
7        0     0    48     0
8        7     4     1    36
9        0    43     1     4
10       0    43     2     3
11      17     2    13    16
12      12    10     5    21
13      11     6     5    26
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  OAF1_MA0348.1
XX
ID  OAF1_MA0348.1
XX
DE  MA0348.1 OAF1; from JASPAR
P0       a     c     g     t
1       11    32    11    46
2        0   100     0     0
3        0     0   100     0
4        0     0   100     0
5       56     0    43     0
6        7     0    86     7
7       68     4     4    25
8       14     7     7    71
9       56    14    14    14
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  OPI1_MA0349.1
XX
ID  OPI1_MA0349.1
XX
DE  MA0349.1 OPI1; from JASPAR
P0       a     c     g     t
1       34   109    42    34
2        0     0   279     0
3      166     2     0     0
4      169     0     0     0
5        0   268     0     0
6        0   273     0     0
7       59    58    88    12
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TOD6_MA0350.1
XX
ID  TOD6_MA0350.1
XX
DE  MA0350.1 TOD6; from JASPAR
P0       a     c     g     t
1      362   188   218   230
2      144   213   369   273
3      101   261   370   266
4      239   327   206   226
5      369   116   235   278
6      212   366   133   288
7      649    50    96   203
8       67   396   436    98
9       20   784   193     1
10       2    29    46   921
11       2   991     2     3
12     975     0     1    21
13       1     1    18   978
14       0   978     1    18
15      52    19   869    57
16     135   655   186    22
17     181   300   383   135
18     232   282   109   376
19     187   280   111   421
20     306   213   172   308
21     201   150   320   327
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  DOT6_MA0351.1
XX
ID  DOT6_MA0351.1
XX
DE  MA0351.1 DOT6; from JASPAR
P0       a     c     g     t
1      278   135   231   354
2      194   250   266   289
3      156   332   223   287
4      131   249   268   351
5      239    85   400   273
6      141   480   145   232
7      516    59   138   285
8       79   420   392   107
9       29   770   197     3
10       6    52    83   858
11       1   990     4     2
12     972     0     1    25
13       1     0    27   969
14       0   968     1    28
15      36     9   876    78
16     223   575   177    22
17     280   265   252   202
18     188   298    72   441
19     163   435   208   192
20     153   371   183   290
21     185   149   202   462
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PDR1_MA0352.2
XX
ID  PDR1_MA0352.2
XX
DE  MA0352.2 PDR1; from JASPAR
P0       a     c     g     t
1       29    47    36    32
2       29    32    37    46
3       18    16     6   104
4        2     9     0   133
5        1   140     0     3
6        7   130     3     4
7       72     1    70     1
8        0   132     0    12
9        2     1   138     3
10       1     1   142     0
11     132     1     7     4
12     105     8    19    12
13      42    38    37    27
14      45    29    42    28
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PDR3_MA0353.1
XX
ID  PDR3_MA0353.1
XX
DE  MA0353.1 PDR3; from JASPAR
P0       a     c     g     t
1        0     0     0   203
2        0   203     0     0
3        0   203     0     0
4        0     0   203     0
5        0   203     0     0
6        0     0   203     0
7        0     0   203     0
8      203     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PDR8_MA0354.1
XX
ID  PDR8_MA0354.1
XX
DE  MA0354.1 PDR8; from JASPAR
P0       a     c     g     t
1       38    15    38     8
2        0   100     0     0
3        0     0   100     0
4        0     0   100     0
5      100     0     0     0
6        8     8    85     0
7      100     0     0     0
8       13     6     6    75
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHD1_MA0355.1
XX
ID  PHD1_MA0355.1
XX
DE  MA0355.1 PHD1; from JASPAR
P0       a     c     g     t
1       36    27    19    18
2        9    43    39     8
3       37    44    15     3
4        0    10     0    89
5        0     0    99     0
6        0    99     0     1
7       91     1     7     1
8        8    22    36    34
9       22    42    22    14
10      32    28    21    19
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHO2_MA0356.1
XX
ID  PHO2_MA0356.1
XX
DE  MA0356.1 PHO2; from JASPAR
P0       a     c     g     t
1       52     0     8    41
2       12     0     0    88
3      100     0     0     0
4       56     0     0    44
5       23     0     0    77
6       63     0     0    37
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PHO4_MA0357.1
XX
ID  PHO4_MA0357.1
XX
DE  MA0357.1 PHO4; from JASPAR
P0       a     c     g     t
1       94   393   507     4
2       97   900     0     1
3      967     1    28     2
4        0   911     1    85
5       85     1   911     0
6        2    28     1   967
7        1     0   900    97
8        4   507   393    94
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PUT3_MA0358.1
XX
ID  PUT3_MA0358.1
XX
DE  MA0358.1 PUT3; from JASPAR
P0       a     c     g     t
1        9    72     9     9
2        2    90     2     6
3        0   100     0     0
4        0     0   100     0
5        0     0   100     0
6        0     0   100     0
7       52    15    19    15
8       46    13    17    25
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RAP1_MA0359.2
XX
ID  RAP1_MA0359.2
XX
DE  MA0359.2 RAP1; from JASPAR
P0       a     c     g     t
1      101    17   134    84
2      200    10   105    21
3        3     4    39   290
4        4     5   317    10
5       13    27     4   292
6      214    36    51    35
7        9    54     0   273
8        6     0   327     3
9        0     0   318    18
10      48     5   281     2
11       1     5     2   328
12       3    18   264    51
13      27    48    24   237
14     146    22    68   100
15     106    50    58   122
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RDR1_MA0360.1
XX
ID  RDR1_MA0360.1
XX
DE  MA0360.1 RDR1; from JASPAR
P0       a     c     g     t
1       25    11    11    54
2        0     0   100     0
3        0   100     0     0
4        0     0   100     0
5        0     0   100     0
6      100     0     0     0
7       86     0     0    14
8       27    48    13    13
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RDS1_MA0361.1
XX
ID  RDS1_MA0361.1
XX
DE  MA0361.1 RDS1; from JASPAR
P0       a     c     g     t
1        1    91     1     7
2        0    11    89     0
3        0     0   100     0
4        0    84    16     0
5        0    95     5     0
6        0    11    89     0
7       32    32    32     5
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RDS2_MA0362.1
XX
ID  RDS2_MA0362.1
XX
DE  MA0362.1 RDS2; from JASPAR
P0       a     c     g     t
1       34    39    18     8
2        0     0     0   100
3        0   100     0     0
4        0     0   100     0
5        0     0   100     0
6       12     1    86     1
7       17     7    70     7
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  REB1_MA0363.2
XX
ID  REB1_MA0363.2
XX
DE  MA0363.2 REB1; from JASPAR
P0       a     c     g     t
1      212   114   109   256
2      159   167   146   219
3       86   177   113   315
4      281    79   281    50
5        8    20     5   658
6       28    28     7   628
7      679     1     3     8
8        2   684     2     3
9        4   681     5     1
10       3   681     3     4
11      11     5   578    97
12     104   133   377    77
13     296   244    58    93
14     133   178   118   262
15     186   115   147   243
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  REI1_MA0364.1
XX
ID  REI1_MA0364.1
XX
DE  MA0364.1 REI1; from JASPAR
P0       a     c     g     t
1        0    95     0     5
2        5    95     0     0
3        0   100     0     0
4        0   100     0     0
5        0     0     0   100
6        0     0    95     5
7       56     8    23    13
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RFX1_MA0365.1
XX
ID  RFX1_MA0365.1
XX
DE  MA0365.1 RFX1; from JASPAR
P0       a     c     g     t
1       12    27    42    19
2        0     0   100     0
3        0     0     0   100
4        0     0     0   100
5       31     0    69     0
6        0   100     0     0
7        2    48     2    48
8       88     4     4     4
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RGM1_MA0366.1
XX
ID  RGM1_MA0366.1
XX
DE  MA0366.1 RGM1; from JASPAR
P0       a     c     g     t
1      100     0     0     0
2        0     0   100     0
3        0     0   100     0
4        0     0   100     0
5       11    12    77     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RGT1_MA0367.1
XX
ID  RGT1_MA0367.1
XX
DE  MA0367.1 RGT1; from JASPAR
P0       a     c     g     t
1       42    13    23    22
2       34    10    34    22
3       37     0     0    63
4       22     2     0    77
5       27    15    17    41
6       30    18     4    48
7        0    12     0    88
8        0   100     0     0
9        0   100     0     0
10       4     0    89     8
11      32    13    33    22
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RIM101_MA0368.1
XX
ID  RIM101_MA0368.1
XX
DE  MA0368.1 RIM101; from JASPAR
P0       a     c     g     t
1        9    43     9    39
2        0     0   100     0
3        0   100     0     0
4        0   100     0     0
5      100     0     0     0
6      100     0     0     0
7        0     0   100     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RLM1_MA0369.1
XX
ID  RLM1_MA0369.1
XX
DE  MA0369.1 RLM1; from JASPAR
P0       a     c     g     t
1       10    10    10     8
2       14     2    16    10
3       11     0     1    30
4        0     0     5    37
5        0    42     0     0
6        0     0     0    42
7       42     0     0     0
8       16     0     0    26
9       22     0     0    20
10      23     0     0    19
11      32     0     0    10
12       0     0     0    42
13      42     0     0     0
14       0     0    42     0
15      27    14     0     1
16       4    15     2    20
17       7    16     3    16
18      10    13    10     7
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RME1_MA0370.1
XX
ID  RME1_MA0370.1
XX
DE  MA0370.1 RME1; from JASPAR
P0       a     c     g     t
1        0    52     0   131
2       16    96    33    69
3       16   100    49    58
4      160     0     0     0
5      119     0    27    22
6      139    29     0     2
7       27     0   244     0
8       34     0   229     0
9       92    39    35    22
10     126     0    19    18
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ROX1_MA0371.1
XX
ID  ROX1_MA0371.1
XX
DE  MA0371.1 ROX1; from JASPAR
P0       a     c     g     t
1        0     4     0     4
2        0     6     0     2
3        2     4     0     2
4        7     1     0     0
5        0     0     0     8
6        0     0     1     7
7        0     0     8     0
8        0     0     0     8
9        0     0     0     8
10       0     5     1     2
11       1     0     1     6
12       0     5     2     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RPH1_MA0372.1
XX
ID  RPH1_MA0372.1
XX
DE  MA0372.1 RPH1; from JASPAR
P0       a     c     g     t
1       53    14    14    19
2        0   100     0     0
3        0   100     0     0
4        0   100     0     0
5        0   100     0     0
6        0     0     0   100
7       63     1     7    28
8       74     7     7    13
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RPN4_MA0373.1
XX
ID  RPN4_MA0373.1
XX
DE  MA0373.1 RPN4; from JASPAR
P0       a     c     g     t
1       23     0    67     9
2        0     0   100     0
3        0     0    12    88
4        0     0   100     0
5        0     0   100     0
6        0   100     0     0
7       13     0    87     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RSC3_MA0374.1
XX
ID  RSC3_MA0374.1
XX
DE  MA0374.1 RSC3; from JASPAR
P0       a     c     g     t
1        0    91     5     4
2        0     0    99     0
3        0    99     0     0
4        0     0    99     0
5        6    55    18    20
6       19    30    37    14
7       23    26    34    18
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RSC30_MA0375.1
XX
ID  RSC30_MA0375.1
XX
DE  MA0375.1 RSC30; from JASPAR
P0       a     c     g     t
1       19    50    19    12
2       15    38    46     0
3        0    92     8     0
4        0     8    92     0
5        0    92     8     0
6        0     0   100     0
7        0    85    15     0
8        0    31    69     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RTG3_MA0376.1
XX
ID  RTG3_MA0376.1
XX
DE  MA0376.1 RTG3; from JASPAR
P0       a     c     g     t
1      393   145   330   130
2      220   147   267   364
3      357   322   207   112
4      164   176   295   363
5      156   121   250   472
6      534   113   203   148
7       34    27   662   275
8       19   953    19     7
9      905     5    81     7
10       5   949    16    28
11      28    16   949     5
12       7    81     5   905
13       7    19   953    19
14     275   662    27    34
15     152   332   156   358
16     111   270   187   430
17     398   122   276   202
18     275   257   296   171
19     323   165    33   477
20     249   396   108   245
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SFL1_MA0377.1
XX
ID  SFL1_MA0377.1
XX
DE  MA0377.1 SFL1; from JASPAR
P0       a     c     g     t
1      272   183   130   414
2      368   262   124   244
3      281   244   332   141
4      382   256   117   244
5      314    98   349   237
6      368   221   198   212
7      727    30   124   118
8      127   178   108   586
9      506   261   226     6
10      18    10   960    10
11     956    16     5    21
12     959     9    21     9
13      22    47   806   123
14     616   129   218    36
15     596   134    36   232
16     535   221    48   194
17     256   156   146   440
18     492    96   217   193
19     431    67    99   401
20     369   123   146   360
21     409   121    72   395
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SFP1_MA0378.1
XX
ID  SFP1_MA0378.1
XX
DE  MA0378.1 SFP1; from JASPAR
P0       a     c     g     t
1      427   159   161   250
2      202   293   194   309
3      289   174   192   343
4      250   217   293   238
5      353    77   297   271
6      533    42   342    82
7      782    73    71    73
8      898    36    46    18
9      904     9    50    35
10     855    14    42    86
11     541    34    16   408
12      86    42    14   855
13      35    50     9   904
14      18    46    36   898
15      73    71    73   782
16     126   405    91   376
17      82   354   103   459
18     327   201   165   306
19     115   428   154   301
20     217    83   374   324
21     216   169   405   208
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  MOT2_MA0379.1
XX
ID  MOT2_MA0379.1
XX
DE  MA0379.1 MOT2; from JASPAR
P0       a     c     g     t
1       93     2     0     5
2        0     0     0    99
3       99     0     0     0
4        0     0     0    99
5       99     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SIP4_MA0380.1
XX
ID  SIP4_MA0380.1
XX
DE  MA0380.1 SIP4; from JASPAR
P0       a     c     g     t
1        8    48    13    32
2        0    27     0    73
3        0    99     0     1
4        0   100     0     0
5        0     0   100     0
6       23     0    63    14
7       57    16    25     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SKN7_MA0381.1
XX
ID  SKN7_MA0381.1
XX
DE  MA0381.1 SKN7; from JASPAR
P0       a     c     g     t
1        0     0   100     0
2        0     0   100     0
3        0   100     0     0
4        0    84    16     0
5       49    15    37     0
6       21    18    28    32
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SKO1_MA0382.2
XX
ID  SKO1_MA0382.2
XX
DE  MA0382.2 SKO1; from JASPAR
P0       a     c     g     t
1       87    38    54    88
2       75    61    54    77
3       62    88    40    77
4       93    21    93    60
5      244     6    12     5
6        3     3     0   261
7        3     0   209    55
8      264     1     2     0
9        0   256     0    11
10       2     0   263     2
11       9     8     2   248
12     186    67     5     9
13     186     8    24    49
14      47    20    40   160
15      62    52    42   111
16      62    30    90    85
17      82    44    62    79
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SNT2_MA0384.1
XX
ID  SNT2_MA0384.1
XX
DE  MA0384.1 SNT2; from JASPAR
P0       a     c     g     t
1        0    89   313  2817
2        0     0  3136     0
3     1405   269  1721     0
4        0     0     0  3136
5     3120     0    35     0
6        7     0  3130     0
7        0  3136     0     0
8        0     0  3136     0
9        8  3072    75     0
10     168  2969     0     0
11     944   231  1666   589
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SOK2_MA0385.1
XX
ID  SOK2_MA0385.1
XX
DE  MA0385.1 SOK2; from JASPAR
P0       a     c     g     t
1       50    19    16    16
2       15    49    18    18
3       41    47     9     3
4        4     1     4    91
5        4     1    94     1
6        4    91     1     4
7       97     1     1     1
8        5     5    50    40
9       17    21    52    10
10      28    32    22    18
11      30    23    23    23
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SPT15_MA0386.1
XX
ID  SPT15_MA0386.1
XX
DE  MA0386.1 SPT15; from JASPAR
P0       a     c     g     t
1      277   250   386    84
2      337   191   147   323
3      253   345    88   313
4      160   200   154   484
5      545   139   154   160
6      115    98   701    84
7      287   198   266   247
8      332   110    27   529
9      907     7    25    59
10      12     7    23   955
11     981     2     4    12
12      38     1     5   954
13     970     2     6    20
14      44     4     3   947
15     883    10    24    81
16     186    62    94   657
17     264   122   129   483
18     137   380   369   112
19     266   228   384   121
20     350   176   172   301
21     248   193   270   286
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SPT2_MA0387.1
XX
ID  SPT2_MA0387.1
XX
DE  MA0387.1 SPT2; from JASPAR
P0       a     c     g     t
1       97     0     3    57
2       66    44    12    58
3        0     0     0   161
4        4     0     0   152
5      130     0    54     0
6      120    23    46     0
7       83    66    60     2
8        0     0   105   102
9      161     0     0     0
10     122     0    69     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SPT23_MA0388.1
XX
ID  SPT23_MA0388.1
XX
DE  MA0388.1 SPT23; from JASPAR
P0       a     c     g     t
1       95     0   119     0
2      160     0     0     0
3      143     0    22     2
4      134     0     0    23
5        1    36     0   135
6        0   138    67    46
7      108    37    18    16
8      157     0     6     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SRD1_MA0389.1
XX
ID  SRD1_MA0389.1
XX
DE  MA0389.1 SRD1; from JASPAR
P0       a     c     g     t
1       66     9    19     6
2        6     2    90     2
3       97     0     0     3
4       13     0     2    85
5        0    99     0     0
6        4    19     3    74
7       46    30    13    11
8       24    42    18    16
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  STB3_MA0390.1
XX
ID  STB3_MA0390.1
XX
DE  MA0390.1 STB3; from JASPAR
P0       a     c     g     t
1       98   168   546   185
2      166   173   202   457
3      182   415    57   343
4      194   379   107   318
5      507    59   189   243
6      547   120   151   181
7      776    35    71   116
8      499    27    33   440
9       99    11     2   886
10      53     5     1   939
11      10     4     2   982
12      13     0    15   970
13       0    64    20   915
14       3   990     2     4
15     965     7    25     2
16     150   587    84   177
17     129   120   137   613
18     166   326   200   306
19     308   253   125   312
20     233   170   315   280
21     226   182   296   293
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  STB4_MA0391.1
XX
ID  STB4_MA0391.1
XX
DE  MA0391.1 STB4; from JASPAR
P0       a     c     g     t
1        9    43     9    39
2        0     0     0   100
3        0   100     0     0
4        0     0   100     0
5        0     0   100     0
6      100     0     0     0
7       75     1     1    23
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  STB5_MA0392.1
XX
ID  STB5_MA0392.1
XX
DE  MA0392.1 STB5; from JASPAR
P0       a     c     g     t
1        0    99     0     0
2        0     0    99     0
3        0     0    99     0
4       23    19    26    32
5       23    23    47     8
6       19    25    12    45
7        6    12     6    76
8       96     1     1     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  STE12_MA0393.1
XX
ID  STE12_MA0393.1
XX
DE  MA0393.1 STE12; from JASPAR
P0       a     c     g     t
1        0  3261    96 25022
2     4297     0 24137   198
3    27122     0  1278     0
4    25290   167  2703     0
5    27887     0     0     0
6       98 27779     0    97
7    15301  3002  9387  2275
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  STP1_MA0394.1
XX
ID  STP1_MA0394.1
XX
DE  MA0394.1 STP1; from JASPAR
P0       a     c     g     t
1        4    82    33    87
2       49     0   192     0
3        0   279     0     0
4        0     0   266     4
5       12     0   255     0
6        0   279     0     0
7        4     0   119    87
8        4    94    70    57
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  STP2_MA0395.1
XX
ID  STP2_MA0395.1
XX
DE  MA0395.1 STP2; from JASPAR
P0       a     c     g     t
1      180   258   175   385
2      288   242   293   174
3      438   193   211   157
4      276   209   176   336
5      126   405   143   325
6      238   111   567    81
7      113    11   829    45
8       34   647     5   312
9        2     5   987     4
10       0   849   148     1
11       1   866   128     3
12       8    13   948    29
13      14   520     4   460
14     618    12   338    30
15      86   433   139   341
16     218   312   359   109
17     326   240   205   227
18     231   427   161   179
19     158   237   344   259
20     342   265   122   268
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  STP3_MA0396.1
XX
ID  STP3_MA0396.1
XX
DE  MA0396.1 STP3; from JASPAR
P0       a     c     g     t
1       25     9    48    18
2       18    42    21    19
3        0     0     0   100
4      100     0     0     0
5        0     0   100     0
6        0   100     0     0
7        0     0   100     0
8        0    64    18    18
9       45    17    19    20
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  STP4_MA0397.1
XX
ID  STP4_MA0397.1
XX
DE  MA0397.1 STP4; from JASPAR
P0       a     c     g     t
1       19    11    56    15
2       40    28    24     7
3        0     0     0   100
4      100     0     0     0
5        0     0   100     0
6        0   100     0     0
7        0     0   100     0
8        0    73    17    10
9       54    15    16    15
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SUM1_MA0398.1
XX
ID  SUM1_MA0398.1
XX
DE  MA0398.1 SUM1; from JASPAR
P0       a     c     g     t
1       50    14    14    22
2       52     8     8    32
3       62     2     2    34
4      100     0     0     0
5        0     0     0   100
6        8     0     0    92
7       24     0     0    76
8       28     5     5    61
9       15    15    11    59
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SUT1_MA0399.1
XX
ID  SUT1_MA0399.1
XX
DE  MA0399.1 SUT1; from JASPAR
P0       a     c     g     t
1       23   142    36    29
2       48     5   185     0
3        0   266     0     0
4        8    61   188     1
5        0    46   226     0
6       49    35   151     0
7       44    14   170     4
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SUT2_MA0400.1
XX
ID  SUT2_MA0400.1
XX
DE  MA0400.1 SUT2; from JASPAR
P0       a     c     g     t
1      333   232   112   321
2      225   206   264   303
3      269   218   277   233
4      354   200   100   345
5      248   216   153   381
6      522    85   287   104
7      940     6    12    40
8      971     0    11    16
9        0   709   287     2
10       1    16     0   981
11       2   992     1     2
12       1   984    12     1
13       2     1   993     2
14     789    16   168    24
15     523    80   257   137
16     375   252   154   217
17     338   214   168   279
18     279   204   209   306
19     163   253   252   330
20     281   136   244   338
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SWI4_MA0401.1
XX
ID  SWI4_MA0401.1
XX
DE  MA0401.1 SWI4; from JASPAR
P0       a     c     g     t
1       62     8    15    15
2        0   100     0     0
3       15     0    85     0
4        0   100     0     0
5        0     0   100     0
6      100     0     0     0
7       94     2     2     2
8       62     8     8    23
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SWI5_MA0402.1
XX
ID  SWI5_MA0402.1
XX
DE  MA0402.1 SWI5; from JASPAR
P0       a     c     g     t
1        8     8    23    62
2        0     0   100     0
3        0   100     0     0
4        0     0     0   100
5        0     0   100     0
6        0     0   100     0
7        4     4    27    65
8       25    25    10    40
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TBF1_MA0403.2
XX
ID  TBF1_MA0403.2
XX
DE  MA0403.2 TBF1; from JASPAR
P0       a     c     g     t
1       47    30    20    63
2       59    38    15    48
3       48    40    14    58
4       43    22    29    66
5      131     6    10    13
6      145     1    12     2
7       11   149     0     0
8        1   155     1     3
9        0   159     1     0
10       0     0     1   159
11     139     4    16     1
12     110    16    17    17
13      50    47    26    37
14      50    28    25    57
15      52    28    15    65
16      52    15    27    66
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TBS1_MA0404.1
XX
ID  TBS1_MA0404.1
XX
DE  MA0404.1 TBS1; from JASPAR
P0       a     c     g     t
1       12    78     5     5
2        3     3    90     3
3        2     2    95     2
4       75     8     8     8
5       20    13    13    53
6        0   100     0     0
7        0   100     0     0
8        8     8    75     8
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TEA1_MA0405.1
XX
ID  TEA1_MA0405.1
XX
DE  MA0405.1 TEA1; from JASPAR
P0       a     c     g     t
1        5     5    75    15
2        0   100     0     0
3        0     0   100     0
4        0    40    60     0
5       50     0    50     0
6        0    80    20     0
7       60     0    10    30
8       15    20     5    60
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TEC1_MA0406.1
XX
ID  TEC1_MA0406.1
XX
DE  MA0406.1 TEC1; from JASPAR
P0       a     c     g     t
1       52     7    34     7
2        0    95     5     0
3      100     0     0     0
4        5     0     0    95
5        0     0     0   100
6        0    91     9     0
7        0    68     0    32
8       10    47    15    28
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  THI2_MA0407.1
XX
ID  THI2_MA0407.1
XX
DE  MA0407.1 THI2; from JASPAR
P0       a     c     g     t
1      103     0  1162     0
2        0     0  1265     0
3      680   573     0    61
4     1265     0     0     0
5     1265     0     0     0
6        0  1024     0   241
7        0   543     0   805
8       71   599   571    90
9      438     0     0   826
10     921     0     0   344
11    1265     0     0     0
12       0     0  1265     0
13    1265     0     0     0
14     813     0   451     0
15     129  1068    89     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TOS8_MA0408.1
XX
ID  TOS8_MA0408.1
XX
DE  MA0408.1 TOS8; from JASPAR
P0       a     c     g     t
1       28    47    20     5
2        1    16     1    82
3        0     0   100     0
4        0     0     0   100
5        0   100     0     0
6      100     0     0     0
7       64     7     7    22
8       50    15    15    19
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TYE7_MA0409.1
XX
ID  TYE7_MA0409.1
XX
DE  MA0409.1 TYE7; from JASPAR
P0       a     c     g     t
1        3    93     3     3
2      100     0     0     0
3        0   100     0     0
4        0    10    90     0
5        0     0     0   100
6        0     0   100     0
7       78     8     8     8
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  UGA3_MA0410.1
XX
ID  UGA3_MA0410.1
XX
DE  MA0410.1 UGA3; from JASPAR
P0       a     c     g     t
1       10    44    27    19
2        4    28    63     4
3        0     0   100     0
4        0   100     0     0
5        0     0   100     0
6        0     0   100     0
7        2    27    69     2
8       85     2     2    10
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  UPC2_MA0411.1
XX
ID  UPC2_MA0411.1
XX
DE  MA0411.1 UPC2; from JASPAR
P0       a     c     g     t
1       16    25    19    40
2       59     3    28     9
3       38     0     0    62
4      100     0     0     0
5        0   100     0     0
6        1     0    99     0
7       83     5     9     4
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  UME6_MA0412.2
XX
ID  UME6_MA0412.2
XX
DE  MA0412.2 UME6; from JASPAR
P0       a     c     g     t
1       97    30    28    63
2      101    17    25    75
3       64     9    13   132
4        2     8     6   202
5      194     1     3    20
6       10     9   198     1
7        0   216     1     1
8        0   218     0     0
9        3     0   215     0
10       0   217     1     0
11       2   185    12    19
12      32    82    97     7
13     160    16    22    20
14      63    47    66    42
15      55    56    86    21
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  USV1_MA0413.1
XX
ID  USV1_MA0413.1
XX
DE  MA0413.1 USV1; from JASPAR
P0       a     c     g     t
1      269   223   250   255
2      306   182   216   294
3      342   168   206   283
4      131   193   146   528
5      133    55   157   652
6      318   551   101    28
7        1   967    26     3
8        0   977     0    20
9        3   993     1     1
10       1   981     0    16
11       0     6     3   989
12       0     2   869   126
13     956     0    36     5
14     850    92    21    35
15     174   257   162   405
16     233   235    93   438
17     212   191   197   399
18     181   185   321   311
19     205   100   265   428
20     219   297   365   116
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  XBP1_MA0414.1
XX
ID  XBP1_MA0414.1
XX
DE  MA0414.1 XBP1; from JASPAR
P0       a     c     g     t
1       12    51     7    31
2        0     0     0   100
3        0   100     0     0
4        0     0   100     0
5      100     0     0     0
6       40     8    50     3
7       31    22    31    15
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YAP1_MA0415.1
XX
ID  YAP1_MA0415.1
XX
DE  MA0415.1 YAP1; from JASPAR
P0       a     c     g     t
1      297   261   211   230
2      143   225   313   318
3      148   128   305   418
4      240   121   201   436
5      212   210   327   249
6      397   528    50    23
7        5     3     8   981
8        8    13     6   971
9      976     3     9    10
10       1   900    36    60
11      60    36   900     1
12      10     9     3   976
13     971     6    13     8
14     981     8     3     5
15      35    28   516   418
16     320   332   228   118
17     182   273   154   390
18     247   308   204   239
19     248   254   309   187
20     199   195   177   426
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YAP3_MA0416.1
XX
ID  YAP3_MA0416.1
XX
DE  MA0416.1 YAP3; from JASPAR
P0       a     c     g     t
1       65    19    12     4
2        0     0     0   100
3        0     0     0   100
4      100     0     0     0
5        0   100     0     0
6        0     0   100     0
7        2     2     2    94
8       50    12    19    19
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YAP5_MA0417.1
XX
ID  YAP5_MA0417.1
XX
DE  MA0417.1 YAP5; from JASPAR
P0       a     c     g     t
1      122     0     0    34
2      124     0    67     0
3       86     0   136     0
4        0   264     0    17
5      161     0     0     0
6       19     0     0   137
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YAP6_MA0418.1
XX
ID  YAP6_MA0418.1
XX
DE  MA0418.1 YAP6; from JASPAR
P0       a     c     g     t
1      174   297   371   156
2      228   141   587    41
3      134    92   332   440
4      114   434   109   342
5      252   231    72   444
6       43   114   646   195
7      491   412    36    58
8       27    23    56   892
9       25    21   112   840
10     889    32    49    28
11      38   555   103   302
12     172    48   722    56
13      67    85    37   809
14     647   193    64    95
15     829    62    72    34
16     162    76   481   278
17     228   474   142   155
18     337    85   428   148
19     430   127   302   139
20     166   439   243   150
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YAP7_MA0419.1
XX
ID  YAP7_MA0419.1
XX
DE  MA0419.1 YAP7; from JASPAR
P0       a     c     g     t
1     7195  2582   195   869
2      195     0     0 10410
3        0     0  2132  8375
4    10172    98    97   172
5     1260  1168  8333     0
6        0    97     0 10436
7     7781  2526   198     0
8    10507     0     0     0
9       98     0  6311  4323
10    2246  7759   509   394
11    6606  1126  2313  1229
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ERT1_MA0420.1
XX
ID  ERT1_MA0420.1
XX
DE  MA0420.1 ERT1; from JASPAR
P0       a     c     g     t
1       51    10    22    16
2        6    47     6    41
3        0   100     0     0
4        0     0   100     0
5        0     0   100     0
6      100     0     0     0
7      100     0     0     0
8        4    63     4    28
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NSI1_MA0421.1
XX
ID  NSI1_MA0421.1
XX
DE  MA0421.1 NSI1; from JASPAR
P0       a     c     g     t
1      174     0   274  1457
2        0     0    40  1817
3        0   103     0  1784
4     1832     0     0     0
5        0  1832     0     0
6        0  1832     0     0
7      118  1739     0     0
8        0     0  1832     0
9       49     0  1784     0
10     779   841   117   291
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  URC2_MA0422.1
XX
ID  URC2_MA0422.1
XX
DE  MA0422.1 URC2; from JASPAR
P0       a     c     g     t
1       25    38    11    27
2        0   100     0     0
3        0     0   100     0
4        0     0   100     0
5       74     9    17     0
6       23    17    46    14
7       56     0     0    43
8       14     0     0    85
9       94     6     0     0
10      36    17    15    32
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YER130C_MA0423.1
XX
ID  YER130C_MA0423.1
XX
DE  MA0423.1 YER130C; from JASPAR
P0       a     c     g     t
1       35    28    23    13
2        0    97     1     2
3        0    91     0     9
4        0   100     0     0
5        0   100     0     0
6        0     0     0   100
7       60     0     8    32
8       21     0     4    75
9       32    28     5    34
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YER184C_MA0424.1
XX
ID  YER184C_MA0424.1
XX
DE  MA0424.1 YER184C; from JASPAR
P0       a     c     g     t
1       28    37     6    28
2        2    17    10    71
3        0    77    15     8
4        0   100     0     0
5        0     0   100     0
6        0     0   100     0
7       69    31     0     0
8       46    15    23    15
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YGR067C_MA0425.1
XX
ID  YGR067C_MA0425.1
XX
DE  MA0425.1 YGR067C; from JASPAR
P0       a     c     g     t
1       36    24    27    14
2        0   100     0     0
3        0    99     0     1
4        0   100     0     0
5        1    99     0     0
6       41     0    31    28
7        0    98     1     1
8       27    26    17    31
9       23    26    20    31
10      24    26    20    30
11      25    24    25    26
12      25    22    26    27
13      22    26    26    26
14      26    24    26    25
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YHP1_MA0426.1
XX
ID  YHP1_MA0426.1
XX
DE  MA0426.1 YHP1; from JASPAR
P0       a     c     g     t
1        0     0     0   203
2      203     0     0     0
3      203     0     0     0
4        0     0     0   203
5        0     0     0   203
6        0     0   203     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YKL222C_MA0428.1
XX
ID  YKL222C_MA0428.1
XX
DE  MA0428.1 YKL222C; from JASPAR
P0       a     c     g     t
1       48    23    15    15
2       69    10    19     2
3        0   100     0     0
4        0     0   100     0
5        0     0   100     0
6       92     0     8     0
7       52     2    44     2
8       65     6     6    23
9       10    10    10    69
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YLL054C_MA0429.1
XX
ID  YLL054C_MA0429.1
XX
DE  MA0429.1 YLL054C; from JASPAR
P0       a     c     g     t
1        0   100     0     0
2        0     0   100     0
3        0     0   100     0
4       21    64    14     0
5        9    66    16     9
6       13     5    77     5
7       56    14     7    21
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YLR278C_MA0430.1
XX
ID  YLR278C_MA0430.1
XX
DE  MA0430.1 YLR278C; from JASPAR
P0       a     c     g     t
1       13    42    13    31
2        0   100     0     0
3        0     0   100     0
4        0     0   100     0
5       96     0     4     0
6        4    14    82     0
7       23     2     5    70
8       27     5     5    63
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  TDA9_MA0431.1
XX
ID  TDA9_MA0431.1
XX
DE  MA0431.1 TDA9; from JASPAR
P0       a     c     g     t
1       32    24    23    21
2        0   100     0     0
3        0   100     0     0
4        0   100     0     0
5        0   100     0     0
6       30     0    36    34
7        0   100     0     0
8       44    16     3    38
9       19    35    15    31
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YNR063W_MA0432.1
XX
ID  YNR063W_MA0432.1
XX
DE  MA0432.1 YNR063W; from JASPAR
P0       a     c     g     t
1        8     8    18    68
2        0   100     0     0
3        0     0   100     0
4        0     0   100     0
5      100     0     0     0
6       10     0    80    10
7       90     0     0    10
8       33     3     3    63
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YOX1_MA0433.1
XX
ID  YOX1_MA0433.1
XX
DE  MA0433.1 YOX1; from JASPAR
P0       a     c     g     t
1        1    25     7    66
2        0     0     0   100
3      100     0     0     0
4      100     0     0     0
5        0     0     0   100
6        0     0     0   100
7       62     3    15    21
8       54    13    13    19
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YPR013C_MA0434.1
XX
ID  YPR013C_MA0434.1
XX
DE  MA0434.1 YPR013C; from JASPAR
P0       a     c     g     t
1       11    36    11    41
2        0     0   100     0
3        9    26     0    65
4       63    26     0    11
5       40     0    60     0
6      100     0     0     0
7        0     0     0   100
8        0   100     0     0
9       36    30    17    17
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YPR015C_MA0435.1
XX
ID  YPR015C_MA0435.1
XX
DE  MA0435.1 YPR015C; from JASPAR
P0       a     c     g     t
1       75   242   144   537
2      158   204   341   295
3      382   170   173   274
4      376   240   137   245
5      274    50   462   212
6      552   169   240    37
7       16   747    17   218
8        4     4   988     2
9        2    16     3   978
10     983     7     3     5
11     884     1   109     4
12     986     2     4     6
13       5     2     2   989
14       4   983     1     9
15     326   575    55    43
16      86   221    77   613
17     258   190   196   354
18     450   110   173   266
19     281   396    62   260
20     333   330    68   266
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YPR022C_MA0436.1
XX
ID  YPR022C_MA0436.1
XX
DE  MA0436.1 YPR022C; from JASPAR
P0       a     c     g     t
1       15    66     0    20
2        0   100     0     0
3        0   100     0     0
4        0   100     0     0
5      100     0     0     0
6        0   100     0     0
7       23    26    34    18
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YPR196W_MA0437.1
XX
ID  YPR196W_MA0437.1
XX
DE  MA0437.1 YPR196W; from JASPAR
P0       a     c     g     t
1       61     0    26    13
2       32     0     0    68
3       17     0     0    83
4       24     4     2    69
5       17    44    11    28
6       12    32     7    49
7        0    93     7     0
8        0   100     0     0
9        3     0    93     5
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YRM1_MA0438.1
XX
ID  YRM1_MA0438.1
XX
DE  MA0438.1 YRM1; from JASPAR
P0       a     c     g     t
1       51    15    23    11
2        2    94     2     2
3        0     0   100     0
4        0     0   100     0
5      100     0     0     0
6       60    12    16    12
7       93     1     1     5
8       12     8     4    76
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  YRR1_MA0439.1
XX
ID  YRR1_MA0439.1
XX
DE  MA0439.1 YRR1; from JASPAR
P0       a     c     g     t
1       21    42    22    15
2       16    16    10    57
3       22     8     4    66
4       89     1     2     8
5       19     9     4    68
6       28    33     8    32
7        4    22     7    68
8        0   100     0     0
9        0   100     0     0
10       3     0    95     2
11       6    41    14    39
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ZAP1_MA0440.1
XX
ID  ZAP1_MA0440.1
XX
DE  MA0440.1 ZAP1; from JASPAR
P0       a     c     g     t
1      169     0     0     0
2        0   279     0     0
3        0   279     0     0
4        0    97     0   107
5        0     0     0   169
6      107    77    17     0
7      169     0     0     0
8      119    17    57     0
9        0     0   279     0
10       0     0   279     0
11       0     0     0   169
12      21    97    37    58
13     119     0    57     9
14       9    17     0   145
15      46     0   198     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ZMS1_MA0441.1
XX
ID  ZMS1_MA0441.1
XX
DE  MA0441.1 ZMS1; from JASPAR
P0       a     c     g     t
1       15    15    15    55
2       31    11    11    46
3        0   100     0     0
4        0   100     0     0
5        0   100     0     0
6        0   100     0     0
7       19     4    54    24
8        5    85     5     5
9       50    10    10    30
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NCU00019_MA0929.1
XX
ID  NCU00019_MA0929.1
XX
DE  MA0929.1 NCU00019; from JASPAR
P0       a     c     g     t
1      326   225   225   225
2      225   225   225   326
3      320   127   230   323
4      150    55   741    55
5      111     0     0   889
6      803   101     0    96
7      999     0     0     0
8      898     0     0   101
9       26   521    26   428
10     923    26    26    26
11     438   219   124   219
12     413   196   196   196
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  rst2_MA1431.1
XX
ID  rst2_MA1431.1
XX
DE  MA1431.1 rst2; from JASPAR
P0       a     c     g     t
1      256   163   193   386
2      391   188   204   214
3       22   937    13    26
4        7   880     4   108
5        0   996     2     1
6        6   990     0     2
7      208    40   529   221
8       56   896     2    44
9      382   216   131   269
10     165   379   141   313
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  cre-1_MA1432.1
XX
ID  cre-1_MA1432.1
XX
DE  MA1432.1 cre-1; from JASPAR
P0       a     c     g     t
1        8   985     0     5
2        0   758     0   240
3       65   872    39    23
4        0   999     0     0
5      499     0   489    11
6      139   376   252   230
7      284   230   244   240
8      280   298   261   158
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  msn-1_MA1433.1
XX
ID  msn-1_MA1433.1
XX
DE  MA1433.1 msn-1; from JASPAR
P0       a     c     g     t
1      238   442   231    88
2        5   971     5    17
3        0   933     0    65
4        0   988     0    11
5        5   884     0   109
6       22    16    11   950
7      246     0   447   305
8      547    42   147   263
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NCU02182_MA1434.1
XX
ID  NCU02182_MA1434.1
XX
DE  MA1434.1 NCU02182; from JASPAR
P0       a     c     g     t
1      469     0     0   530
2      999     0     0     0
3       27   972     0     0
4        0   999     0     0
5        0   999     0     0
6      972    27     0     0
7       12   371   269   346
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  nit-4_MA1435.1
XX
ID  nit-4_MA1435.1
XX
DE  MA1435.1 nit-4; from JASPAR
P0       a     c     g     t
1      210   310   204   274
2       11    70    11   906
3        0   999     0     0
4        0   884     0   115
5        0   115   884     0
6        0   999     0     0
7        0     0   999     0
8        0     0   999     0
9      388   181   320   110
10     238   238   285   238
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  nuc-1_MA1436.1
XX
ID  nuc-1_MA1436.1
XX
DE  MA1436.1 nuc-1; from JASPAR
P0       a     c     g     t
1      250   549    50   150
2        0   998     0     0
3      934    64     0     0
4        0   999     0     0
5        0     0   999     0
6        0     0     0   999
7        0     0   999     0
8      272   454    45   227
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  wc-1_MA1437.1
XX
ID  wc-1_MA1437.1
XX
DE  MA1437.1 wc-1; from JASPAR
P0       a     c     g     t
1      349   449   100   100
2      200   799     0     0
3        0     0   999     0
4      999     0     0     0
5        0     0     0   999
6        0   999     0     0
7        0     0   805   193
8      624   208    83    83
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
