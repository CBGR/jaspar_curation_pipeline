AC  v1g160868_UN0273.1
XX
ID  v1g160868_UN0273.1
XX
DE  UN0273.1 v1g160868; from JASPAR
P0       a     c     g     t
1      280   266   173   279
2       93    93    93   718
3      707   190     0   102
4      916     0     0    83
5        0     0     0   999
6        0     0   102   897
7      806   102     0    90
8      999     0     0     0
9      159    76    76   687
10     258   156   257   327
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
