AC  ceh-74_UN0105.1
XX
ID  ceh-74_UN0105.1
XX
DE  UN0105.1 ceh-74; from JASPAR
P0       a     c     g     t
1      300   201   201   296
2      318   126   429   126
3      170    75   677    75
4        0    93     0   906
5        0     0   999     0
6      999     0     0     0
7        0     0   999     0
8        0     0   999     0
9      239    48   366   345
10     332   314   229   124
11     271   267   174   286
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  lin-28_UN0106.1
XX
ID  lin-28_UN0106.1
XX
DE  UN0106.1 lin-28; from JASPAR
P0       a     c     g     t
1      971     9     9     9
2      971     9     9     9
3      971     9     9     9
4      971     9     9     9
5        9     9     9   971
6      971     9     9     9
7      971     9     9     9
8      971     9     9     9
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
