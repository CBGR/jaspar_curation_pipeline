AC  Phyra84400_UN0304.1
XX
ID  Phyra84400_UN0304.1
XX
DE  UN0304.1 Phyra84400; from JASPAR
P0       a     c     g     t
1      145   527   107   219
2      172   109   652    65
3      179   556    38   224
4       41   923    13    21
5      127    27   510   335
6       49   232    59   657
7        0   999     0     0
8       20     2   899    76
9       39    53   834    72
10     682   189    76    51
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
