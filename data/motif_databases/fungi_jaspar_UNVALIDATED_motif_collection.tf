AC  rsv2_UN0081.1
XX
ID  rsv2_UN0081.1
XX
DE  UN0081.1 rsv2; from JASPAR
P0       a     c     g     t
1      222   120   465   191
2      117    91   733    56
3      160   100   258   481
4        0     2   993     3
5        3     0   995     0
6       11   925    19    44
7      207    71   643    77
8      407   198   181   212
9      287   230   230   252
10     275   214   232   278
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  HCAG_04779_UN0082.1
XX
ID  HCAG_04779_UN0082.1
XX
DE  UN0082.1 HCAG_04779; from JASPAR
P0       a     c     g     t
1      250   126   193   429
2      116   218   516   149
3      851   122     5    20
4        9     2   981     5
5        0   998     0     1
6        0   997     1     0
7        2    84   533   379
8        9   963     2    23
9      574   151   141   132
10     279   233   151   334
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ada-1_UN0083.1
XX
ID  ada-1_UN0083.1
XX
DE  UN0083.1 ada-1; from JASPAR
P0       a     c     g     t
1      304   231   231   231
2      340   274   110   275
3       59   582   224   133
4      999     0     0     0
5        0   999     0     0
6        0     0   999     0
7        0     0     0   999
8        0    72   927     0
9       90    18   272   618
10     139   581   139   139
11     351   190   268   190
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ada-3_UN0084.1
XX
ID  ada-3_UN0084.1
XX
DE  UN0084.1 ada-3; from JASPAR
P0       a     c     g     t
1      561   157   157   122
2      154   678    71    95
3       40    20   357   581
4        0   953    46     0
5        0    45   953     0
6        0   990     9     0
7        0    18   971     9
8      690   154    83    71
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  clr-2_UN0085.1
XX
ID  clr-2_UN0085.1
XX
DE  UN0085.1 clr-2; from JASPAR
P0       a     c     g     t
1      338   171   258   231
2      317   174   250   257
3      238   247   240   273
4      229   271   140   359
5      353   190   199   256
6        4   985     0     9
7        9    57   932     0
8       19     0   974     5
9      881    38    58    21
10     338    68   542    50
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  col-24_UN0086.1
XX
ID  col-24_UN0086.1
XX
DE  UN0086.1 col-24; from JASPAR
P0       a     c     g     t
1       67   333   333   266
2        0   998     0     0
3        0     0   998     0
4        0     0   998     0
5       37   333     0   629
6        0     0     0   998
7      998     0     0     0
8      399     0     0   599
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NCU01074_UN0087.1
XX
ID  NCU01074_UN0087.1
XX
DE  UN0087.1 NCU01074; from JASPAR
P0       a     c     g     t
1      200   200   200   398
2      260    51   637    51
3       27    27   402   541
4      541   402    27    27
5        0     0     0   999
6        0     0     0   999
7      999     0     0     0
8        0   888   111     0
9       49    49   299   601
10     291   198   310   198
11     222   222   222   333
12     222   333   222   222
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NCU01629_UN0088.1
XX
ID  NCU01629_UN0088.1
XX
DE  UN0088.1 NCU01629; from JASPAR
P0       a     c     g     t
1       71   138    23   766
2      226     3   714    55
3       16   692     0   290
4       14     0   956    28
5       13   698   286     0
6        0   993     0     5
7      227    72   605    94
8       30   718    64   186
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NCU03421_UN0089.1
XX
ID  NCU03421_UN0089.1
XX
DE  UN0089.1 NCU03421; from JASPAR
P0       a     c     g     t
1      144    32   805    18
2      117   136    71   674
3       41   932    15    10
4      116   721     5   156
5       32     7   939    21
6      113    14     6   865
7        2     5   980    10
8       15   956    10    17
9      242    72   608    76
10     128   498   196   176
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NCU04211_UN0090.1
XX
ID  NCU04211_UN0090.1
XX
DE  UN0090.1 NCU04211; from JASPAR
P0       a     c     g     t
1      308   230   230   230
2      308   230   230   230
3      230   230   308   230
4      422   304    97   175
5        0     0    78   921
6        0     0   216   783
7      999     0     0     0
8        0   921     0    78
9       97    19   207   675
10     675    97    19   207
11     941    19    19    19
12     243   152   235   367
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NCU05061_UN0091.1
XX
ID  NCU05061_UN0091.1
XX
DE  UN0091.1 NCU05061; from JASPAR
P0       a     c     g     t
1      155   332   356   155
2      411    45   498    45
3        0     0   999     0
4      999     0     0     0
5        0     0   999     0
6        0     0   999     0
7        0   999     0     0
8        0   999     0     0
9       94   426    94   384
10     290   299   205   205
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NCU05637_UN0092.1
XX
ID  NCU05637_UN0092.1
XX
DE  UN0092.1 NCU05637; from JASPAR
P0       a     c     g     t
1      897     0     0   100
2       71   214   570   143
3        0     0     0   997
4      855     0    71    71
5      926     0    71     0
6        0     0     0   997
7       91   906     0     0
8      996     1     1     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NCU05909_UN0093.1
XX
ID  NCU05909_UN0093.1
XX
DE  UN0093.1 NCU05909; from JASPAR
P0       a     c     g     t
1      338   291   142   227
2      145   135   488   230
3       63   458   109   368
4      473   278   186    60
5       22    26     8   943
6       42    40   808   108
7        0   992     0     6
8        0   997     0     2
9       24    10     0   964
10       0   985     5     7
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NCU06487_UN0094.1
XX
ID  NCU06487_UN0094.1
XX
DE  UN0094.1 NCU06487; from JASPAR
P0       a     c     g     t
1      241   165   243   349
2      110   208   433   247
3      659   250    14    75
4       38    13   920    27
5        1   992     0     6
6        3   992     4     0
7        6    80   561   351
8       12   960     0    26
9      538   175   152   132
10     262   254   162   320
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NCU09576_UN0095.1
XX
ID  NCU09576_UN0095.1
XX
DE  UN0095.1 NCU09576; from JASPAR
P0       a     c     g     t
1      312    93   562    31
2        0   910     0    89
3        0    21   977     0
4       21   977     0     0
5        0   999     0     0
6      869   108     0    21
7        0   977     0    22
8      290   419    96   193
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  NCU10697_UN0096.1
XX
ID  NCU10697_UN0096.1
XX
DE  UN0096.1 NCU10697; from JASPAR
P0       a     c     g     t
1        0   634    91   272
2        0     0     0   998
3        0   998     0     0
4        0     0   998     0
5        0     0   420   578
6      945     0    53     0
7        0     0     0   998
8      187     0     0   811
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  sub-1_UN0097.1
XX
ID  sub-1_UN0097.1
XX
DE  UN0097.1 sub-1; from JASPAR
P0       a     c     g     t
1      224   326   224   224
2      203   299   391   105
3      225   724    24    24
4        0     0   999     0
5      999     0     0     0
6        0     0     0   999
7        0   999     0     0
8        0     0   897   102
9      347   117   509    25
10     144   144   144   567
11     225   324   225   225
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  vad-2_UN0098.1
XX
ID  vad-2_UN0098.1
XX
DE  UN0098.1 vad-2; from JASPAR
P0       a     c     g     t
1      246   255   244   254
2      197   506   100   196
3      121   121   633   122
4      599    96   105   199
5        0   504     0   494
6        0   999     0     0
7        0    96   903     0
8      105     0   894     0
9      400   197   100   301
10     249   252   347   150
11     224   224   224   326
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  CNI01970_UN0099.1
XX
ID  CNI01970_UN0099.1
XX
DE  UN0099.1 CNI01970; from JASPAR
P0       a     c     g     t
1      107   142   154   595
2       25   576    67   330
3        0     0   999     0
4        0   993     6     0
5        0   999     0     0
6      774     0   225     0
7        7   957     0    35
8      165   446    29   359
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  PGTG_04827_UN0100.1
XX
ID  PGTG_04827_UN0100.1
XX
DE  UN0100.1 PGTG_04827; from JASPAR
P0       a     c     g     t
1      395   320   183   100
2      101   647    57   193
3        0   915     0    83
4        0   997     0     1
5        0   988     0    10
6        1    20     0   977
7      105    96   562   235
8      137   137   382   342
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SCHCODRAFT_110010_UN0101.1
XX
ID  SCHCODRAFT_110010_UN0101.1
XX
DE  UN0101.1 SCHCODRAFT_110010; from JASPAR
P0       a     c     g     t
1      250   143   143   462
2      333   137    49   479
3        0   426     0   573
4        0   999     0     0
5        0     0   999     0
6        0     0   999     0
7      910     0    88     0
8      999     0     0     0
9      106   288   205   399
10     200   397   200   200
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  SCHCODRAFT_67234_UN0102.1
XX
ID  SCHCODRAFT_67234_UN0102.1
XX
DE  UN0102.1 SCHCODRAFT_67234; from JASPAR
P0       a     c     g     t
1        0   222   554   222
2      832    42     0   125
3        0   160     0   839
4        0   998     0     0
5        0    40   958     0
6        0     0   998     0
7      911    43    43     0
8      266   200   532     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  FG04288.1_UN0103.1
XX
ID  FG04288.1_UN0103.1
XX
DE  UN0103.1 FG04288.1; from JASPAR
P0       a     c     g     t
1      120   108   168   602
2        0   627    45   327
3        0     0   999     0
4        0   999     0     0
5        0   999     0     0
6      771   141    23    63
7        0   991     0     8
8      207   487   109   195
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  RO3G_06280_UN0274.1
XX
ID  RO3G_06280_UN0274.1
XX
DE  UN0274.1 RO3G_06280; from JASPAR
P0       a     c     g     t
1      230   216   212   340
2      134   677    40   148
3        9    19   964     5
4        3   995     0     1
5        1   997     0     0
6      789    55    84    70
7        6   955     7    31
8      223   420    90   265
9      247   311   230   210
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_00835_UN0277.1
XX
ID  ANIA_00835_UN0277.1
XX
DE  UN0277.1 ANIA_00835; from JASPAR
P0       a     c     g     t
1      561    62   187   187
2       40   120     0   839
3        0   998     0     0
4        0   998     0     0
5        0     0   998     0
6        0     0   998     0
7      927     0    71     0
8      726   136    91    45
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_01298_UN0278.1
XX
ID  ANIA_01298_UN0278.1
XX
DE  UN0278.1 ANIA_01298; from JASPAR
P0       a     c     g     t
1      122   224   209   443
2       18   970     5     5
3      745    85    77    91
4        7   773    56   162
5      141    67   784     6
6       16    11    45   926
7        1     3   985     9
8      829    75    44    50
9       20   576    43   360
10     211   401   147   238
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_01705_UN0279.1
XX
ID  ANIA_01705_UN0279.1
XX
DE  UN0279.1 ANIA_01705; from JASPAR
P0       a     c     g     t
1      125   374    62   437
2        0     0   998     0
3        0   998     0     0
4        0     0   998     0
5        0     0   998     0
6      707     0   291     0
7      738     0   260     0
8        0     0     0   998
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_01729_UN0280.1
XX
ID  ANIA_01729_UN0280.1
XX
DE  UN0280.1 ANIA_01729; from JASPAR
P0       a     c     g     t
1      237   208   326   227
2      527   148   210   114
3       30   907    32    29
4        0   999     0     0
5        0     0   999     0
6       10    32   947     9
7      254   298   269   177
8      428    60    73   437
9      423   192   214   170
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_01812_UN0281.1
XX
ID  ANIA_01812_UN0281.1
XX
DE  UN0281.1 ANIA_01812; from JASPAR
P0       a     c     g     t
1      217   183   296   302
2      770     0   227     2
3        0     0     0   998
4        1    19   919    59
5      930     0    54    14
6       58   478   403    58
7       24     7    45   922
8      106   878     6     8
9      878     8    39    73
10      80   350   136   432
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_01824_UN0282.1
XX
ID  ANIA_01824_UN0282.1
XX
DE  UN0282.1 ANIA_01824; from JASPAR
P0       a     c     g     t
1      535     0   178   285
2      142    24     0   832
3        0   910    66    22
4        0   644   333    22
5        0   999     0     0
6        0   977    22     0
7        0     0   999     0
8      734     0   264     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_02553_UN0283.1
XX
ID  ANIA_02553_UN0283.1
XX
DE  UN0283.1 ANIA_02553; from JASPAR
P0       a     c     g     t
1      192   320   172   314
2       19   895    16    68
3        8     2   988     0
4       11    18   966     4
5      630   109   218    41
6      135   261   435   168
7      444    37    35   481
8       70    19    19   890
9      743    30    29   196
10     446   133   276   144
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_02725_UN0284.1
XX
ID  ANIA_02725_UN0284.1
XX
DE  UN0284.1 ANIA_02725; from JASPAR
P0       a     c     g     t
1      100   150   599   150
2       80   193   483   241
3        0   951    32    16
4        0   983     0    16
5       16    64   918     0
6       33    33   916    16
7      272   363   290    72
8      200    89   488   222
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_02785_UN0285.1
XX
ID  ANIA_02785_UN0285.1
XX
DE  UN0285.1 ANIA_02785; from JASPAR
P0       a     c     g     t
1      284   218   279   218
2      250   351   251   146
3       50    50   116   782
4       35   893    35    35
5        0   999     0     0
6        0   130   869     0
7        0     0   999     0
8      743   126   130     0
9       95   194   429   280
10     297   308   227   166
11     199   341   259   199
12     214   214   356   214
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_02839_UN0286.1
XX
ID  ANIA_02839_UN0286.1
XX
DE  UN0286.1 ANIA_02839; from JASPAR
P0       a     c     g     t
1        1     1     1   994
2        1   167     1   829
3        1   829   167     1
4        1   167   829     1
5        1   995     1     1
6        1   995     1     1
7        1     1   995     1
8      795     1   200     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_03986_UN0287.1
XX
ID  ANIA_03986_UN0287.1
XX
DE  UN0287.1 ANIA_03986; from JASPAR
P0       a     c     g     t
1        0   333     0   665
2        0   999     0     0
3        0     0   999     0
4        0     0   999     0
5        0   734    88   176
6      176     0   822     0
7      734     0     0   264
8      241   103     0   654
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_04606_UN0288.1
XX
ID  ANIA_04606_UN0288.1
XX
DE  UN0288.1 ANIA_04606; from JASPAR
P0       a     c     g     t
1      269   653    38    38
2        0    23    69   906
3       23     0   279   697
4      976     0    23     0
5       93   883     0    23
6      175    75   200   549
7      675   108    27   189
8      999     0     0     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_05609_UN0289.1
XX
ID  ANIA_05609_UN0289.1
XX
DE  UN0289.1 ANIA_05609; from JASPAR
P0       a     c     g     t
1        0     0     0   997
2      997     0     0     0
3      614   154   230     0
4        0   384     0   614
5        0   997     0     0
6        0     0   997     0
7      921     0    77     0
8        1   711   143   143
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_06762_UN0290.1
XX
ID  ANIA_06762_UN0290.1
XX
DE  UN0290.1 ANIA_06762; from JASPAR
P0       a     c     g     t
1        0   998     0     0
2        0     0   998     0
3        0     0   998     0
4        0   411     0   587
5        0     0     0   998
6      998     0     0     0
7       67   266     0   665
8      332     1     1   664
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_06788_UN0291.1
XX
ID  ANIA_06788_UN0291.1
XX
DE  UN0291.1 ANIA_06788; from JASPAR
P0       a     c     g     t
1      229   245   197   328
2      198   392   162   245
3       43   936     1    17
4       16     0   959    23
5       14     6   953    24
6      919    14    39    25
7       39   353   588    19
8      196    22     1   779
9      393    38    19   547
10     391    88    83   436
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_06846_UN0292.1
XX
ID  ANIA_06846_UN0292.1
XX
DE  UN0292.1 ANIA_06846; from JASPAR
P0       a     c     g     t
1      190   380   380    48
2        0     0     0   999
3        0   999     0     0
4        0     0   999     0
5        0   139   860     0
6        0   721    28   250
7      200     0     0   799
8      922    38    38     0
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_07170_UN0293.1
XX
ID  ANIA_07170_UN0293.1
XX
DE  UN0293.1 ANIA_07170; from JASPAR
P0       a     c     g     t
1        1     1     1   996
2        0   897   100     0
3      897     0   100     0
4        0   598     0   399
5      698     0   299     0
6        0   200     0   797
7        0     0   997     0
8      995     1     1     1
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_07553_UN0294.1
XX
ID  ANIA_07553_UN0294.1
XX
DE  UN0294.1 ANIA_07553; from JASPAR
P0       a     c     g     t
1       76   923     0     0
2      806     0   142    50
3        0   906     0    93
4      113     0   886     0
5       46   120     0   833
6        0     0   979    20
7       67    58   781    92
8      162   487   175   175
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_08535_UN0295.1
XX
ID  ANIA_08535_UN0295.1
XX
DE  UN0295.1 ANIA_08535; from JASPAR
P0       a     c     g     t
1        1   994     1     1
2        1     1   994     1
3        1     1   994     1
4      994     1     1     1
5      994     1     1     1
6      795     1     1   200
7      200     1     1   795
8      661     3   332     3
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_10295_UN0296.1
XX
ID  ANIA_10295_UN0296.1
XX
DE  UN0296.1 ANIA_10295; from JASPAR
P0       a     c     g     t
1      281   173   286   258
2      217   132   132   517
3      713     0   286     0
4      661     0     0   337
5      401     0     0   598
6        0   113     0   886
7      914     0     0    85
8      457   113     0   429
9      334    76    76   512
10     117   207   117   556
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_10483_UN0297.1
XX
ID  ANIA_10483_UN0297.1
XX
DE  UN0297.1 ANIA_10483; from JASPAR
P0       a     c     g     t
1      199   231   224   345
2       57     5   933     3
3        4     0     0   994
4      426   570     0     2
5      993     0     1     3
6        6     5   803   184
7        9   970     4    15
8      420   210   219   149
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_10541_UN0298.1
XX
ID  ANIA_10541_UN0298.1
XX
DE  UN0298.1 ANIA_10541; from JASPAR
P0       a     c     g     t
1      221   231   337   209
2      222   322   187   266
3      187    32   739    39
4        0   993     0     5
5       14     0   984     0
6        6    16   968     9
7      923    12    22    41
8      869    28    65    35
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_10597_UN0299.1
XX
ID  ANIA_10597_UN0299.1
XX
DE  UN0299.1 ANIA_10597; from JASPAR
P0       a     c     g     t
1       77   230   614    77
2        0     0     0   998
3        0   998     0     0
4        0   998     0     0
5        0     0   998     0
6        0     0   998     0
7      998     0     0     0
8       91    91   272   544
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_10600_UN0300.1
XX
ID  ANIA_10600_UN0300.1
XX
DE  UN0300.1 ANIA_10600; from JASPAR
P0       a     c     g     t
1        1   554     1   443
2        0   998     0     0
3        0     0   998     0
4        0     0   998     0
5      998     0     0     0
6      250     0     0   748
7      998     0     0     0
8      665     0     0   333
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_10789_UN0301.1
XX
ID  ANIA_10789_UN0301.1
XX
DE  UN0301.1 ANIA_10789; from JASPAR
P0       a     c     g     t
1      356   184   184   273
2      284   107   501   107
3       46   859    46    46
4       22   931    22    22
5        0     0    86   913
6        0   999     0     0
7        0     0   999     0
8        0     0   999     0
9      156   617    65   160
10     142   233   142   480
11     300   203   293   203
12     317   227   227   227
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  ANIA_10912_UN0302.1
XX
ID  ANIA_10912_UN0302.1
XX
DE  UN0302.1 ANIA_10912; from JASPAR
P0       a     c     g     t
1        2   745     2   250
2        1     1     1   994
3        1   994     1     1
4        1     1   994     1
5        1     1     1   994
6      994     1     1     1
7        1     1     1   994
8      250     2     2   745
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Phyra42912_UN0303.1
XX
ID  Phyra42912_UN0303.1
XX
DE  UN0303.1 Phyra42912; from JASPAR
P0       a     c     g     t
1      248   105   546    99
2      195   716     8    79
3       87   337   498    76
4        0   998     0     0
5      994     0     1     4
6        4     2     3   989
7        4   980     3    11
8      228   399   121   249
9      115   452   116   315
10     223   305   165   305
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
AC  Kar4_UN0435.1
XX
ID  Kar4_UN0435.1
XX
DE  UN0435.1 Kar4; from JASPAR
P0       a     c     g     t
1       41    10    26    26
2       31    24    14    34
3       21    13    10    59
4        2     3     4    94
5        0     0   103     0
6        1     2     1    99
7        3     1     3    96
8        8     1     1    93
9        0   101     1     1
10      98     0     1     4
11      28     9    37    29
12      40    19    17    27
13      31    19    13    40
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
