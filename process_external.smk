##################
## Output files ##
##################

## 1) Motif conversion:
EXTERNAL_TRANSFAC_PFM                 = os.path.join(EXTERNAL_RESULTS_DIR, "motifs", "jaspar", "pfm", "{external_dataset_ID}_peak-motifs_m1.tf")
EXTERNAL_JASPAR_PFM                   = os.path.join(EXTERNAL_RESULTS_DIR, "motifs", "jaspar", "pfm", "{external_dataset_ID}_peak-motifs_m1.jaspar")

## PWM and LOGO:
EXTERNAL_JASPAR_LOGO                  = os.path.join(EXTERNAL_RESULTS_DIR, "motifs", "jaspar", "logos", "{external_dataset_ID}_peak-motifs_m1_logo.png")

## 2) Motif comparison:
EXTERNAL_PFM_COMPA_MATCHES            = os.path.join(EXTERNAL_RESULTS_DIR, "motifs", "comparison", "{external_dataset_ID}_peak-motifs_m1_motif_comparison.tab")

## 3) Motif annotations:
EXTERNAL_UNIPROT_ANNOTATIONS          = os.path.join(EXTERNAL_RESULTS_DIR, "annotations", "{external_dataset_ID}_uniprot_annotation.txt")
EXTERNAL_MOTIF_ANNOT_TMP              = os.path.join(EXTERNAL_RESULTS_DIR, "annotations", "{external_dataset_ID}_curation_tab_entry_tmp.txt")
EXTERNAL_MOTIF_ANNOT                  = os.path.join(EXTERNAL_RESULTS_DIR, "annotations", "{external_dataset_ID}_curation_tab_entry.txt")
EXTERNAL_MOTIF_ANNOT_TEX              = os.path.join(EXTERNAL_RESULTS_DIR, "annotations", "{external_dataset_ID}.external_logo.tex")
EXTERNAL_MOTIF_ANNOT_PDF              = os.path.join(EXTERNAL_RESULTS_DIR, "annotations", "{external_dataset_ID}.external_logo.pdf")

## 4) Curation table:
EXTERNAL_CURATION_TABLE               = os.path.join(EXTERNAL_CURATION_DIR, "external_motifs_curation.tab")
EXTERNAL_CURATION_PDF                 = os.path.join(EXTERNAL_CURATION_DIR, "external_motifs_curation.pdf")

## 5) Motifs in JASPAR format with renamed header:
EXTERNAL_JASPAR_MOTIF_CONCAT          = os.path.join(EXTERNAL_CURATION_DIR, "selected_motifs.jaspar")

## 6) Motif clustering:
EXTERNAL_MOTIF_CLUSTERING_CORE        = os.path.join(EXTERNAL_CURATION_DIR, "motif_clustering_with_CORE", "matrix-clustering_{external_batch_name}.txt")
EXTERNAL_MOTIF_CLUSTERING_UNVALIDATED = os.path.join(EXTERNAL_CURATION_DIR, "motif_clustering_with_UNVALIDATED", "matrix-clustering_{external_batch_name}.txt")

## 7) Exported table to google sheet document
EXTERNAL_GOOGLE_SHEETS_LOG            = os.path.join(EXTERNAL_CURATION_DIR, "Google_sheets_exported_log.txt")

###########
## RULES ##
###########

##----------------------##
## 1) Motif preparation ##
##----------------------##

rule copy_input_JASPAR_motif_into_folder_structure:
    """
    1.1. Copy the original motif into the results folder structure.
    """
    input:
        lambda wildcards: external_dataset_dict[wildcards.external_dataset_ID]["Peaks"]
    output:
        EXTERNAL_JASPAR_PFM
    message:
        "; Copying input motif into results folder structure for: {wildcards.external_dataset_ID} "
    shell:
        """
        cp {input} {output}
        """

rule external_input_motif_to_TRANSFAC_format:
    """
    1.2. Convert input motif (in JASPAR format) to TRANSFAC format.
    """
    input:
        EXTERNAL_JASPAR_PFM
    output:
        EXTERNAL_TRANSFAC_PFM
    message:
        "; Converting external motif into TRANSFAC format for: {wildcards.external_dataset_ID} "
    params:
        RSAT          = config["RSAT"],
        return_fields = "counts",
        prefix        = "peak-motifs"
    shell:
        """
        {params.RSAT}/perl-scripts/convert-matrix -v 2 \
        -from jaspar -to tf \
        -i {input} \
        -return {params.return_fields} \
        -prefix {params.prefix} \
        -o {output}
        """

rule generate_external_matrix_logo:
    """
    1.3. Generate motif logos using RSAT convert-matrix.
    """
    input:
        jaspar_pfm   = EXTERNAL_JASPAR_PFM, \
        transfac_pfm = EXTERNAL_TRANSFAC_PFM ## To connect with the previous rule
    output:
        EXTERNAL_JASPAR_LOGO
    message:
        "; Generating sequence LOGO from JASPAR matrix for: {wildcards.external_dataset_ID} "
    params:
        logo_dir  = os.path.join(EXTERNAL_RESULTS_DIR, "motifs", "jaspar", "logos"),
        logo_name = "{external_dataset_ID}_peak-motifs_m1",
        RSAT      = config["RSAT"]
    shell:
        """
        {params.RSAT}/perl-scripts/convert-matrix -v 2 \
        -i {input.jaspar_pfm} \
        -from jaspar -to jaspar \
        -return logo \
        -logo_dir {params.logo_dir} \
        -logo_no_title \
        -prefix {params.logo_name}
        """

##------------------------------------------------------##
## 2) Motif comparison with know motifs to infer the TF ##
##------------------------------------------------------##

rule compare_external_motif_vs_databases:
    """
    2.1. Compare each external motif with a reference database.
        Motif databases are indicated in the config.
        For each species databases can be specified.
    """
    input:
        transfac_pfm     = EXTERNAL_TRANSFAC_PFM, \
        reference_motifs = lambda wildcards: config["motif_databases"][external_dataset_dict[wildcards.external_dataset_ID]["Taxon"]], \
        sequence_logo    = EXTERNAL_JASPAR_LOGO ## To connect with the previous rule
    output:
        EXTERNAL_PFM_COMPA_MATCHES
    message:
        "; Comparing external motif vs reference databases for: {wildcards.external_dataset_ID} "
    params:
        scripts_bin             = os.path.join(config["bin"], "motif_comparison"),
        nb_of_matrices_to_match = config['nb_comparison_matches'],
        TF_name                 = lambda wildcards: external_dataset_dict[wildcards.external_dataset_ID]["TF"]
    shell:
        """
        bash {params.scripts_bin}/Compare_motifs.sh \
        -q {input.transfac_pfm} \
        -r {input.reference_motifs} \
        -t {params.nb_of_matrices_to_match} \
        -n {params.TF_name} \
        -o {output}
        """

##----------------##
## 3) Annotations ##
##----------------##

rule get_uniprot_annotations_for_external_motif:
    """
    3.1. Retrieving UniProt annotations for each motif.
        This output will be temporal, since it is later added to the main table.
    """
    input:
        EXTERNAL_PFM_COMPA_MATCHES ## To connect with the previous rule
    output:
        temp(EXTERNAL_UNIPROT_ANNOTATIONS)
    message:
        "; Retrieving UniProt annotation for: {wildcards.external_dataset_ID} "
    params:
        scripts_bin               = os.path.join(config["bin"], "retrieve_data"), \
        dset_TF_name              = lambda wildcards: external_dataset_dict[wildcards.external_dataset_ID]["TF"], \
        tax_ID                    = lambda wildcards: external_dataset_dict[wildcards.external_dataset_ID]["Taxon_ID"], \
        n_hits_suggested_uniprots = config["n_hits_suggested_uniprots"]
    shell:
        """
        Rscript {params.scripts_bin}/get_uniprot_annotations.R \
        -g {params.dset_TF_name} \
        -t {params.tax_ID} \
        -n {params.n_hits_suggested_uniprots} \
        -o {output};
        """

rule annotate_external_motifs:
    """
    3.2. Create a curation table entry for each motif given retrieved information and generated files.
    """
    input:
        jaspar_pfm         = EXTERNAL_JASPAR_PFM, \
        suggested_uniprots = EXTERNAL_UNIPROT_ANNOTATIONS, \
        previous_jaspar    = config["previous_jaspar_annotations"]
    output:
        temp(EXTERNAL_MOTIF_ANNOT_TMP)
    message:
        "; Annotating and creating curation table entry for: {wildcards.external_dataset_ID} "
    params:
        scripts_bin   = os.path.join(config["bin"], "annotations"), \
        dataset_dir   = EXTERNAL_RESULTS_DIR, \
        dset_TF_name  = lambda wildcards: external_dataset_dict[wildcards.external_dataset_ID]["TF"], \
        taxon         = lambda wildcards: external_dataset_dict[wildcards.external_dataset_ID]["Taxon"], \
        tax_ID        = lambda wildcards: external_dataset_dict[wildcards.external_dataset_ID]["Taxon_ID"], \
        organism_name = lambda wildcards: external_dataset_dict[wildcards.external_dataset_ID]["Organism"], \
        data          = lambda wildcards: external_dataset_dict[wildcards.external_dataset_ID]["Data"], \
        source        = lambda wildcards: external_dataset_dict[wildcards.external_dataset_ID]["Source"]
    shell:
        """
        suggested_uniprots=$(cut -f 5 {input.suggested_uniprots} | grep -v 'Suggested_Uniprot' | uniq | paste -s -d '-') ;

        bash {params.scripts_bin}/annotate_external_motifs.sh \
        {input.jaspar_pfm} \
        {input.previous_jaspar} \
        $suggested_uniprots \
        {params.dataset_dir} \
        {wildcards.external_dataset_ID} \
        {params.dset_TF_name} \
        {params.organism_name} \
        {params.tax_ID} \
        {params.taxon} \
        {params.data} \
        {params.source} \
        {output} ;
        """

rule add_external_motif_comparison_to_annotations:
    """
    3.3. Add matrix comparison results to the annotations (an entry in the curation table).
    """
    input:
        EXTERNAL_MOTIF_ANNOT_TMP
    output:
        EXTERNAL_MOTIF_ANNOT
    message:
        "; Adding matrix comparison to the curation table entry for: {wildcards.external_dataset_ID} "
    params:
        scripts_bin          = os.path.join(config["bin"], "annotations"), \
        motif_comparison_dir = os.path.join(EXTERNAL_RESULTS_DIR, "motifs", "comparison")
    shell:
        """
        Rscript {params.scripts_bin}/add_comparison_results_to_annotation_table.R \
        -d {input} \
        -r {params.motif_comparison_dir} \
        -o {output}
        """

rule make_pdf_logos_for_external_motifs:
    """
    3.4. Converts the logos of motifs into the latex and pdf format.
        This later is used to concatenate all results into a single PDF file.
    """
    input:
        EXTERNAL_MOTIF_ANNOT
    output:
        EXTERNAL_MOTIF_ANNOT_TEX, \
        EXTERNAL_MOTIF_ANNOT_PDF
    message:
        "; Creating TEX and PDF logos for: {wildcards.external_dataset_ID} "
    params:
        scripts_bin = os.path.join(config["bin"], "annotations")
    shell:
        """
        bash {params.scripts_bin}/create_latex_logos_for_external_motifs.sh \
          -l {params.scripts_bin}/latex_header.txt \
          -i {input} \
          -o {output}
        """

##---------------------------------------------------##
## 4) Combinding all annotations into curation table ##
##---------------------------------------------------##

rule collect_individual_external_motifs_annotations:
    """
    4.1. Concatenate the individual curation table entries.
        This table is basically the final curation table.
    """
    input:
        curation_tab_entry = expand(EXTERNAL_MOTIF_ANNOT, zip, external_dataset_ID = external_dataset_IDs, external_batch_name = EXTERNAL_BATCH_NAME), \
        motifs_in_tex      = expand(EXTERNAL_MOTIF_ANNOT_TEX, zip, external_dataset_ID = external_dataset_IDs, external_batch_name = EXTERNAL_BATCH_NAME), \
        motifs_in_pdf      = expand(EXTERNAL_MOTIF_ANNOT_PDF, zip, external_dataset_ID = external_dataset_IDs, external_batch_name = EXTERNAL_BATCH_NAME)
    output:
        EXTERNAL_CURATION_TABLE
    message:
        "; Concatenating the tables with the annotated motifs."
    params:
        out_dir = os.path.join(OUT_DIR, "external")
    shell:
        """
        ls {params.out_dir}/{wildcards.external_batch_name}/*/annotations/*_curation_tab_entry.txt \
        | xargs cat \
        | uniq \
        | sed -e '1i\curation_id\\tPWM\\tcurrent_BASE_ID\\tcurrent_VERSION\\tTF_NAME\\tUniProt\\tTAX_ID\\tclass\\tfamily\\tTFBS_shape_ID\\tData\\tSource\\tValidation\\tComment\\tDecision\\tBED\\tFASTA\\tlogo\\tlogo_rc\\tCent_pval\\tCent_plot\\tcuration_comment\\tDataset_TF_NAME\\tSuggested_MATRIX_ID\\tSuggested_TF_NAME\\tSuggested_UniProt_prev_jaspar\\tSuggested_class\\tSuggested_family\\tSuggested_UniProt_mygene\\tpdf_path\\tmatrix_comparison' > {output};
        """

rule collect_pdf_logos_into_single_document:
    """
    4.2. All logos in tex, pdf format are collected and combined into a single pdf.
        This document is useful to visualize all motifs and is nice to have during curation.
    """
    input:
        EXTERNAL_CURATION_TABLE
    output:
        EXTERNAL_CURATION_PDF
    message:
        "; Collecting all PDFs with motif logos into a single file."
    shell:
        """
        PDF_FILES=` awk -F"\\t" 'FNR>=2 {{ print $30 }}' {input} | uniq | xargs `
        pdfunite $PDF_FILES {output};
        """

##---------------------------------##
## 5. Selected motifs for curation ##
##---------------------------------##

rule collect_external_motifs_rename_jaspar_motif_header:
    """
    5.1. Write the correct name to the jaspar motifs.
    The motifs resulting from peak-motifs have a header that looks like this:
    >peak-motifs_m3 peak-motifs_m3
    Change this header for an informative one
    >CTCF CTCF

    # 0 base
    Column 1  = Path to jaspar file
    Column 22 = Dataset TF name
    """
    input:
        curation_tab = EXTERNAL_CURATION_TABLE, \
        curation_pdf = EXTERNAL_CURATION_PDF ## To connect with a previous rule
    output:
        EXTERNAL_JASPAR_MOTIF_CONCAT
    message:
        "; Collecting external motifs into a single file."
    shell:
        """
        grep -v Dataset_TF_NAME {input.curation_tab} | \
        perl -lne '@sl = split(/\\t/, $_); \

            $convert_cmd = " convert-matrix -i ".$sl[1]. " -from jaspar -to jaspar -attr id ".$sl[22]."_".$sl[0]." -attr name ".$sl[22]."_".$sl[0]." -return counts > ".$sl[1]."tmp" ; \
            $rename_file = "mv ".$sl[1]."tmp ".$sl[1] ; \
            system($convert_cmd); \
            system($rename_file); ' ;

        grep -v Dataset_TF_NAME {input.curation_tab} | cut -f2 | xargs cat  > {output}
        """

##---------------------##
## 6. Motif clustering ##
##---------------------##

rule cluster_external_motifs_with_CORE:
    """
    6.1. Cluster all motifs with CORE JASPAR collection.
    """
    input:
        curation_tab     = EXTERNAL_CURATION_TABLE, \
        reference_motifs = lambda wildcards: config["CORE_motif_collections"][external_batch_name_to_taxon_dict[wildcards.external_batch_name]], \
        motifs_jaspar    = EXTERNAL_JASPAR_MOTIF_CONCAT ## To connect with the previous rule
    output:
        EXTERNAL_MOTIF_CLUSTERING_CORE
    message:
        "; Clustering discovered motifs with JASPAR CORE."
    params:
        v               = "2", \
        RSAT            = config["RSAT"], \
        bin             = os.path.join(config["bin"], "clustering"), \
        collection_name = "{external_batch_name}", \
        output_dir      = os.path.join(EXTERNAL_CURATION_DIR, "motif_clustering_with_CORE")
    shell:
        """
        echo "; Building clustering tables for subsets of motifs."
        Rscript {params.bin}/make_clustering_table.R \
        -m {input.curation_tab} \
        -o {params.output_dir} \
        -t TRUE \
        -e jaspar \
        -s 150 \
        -c {params.collection_name} \
        -j {input.reference_motifs} ;

        echo "; Clustering motifs in batches."
        for file in {params.output_dir}/clustering_tables/discovered_motifs_*.tab ;
        do
            filename=$(basename -- "$file") ;
            dataset_name="${{filename%.tab}}" ;
            echo "; Running clustering on $dataset_name" ;
            subset_output_dir={params.output_dir}/"$dataset_name" ;
            mkdir -p $subset_output_dir ;

            portable_tree="$subset_output_dir"/"$dataset_name"_portable_logo_tree.html ;

            if test -f "$portable_tree"; then
                echo "$portable_tree already exists. Skipping clustering."
            else
                {params.RSAT}/perl-scripts/matrix-clustering  \
                -v {params.v} \
                -matrix_file_table $file \
                -hclust_method average \
                -calc sum \
                -title "Selected motifs vs JASPAR 2022 CORE" \
                -label_motif name -metric_build_tree Ncor \
                -lth w 5 -lth cor 0.6 -lth Ncor 0.4 \
                -label_in_tree name -quick -return json \
                -o $subset_output_dir/"$dataset_name" ;
            fi ;

            rm $subset_output_dir/"$dataset_name"_aligned_logos/*.tf ;
            mv $subset_output_dir/"$dataset_name"_aligned_logos/ {params.output_dir} ;

            cp -r $subset_output_dir/js {params.output_dir} ;
            mv "$portable_tree" {params.output_dir} ;

            rm -r "$subset_output_dir" ;

        done ;

        rm -r {params.output_dir}/clustering_tables ;

        echo "Motif clustering with CORE DONE." > {output} ;
        """

rule cluster_external_motifs_with_UNVALIDATED:
    """
    6.2. Cluster all motifs with UNVALIDATED JASPAR collection.
    """
    input:
        curation_tab     = EXTERNAL_CURATION_TABLE, \
        reference_motifs = lambda wildcards: config["UNVALIDATED_motif_collections"][external_batch_name_to_taxon_dict[wildcards.external_batch_name]], \
        motifs_jaspar    = EXTERNAL_JASPAR_MOTIF_CONCAT ## To connect with the previous rule
    output:
        EXTERNAL_MOTIF_CLUSTERING_UNVALIDATED
    message:
        "; Clustering discovered motifs with JASPAR UNVALIDATED."
    params:
        v               = "2", \
        bin             = os.path.join(config["bin"], "clustering"), \
        RSAT            = config["RSAT"], \
        collection_name = "{external_batch_name}", \
        output_dir      = os.path.join(EXTERNAL_CURATION_DIR, "motif_clustering_with_UNVALIDATED")
    shell:
        """
        echo "; Building clustering tables for subsets of motifs."
        Rscript {params.bin}/make_clustering_table.R \
        -m {input.curation_tab} \
        -o {params.output_dir} \
        -t TRUE \
        -e jaspar \
        -s 500 \
        -c {params.collection_name} \
        -j {input.reference_motifs} ;

        echo "; Clustering motifs in batches."
        for file in {params.output_dir}/clustering_tables/discovered_motifs_*.tab ;
        do
            filename=$(basename -- "$file") ;
            dataset_name="${{filename%.tab}}" ;
            echo "; Running clustering on $dataset_name" ;
            subset_output_dir={params.output_dir}/"$dataset_name" ;
            mkdir -p $subset_output_dir ;

            portable_tree="$subset_output_dir"/"$dataset_name"_portable_logo_tree.html ;

            if test -f "$portable_tree"; then
                echo "$portable_tree already exists. Skipping clustering."
            else
                {params.RSAT}/perl-scripts/matrix-clustering  \
                -v {params.v} \
                -matrix_file_table $file \
                -hclust_method average \
                -calc sum \
                -title "Selected motifs vs JASPAR 2022 UNVALIDATED" \
                -label_motif name -metric_build_tree Ncor \
                -lth w 5 -lth cor 0.6 -lth Ncor 0.4 \
                -label_in_tree name -quick -return json \
                -o $subset_output_dir/"$dataset_name" ;
            fi ;

            rm $subset_output_dir/"$dataset_name"_aligned_logos/*.tf ;
            mv $subset_output_dir/"$dataset_name"_aligned_logos/ {params.output_dir} ;

            cp -r $subset_output_dir/js {params.output_dir} ;
            mv "$portable_tree" {params.output_dir} ;

            rm -r "$subset_output_dir" ;

        done ;

        rm -r {params.output_dir}/clustering_tables ;

        echo "Motif clustering with UNVALIDATED DONE." > {output} ;
        """

##------------------------------##
## 7. Exporting as google sheet ##
##------------------------------##

rule export_external_motifs_curation_table_to_googlesheets:
    """
    7.1. Automatically upload the curation table in a google document.
    """
    input:
        EXTERNAL_CURATION_TABLE
    output:
        EXTERNAL_GOOGLE_SHEETS_LOG
    message:
        "; Exporting curation table to a google sheet "
    params:
        gs_url   = config['gs_url'], ## Google sheet url
        json_key = config['google_token'], \
        date     = todays_date
    shell:
        """
        Rscript bin/export_tables/Upload_table_to_googlesheet.R \
          -t {input} \
          -o A \
          -u {params.gs_url} \
          -n {wildcards.external_batch_name}_{params.date} \
          -j {params.json_key} ;


        echo "Exported tables to google sheets." > {output}
        """
