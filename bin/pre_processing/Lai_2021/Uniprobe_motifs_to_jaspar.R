#############################
## Load required libraries ##
#############################

## List of packages to install from CRAN
required.packages = c("data.table",
                      "dplyr",
                      "furrr",
                      "universalmotif")

for (lib in required.packages) {
  suppressPackageStartupMessages(library(lib, character.only = TRUE, quietly = TRUE))
}


###############
## Functions ##
###############

parse.uniprobe.motif <- function(up.motif        = NULL,
                                 up.motif.parsed = NULL) {
  
  uniprobe.motifs.original <- readLines(con = up.motif)
  
  ## These are the lines that follow the motif header '#' and will be removed
  lines.rm <- which(grepl(uniprobe.motifs.original, pattern = "^#")) + 1
  uniprobe.motifs.original <- uniprobe.motifs.original[-lines.rm]
  uniprobe.motifs.original <- gsub(uniprobe.motifs.original, pattern = "^#.+$", replacement = "")
  
  ## Header update: we will use the TF name which is in the filename
  TF.name <- gsub(basename(up.motif), pattern = ".pwm$", replacement = "")
  TF.name <- gsub(basename(TF.name), pattern = "_RC$", replacement = "")
  
  ## Add the DNA alphabet at the beginning of each row
  ## 5 : header + A, C, G, T lines
  nb.motifs   <- length(uniprobe.motifs.original)/5
  text.to.add <- c(TF.name, "A: ", "C: ", "G: ", "T: ")
  
  ## Add the text.to.add at the beginning of the text
  uniprobe.motifs.original <- paste0(rep(text.to.add, times = nb.motifs), uniprobe.motifs.original)
  
  
  ## Export motif
  dir.create(dirname(up.motif.parsed), recursive = TRUE, showWarnings = FALSE)
  writeLines(text = uniprobe.motifs.original,
             con  = up.motif.parsed)
}


##########
## Main ##
##########

original.motifs.folder <- "Uniprobe/LAI20A"
parsed.motifs.folder   <- "Uniprobe_to_curate"
jaspar.motifs.folder   <- "Motifs"
uniprobe.single.file   <- file.path(parsed.motifs.folder, "Uniprobe_motifs_concat.uniprobe")


## List Uniprobe motif files
uniprobe.motif.files <- data.table(Original = file.path(original.motifs.folder, list.files(original.motifs.folder, pattern = "RC.pwm"))) %>% 
                          mutate(Parsed = file.path(parsed.motifs.folder, gsub(basename(Original), pattern = "_RC\\.pwm$", replacement = ".uniprobe")))

## Export parsed motifs in Uniprobe format
message("; Exporting motifs in correct uniprobe format")
purrr::walk2(.x = uniprobe.motif.files$Original,
             .y = uniprobe.motif.files$Parsed,
             .f = ~parse.uniprobe.motif(up.motif        = .x,
                                        up.motif.parsed = .y))

## Concatenate all uniprobe parsed motifs in a single file
concat.command <- paste0("cat ", parsed.motifs.folder, "/* > ", uniprobe.single.file)
message(concat.command)
system(concat.command)     


## Read the uniprobe file
um.uniprobe <- universalmotif::read_uniprobe(file = uniprobe.single.file)

## Convert the frequency to a count matrix
um.uniprobe <- universalmotif::convert_type(um.uniprobe, type = "PCM")
names(um.uniprobe) <- paste0(names(um.uniprobe), "_m", data.table::rowid(names(um.uniprobe)))

uniprobe2jaspar.file <- file.path(jaspar.motifs.folder, paste0(names(um.uniprobe), ".jaspar"))
message("; Converting uniprot motifs to jaspar")
dir.create(jaspar.motifs.folder, recursive = TRUE, showWarnings = FALSE)
purrr::walk2(.x = um.uniprobe,
             .y = uniprobe2jaspar.file,
             .f = ~universalmotif::write_jaspar(motifs = .x,
                                                file   = .y))

unlink(parsed.motifs.folder, recursive = TRUE)

