---
title: "Download motifs from FactorBook"
author: 
- Jaime A Castro-Mondragon
date: "Last update: `r Sys.Date()`"

output:
  html_document:
      toc: true
      toc_depth: 3
      toc_float: true
      theme: paper
geometry: margin = 2cm
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


## Publication

https://doi.org/10.1093/nar/gkab1039

[Factorbook](http://www.factorbook.org/) contains human TF binding motifs
derived from SELEX experiments and ENCODE ChIP-seq datasets.

$~$

$~$

### Download human TF motifs

1. Go to: [Factorbook](http://www.factorbook.org/)

2. Click on the **Downloads** button at the top-left corner.

3. Click on the **TF Motif Catalog** button.

4. Download and uncompress the [MEME ChIP-seq Catalog](https://screen-beta-api.wenglab.org/factorbook_downloads/complete-factorbook-catalog.meme.gz) and the [HT-SELEX Catalog](https://screen-beta-api.wenglab.org/factorbook_downloads/all-selex-motifs.meme.gz) *gz* files. 

5. Save the ChIP-seq motifs in a folder named `ChIP-seq` and the SELEX motifs in a folder named `SELEX`.

$~$

```{r ChIp_exo_table, eval=TRUE, echo=FALSE, fig.cap="", cache=FALSE, include=TRUE, fig.align='center'}

knitr::include_graphics("images/Factorbook_human.png", dpi = 100)
```

$~$

$~$

### Download ENCODE experiment metadata

The names provided in the MEME motifs correspond to an ENCODe experiment ID followed
by the motif consensus, for example `ENCSR437GBJ_AAAGAGGAASTGAAA`.

To ease the curation process, these motifs have to be renamed to their associated
experiment TF name.


1. Go to the [Encode experiment](https://www.encodeproject.org/report/?type=Experiment) table.

2. In the **assay title** tab select the option **TF ChIP-seq**. This will display a table as shown in the following figure.

$~$

```{r Encode_experiments, eval=TRUE, echo=FALSE, fig.cap="", cache=FALSE, include=TRUE, fig.align='center'}

knitr::include_graphics("images/Encode_experiments.png", dpi = 100)
```

3. Click on the **Download TSV** button (top-center) to get the experiment matrix as a text file.

4. Save this table in a folder named `tables`.

$~$

$~$


### Download metadata

1. Go to SRA database accession [ERP010942](https://www.ncbi.nlm.nih.gov/Traces/study/?acc=ERP010942&o=acc_s%3Aa)

2. Click on the **Metadata** button (indicated with red frame) to download the metadata. The table must be named `Jolma_2013_SRA.txt`.

3. Save this table in a folder named `tables`.

4. Repeat steps 1-3 with accession [ERP010942](https://www.ncbi.nlm.nih.gov/Traces/study/?acc=ERP010942&o=acc_s%3Aa). The downloaded table must be named named `Yin_2017_SRA.txt`.

$~$

```{r metadata, eval=TRUE, echo=FALSE, fig.cap="", cache=FALSE, include=TRUE, fig.align='center'}

knitr::include_graphics("images/metadata_download.png", dpi = 100)
```



### Convert ChIP-seq motifs: MEME to JASPAR

By *kinitting* this file, the motifs will be parsed and converted from `MEME` to 
`jaspar` format.

```{r MEME2JASPAR, eval=TRUE, echo=FALSE, fig.cap="", cache=FALSE, include=TRUE, fig.align='center', message=FALSE, warning=FALSE}

library(data.table)
library(dplyr)
library(purrr)
library(universalmotif)


###############
## Functions ##
###############

set.um.id.name <- function(um = NULL,
                           new.id = "New_ID") {
  um@name <- new.id
  return(um)
}



##########
## Main ##
##########

## Read ENCODE metadata table + select relevant columns
encode.metadata <- fread("tables/Encode_experiments_metadata.tsv") %>% 
                      select(Accession, "Target gene symbol", Organism, "Biosample term name") %>% 
                      rename(TF     = "Target gene symbol",
                             Origin = "Biosample term name")


## Read MEME file
factorbook.chipseq.meme.file <- "ChIP-seq/complete-factorbook-catalog.meme"
factorbook.chipseq.um        <- universalmotif::read_meme(file = factorbook.chipseq.meme.file)

## Create mapping table Motif ID <--> Encode Experiment 
meme.to.encode.tab <- data.table(Motif_ID = purrr::map_chr(factorbook.chipseq.um, `[`, "name")) %>% 
                          mutate(Accession = gsub(Motif_ID, pattern = "_.+$", replacement = "")) %>% 
                          left_join(encode.metadata) %>%
                          mutate(New_motif_ID = paste(TF, data.table::rowid(TF), sep = "_")) ## This is the name that will appear in the jaspar motif header


## Rename Universal motif 'name' field
factorbook.chipseq.um.renamed <- purrr::map2(.x = factorbook.chipseq.um,
                                             .y = meme.to.encode.tab$New_motif_ID,
                                             .f = ~set.um.id.name(um     = .x,
                                                                  new.id = .y))

## Export motifs in jaspar format
# universalmotif::write_jaspar(motifs = factorbook.chipseq.um.renamed, file = "ChIP-seq/Factorbook_ChIP-seq.jaspar", overwrite = TRUE)

fb.chipseq2jaspar.file <- file.path("ChIP-seq", paste0(purrr::map_chr(factorbook.chipseq.um.renamed, `[`, "name"), ".jaspar"))
purrr::walk2(.x = factorbook.chipseq.um.renamed,
             .y = fb.chipseq2jaspar.file,
             .f = ~universalmotif::write_jaspar(motifs    = .x,
                                                file      = .y,
                                                overwrite = TRUE))


DT::datatable(meme.to.encode.tab)
```

$~$

$~$

### Convert SELEX-derived motifs: MEME to JASPAR

The MEME motifs contain a header with a SRA ID instead of a TF name, before the
format conversion we need to rename the motifs to an informative name.


```{r SELEX2JASPAR, eval=TRUE, echo=FALSE, fig.cap="", cache=FALSE, include=TRUE, fig.align='center', message=FALSE, warning=FALSE}

## Read MEME file
factorbook.selex.meme.file <- "SELEX/all-selex-motifs.meme"
factorbook.selex.um        <- universalmotif::read_meme(file = factorbook.selex.meme.file)


## Create mapping file, unfortunately we need separate files
jolma2013 <- fread("tables/Jolma_2013_SRA.txt") %>% 
                select(Run, Submitter_Id)
                

yin2017   <- fread("tables/Yin_2017_SRA.txt") %>% 
                select(Run, Submitter_Id)


selex.metadata <- rbind(yin2017, jolma2013) %>% 
                    rename(ID = Run,
                           TF = Submitter_Id) %>% 
                    mutate(TF = gsub(TF, pattern = "_.+$", replacement = "")) %>% 
                    distinct()


## Check all the IDs in the motif file are in the mapping file
sra.ids.motifs <- purrr::map_chr(factorbook.selex.um, `[`, "name")

if (!all(sra.ids.motifs %in% selex.metadata$ID)) {
  stop("Many IDs in the motif files were not found in the mapping table")
}



## Create mapping table Motif ID <--> Encode Experiment 
meme.to.selex.tab <- data.table(ID = purrr::map_chr(factorbook.selex.um, `[`, "name")) %>% 
                          left_join(selex.metadata) %>%
                          mutate(New_motif_ID = paste(TF, data.table::rowid(TF), sep = "_")) ## This is the name that will appear in the jaspar motif header


## Rename Universal motif 'name' field
factorbook.selex.um.renamed <- purrr::map2(.x = factorbook.selex.um,
                                             .y = meme.to.selex.tab$New_motif_ID,
                                             .f = ~set.um.id.name(um     = .x,
                                                                  new.id = .y))

## Export motifs in jaspar format
# universalmotif::write_jaspar(motifs = factorbook.selex.um.renamed, file = "SELEX/Factorbook_SELEX.jaspar", overwrite = TRUE)

fb.selex2jaspar.file <- file.path("SELEX", paste0(purrr::map_chr(factorbook.selex.um.renamed, `[`, "name"), ".jaspar"))
purrr::walk2(.x = factorbook.selex.um.renamed,
             .y = fb.selex2jaspar.file,
             .f = ~universalmotif::write_jaspar(motifs    = .x,
                                                file      = .y,
                                                overwrite = TRUE))

DT::datatable(meme.to.selex.tab)
```

$~$

$~$

After format conversion and renaming headers the motifs in `jaspar` format looks 
like the following example.

The suffix *_{number}* is used to separate motifs from the same TF derived from
different datasets.

$~$

$~$

```bash
>NFATC3_1
A [  76  64  77  33  86  31  27 121 129  23  32  10 121  89 102 ]
C [   0  17   0   9  10   0   1   0   1  50   2   0   0   0   7 ]
G [  27  27  21  76  23 101 104   9   0  59   5 122   7  37  17 ]
T [  29  24  34  14  13   0   0   2   2   0  93   0   4   6   6 ]

>NFATC3_2
A [  7  4 13 49  0  0  1  0  7 20 33  4  8  2 16 ]
C [  1  0  0  7 56  1  3  0  2  9 12 51 18 23  7 ]
G [  8 52 40  0  0  0  3  0 46 20  2  0  1  7  5 ]
T [ 40  0  3  0  0 55 49 56  1  7  9  1 29 24 28 ]

>RUNX1_1
A [  13   0  17 341   0   0 113  18  57  73  69  60  64  83  59 ]
C [ 218  45   0   0   0 341   0 107 108 111  96 128 108 113 155 ]
G [  66   0   0   0   0   0   7 171  76 102  96  81  85  69  85 ]
T [  44 296 324   0 341   0 221  45 100  55  80  72  84  76  42 ]

```