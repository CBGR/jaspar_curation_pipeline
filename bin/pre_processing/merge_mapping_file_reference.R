# ============================================ #
# Load libraries + read command line arguments #
# ============================================ #
required.libraries <- c("data.table",
                        "tidyverse",
                        "optparse")

for (lib in required.libraries) {
  suppressPackageStartupMessages(library(lib, character.only = TRUE, quietly = T))
}

# ======= #
# Options #
# ======= #

option_list = list(
  
  make_option(c("-m", "--mapping_file"), type = "character", default = NULL, 
              help = "Mapping file. (Mandatory) ", metavar = "character"),
  
  make_option(c("-o", "--output_file"), type = "character", default = NULL, 
              help = "Output file (Mandatory)", metavar = "character"),
  
  make_option(c("-r", "--reference_file"), type = "character", default = NULL, 
              help = "Reference file (Mandatory)", metavar = "character")
  
);

message("; Reading arguments from command-line")
opt_parser = OptionParser(option_list = option_list);
opt        = parse_args(opt_parser);

# ==================== #
# Initialize variables #
# ==================== #

mapping.file <- opt$mapping_file
output.file  <- opt$output_file
ref.file     <- opt$reference_file

# ========== #
# How to run #
# ========== #

## Rscript bin/pre_processing/merge_mapping_file_reference.R \
# -m data/mapping_files/JASPAR_2024_mapping_file_2022-11-10.txt \
# -r /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/external_refs.tab \
# -o /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/arabidopsis_external_refs_with_dataset_IDs.tab

## Debug
# setwd("/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/mathelierarea/processed/ieva/projects/JASPAR_2024/jaspar_2024_curation_pipeline")
# mapping.file <- "data/mapping_files/JASPAR_2024_mapping_file_2022-11-10.txt"
# ref.file     <- "/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/external_refs.tab"
# output.file  <- "/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/arabidopsis_external_refs_with_dataset_IDs.tab"

########################
## Merging the tables ##
########################

mapping_tab <- fread(mapping.file, header = TRUE)
ref_tab     <- fread(ref.file, header = TRUE)

joined_tab <- right_join(ref_tab, mapping_tab,
                         by = c("narrowPeak" = "Peaks")) %>%
  dplyr::select(TF_name_original,
                Dataset_ID,
                external_db_ID,
                pubmed,
                encode,
                geo,
                sra,
                other,
                Batch_name) %>%
  arrange(Dataset_ID)

#########################
## Exporting the table ##
#########################

fwrite(joined_tab,
       output.file,
       col.names = TRUE,
       sep = "\t")

## End of Script ##