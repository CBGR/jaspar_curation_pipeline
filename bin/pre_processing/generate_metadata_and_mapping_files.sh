#!/bin/bash

#######################################
## Download JASPAR data and metadata ##
#######################################

## A. Check if metadata was downloaded. If not, download:

FILE=data/metadata/JASPAR_latest_profiles_metadata.tab
if test -f "$FILE";
then
    echo "; Metadata file $FILE exists."
else
    echo "; Metadata file $FILE does not exist. Downloading."
    Rscript bin/retrieve_data/retrieve_latest_matrix_information.R \
    -o data/metadata
fi


## B. Downloading the motifs:

if [ -d "/storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/all_taxons/jaspar_all_motifs" ] ;
then

    echo "; CORE and UNVALIDATED non-redundant profiles for all taxons already exist."

else

    echo "; Downloading CORE and UNVALIDATED non-redundant profiles for all taxons."
    
    ## Creating directory:
    mkdir -p /storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/all_taxons/jaspar_all_motifs

    ## ZIPs:
    wget -O /storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/all_taxons/JASPAR2022_CORE_non-redundant_pfms_jaspar.zip https://jaspar.genereg.net/download/data/2022/CORE/JASPAR2022_CORE_non-redundant_pfms_jaspar.zip ;

    wget -O /storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/all_taxons/JASPAR2022_UNVALIDATED_non-redundant_pfms_jaspar.zip https://jaspar.genereg.net/download/data/2022/collections/JASPAR2022_UNVALIDATED_non-redundant_pfms_jaspar.zip ;

    ## Unzip:
    unzip /storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/all_taxons/JASPAR2022_CORE_non-redundant_pfms_jaspar.zip -d /storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/all_taxons/jaspar_all_motifs ;
    unzip /storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/all_taxons/JASPAR2022_UNVALIDATED_non-redundant_pfms_jaspar.zip -d /storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/all_taxons/jaspar_all_motifs ;

    ## Remove the ZIPs:
    rm /storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/all_taxons/JASPAR2022_CORE_non-redundant_pfms_jaspar.zip ;
    rm /storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/all_taxons/JASPAR2022_UNVALIDATED_non-redundant_pfms_jaspar.zip ;

fi 

## C. Creating collections:

if [ -d "data/motif_databases" ] ;
then

    echo "; CORE and UNVALIDATED non-redundant motif collections exist."

else
    
    echo "; Creating JASPAR CORE and UNVALIDATED motif collections."

    ## Listing all metadata files:
    jaspar_metadata_files=$(find data/metadata -path '*_CORE_UNVALIDATED_*.tab' | tr '\n' ',' | sed 's/,$//')

    Rscript bin/pre_processing/prepare_jaspar_motif_collections.R \
    -j /storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/all_taxons/jaspar_all_motifs \
    -n $jaspar_metadata_files \
    -o data/motif_databases

fi ;

############################
## Generate mapping files ##
############################

# Generate mapping file tables with dataset unique IDs
## The script will take the metadata that was generated above and will create two mapping files:
### 1. A mapping file with datasets for TFs that we do not have a motif (for that taxon) in JASPAR CORE.
### 2. A mapping file with the rest of the datasets (leftovers).

# NOTE: the output file includes the date when this script was ran
Rscript bin/pre_processing/generate_mapping_file.R \
-d data/mapping_files/JASPAR_datasets_info_01.tab \
-o data/mapping_files \
-j data/metadata/JASPAR_latest_profiles_metadata.tab \
-t 1000

## Generating mapping files for GTRD PLANTS data:
Rscript bin/pre_processing/generate_mapping_file.R \
-d /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/JASPAR_datasets_info_plants.tab \
-o data/mapping_files \
-j data/metadata/JASPAR_latest_profiles_metadata.tab \
-t 1000

## Generating mapping files for GTRD NEMATODES data:
Rscript bin/pre_processing/generate_mapping_file.R \
-d /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/JASPAR_datasets_info_nematodes.tab \
-o data/mapping_files \
-j data/metadata/JASPAR_latest_profiles_metadata.tab \
-t 1000

## Generating mapping files for GTRD INSECTS data:
Rscript bin/pre_processing/generate_mapping_file.R \
-d /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/JASPAR_datasets_info_insects.tab \
-o data/mapping_files \
-j data/metadata/JASPAR_latest_profiles_metadata.tab \
-t 1000

## Generating mapping files for GTRD FUNGI data:
Rscript bin/pre_processing/generate_mapping_file.R \
-d /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/JASPAR_datasets_info_fungi.tab \
-o data/mapping_files \
-j data/metadata/JASPAR_latest_profiles_metadata.tab \
-t 1000

## Generating mapping files for GTRD VERTEBRATE data:
Rscript bin/pre_processing/generate_mapping_file.R \
-d /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/JASPAR_datasets_info_human.tab \
-o data/mapping_files \
-j data/metadata/JASPAR_latest_profiles_metadata.tab \
-t 1000 \
-b 1000

Rscript bin/pre_processing/generate_mapping_file.R \
-d /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/JASPAR_datasets_info_zebrafish.tab \
-o data/mapping_files \
-j data/metadata/JASPAR_latest_profiles_metadata.tab \
-t 1000

Rscript bin/pre_processing/generate_mapping_file.R \
-d /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/JASPAR_datasets_info_rat.tab \
-o data/mapping_files \
-j data/metadata/JASPAR_latest_profiles_metadata.tab \
-t 1000

Rscript bin/pre_processing/generate_mapping_file.R \
-d /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/JASPAR_datasets_info_mouse.tab \
-o data/mapping_files \
-j data/metadata/JASPAR_latest_profiles_metadata.tab \
-t 1000 \
-b 1000

### Current JASPAR CORE and UNVALIDATED motifs:
## Preparing the motifs for curation:
Rscript bin/pre_processing/prepare_jaspar_motifs_for_curation.R \
    -j /storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/all_taxons/jaspar_all_motifs \
    -n /storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/Plants/JASPAR_2022_metadata_nonredundant_CORE_UNVALIDATED_plants.tab,/storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/Nematodes/JASPAR_2022_metadata_nonredundant_CORE_UNVALIDATED_nematodes.tab,/storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/Vertebrates/JASPAR_2022_metadata_nonredundant_CORE_UNVALIDATED_vertebrates.tab,/storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/Insects/JASPAR_2022_metadata_nonredundant_CORE_UNVALIDATED_insects.tab,/storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/Fungi/JASPAR_2022_metadata_nonredundant_CORE_UNVALIDATED_fungi.tab,/storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/Urochordates/JASPAR_2022_metadata_nonredundant_CORE_UNVALIDATED_urochordates.tab \
    -o /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Motifs/JASPAR_2022_all_motifs \
    -d data/mapping_files/JASPAR_datasets_info_JASPAR2022.tab ;

Rscript bin/pre_processing/prepare_jaspar_motifs_for_curation.R \
    -j /storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/all_taxons/jaspar_all_motifs \
    -n /storage/mathelierarea/processed/ieva/projects/JASPAR_2024/jaspar_2024_curation_pipeline/data/metadata/JASPAR_2022_metadata_nonredundant_CORE_UNVALIDATED_trematodes.tab,/storage/mathelierarea/processed/ieva/projects/JASPAR_2024/jaspar_2024_curation_pipeline/data/metadata/JASPAR_2022_metadata_nonredundant_CORE_UNVALIDATED_oomycota.tab,/storage/mathelierarea/processed/ieva/projects/JASPAR_2024/jaspar_2024_curation_pipeline/data/metadata/JASPAR_2022_metadata_nonredundant_CORE_UNVALIDATED_cnidaria.tab,/storage/mathelierarea/processed/ieva/projects/JASPAR_2024/jaspar_2024_curation_pipeline/data/metadata/JASPAR_2022_metadata_nonredundant_CORE_UNVALIDATED_dictyostelium.tab \
    -o /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Motifs/JASPAR_2022_non_main_taxons_motifs \
    -d data/mapping_files/JASPAR_datasets_info_JASPAR2022_non_main_taxons.tab

Rscript bin/pre_processing/prepare_jaspar_motifs_for_curation.R \
    -j /storage/mathelierarea/raw/JASPAR/2022/core_unvalidated_nonredundant/all_taxons/jaspar_all_motifs \
    -n /storage/mathelierarea/processed/ieva/projects/JASPAR_2024/jaspar_2024_curation_pipeline/data/insects_profiles_with_bad_names/JASPAR_2022_metadata_nonredundant_CORE_UNVALIDATED_insects_bad_name_profiles.tab \
    -o /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Motifs/JASPAR_2022_all_motifs_missing_insects_profiles \
    -d data/mapping_files/JASPAR_datasets_info_JASPAR2022_two_missing_insects_profiles.tab ;

## Creating a mapping file:
Rscript bin/pre_processing/generate_mapping_file.R \
-d /storage/mathelierarea/processed/ieva/projects/JASPAR_2024/jaspar_2024_curation_pipeline/data/mapping_files/JASPAR_datasets_info_JASPAR2022.tab \
-o data/mapping_files

Rscript bin/pre_processing/generate_mapping_file.R \
-d /storage/mathelierarea/processed/ieva/projects/JASPAR_2024/jaspar_2024_curation_pipeline/data/mapping_files/JASPAR_datasets_info_JASPAR2022_non_main_taxons.tab \
-o data/mapping_files

Rscript bin/pre_processing/generate_mapping_file.R \
-d /storage/mathelierarea/processed/ieva/projects/JASPAR_2024/jaspar_2024_curation_pipeline/data/mapping_files/JASPAR_datasets_info_JASPAR2022_two_missing_insects_profiles.tab \
-o data/insects_profiles_with_bad_names

## Filling in missing infor for current JASPAR 2022 motifs (these motifs are processed in case we update metadata or validate):
Rscript bin/pre_processing/fill_missing_info_for_JASPAR2022_mapping_file.R \
    -m data/mapping_files/JASPAR_2024_mapping_file_2022-12-14.txt \
    -o data/mapping_files/JASPAR_2024_mapping_file_2022-12-14_filled.txt \
    -j /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Motifs/JASPAR_2022_all_motifs/jaspar_motif_metadata_for_curation.tab

Rscript bin/pre_processing/fill_missing_info_for_JASPAR2022_mapping_file.R \
    -m data/mapping_files/JASPAR_2024_mapping_file_2023-07-10.txt \
    -o data/mapping_files/JASPAR_2024_mapping_file_2023-07-10_filled.txt \
    -j /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Motifs/JASPAR_2022_non_main_taxons_motifs/jaspar_motif_metadata_for_curation.tab

Rscript bin/pre_processing/fill_missing_info_for_JASPAR2022_mapping_file.R \
    -m data/insects_profiles_with_bad_names/JASPAR_2024_mapping_file_2023-07-10.txt \
    -o data/insects_profiles_with_bad_names/JASPAR_2024_mapping_file_2023-07-10_filled.txt \
    -j /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Motifs/JASPAR_2022_all_motifs_missing_insects_profiles/jaspar_motif_metadata_for_curation.tab

### Preping the CIS-BP motifs for plants (all except A. thaliana), drosophila and C. elegans
## For this we needed to download the whole collection of CIS-BP motifs (they were downloaded before and placed in raw: /storage/mathelierarea/raw/CIS-BP/whole_collection/pwms)
## Associated metadata for the whole collection of motifs can be found in raw as well: /storage/mathelierarea/raw/CIS-BP/whole_collection/TF_Information_all_motifs.txt
## Then we needed species information to associate the motifs with the taxons (http://cisbp.ccbr.utoronto.ca/data/2.00/DataFiles/SQLDumps/SQLArchive_cisbp_2.00.zip) The file needed was placed nest to the motifs in raw: /storage/mathelierarea/raw/CIS-BP/whole_collection/cisbp_2.00.stats_by_species.sql
## We needed to download the NCBI Taxonomy table to get the taxon IDs, which are needed as input for JASPAR curation pipeline. They were downloaded from here: https://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.zip Needed file is here: /storage/mathelierarea/raw/UCSC/taxonomy/fullnamelineage.dmp
### This is all needed to run the script:
Rscript /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Motifs/CIS-BP/bin/prepare_cisbp_motifs.R \
 -m /storage/mathelierarea/raw/CIS-BP/whole_collection/pwms \
 -s /storage/mathelierarea/raw/CIS-BP/whole_collection/cisbp_2.00.stats_by_species.sql \
 -n /storage/mathelierarea/raw/CIS-BP/whole_collection/TF_Information_all_motifs.txt \
 -p Drosophila_melanogaster,Caenorhabditis_elegans \
 -t Plants \
 -r /storage/mathelierarea/raw/UCSC/taxonomy/fullnamelineage.dmp \
 -o /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Motifs/CIS-BP \
 -j data/metadata/JASPAR_latest_profiles_metadata.tab

### C. elegans motifs from the paper: https://www.embopress.org/doi/full/10.15252/msb.20167131 (PMID: 27777270)
## First the motifs need to be processed and prepared to generate the mapping file. For this:
Rscript /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Motifs/celegans_bass/bin/prepare_bass_celegans_motifs.R \
-m /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Motifs/celegans_bass/msb167131-sup-0003-datasetev2.xlsx \
-o /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Motifs/celegans_bass

## Then generating the actual mapping file:
Rscript bin/pre_processing/generate_mapping_file.R \
-d /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Motifs/celegans_bass/JASPAR_dataset_info.tab \
-o /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Motifs/celegans_bass \
-j data/metadata/JASPAR_latest_profiles_metadata.tab 

### DAP-seq plant datasets from GEO:
Rscript bin/pre_processing/generate_mapping_file.R \
-d /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GEO-DAP-seq/results/JASPAR_datasets_info.tab \
-o /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GEO-DAP-seq/results/ \
-j data/metadata/JASPAR_latest_profiles_metadata.tab \
-t 1000

### Prepare MET4 data and create a mapping file:
Rscript bin/pre_processing/generate_mapping_file.R \
-d /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/MET4/JASPAR_datasets_info.tab \
-o /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/MET4

### ChIP-seq for wrky and gata in A. thaliana::
Rscript bin/pre_processing/generate_mapping_file.R \
-d /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/plants_GATA_WRKY/results/JASPAR_datasets_info_additional_plants.tab \
-o /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/plants_GATA_WRKY/results/ \
-j data/metadata/JASPAR_latest_profiles_metadata.tab \
-t 1000

#################################################
## Additional references and metadata handling ##
#################################################

## Specificly subsetted for ARABIDOPSIS GTRD data:
Rscript bin/pre_processing/merge_mapping_file_reference.R \
 -m data/mapping_files/JASPAR_2024_mapping_file_2022-11-10.txt \
 -r /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/external_refs.tab \
 -o /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/arabidopsis_external_refs_with_dataset_IDs.tab

Rscript bin/pre_processing/get_GEO_dataset_names.R \
 -i /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/arabidopsis_external_refs_with_dataset_IDs.tab \
 -o /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/arabidopsis_external_refs_with_GEO_anno.tab

# ## For all GTRD datasets:
# Rscript bin/pre_processing/get_GEO_dataset_names.R \
#  -i /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/external_refs.tab \
#  -o /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/external_refs_with_GEO_anno.tab

## For drosophila GTRD data:
Rscript bin/pre_processing/merge_mapping_file_reference.R \
 -m data/mapping_files/JASPAR_2024_mapping_file_2022-11-11.txt \
 -r /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/external_refs.tab \
 -o /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/drosophila_external_refs_with_dataset_IDs.tab

Rscript bin/pre_processing/get_GEO_dataset_names.R \
 -i /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/drosophila_external_refs_with_dataset_IDs.tab \
 -o /storage/mathelierarea/processed/JASPAR_data/JASPAR2024/Peaks/GTRD/results/drosophila_external_refs_with_GEO_anno.tab


###################
## End of Script ##
###################