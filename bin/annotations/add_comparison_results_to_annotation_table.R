# ============================================ #
# Load libraries + read command line arguments #
# ============================================ #
required.libraries <- c("data.table",
                        "dplyr",
                        "optparse")

for (lib in required.libraries) {
  suppressPackageStartupMessages(library(lib, character.only = TRUE, quietly = T))
}


option_list = list(
  
  make_option(c("-d", "--curation_table"), type = "character", default = "NULL", 
              help = "(Mandatory) ", metavar = "character"),
  
  make_option(c("-r", "--results_folder"), type = "character", default = "NULL", 
              help = "(Mandatory) ", metavar = "character"),
  
  make_option(c("-o", "--output_table", type = "character", default = "NULL",
              help = "(Mandatory)", metavar = "character"))
  
);
message("; Reading arguments from command-line")
opt_parser = OptionParser(option_list = option_list);
opt = parse_args(opt_parser);

## Set variable names
curation.table.file     <- opt$curation_table
pipeline.results.folder <- opt$results_folder
output.file             <- opt$output_table

# curation.table.file     <- "/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/mathelierarea/processed/ieva/projects/JASPAR_2024/jaspar_2024_curation_pipeline/results/external/Factorbook_SELEX/DatasetID_13488/central_enrichment/selected_motif/DatasetID_13488.501bp.fa.sites.centrimo.best_annot_tmp"
# pipeline.results.folder <- "/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/mathelierarea/processed/ieva/projects/JASPAR_2024/jaspar_2024_curation_pipeline/results/external/Factorbook_SELEX/DatasetID_13488/motifs/comparison"

# ========= #
# How to run: Rscript add_comparison_results_to_annotation_table.R -d <curation_table> -r <results_folder>
# ========= #

# ========= #
# Functions #
# ========= #

## Given a jaspar file resulting from the motif discovery pipeline
## finds its associated motif comparison results
from.jasparmotif.to.comparisonfile <- function(jaspar.file = NULL) {
  
  jaspar.file.bn  <- basename(jaspar.file)
  
  ## We need the motif core name to find the comparison file. The comparison file has
  ## the core name with a different suffix and extension
  ## Example core name: DatasetID_X_peak-motifs_mY
  motif.core.name <- gsub(jaspar.file.bn, pattern = "\\.jaspar$", replacement = "") 
  
  ## Build the path to the comparison file
  comparison.table.file <- file.path(pipeline.results.folder, paste0(motif.core.name, "_motif_comparison.tab"))
  # comparison.table.file <- file.path(pipeline.results.folder, "motifs", "comparison", paste0(motif.core.name, "_motif_comparison.tab"))
  if (!file.exists(comparison.table.file)) {
    stop("; File not found: ", comparison.table.file)
  }
  
  return(comparison.table.file)
}


## Parse the results of the comparison table into a single line
## TF-MotifID-Cor
summary.comparison <- function(comparison.table.file = NULL) {
  
  ## Add a summary line to the comparison table
  comparison.table <- fread(comparison.table.file) %>% 
                        mutate(Reference_ID = gsub(Reference_ID, pattern = "_", replacement = "\\."),
                               Ncor         = round(Ncor, digits = 3)) %>% 
                        mutate(Summary_line = paste(Reference_Name, Reference_ID, Ncor, sep = "--"))
  
  compa.summary <- paste(comparison.table$Summary_line, collapse = ";")
  return(compa.summary)
}


# ==== #
# Main #
# ==== #

## Read curation table
curation.table <- fread(curation.table.file,
                        sep = "\t") %>%
  unique()

## Get the path to the motif comparison tables
comparison.files <- as.vector(sapply(curation.table$V2, from.jasparmotif.to.comparisonfile))

## Parse the motif comparison table in a single line
summary.comparison <- as.vector(sapply(comparison.files, summary.comparison))

## Add a new column to the comparison table and export it
curation.table$Motif_comparison <- summary.comparison
# curation.table.file.new <- gsub(curation.table.file, , pattern = "_tmp$", replacement = "")
# fwrite(curation.table, curation.table.file.new, sep = "\t", row.names = FALSE, col.names = FALSE)
fwrite(curation.table, output.file, sep = "\t", row.names = FALSE, col.names = FALSE)

## End of the script