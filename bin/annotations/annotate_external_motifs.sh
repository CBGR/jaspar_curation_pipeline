## Inputs from the arguments:

PWM=$1
previous_jaspar_annotations=$2
suggested_uniprots_mygene=$3
dataset_dir=$4
dataset_ID=$5
TF_name=$6
organism=$7
tax_ID=$8
taxon=$(echo $9 | tr '[:upper:]' '[:lower:]')
data=${10}
# source=$(echo ${${11}// /_})
source=${11}
output=${12}

# echo $PWM
# echo $previous_jaspar_annotations
# echo $suggested_uniprots_mygene
# echo $dataset_dir
# echo $dataset_ID
# echo $TF_name
# echo $organism
# echo $tax_ID
# echo $taxon
# echo $data
# echo $source
# echo $output

## Initiate the curation ID creation:
i=1

## Process the input and build curation table entry:

motif=$(basename ${PWM%".jaspar"})
motif_parts=(${motif//"_"/" "})

### Building curation ID
curation_ID=${motif_parts[0]}'_'${motif_parts[1]}'-'${i}

### Fillers for curations table:
current_BASE_ID=""
current_VERSION=""
TF_NAME=""
UniProt=""
class=""
family=""
TFBS_shape_ID=""
Validation=""
Comment=""
curation_comment=""
Decision=""
BED=""
FASTA=""
Cent_pval=""
Cent_plot=""

### From arguments:
TAX_ID=${tax_ID}
Data=$data
Source=$source
Dataset_TF_NAME=${TF_name}

## Building paths to logos:
# logo=${dataset_dir}'/motifs/jaspar/logos/'${motif_parts[0]}'_'${motif_parts[1]}'_peak-motifs_'${motif_parts[2]}'_logo.png'
# logo_rc=${dataset_dir}'/motifs/jaspar/logos/'${motif_parts[0]}'_'${motif_parts[1]}'_peak-motifs_'${motif_parts[2]}'_logo_rc.png'
logo=${dataset_dir}'/motifs/jaspar/logos/'${motif_parts[0]}'_'${motif_parts[1]}'_'${motif_parts[2]}'_'${motif_parts[3]}'_logo.png'
logo_rc=${dataset_dir}'/motifs/jaspar/logos/'${motif_parts[0]}'_'${motif_parts[1]}'_'${motif_parts[2]}'_'${motif_parts[3]}'_logo_rc.png'

## Building path to PDF:
pdf_path=${dataset_dir}'/annotations/'${motif_parts[0]}'_'${motif_parts[1]}'.external_logo.pdf'

# echo $pdf_path

## Suggested (this is useful information for the curation):
Suggested_MATRIX_ID="$(grep -Pi "\t${TF_name}\t.*\t${taxon}\t" $previous_jaspar_annotations | cut -d$'\t' -f 1 | sed -z 's/\n/,/g; s/,$//')"
Suggested_TF_NAME="$(grep -Pi "\t${TF_name}\t.*\t${taxon}\t" $previous_jaspar_annotations | cut -d$'\t' -f 2 | sed -z 's/\n/,/g; s/,$//')"
Suggested_UniProt_prev_jaspar="$(grep -Pi "\t${TF_name}\t.*\t${taxon}\t" $previous_jaspar_annotations | cut -d$'\t' -f 17 | sed -z 's/\n/,/g; s/,$//')"
Suggested_class="$(grep -Pi "\t${TF_name}\t.*\t${taxon}\t" $previous_jaspar_annotations | cut -d$'\t' -f 15 | sed -z 's/\n/,/g; s/,$//')"
Suggested_family="$(grep -Pi "\t${TF_name}\t.*\t${taxon}\t" $previous_jaspar_annotations | cut -d$'\t' -f 14 | sed -z 's/\n/,/g; s/,$//')"
Suggested_UniProt_mygene=$suggested_uniprots_mygene

printf "${curation_ID}\t${PWM}\t${current_BASE_ID}\t${current_VERSION}\t${TF_NAME}\t${UniProt}\t${TAX_ID}\t${class}\t${family}\t${TFBS_shape_ID}\t${Data}\t${Source}\t${Validation}\t${Comment}\t${Decision}\t${BED}\t${FASTA}\t${logo}\t${logo_rc}\t${Cent_pval}\t${Cent_plot}\t${curation_comment}\t${Dataset_TF_NAME}\t${Suggested_MATRIX_ID}\t${Suggested_TF_NAME}\t${Suggested_UniProt_prev_jaspar}\t${Suggested_class}\t${Suggested_family}\t${Suggested_UniProt_mygene}\t${pdf_path}\n" >> $output
