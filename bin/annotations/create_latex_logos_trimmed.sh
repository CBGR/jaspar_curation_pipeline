#!/usr/bin/env bash

################################################################################
#
################################################################################

usage="""
\n
$0 -l <latex header file> -i <input file> -o <output file>
\n
""";

#echo "\usepackage[export]{adjustbox}" >> $output
#echo "\usepackage{graphicx}" >> $output

TEMP=`getopt -o ho:i:l:t: -- "$@"`;

if [ $? != 0 ]
then
  echo "Terminating..." >&2;
  exit 1;
fi
eval set -- "$TEMP";

input="NA";
output="NA";
latexheader="NA";
th_pvalue=0;
while true
do
  case "$1" in
    -i) input=$2; shift 2;;
    -o) output=$2; shift 2;;
    -l) latexheader=$2; shift 2;;
    -t) th_pvalue=$2; shift 2;;
    -h) echo -e $usage; exit;;
    --) shift; break;;
    *) echo "Internal error!"; exit 1;;
  esac
done

# Checking options and parameters

if [ ! -e $input ]
then
    echo -e "\nPlease provide the input file\n";
    exit 1;
fi;

if [ ! -e $latexheader ]
then
    echo -e "\nPlease provide the latex header file\n";
    exit 1;
fi;

cat $latexheader > $output;

sed 's/\t/;/g' $input | \
while IFS=';' read -r -a line
do

    # variant=$(basename ${line[1]})
    # variant=${variant%%".jaspar"}
    # variant=(${variant//"_"/" "})
    tfname=$(echo ${line[22]} | tr '_' '-')
    #tfname=$(echo $line | cut -d ' ' -f 2 | tr '_' '-');
    exp_ID=$(basename ${line[1]%".jaspar"} | tr '_' '-');
    curation_ID=$(echo ${line[0]} | tr '_' '-');
    #exp_ID=$(echo $line | cut -d ' ' -f 9 | tr '_' '-');
    centrimo_pval=${line[19]}
    #centrimo_pval=$(echo $line | cut -d ' ' -f 6);

    ## Read motif logo
    # motif_logo_original=${line[1]//"pfm"/"logos"} # change
    # motif_logo_original=${motif_logo_original//".jaspar"/"_logo.png"}
    motif_logo_original=${line[17]}
    motif_logo_original_rc=${line[18]}
    #motif_logo_original=$(echo $line | cut -d ' ' -f 8);
    motif_logo_original_dirname=$(dirname $motif_logo_original);
    # prevent error: ! LaTeX Error: Unknown graphics extension: .16_LE_WA_peak-motifs_m5_logo.png.
    # This removes any internal dots in the basename of the file, except for the dot in the extension (e.g. except the .png part)
    motif_logo_new=$(basename `echo $motif_logo_original` | perl -pe 's/(\.)(?=[^.]*\.)/\_/');
    motif_logo_new_rc=$(basename `echo $motif_logo_original_rc` | perl -pe 's/(\.)(?=[^.]*\.)/\_/');

    motif_logo=${motif_logo_original_dirname}/${motif_logo_new}
    motif_logo_rc=${motif_logo_original_dirname}/${motif_logo_new_rc}

    ## Read centrality plot
    centrimo_plot=${line[20]} # change
    #centrimo_plot_original=$(echo $line | cut -d ' ' -f 11);
    #centrimo_plot_original_dirname=$(dirname $centrimo_plot_original);
    # prevent error: ! LaTeX Error: Unknown graphics extension: .16_LE_WA_peak-motifs_m5_logo.png.
    # This removes any internal dots in the basename of the file, except for the dot in the extension (e.g. except the .png part)
    #centrimo_plot_new=$(basename `echo $centrimo_plot_original` | perl -pe 's/(\.)(?=[^.]*\.)/\_/');
    #centrimo_plot=${centrimo_plot_original_dirname}/${centrimo_plot_new}

    # this create a symbolic link so that that new motif filename will actually exist
    #ln -s $motif_logo_original $motif_logo
    #motif_pdf=$(echo $line | cut -d ' ' -f 7);


    # Based on experiment ID, get data for original motif to plot original and trimmed side by side
    root_folder=$(dirname $(dirname $(dirname $(dirname ${line[1]})))) # sorry about this

    exp_ID_original=$(basename ${line[1]%".jaspar"})

    centrimo_pval_original=$(cat ${root_folder}/central_enrichment/${exp_ID_original//"peak-motifs_"/""}.501bp.fa.sites.centrimo)

    motif_logo_original=${line[17]//"motifs_trimmed"/"motifs"}
    motif_logo_original_rc=${line[18]//"motifs_trimmed"/"motifs"}
    motif_logo_original_dirname=$(dirname ${motif_logo_original//"motifs_trimmed"/"motifs"})
    motif_logo_original_new=$(basename `echo $motif_logo_original` | perl -pe 's/(\.)(?=[^.]*\.)/\_/')
    motif_logo_original_new_rc=$(basename `echo $motif_logo_original_rc` | perl -pe 's/(\.)(?=[^.]*\.)/\_/')
    motif_logo_original=${motif_logo_original_dirname}/${motif_logo_original_new}
    motif_logo_original_rc=${motif_logo_original_dirname}/${motif_logo_original_new_rc}

    centrimo_plot_original=${line[20]//"_trimmed"/""}

    # echo $tfname
    # echo $exp_ID
    # echo $curation_ID
    # echo $motif_logo
    # echo $motif_logo_rc
    # echo $centrimo_pval
    # echo $centrimo_plot
    # echo $output


    #printf "\\section*{$tfname}\n\\section*{$exp_ID}\nCentrality p-value = $centrimo_pval \\\\ \n\\includegraphics{$motif_logo}\nCentrimo plot \\\\ \n\\includegraphics[width=8cm, height=8cm]{$centrimo_plot}\n" >> $output
    echo "\\section*{$tfname}" >> $output;
    echo "\\section*{$exp_ID}" >> $output;
    echo "$curation_ID \\\\" >> $output;
    echo "\newline" >> $output;
    echo "\newline" >> $output;
    echo "\begin{minipage}{0.45\textwidth}" >> $output;
    echo "\\begin{center}" >> $output;
    echo "Original" >> $output;
    echo "\newline" >> $output;
    echo "\newline" >> $output;
    echo "Centrality = $centrimo_pval_original \\\\" >> $output;
    echo "\\includegraphics[width=6cm, height=2cm]{$motif_logo_original}" >> $output;
    echo "\\includegraphics[width=6cm, height=2cm]{$motif_logo_original_rc}" >> $output;
    echo "\\includegraphics[width=6cm, height=6cm]{$centrimo_plot_original}" >> $output;
    echo "\\end{center}" >> $output;
    echo "\end{minipage}%" >> $output;
    echo "\begin{minipage}{0.45\textwidth}" >> $output;
    echo "\\begin{center}" >> $output;
    echo "Trimmed" >> $output;
    echo "\newline" >> $output;
    echo "\newline" >> $output;
    echo "Centrality = $centrimo_pval \\\\" >> $output;
    echo "\\includegraphics[width=6cm, height=2cm]{$motif_logo}" >> $output;
    echo "\\includegraphics[width=6cm, height=2cm]{$motif_logo_rc}" >> $output;
    echo "\\includegraphics[width=6cm, height=6cm]{$centrimo_plot}" >> $output;
    echo "\\end{center}" >> $output;
    echo "\end{minipage}%" >> $output;
    echo "\newpage" >> $output;

done;
echo "\\end{document}" >> $output;
outdir=$(dirname $output);

pdflatex -output-directory $outdir $output;
