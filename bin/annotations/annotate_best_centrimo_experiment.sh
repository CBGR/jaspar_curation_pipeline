#inputs
input_file=$1
previous_jaspar_annotations=$2
suggested_uniprots_mygene=$3
dataset_dir=$4
dataset_ID=$5
TF_name=$6
organism=$7
tax_ID=$8
taxon=$(echo $9 | tr '[:upper:]' '[:lower:]')
data=${10}
source=${11}
output=${12}

# echo $input_file
# echo $previous_jaspar_annotations
# echo $suggested_uniprots_mygene
# echo $dataset_dir
# echo $dataset_ID
# echo $TF_name
# echo $trimmed
#processing

i=1 # used for creation of curation ID

while IFS=$'\t' read -r -a line
do

  centrimo_path=${line[0]}
  centrimo_pval=${line[1]}

  motif=$(basename ${centrimo_path%".501bp.fa.sites.centrimo"})
  motif_parts=(${motif//"_"/" "})

  curation_ID=${motif_parts[0]}'_'${motif_parts[1]}'-'${i}

  PWM=${dataset_dir}'/motifs/jaspar/pfm/'${motif_parts[0]}'_'${motif_parts[1]}'_peak-motifs_'${motif_parts[2]}'.jaspar'

  current_BASE_ID=""
  current_VERSION=""
  TF_NAME=""
  UniProt=""

  TAX_ID=${tax_ID}

  class=""
  family=""
  TFBS_shape_ID=""

  Data=$data
  Source=$source

  Validation=""
  Comment=""
  curation_comment=""
  Decision=""

  BED=${dataset_dir}'/matrix_sites/'${motif_parts[0]}'_'${motif_parts[1]}'_peak-motifs_'${motif_parts[2]}'.tf.sites.bed'

  FASTA=${dataset_dir}'/matrix_sites/'${motif_parts[0]}'_'${motif_parts[1]}'_peak-motifs_'${motif_parts[2]}'.tf.sites.fasta'

  logo=${dataset_dir}'/motifs/jaspar/logos/'${motif_parts[0]}'_'${motif_parts[1]}'_peak-motifs_'${motif_parts[2]}'_logo.png'
  logo_rc=${dataset_dir}'/motifs/jaspar/logos/'${motif_parts[0]}'_'${motif_parts[1]}'_peak-motifs_'${motif_parts[2]}'_logo_rc.png'

  Cent_pval=${centrimo_pval}
  Cent_plot=${centrimo_path//".501bp.fa.sites.centrimo"/"_501bp_fa_sites_centrimo"}'.png'

  Dataset_TF_NAME=${TF_name}

  Suggested_MATRIX_ID="$(grep -Pi "\t${TF_name}\t.*\t${taxon}\t" $previous_jaspar_annotations | cut -d$'\t' -f 1 | sed -z 's/\n/,/g; s/,$//')"

  Suggested_TF_NAME="$(grep -Pi "\t${TF_name}\t.*\t${taxon}\t" $previous_jaspar_annotations | cut -d$'\t' -f 2 | sed -z 's/\n/,/g; s/,$//')"

  Suggested_UniProt_prev_jaspar="$(grep -Pi "\t${TF_name}\t.*\t${taxon}\t" $previous_jaspar_annotations | cut -d$'\t' -f 17 | sed -z 's/\n/,/g; s/,$//')"

  Suggested_class="$(grep -Pi "\t${TF_name}\t.*\t${taxon}\t" $previous_jaspar_annotations | cut -d$'\t' -f 15 | sed -z 's/\n/,/g; s/,$//')"

  Suggested_family="$(grep -Pi "\t${TF_name}\t.*\t${taxon}\t" $previous_jaspar_annotations | cut -d$'\t' -f 14 | sed -z 's/\n/,/g; s/,$//')"

  Suggested_UniProt_mygene=$suggested_uniprots_mygene

  # pdf_path=${input_file}'_annot.pdf'
  pdf_path=${dataset_dir}'/annotations/'${motif_parts[0]}'_'${motif_parts[1]}'.internal_logo.pdf'

  printf "${curation_ID}\t${PWM}\t${current_BASE_ID}\t${current_VERSION}\t${TF_NAME}\t${UniProt}\t${TAX_ID}\t${class}\t${family}\t${TFBS_shape_ID}\t${Data}\t${Source}\t${Validation}\t${Comment}\t${Decision}\t${BED}\t${FASTA}\t${logo}\t${logo_rc}\t${Cent_pval}\t${Cent_plot}\t${curation_comment}\t${Dataset_TF_NAME}\t${Suggested_MATRIX_ID}\t${Suggested_TF_NAME}\t${Suggested_UniProt_prev_jaspar}\t${Suggested_class}\t${Suggested_family}\t${Suggested_UniProt_mygene}\t${pdf_path}\n" \
  >> $output

  i=$((i+1))

done < $input_file
