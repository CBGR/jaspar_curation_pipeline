#####################
## Load R packages ##
#####################
required.libraries <- c("data.table",
                        "googledrive",
                        "googlesheets4",
                        "optparse")

for (lib in required.libraries) {
  if (!require(lib, character.only = TRUE)) {
    install.packages(lib)
    suppressPackageStartupMessages(library(lib, character.only = TRUE))
  }
}


####################
## Read arguments ##
####################
option_list = list(
  
  make_option(c("-t", "--table"), type = "character", default = NULL, 
              help = "Any table provided as text file (Mandatory).", metavar = "character"),
  
  make_option(c("-u", "--url"), type = "character", default = NULL, 
              help = "An existing googlesheet url. This is only required when a sheet is overwritten (option -o O). ", metavar = "character"),  
  
  make_option(c("-n", "--tab_name"), type = "character", default = "This-is-a-test", 
              help = "Name assigned to a created google sheet. ", metavar = "character"),  
  
  make_option(c("-o", "--options"), type = "character", default = "N", 
              help = "Either to create a new sheet [N], overwrite [O] an existing tab within a sheet or add [A] a new tab within an existing sheet, in cases [O and A] both a valid url and a tab name are required. [Default \"%default\"] ", metavar = "character"),
  
  make_option(c("-r", "--table_header"), type = "logical", default = TRUE, 
              help = "Indicates whether the input table has a header. [Default \"%default\"] ", metavar = "logical"),
  
  make_option(c("-j", "--json_private_key"), type = "character", default = NULL, 
              help = "A json file containig the prive key to authenticate connection with Google Sheet API. [Default \"%default\"] ", metavar = "character")
  
);
opt_parser = OptionParser(option_list = option_list);
opt = parse_args(opt_parser);


########################
## Set variable names ##
########################
in.tab.file <- opt$table
tab.header  <- opt$table_header
gs.url      <- opt$url
gs.tab      <- opt$tab_name
gs.url.opt  <- tolower(opt$options)
json.key    <- opt$json_private_key


## Debug
# in.tab.file <- "Jaspar_2022_metadata_all_table.tab"
# tab.header  <- TRUE
# gs.url      <- "https://docs.google.com/spreadsheets/d/1P6BgNtDvcheo-0gf1nzEY__Q27GWJG3StoKv-6xuAWE"
# gs.name     <- "This-is-a-test"
# gs.url.opt  <- "n"
# 
# gs.url.opt  <- "o"
# gs.tab      <- "Test_gd4"
# 
# gs.url.opt  <- "a"
# gs.tab      <- "Sheet6"


###############
## Functions ##
###############

## s.url     : sheet URL. Example: https://docs.google.com/spreadsheets/d/1P6BgNtDvcheo-0gf1nzEY__Q27GWJG3StoKv-6xuAWE
## s.tab     : sheet tab_name. Example: Sheet4
## write.opt : a = append new tab to existing sheet; o = overwrite an existing tab within a sheet  
add.info.to.gs <- function(s.url     = NULL,
                           s.tab     = NULL,
                           write.opt = NULL) {
  
  ## Read the google sheet: id + tab_name
  switch(  
    write.opt,  
    
    "a" = url.tab <- read_sheet(s.url),  
    
    "o" = url.tab <- read_sheet(s.url, gs.tab)
  ) 
  
  ## Overwrite an existing sheet given the ID + Tab_name
  sheet_write(in.tab, ss = s.url, sheet = s.tab)
}



## Check valid output options
if (!gs.url.opt %in% c("n", "o", "a")) {
  stop("Incorrect option. Supported options: n (new sheet); o (overwrite sheet)")
}

## Check requirements for overwrite a sheet are provided (non-empty) 
if (gs.url.opt %in% c("o", "a")) {
  if (is.null(c(gs.url, gs.tab))) {
    stop("When option '-o O' to overwrite an existing google sheet is activated a valid google sheet URL [--url] and tab [--tab] name are required. Please verify the provided names are correct.")
  }
}

if (!file.exists(json.key)) {
  stop("; JSON input file does not exist.")
}



##########
## Main ##
##########

## 1. Go to https://console.cloud.google.com
## 2. Follow the instructions in https://gargle.r-lib.org/articles/get-api-credentials.html#service-account-token . This will produce a json file that will be the 'path' parameter in the following code line
## 3. Be sure that the Google Sheets API is active: https://console.cloud.google.com/apis/api/sheets.googleapis.com/overview
##
## The following line is required only when this script is launched from the command-line
## (or in the non-interactive way)
##
## Run the following line, this will open a tab for authentication in your browser
## you have to do this once before launching the script
gs4_auth(path = json.key)
# gs4_deauth()

message("; Write option: ", gs.url.opt)

## Read input table
in.tab <- fread(in.tab.file, header = tab.header)


switch(  
  gs.url.opt,  
  
  ## New: create a new sheet
  "n" = in.tab.gs <- gs4_create(gs.tab,
                                sheets = in.tab),  
  
  ## Overwrite: given a valid sheet url and an existing tab name
  ##            replace the content in the tab by the input table
  "o" = in.tab.gs <- add.info.to.gs(s.url     = gs.url,
                                    s.tab     = gs.tab,
                                    write.opt = "o"),
  
  ## Overwrite: given a valid sheet url and tab name
  ##            create a new tab and populate it with the content from the input table
  "a" = in.tab.gs <- add.info.to.gs(s.url     = gs.url,
                                    s.tab     = gs.tab,
                                    write.opt = "a")
)

