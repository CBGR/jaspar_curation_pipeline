#!/usr/bin/env bash

usage() { echo "Usage: $0 -q <query_motif> -r <reference_motifs> [-t <10>] -n <TF_name> -o <output_file>" 1>&2; exit 1; }

echo "; Reading arguments from command-line"
while getopts ":q:r:t:n:o:" o; do
    case "${o}" in
    
        q)
            query_motif=${OPTARG}
            ;;
    
    
        r)
            reference_motifs=${OPTARG}
            ;;


        n)
            TF_name=${OPTARG}
            ;;  
           
        o)
            output_file=${OPTARG}
            ;;    
            
                
        t)
            top_hits=${OPTARG}
            #(t == 10 ) || usage
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${top_hits}" ]; then
    usage
fi


cor_thr=-1
w_thr=0
compa_ID=`uuidgen` ;

basedir_out=$(dirname "$output_file")
compa_file="$basedir_out/Motif_comparison_$TF_name.$compa_ID.txt"
tmp_final_file="$basedir_out/Tmp_Motif_comparison_$TF_name.$compa_ID.txt"


top_X_file="$basedir_out/Motif_comparison_${TF_name}_${compa_ID}_top_${top_hits}_hits.txt"
TF_hit_file="$basedir_out/Motif_comparison_${TF_name}_${compa_ID}_hit.txt"

## Output directory
echo "; Output directory: $basedir_out"
mkdir -p $basedir_out ;

## Launch comparison Query vs Database
echo "; Comparing query motif vs reference database"
compare-matrices-quick -file1 $query_motif -file2 $reference_motifs -mode matches -lth_ncor $cor_thr -lth_cor $cor_thr -lth_w $w_thr -lth_ncor1 $cor_thr -lth_ncor2 $cor_thr -o $compa_file
#echo "; Comparison ready"

## Get top X matches
more $compa_file | grep -v '^;' | grep -v "^#" | cut -f1,2,3,4,5,6 | sort -k6,6 -k5,5 -r | head -n $top_hits > $top_X_file

## Get the comparison corresponding to the input TF name (if any)
more $compa_file | grep -v '^;' | grep -v "^#" | cut -f1,2,3,4,5,6 | awk -v TF="$TF_name" 'tolower($4) ==tolower(TF)' > $TF_hit_file


cat $top_X_file $TF_hit_file | sort | uniq | sort -k6,6 -k5,5 -r > $tmp_final_file
sed '1i Query_ID\tReference_ID\tQuery_Name\tReference_Name\tCor\tNcor' $tmp_final_file > $output_file


#echo "; Comparison results for $TF_name: $compa_file"
#echo "; Top $top_hits hits in comparison results for $TF_name: $top_X_file"
#echo "; Hits in comparison results for $TF_name in reference motif file: $TF_hit_file"
echo "; Selected hits for $TF_name: $output_file"

rm $tmp_final_file $top_X_file $TF_hit_file $compa_file








