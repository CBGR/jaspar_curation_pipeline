## This function will take as input the following parameters:
### - Comma-separated list of gene symbols
### - Comma-separated list of taxon IDs
### - Path to an output file
### - (Optional) Number of top hits

## With these, it will query the mygene.info service and return a list of suggested UniProt IDs for each gene.
## All results are concatenated in a data frame and saved in the specified output

#####################
## Library loading ##
#####################

required.libraries <- c("mygene",
                        "optparse",
                        "tidyverse")

for (lib in required.libraries) {
  suppressPackageStartupMessages(library(lib, character.only = TRUE, quietly = T))
}

######################
## Argument parsing ##
######################

option_list = list(

  make_option(c("-g", "--genes"), type = "character", default = NULL,
              help = "Comma-separated list of genes (Mandatory)", metavar = "character"),

  make_option(c("-t", "--taxon_IDs"), type = "character", default = NULL,
              help = "Comma-separated list of taxon IDs (Mandatory)", metavar = "character"),

  make_option(c("-n", "--n_hits"), type = "integer", default = 5,
              help = "Integer specifying the number of top hits to return for each gene (Optional)", metavar = "integer"),

  make_option(c("-o", "--output"), type = "character", default = NULL,
              help = "Path to the output file (Mandatory)", metavar = "character")
);
opt_parser = OptionParser(option_list = option_list);
opt = parse_args(opt_parser);


genes_in    = opt$genes
taxon_IDs   = opt$taxon_IDs
top_hits    = opt$n_hits
output_file = opt$output

# ## Debug
# genes_in  <- "CUX1::HOXA13"
# taxon_IDs <- "9606"
# top_hits  <- 1

genes_in    = unlist(strsplit(genes_in, ","))
genes_in    = unlist(strsplit(genes_in, "::"))
taxon_IDs   = unlist(strsplit(taxon_IDs, ","))
taxon_IDs   = as.integer(taxon_IDs)

#########################
## Function definition ##
#########################

query_mygene = function(gene, taxon_ID, top_hits = 5) {

  # Return NA if no taxon ID is available
  if (any(is.na(taxon_ID), taxon_ID == "nan", gene == "NOT")) {
    df_out = data.frame(Gene_query = gene,
                        Taxon_ID = taxon_ID,
                        Synonyms = NA,
                        UniProt = NA,
                        Suggested_UniProt = NA)
    return(df_out)
  }

  # launch query
  genes_query = query(q = gene, scopes = "symbol", species = taxon_ID, fields = "alias,uniprot.Swiss-Prot", size = top_hits, returnall = T)

  # get results
  synonyms = lapply(genes_query$hits$alias, function(x) {paste0("(", paste(x, collapse = ","), ")")})
  synonyms = unlist(synonyms)
  synonyms = paste(synonyms, collapse = ",")

  uniprots = ifelse("uniprot" %in% colnames(genes_query$hits), paste(unlist(genes_query$hits$uniprot), collapse = ","), rep(NA, top_hits))

  suggested_uniprots = ifelse("uniprot" %in% colnames(genes_query$hits), genes_query$hits$uniprot[1,1], NA)

  df_out = data.frame(Gene_query = gene,
                      Taxon_ID = taxon_ID,
                      Synonyms = synonyms,
                      UniProt = uniprots,
                      Suggested_UniProt = suggested_uniprots)
  colnames(df_out) <- c("Gene_query",
                        "Taxon_ID",
                        "Synonyms",
                        "UniProt",
                        "Suggested_UniProt")
  return(df_out)

}

###################################
## Checking for potential errors ##
###################################

if (length(genes_in) != length(taxon_IDs)) {

  message("; Making gene and taxon ID lists the same length.")
  # stop("The list of genes and the list of taxon IDs must have the same length!")

  taxon_IDs <- rep(taxon_IDs, time = length(genes_in))

}

###############
## Main body ##
###############

results = mapply(function(gene, taxon_ID, top_hits) {query_mygene(gene, taxon_ID, top_hits = top_hits)}, gene = genes_in, taxon_ID = taxon_IDs, top_hits = top_hits, SIMPLIFY = F)

results_df = do.call(rbind, results)

if (nrow(results_df) != 1) {
  results_df <- results_df[!is.na(results_df$Suggested_UniProt),]
}

###########################################################
## If no UniProt suggestions, replace with no_suggestion ##
###########################################################

if (nrow(results_df) == 0) {
  
  results_df[1, ] <- c(paste(genes_in, collapse = ","),
                        paste(taxon_IDs, collapse = ","),
                        NA,
                        NA,
                        "no_suggestions")
  
}

######################
## Export the table ##
######################

write.table(results_df, output_file, col.names = F, row.names = F, sep = "\t", quote = F)
